workspace "ShadeENGINE"
	configurations { "Debug", "Release" }

	--platforms { "x86", "x86_64" }

	links { "OpenGL.framework", "SDL2.framework" }
	frameworkdirs { "/Library/Frameworks" }

	defines { "COMPILING_SHADE" }

project "ShadeEngine"
	kind "StaticLib"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs"
	location "Source/ShadeEngine"
	targetprefix = ""

	files { "Source/ShadeEngine/Include/**.hxx", "Source/ShadeEngine/Source/**.cxx", "Source/ShadeEngine/Shaders/*.glsl" }

	includedirs { "Source/ShadeEngine/Include/" }

	defines { "OSX" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

project "Shade Game"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/Shade Game"
	targetprefix = ""
	platforms { "Standalone", "Hotswap" }
	kind "WindowedApp"

	files { "Source/Shade Game/Source/**.hxx", "Source/Shade Game/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/" }

	links { "ShadeEngine" }

	defines { "OSX" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

	--[[filter "platforms:Standalone"
		kind "WindowedApp"

	filter "platforms:Hotswap"
		kind "SharedLib"
		defines { "HOTSWAP" }]]

project "ShadeEditor"
	kind "WindowedApp"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/ShadeEditor"

	files { "Source/ShadeEditor/Source/**.hxx", "Source/ShadeEditor/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/" }

	links { "ShadeEngine" }

	defines { "OSX" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"