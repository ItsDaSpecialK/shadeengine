workspace "ShadeENGINE"
	configurations { "Debug", "Release" }

	frameworkdirs { "/Library/Frameworks" }
	links { "OpenGL32", "SDL2", "SDL2main" }

	libdirs { "Extern/SDL2/Win32" }
	defines { "COMPILING_SHADE" }
	platforms { "Standalone", "Hotswap" }

	objdir "%{prj.location}/Intermediate"

	architecture "x86_64"

	filter { "system:windows", "action:vs*" }
		systemversion "latest"

project "ShadeEngine"
	kind "StaticLib"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs"
	location "Source/ShadeEngine"
	targetprefix = ""

	files { "Source/ShadeEngine/Include/**.hxx", "Source/ShadeEngine/Source/**.cxx", "Source/ShadeEngine/Shaders/*.glsl" }

	includedirs { "Source/ShadeEngine/Include/", "Extern" }

	--defines { "OSX" }
	defines { "WIN32", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

project "Shade Game"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/Shade Game"
	targetprefix = ""

	libdirs { "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs" }
	dependson { "ShadeEngine" }

	files { "Source/Shade Game/Source/**.hxx", "Source/Shade Game/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/", "Extern" }

	links { "ShadeEngine.lib" }

	--defines { "OSX" }
	defines { "WIN32", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

	filter "platforms:Standalone"
		kind "ConsoleApp"

	filter "platforms:Hotswap"
		kind "SharedLib"
		defines { "HOTSWAP" }

project "ShadeEditor"
	kind "WindowedApp"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/ShadeEditor"

	libdirs { "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs" }
	dependson { "ShadeEngine", "Shade Game" }
	entrypoint "mainCRTStartup"

	files { "Source/ShadeEditor/Source/**.hxx", "Source/ShadeEditor/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/", "Extern" }

	links { "ShadeEngine.lib" }

	defines { "WIN32", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"