//
//  main.hxx
//  ShadeEditor
//
//  Created by Konrad Kraemer on 6/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <iostream>
#ifdef WIN32
#include <Windows.h>
#else
#include <dlfcn.h>
#endif
#include <ShadeENGINE/Actor/Actor.hxx>
