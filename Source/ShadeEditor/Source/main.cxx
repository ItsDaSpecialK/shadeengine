//
//  main.cxx
//  ShadeEditor
//
//  Created by Konrad Kraemer on 6/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "main.hxx"

int main() {

	std::cout << "Trying to run \'gameInit()\'" << std::endl;

#ifdef WIN32

	std::cout << "Opening library \'Shade Game.dll\'" << std::endl;
	HINSTANCE pHandle = LoadLibrary(L"Shade Game.dll");

	if (!pHandle) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xC0);
		std::cout << "Cannot open the library" << std::endl << GetLastError() << std::endl;
		std::cin.ignore();
		return 1;
	}

#else

#ifdef LINUX
	std::cout << "Opening library \'libShadeGame.so\'" << std::endl;
	void* pHandle = dlopen("./libShadeGame.so", RTLD_LAZY);
#else
	std::cout << "Opening library \'Shade Game.dylib\'" << std::endl;
	void* pHandle = dlopen("./Shade Game.dylib", RTLD_LAZY);
#endif

	if (!pHandle) {
		std::cerr << "Cannot open the library" << std::endl << dlerror() << std::endl;
		return 1;
	}

#endif
	std::cout << "Successfully opened the library..." << std::endl << "Loading functions 'gameInit' and 'getTestActorClass'" << std::endl;

	void (* pFunction)(void) = nullptr;
	ShadeEngine::ShadeClass* (* pFunction2)(void) = nullptr;

#ifdef WIN32
	pFunction = (void (*)(void))GetProcAddress(pHandle, "gameInit");
	if (!pFunction) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xC0); // [07-norm][05-Red][0C-BrightRed][0D-MAGENTA][0F-White]
		std::cout << "Cannot load Function \'gameInit\'" << std::endl << GetLastError() << std::endl;
		std::cin.ignore();
		return 1;
	}
	pFunction2 = (ShadeEngine::ShadeClass* (*)(void))GetProcAddress(pHandle, "getTestActorClass");
	if (!pFunction2) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xC0);
		std::cout << "Cannot load Function \'getTestActorClass\'" << std::endl << GetLastError() << std::endl;
		std::cin.ignore();
		return 1;
	}
#else

	dlerror();
	pFunction = (void (*)(void))dlsym(pHandle, "gameInit");
	const char* error = dlerror();
	if (error) {
		std::cerr << "Cannot load Function \'gameInit\'" << std::endl << error << std::endl;
		return 1;
	}
	dlerror();
	pFunction2 = (ShadeEngine::ShadeClass* (*)(void))dlsym(pHandle, "getTestActorClass");
	const char* error2 = dlerror();
	if (error2) {
		std::cerr << "Cannot load Function \'getTestActorClass\'" << std::endl << error2 << std::endl;
		return 1;
	}

#endif

	std::cout << "Sucessfully Loaded the Fuctions..." << std::endl << "Running 'getTestActorClass'" << std::endl;

	// can't run anymore because Scene has a rendermanager which has opengl functions.
	// therefore, you need to make a window before an sscene...
	/*Scene* pScene = new Scene();

	ConstructionHelper* pHelper = new ConstructionHelper(pScene);

	ShadeClass* pClass = pFunction2();

	Object* pActor = (Object*)pClass->CreateNew(pHelper);

	delete pActor;*/

	std::cout << std::endl << "Running 'gameInit'" << std::endl;
	pFunction();


	std::cout << "Closing the library..." << std::endl;

#ifdef WIN32
	FreeLibrary(pHandle);
#else
	dlclose(pHandle);
#endif
	return 0;
}
