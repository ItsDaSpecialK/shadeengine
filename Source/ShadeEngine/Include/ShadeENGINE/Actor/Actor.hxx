//
//  Actor.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/DynamicObject.hxx"
#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"
#include "ShadeENGINE/Component/Component.hxx"

namespace ShadeEngine {

class Scene;

SHADE_CLASS()
/** Root Class for an Object that can be placed in and interact with a Scene on its own.
 *  @see  Object
 */
class Actor : public CDynamicObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	Actor(Scene* pScene);

	/// Default Destructor
	virtual ~Actor();

	/** Method subordinates the current actor to another Actor.
	 *  @param  pOther  The Actor pointer to the parent.
	 */
	inline void attachTo(Actor* pOther);

	/// Method sets current Actor's parent to the Scene.
	inline void detatch();

	/** Simple getter for the Actor's Scene.
	 *  @return  The Actor's Scene
	 */
	inline Scene* getScene() { return _pScene; }

	/** Makes sure to update all the Actor's Components.
	 *  @param  delta  A uint16 representing the milliseconds since the last call
	 */
	virtual void update(uint16 delta) override;

	/// Simple do-nothing override of init
	virtual void init() override { }

protected:
	/** Creates a component and attaches it to this actor.
	 *  @return  Pointer to the newly created component cast to type T.
	 */
	template<typename T> T* addComponent() {
		T* pComponent = new T(_pScene);
		attachComponent(pComponent);
		return pComponent;
	}

	/// A smart buffer containing all the Actor's Components
	TSmartBuffer<CComponent*> _components;

private:
	/** Attaches a component to the Actor.
	 *  @param  pComponent  A pointer towards the component to be attached
	 *  @see                ConstructionHelper::AddComponent(Actor*)
	 */
	void attachComponent(CComponent* pComponent);

	/// A pointer to the scene containing the current Actor
	Scene* _pScene;

	SHADE_REFLECTION_BEGIN(Actor, CDynamicObject)
	SHADE_REFLECTION_END
};

}
