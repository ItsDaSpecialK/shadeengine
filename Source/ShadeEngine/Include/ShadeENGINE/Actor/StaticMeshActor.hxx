//
//  StaticMeshActor.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Actor/Actor.hxx"
#include "ShadeENGINE/Component/MeshComponent.hxx"
#include "ShadeENGINE/Rendering/StaticMesh.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** Class used for placing static meshes into the scene.
 *  @see  StaticMeshComponent
 */
class StaticMeshActor : public Actor {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	StaticMeshActor(Scene* pScene);

	/// Default Destructor
	virtual ~StaticMeshActor() { }

	/** Simpler setter for the Material.
	 *  @param  material  The Material to assign to the mesh component.
	 */
	inline void setMaterial(Material* pMaterial) { _pMeshComp->setMaterial(pMaterial); }

	/** Sets the Brush's static mesh
	 *  @param  pMesh  The Static Mesh to use when rendering this Brush.
	 */
	void setMesh(StaticMesh* pMesh) { _pMeshComp->setMesh(pMesh); }

	/** Simple getter for the Mesh Component
	 *  @return  The Brush's Mesh Component.
	 */
	inline StaticMeshComponent* GetMeshComp() { return _pMeshComp; }
protected:
private:
	/// A pointer to the Actor's StaticMeshComponent.
	StaticMeshComponent* _pMeshComp;

	SHADE_REFLECTION_BEGIN(StaticMeshActor, Actor)
	SHADE_REFLECTION_END
};

}
