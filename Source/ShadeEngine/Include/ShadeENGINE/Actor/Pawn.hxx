//
//  Pawn.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Actor/Actor.hxx"
#include "ShadeENGINE/Component/CameraComponent.hxx"

namespace ShadeEngine {

class CController;

enum class EActionType : bool { PRESSED, RELEASED };

SHADE_CLASS()
/** Root Class for an Actor which can be controlled by a Player or AI.
 *  @see  Actor
 *  @see  CController
 */
class Pawn : public Actor {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	Pawn(Scene* pScene);

	/// Default Destructor
	virtual ~Pawn() { }

	/** Simple getter for the Controller possessing this Pawn.
	 *  @return  this Pawn's Controller.
	 */
	CController* getController() { return _pController; }

	/** Update all Component's Rotations to match the controller, if they request so.
	 *  @param  delta  A uint16 representing the milliseconds since the last call
	 */
	virtual void update(uint16 delta) override;

	/** Override this to add Axis input to your custom Pawn. Does Nothing.
	 *  @param name   A string representing the name of the Axis.
	 *  @param value  A float representing the value passed by the Axis.
	 *  @param delta  An int representing the milliseconds since the last call.
	 */
	virtual void processAxisInput(std::string name, float value, uint16 delta) { }

	/** Override this to add Action input to your custom Pawn. Does Nothing.
	 *  @param  name  A string representing the name of the Action.
	 *  @param  type  An ActionType stating what happened (Pressed or Released).
	 */
	virtual void processActionInput(std::string name, EActionType type) { }

	/** A simple getter for the Pawn's CameraComponent.
	 *  @return  The Pawn's Camera Component.
	 */
	CameraComponent* getCamera() { return _pCameraComponent; }

protected:
private:
	friend class CController;

	/** Simple setter for this Pawn's Controller.
	 *  CController::possess(Pawn*) and unposess coll this.
	 *  @param  pController  A pointer to controller which will possess the Pawn.
	 *  @see                 CController::possess(Pawn*)
	 *  @see                 CController::unPossess()
	 */
	void setController(CController* pController) { _pController = pController; }

	/// A pointer to the Controller possessing the Pawn.
	CController* _pController;
	/// A pointer to the Pawn's CameraComponent.
	CameraComponent* _pCameraComponent;


	SHADE_REFLECTION_BEGIN(Pawn, Actor)
	SHADE_REFLECTION_END
};

}
