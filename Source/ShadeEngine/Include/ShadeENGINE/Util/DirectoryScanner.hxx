//
//  DirectoryScanner.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <string>

namespace ShadeEngine {

/// Struct for something contained by a folder.
struct FolderItem {
	std::string name;
	std::string path;
	bool isFile;
	virtual ~FolderItem() { if (pNext != nullptr) delete pNext; }
	FolderItem* pNext;
};

/// Struct for a folder.
struct Folder : public FolderItem {
	FolderItem* pChild;
	virtual ~Folder() { if (pChild != nullptr) delete pChild; }
};

/// Static class for scanning a directory.
class DirectoryScanner {
public:
	/** Method which returns a folder tree representing everything in a directory.
	 *  @param  path  Path to the folder to read.
	 */
	static Folder* getAllFilesInDirectory(std::string path);
protected:
private:
};

}
