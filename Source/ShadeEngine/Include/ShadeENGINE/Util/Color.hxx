//
//  Color.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/VectorBase.hxx"

namespace ShadeEngine {

/// A class representing a color. Uses floats in the backend, and stores rgba from 0.0 to 1.0
class Color : public TVector4<float> {
public:

	/// Simple copy constructor.
	explicit Color(const TVector<float, 4>& copy) : TVector4<float>(copy) { }

	/** Simple constructor taking rgba values as floats.
	 *  @param  r  The red component (from 0.0 to 1.0)
	 *  @param  g  The green component (from 0.0 to 1.0)
	 *  @param  b  The blue component (from 0.0 to 1.0)
	 *  @param  a  The alpha component (from 0.0 to 1.0)
	 */
	Color(float r = 0.f, float g = 0.f, float b = 0.f, float a = 0.f ) {
		(*this)[0] = r;
		(*this)[1] = g;
		(*this)[2] = b;
		(*this)[3] = a;
	}

	/** Creates a color using standard 0-255 rgba values.
	 *  @param  r  The red component (from 0 - 255)
	 *  @param  g  The green component (from 0 - 255)
	 *  @param  b  The blue component (from 0 - 255)
	 *  @param  a  The alpha component (from 0 - 255)
	 */
	static Color rgba(uint8 r, uint8 g, uint8 b, uint8 a) {
		return Color(r / 255.f, g / 255.f, b / 255.f, a / 255.f);
	}

	/** Creates an opaque color using standard 0-255 rgb values.
	 *  @param  r  The red component (from 0 - 255)
	 *  @param  g  The green component (from 0 - 255)
	 *  @param  b  The blue component (from 0 - 255)
	 */
	static Color rgb(uint8 r, uint8 g, uint8 b) {
		return Color(r / 255.f, g / 255.f, b / 255.f, 1.f);
	}

protected:
private:

};

}
