//
//  MapLoader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/21/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/AssetStorage/EdScenario.hxx"
#include <stdio.h>
#include "ShadeENGINE/Engine/AssetManager.hxx"

namespace ShadeEngine {

/// Static class for loading maps.
class MapLoader {
public:
	/** Method Reads the header of a map file, and creates a map
	 *  based upon the header's info.
	 *  @param  pFile  Pointer to the file to read
	 *  @return        The newly created scenario, if a valid map file is passed in, null otherwise.
	 */
	static EdScenario* createMap(FILE* pFile);

	/** Populates the scene for a scenario
	 *  @param  pScenario  pointer to the scenario who's scene to populate
	 *  @param  pFile      pointer to the map file to read.
	 *  @param  assetMan   The asset manager to load the scene for.
	 */
	static void loadScenarioScene(EdScenario* pScenario, FILE* pFile, const CAssetManager& assetMan);

protected:
private:
	/** Reads the parameters for a static mesh actor from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene    The scene to add the static mesh actor to.
	 *  @param  pFile     The map file which is being read.
	 *  @param  assetMan  The asset manager to use when setting up the mesh.
	 */
	static void setupStaticMesh(Scene* pScene, FILE* pFile, const CAssetManager& assetMan);

	/** Reads the parameters for a spawn point from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene  The scene to add the spawn point to.
	 *  @param  pFile   The map file which is being read.
	 */
	static void setupSpawnPoint(Scene* pScene, FILE* pFile);

	/** Reads the parameters for a directional light from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene  The scene to add the directional light to.
	 *  @param  pFile   The map file which is being read.
	 */
	static void setupDirectionalLight(Scene* pScene, FILE* pFile);

	/** Reads the parameters for a point light from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene  The scene to add the point light to.
	 *  @param  pFile   The map file which is being read.
	 */
	static void setupPointLight(Scene* pScene, FILE* pFile);

	/** Reads the parameters for a spot light from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene  The scene to add the spot light to.
	 *  @param  pFile   The map file which is being read.
	 */
	static void setupSpotLight(Scene* pScene, FILE* pFile);

	/** Reads the parameters for the CSG Geometry from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene    The scene to add the CSG to.
	 *  @param  pFile     The map file which is being read.
	 *  @param  assetMan  The asset manager to use when setting up CSG Geometry
	 */
	static void setupCSGGeometry(Scene* pScene, FILE* pFile, const CAssetManager& assetMan);

	/** Reads the parameters for a Box Brush from the
	 *  provided map file, and adds it to the scene.
	 *
	 *  @param  pScene  The scene to add the CSG to.
	 *  @param  pFile   The map file which is being read.
	 *  @return         The loaded boxbrush.
	 */
	static void loadBoxBrushFromMap(Scene* pScene, FILE* pFile);
};

}
