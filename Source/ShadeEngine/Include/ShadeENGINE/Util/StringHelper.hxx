//
//  StringHelper.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <string>
#include <stdio.h>
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

/// Static Helper class which processes strings.
class StringHelper {
public:
	/** Gets the extension from a path.
	 *  @param  path  A path.
	 *  @return The extension of the specified file.
	 */
	static std::string getExtension(std::string path);

	/** Gets the filename from a path.
	 *  @param  path  A path.
	 *  @return  The name of the specified file, without the extension.
	 */
	static std::string getFilename(std::string path);

	/** Gets the path to the folder containing the item at path, or returns path if it
	 *  points to the folder.
	 *
	 *  @param  path  The path to the file whose containing directory to return.
	 *  @return       The path to the file's parent folder, or the path to the folder itself,
	 *                if it is a folder.
	 */
	static std::string getContainingFolder(std::string path);

	/** Helper method which reads a string from a file.
	 *  @param  address  The address within the ifstream where the string starts.
	 *  @param  pFile    the File in which to find the string.
	 */
	static std::string getStringFromFile(uint32 address, FILE* pFile);
protected:
private:
};

}
