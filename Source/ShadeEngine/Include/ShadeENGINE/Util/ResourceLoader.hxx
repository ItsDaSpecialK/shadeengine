//
//  ResourceLoader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/StaticMesh.hxx"
#include "ShadeENGINE/Rendering/Texture.hxx"
#include "ShadeENGINE/Rendering/CubeMap.hxx"
#include "ShadeENGINE/Rendering/Material.hxx"
#include <string>
#include "ShadeENGINE/Engine/BSP/Polygon.hxx"
#include "ShadeENGINE/Engine/AssetManager.hxx"

namespace ShadeEngine {

/// Serves as a placeholder while loading the map.
struct MaterialPlaceholder : public Material {
	char* pTexturePath;
};

/// Static class which loads resources.
class ResourceLoader {
public:
	/** Loads a Mesh from a file.
	 *  @param  filename  Path to the mesh.
	 *  @return           The loaded mesh.
	 */
	static StaticMesh* loadMesh(std::string filename);

	/** Loads a Texture from a file.
	 *  @param  filename  Path to the texture.
	 *  @return           The loaded texture object.
	 */
	static Texture* loadTexture(std::string filename);

	/** Loads a CubeMap from a file.
	 *  @param  filenames  Paths to the textures.
	 *  @return            The loaded CubeMap object.
	 */
	static CubeMap* loadCubeMap(std::vector<std::string> filenames);

	/** Loads a Material from a file.
	 *  @param  filename  Path to the material.
	 *  @param  assetMan  The asset manager to load the material for.
	 *  @return           The loaded Material.
	 */
	static Material* loadMaterial(std::string filename, const CAssetManager& assetMan);

protected:
private:
	/** Loads a Texture from a file.
	 *  @param  filename  Path to the texture.
	 *  @param  width     Variable in which to store the width
	 *  @param  height    Variable in which to store the height
	 *  @param  invertY   Whether to invert the Y axis when loading
	 *  @return           The loaded texture as a byte array
	 */
	static byte* loadRawTexture(std::string filename, uint16& width, uint16& height, bool invertY = false);
};

}
