//
//  Vector.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/VectorBase.hxx"

namespace ShadeEngine {

template<typename T>
class TVector2 : public TVector<T, 2> {
public:
	TVector2(T x = T(0), T y = T(0)) {
		(*this)[0] = x;
		(*this)[1] = y;
	}

	TVector2(const TVector<T, 2>& copy) { }

	inline T getX() const { return (*this)[0]; }
	inline T getY() const { return (*this)[1]; }

	inline void setX(const T& x) { (*this)[0] = x; }
	inline void setY(const T& y) { (*this)[1] = y; }

	inline void set(const T& x, const T& y) { setX(x); setY(y); }
protected:
private:
};

template<typename T>
class TVector3 : public TVector<T, 3> {
public:
	TVector3() { }

	TVector3(T x, T y, T z) {
		(*this)[0] = x;
		(*this)[1] = y;
		(*this)[2] = z;
	}

	TVector3(const TVector<T, 3>& copy) {
		(*this)[0] = copy[0];
		(*this)[1] = copy[1];
		(*this)[2] = copy[2];
	}
protected:
private:
};

template<typename T>
class TVector4 : public TVector<T, 4> {
public:
	TVector4() { }

	TVector4(T w, T x, T y, T z) {
		(*this)[0] = w;
		(*this)[1] = x;
		(*this)[2] = y;
		(*this)[3] = z;
	}

	TVector4(const TVector<T, 4>& copy) {
		(*this)[0] = copy[0];
		(*this)[1] = copy[1];
		(*this)[2] = copy[2];
		(*this)[3] = copy[3];
	}
protected:
private:
};

/// The standard vector class. Will have SIMD optimization for supported platforms.
class Vector3f : public TVector3<float> {
public:

	/// Simple copy constructor.
	explicit Vector3f(const TVector<float, 3>& copy) : TVector3<float>(copy) { }

	/** Simple constructor for initializing the vector to a predefined value.
	 *  @param  x  The x component to set in the vector
	 *  @param  y  The y component to set in the vector
	 *  @param  z  The z component to set in the vector
	 */
	Vector3f(float x = 0.f, float y = 0.f, float z = 0.f) {
		(*this)[0] = x;
		(*this)[1] = y;
		(*this)[2] = z;
	}

	/** Gets the cross product of this vector and another.
	 *  @param  other  The other vector to use when calculating the cross product.
	 *  @return        The cross product vector.
	 */
	Vector3f cross(const Vector3f& other) const;

	/** Rotates another vector around the axis that is this vector.
	 *  @param  angle  The angle to rotate this vector by, in degrees.
	 *  @param  other  The vector which will be rotated.
	 *  @return        The rotated vector.
	 */
	Vector3f rotate(float angle, const Vector3f& other) const;

	/** Calculates the dot product of this vector, and another.
	 *  The dot product divided by the length of both vectors
	 *  is the cosine of the angle between them.
	 *  @param  value  The other vector to use when calculating the dot product.
	 *  @return        The resulting value.
	 */
	float dot(const Vector3f& value) const;

	/** Gets the length of this vector, squared.
	 *  Use this instead of length if the length will be squared anyways.
	 *
	 *  @return  The length of this vector times itself.
	 */
	inline float lengthSq() const { return this->dot(*this); }

	/** Gets teh length of this vector.
	 *  @return  The length fo this vector.
	 */
	inline float length() const { return sqrt(lengthSq()); }

	/** Normalizes this vector, and then returns itself.
	 *  The normal vector is a vector which points in
	 *  the same direction, but has a length of 1.
	 *
	 *  @return  The new normalized vector.
	 */
	inline Vector3f normalize() { return *this /= length(); }

	/** Linearly inerpolates between itself and another vector.
	 *  Due to the nature of this function, it does not work with integer vectors.
	 *
	 *  @param  other  The other vector we are interpolating against.
	 *  @param  t      The interpolation time. 0 is this vector, 1 is the other.
	 */
	inline Vector3f lerp(const Vector3f& other, float t) const {
		return (*this) * (1 - t) + other * t;
	}

	/// Standard + operator for adding vectors together.
	Vector3f operator+(const Vector3f& addend) const;

	/// Standard - operator for subtracting vectors.
	Vector3f operator-(const Vector3f& subtrahend) const;

	/// Standard * operator for multiplying a vector by a number.
	Vector3f operator*(float multiplicand) const;

	/// Standard / operator for dividing a vector by a number.
	Vector3f operator/(float dividend) const;

	/// Standard += operator for adding vectors together.
	Vector3f& operator+=(const Vector3f& addend);

	/// Standard -= operator for subtracting vectors.
	Vector3f& operator-=(const Vector3f& subtrahend);

	/// Standard *= operator for multiplying a vector by a number.
	Vector3f& operator*=(float multiplicand);

	/// Standard /= operator for dividing a vector by a number.
	Vector3f& operator/=(float dividend);

	///////////////
	//  Getters  //
	///////////////

	/** Simple getter for the x component of the vector.
	 *  @return  The x component of the vector.
	 */
	inline float getX() const { return (*this)[0]; }

	/** Simple getter for the y component of the vector.
	 *  @return  The y component of the vector.
	 */
	inline float getY() const { return (*this)[1]; }

	/** Simple getter for the z component of the vector.
	 *  @return  The z component of the vector.
	 */
	inline float getZ() const { return (*this)[2]; }

	///////////////
	//  Setters  //
	///////////////

	/** Simple setter for the x component of the vector.
	 *  @param  x  The new x component of the vector.
	 */
	inline void setX(float x) { (*this)[0] = x; }

	/** Simple setter for the y component of the vector.
	 *  @param  y  The new y component of the vector.
	 */
	inline void setY(float y) { (*this)[1] = y; }

	/** Simple setter for the z component of the vector.
	 *  @param  z  The new z component of the vector.
	 */
	inline void setZ(float z) { (*this)[2] = z; }





	/** Simple setter for the x, y, and z components of the vector.
	 *  @param  x  The new x component of the vector.
	 *  @param  y  The new y component of the vector.
	 *  @param  z  The new z component of the vector.
	 */
	inline void set(float x, float y, float z) { setX(x); setY(y); setZ(z); }

protected:
private:

};

typedef TVector2<int> Vector2i;
typedef TVector3<int> Vector3i;
typedef TVector4<int> Vector4i;

typedef TVector2<float> Vector2f;
// typedef TVector3<float> Vector3f;
typedef TVector4<float> Vector4f;

Vector2f lerp(const Vector2f& start, const Vector2f& end, float t);

}
