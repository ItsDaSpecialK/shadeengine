//
//  ShadeIntegers.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <climits>

// 8-bit
#define SHADE_TYPE8 char
#if CHAR_BIT != 8
#error Shade Engine currently only supports platforms with 8 bits per byte.
#endif

// 16-bit
#if USHRT_MAX == 65535
#define SHADE_TYPE16 short
#elif UINT_MAX == 65535
#define SHADE_TYPE16 int
#else
#error System doesn't have correctly defined 16 bit integer type.
#define SHADE_TYPE16 short
#endif

// 32-bit
#if USHRT_MAX == 4294967295
#define SHADE_TYPE32 short
#elif UINT_MAX == 4294967295
#define SHADE_TYPE32 int
#elif ULONG_MAX == 4294967295
#define SHADE_TYPE32 long
#else
#error System doesn't have correctly defined 32 bit integer type.
#define SHADE_TYPE32 long
#endif

// 64-bit
#if USHRT_MAX == 18446744073709551615ULL
#define SHADE_TYPE64 short
#elif UINT_MAX == 18446744073709551615ULL
#define SHADE_TYPE64 int
#elif ULONG_MAX == 18446744073709551615ULL
#define SHADE_TYPE64 long
#elif ULLONG_MAX == 18446744073709551615ULL
#define SHADE_TYPE64 long long
#else
#error System doesn't have correctly defined 64 bit integer type.
#define SHADE_TYPE64 long long
#endif

/// Signed 8-bit integer [-128 to 127]
typedef signed SHADE_TYPE8 int8;

/// Unsigned 8-bit integer [0 to 255]
typedef unsigned SHADE_TYPE8 uint8;

/// Signed 16-bit integer [−32,768 to 32,767]
typedef signed SHADE_TYPE16 int16;

/// Unsigned 16-bit integer [0 to 65,535]
typedef unsigned SHADE_TYPE16 uint16;

/// Signed 32-bit integer [-2,147,483,648 to 2,147,483,647]
typedef signed SHADE_TYPE32 int32;

/// Unsigned 32-bit integer [0 to 4,294,967,295]
typedef unsigned SHADE_TYPE32 uint32;

/// Signed 64-bit integer [−9,223,372,036,854,775,808 to 9,223,372,036,854,775,807]
typedef signed SHADE_TYPE64 int64;

/// Unsigned 64-bit integer [0 to 18,446,744,073,709,551,615]
typedef unsigned SHADE_TYPE64 uint64;






/// Signed 8-bit integer [-128 to 127]
typedef signed SHADE_TYPE8 s8;

/// Unsigned 8-bit integer [0 to 255]
typedef unsigned SHADE_TYPE8 u8;

/// Signed 16-bit integer [−32,768 to 32,767]
typedef signed SHADE_TYPE16 s16;

/// Unsigned 16-bit integer [0 to 65,535]
typedef unsigned SHADE_TYPE16 u16;

/// Signed 32-bit integer [-2,147,483,648 to 2,147,483,647]
typedef signed SHADE_TYPE32 s32;

/// Unsigned 32-bit integer [0 to 4,294,967,295]
typedef unsigned SHADE_TYPE32 u32;

/// Signed 64-bit integer [−9,223,372,036,854,775,808 to 9,223,372,036,854,775,807]
typedef signed SHADE_TYPE64 s64;

/// Unsigned 64-bit integer [0 to 18,446,744,073,709,551,615]
typedef unsigned SHADE_TYPE64 u64;





/// Raw Byte
typedef unsigned SHADE_TYPE8 byte;






#undef SHADE_TYPE8
#undef SHADE_TYPE16
#undef SHADE_TYPE32
#undef SHADE_TYPE64
