//
//  Matrix.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeMath.hxx"
#include "ShadeENGINE/Core/Math/Vector.hxx"

namespace ShadeEngine {

template<typename T, unsigned int D>
class TMatrix {
public:
	/** Simple setter for a field in the matrix. Keep in mind that the matrix is stored
	 *  in Column-Major order, so when iterating, the inner loop should be the row.
	 *  @param  row    The row where the value goes
	 *  @param  col    The column where the value goes
	 *  @param  val  The value to set.
	 */
	inline void set(uint8 row, uint8 col, T val) { _m[col][row] = val; }

	/// Simple const array operator for accessing the underlying array.
	inline const T* operator[](int index) const { return _m[index]; }

	/// Simple array operator for accessing the underlying array.
	inline T* operator[](int index) { return _m[index]; }

protected:
	/// The array which holds the matrix's values.
	T _m[D][D];
private:

};

/// Base class for a 4x4 matrix of floating points. Note that values are stored in column major order.
class Matrix : public TMatrix<float, 4> {
public:
	/// Default No-args constructor. Does nothing.
	Matrix() { }

	/// Simple copy constructor.
	Matrix(const Matrix& other);

	/// Simple constructor that takes a 4x4 2d array as input (row major).
	Matrix(float i00, float i01, float i02, float i03,
	       float i10, float i11, float i12, float i13,
	       float i20, float i21, float i22, float i23,
	       float i30, float i31, float i32, float i33);

	/** Initializes this matrix to the identity matrix
	 *  @return  This matrix, now initialized to the identity matrix
	 */
	Matrix& initToIdentity();

	/** Initializes this matrix to a scale matrix
	 *  @param  value  The vector to scale by.
	 *  @return        This matrix, now initialized to a scale matrix
	 */
	Matrix& initToScale(const Vector3f& value);

	/** Initializes this matrix to a translation matrix
	 *  @param  value  The vector to translate by.
	 *  @return        This matrix, now initialized to a translation matrix
	 */
	Matrix& initToTranslation(const Vector3f& value);

	/** Initializes the matrix to a rotation matrix using a forward, up, and right vector.
	 *  @param  forward  A vector pointing in the forward direction.
	 *  @param  up       A vector pointing in the up direction.
	 *  @param  right    A vector pointing to the right.
	 */
	Matrix& initRotationFromVectors(const Vector3f& forward,
	                                const Vector3f& up,
	                                const Vector3f& right);

	/** Initializes the matrix to a perspective matrix.
	 *  @param  fov          The field of view of the perspective, in degrees.
	 *  @param  aspectRatio  The aspect ratio of the surface to draw to.
	 *  @param  zNear        The closest distance something should be drawn at.
	 *  @param  zFar         The furthest something should be drawn at.
	 */
	Matrix& initToPerspective(float fov, float aspectRatio, float zNear, float zFar );

	/** Initializes the matrix to a orthographic view matrix.
	 *  @param  right   The right side of the view.
	 *  @param  left    The left side of the view.
	 *  @param  top     The top of the view.
	 *  @param  bottom  The bottom of the view.
	 *  @param  far     The furthest distance to be drawn.
	 *  @param  near    The nearest distance to be drawn.
	 */
	Matrix& initToOrthographic(float right, float left, float top, float bottom, float far, float near);

	/** Gets a transposed version of this matrix (flips colums and rows)
	 *  @return  A transposed version of this matrix
	 */
	Matrix transpose() const;

	/** Transforms a Vector3f, which is one smaller than this matrix, by this matrix.
	 *  This is often used in 3d rendering since often vector3's and matrix4's are used together.
	 *  This treats the vector like a vector of the matrix's size, with a 1 in the last value.
	 *
	 *  @param  vec  The vector to transform.
	 *  @return      The transformed vector.
	 */
	Vector3f transform(const Vector3f& vec) const;

	/** Transforms a Vector4f by this matrix.
	 *
	 *  @param  vec  The vector to transform.
	 *  @return      The transformed vector.
	 */
	Vector4f transform(const Vector4f& vec) const;

	/// Simple * operator which multiplies this matrix by another.
	Matrix operator*(const Matrix& other) const;

protected:
private:

};

typedef Matrix Matrix4;

/// Base class for a 3x3 matrix of floating points. Note that values are stored in column major order.
class Matrix3 : public TMatrix<float, 3> {
public:
	/// Default No-args constructor. Does nothing.
	Matrix3() { }

	/// Simple copy constructor.
	Matrix3(const Matrix3& other);

	/// Simple constructor that takes a 4x4 2d array as input (row major).
	Matrix3(float i00, float i01, float i02,
	        float i10, float i11, float i12,
	        float i20, float i21, float i22);

	/** Initializes this matrix to the identity matrix
	 *  @return  This matrix, now initialized to the identity matrix
	 */
	Matrix3& initToIdentity();

	/** Gets a transposed version of this matrix (flips colums and rows)
	 *  @return  A transposed version of this matrix
	 */
	Matrix3 transpose() const;

	/// Simple * operator which multiplies this matrix by another.
	Matrix3 operator*(const Matrix3& other) const;

protected:
private:
};

}
