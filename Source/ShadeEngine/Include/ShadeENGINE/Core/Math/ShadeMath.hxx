//
//  ShadeMath.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include <cmath>
#include <cstdlib>

#define MATH_PI 3.1415926535897932384626433832795
#define toRadians(x) (float)(((x) * MATH_PI / 180.0f))
#define toDegrees(x) (float)(((x) * 180.0f / MATH_PI))

namespace ShadeEngine {

/// provides the user with some standard library methods.

/// Rounds a floating point number up (ex: 4.1 becomes 5).
using std::ceil;

/// Rounds a floating point number down (ex: 4.9 becomes 4).
using std::floor;

/** Gets the power of a number
 *  @param   x
 *  @param   y
 *  @return  x^y
 */
using std::pow;

/// Gets the square root of a float.
using std::sqrt;

/// Gets the absolute value of a number (always returns a postive number)
using std::abs;

/** Clamps a value between two other values.
 *  The returned value will greater than or equal to the min, and less than or equal to the max.
 *  @param  value  The value which is being clamped.
 *  @param  min    The minimum value which would be output.
 *  @param  max    The maximum value which would be output.
 *  @return        The clamped result.
 */
template<typename T>
T clamp(const T& value, const T& min, const T& max) {
	if (value < min)
		return min;
	else if (value > max)
		return max;
	else
		return value;
}

}
