//
//  Quaternion.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/Matrix.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"

namespace ShadeEngine {

/// Class representing a Quaternion. Used to represent rotation internally.
class Quaternion : public TVector4<float> {
public:
	/** Simple constructor which takes in 4 paramaters for the four components, and sets them.
	 *  @param  w  The real component.
	 *  @param  x  The imaginary x component.
	 *  @param  y  The imaginary y component.
	 *  @param  z  The imaginary z component.
	 */
	Quaternion(float w = 1.f, float x = 0.f, float y = 0.f, float z = 0.f) {
		(*this)[0] = w;
		(*this)[1] = x;
		(*this)[2] = y;
		(*this)[3] = z;
	}

	/// Simple copy constructor which copies a Vector4's components into a new quaternion.
	explicit Quaternion(const TVector<float, 4>& copy) : TVector4<float>(copy) { }

	/** Creates a quaternion with an axis and an angle.
	 *  @param  axis   The axis of rotation.
	 *  @param  angle  The number of DEGREES to rotate around this axis.
	 */
	Quaternion(const Vector3f& axis, float angle);

	/** Calculates the dot product of this quaternion, and another.
	 *  The dot product divided by the length of both quaternions
	 *  is the cosine of the angle between them.
	 *  @param  value  The other quaternion to use when calculating the dot product.
	 *  @return        The resulting value.
	 */
	float dot(const Quaternion& value) const;

	/** Gets the length of this quaternion, squared.
	 *  Use this instead of length if the length will be squared anyways.
	 *
	 *  @return  The length of this quaternion times itself.
	 */
	inline float lengthSq() const { return this->dot(*this); }

	/** Gets teh length of this quaternion.
	 *  @return  The length fo this quaternion.
	 */
	inline float length() const { return sqrt(lengthSq()); }

	/** Normalizes this quaternion, and then returns itself.
	 *  The normal vector is a quaternion which points in
	 *  the same direction, but has a length of 1.
	 *
	 *  @return  The new normalized quaternion.
	 */
	inline Quaternion normalize() { return *this /= length(); }

	/// Conjugates the quaternion. (negates the imaginary components).
	inline Quaternion conjugate() const { return Quaternion(getW(), -getX(), -getY(), -getZ()); }

	/** Gets a rotation matrix from this quaternion.
	 *  @return  A rotation matrix which can be used for 3d rotation.
	 */
	Matrix toRotationMatrix() const;

	/// Standard + operator for adding quaternions together.
	Quaternion operator+(const Quaternion& addend) const;

	/// Standard - operator for subtracting quaternions.
	Quaternion operator-(const Quaternion& subtrahend) const;

	/// Standard * operator for multiplying a quaternion by a number.
	Quaternion operator*(float multiplicand) const;

	/// Standard / operator for dividing a quaternion by a number.
	Quaternion operator/(float dividend) const;

	/// Standard += operator for adding quaternions together.
	Quaternion& operator+=(const Quaternion& addend);

	/// Standard -= operator for subtracting quaternions.
	Quaternion& operator-=(const Quaternion& subtrahend);

	/// Standard *= operator for multiplying a quaternion by a number.
	Quaternion& operator*=(float multiplicand);

	/// Standard /= operator for dividing a quaternion by a number.
	Quaternion& operator/=(float dividend);

	/// Simple operator for multiplying another quaternion by this quaternion.
	Quaternion operator*(const Quaternion& other) const;

	/// Simple operator for multiplying a vector by this quaternion.
	Quaternion operator*(const Vector3f& other) const;

	/// Rotates a vector around this quaternion.
	Vector3f rotate(const Vector3f& vec) const;

	///////////////
	//  Getters  //
	///////////////

	/** Simple getter for the w component of the vector.
	 *  @return  The w component of the vector.
	 */
	inline float getW() const { return (*this)[0]; }

	/** Simple getter for the x component of the vector.
	 *  @return  The x component of the vector.
	 */
	inline float getX() const { return (*this)[1]; }

	/** Simple getter for the y component of the vector.
	 *  @return  The y component of the vector.
	 */
	inline float getY() const { return (*this)[2]; }

	/** Simple getter for the z component of the vector.
	 *  @return  The z component of the vector.
	 */
	inline float getZ() const { return (*this)[3]; }

	///////////////
	//  Setters  //
	///////////////

	/** Simple setter for the w component of the vector.
	 *  @param  w  The new w component of the vector.
	 */
	inline void setW(float w) { (*this)[0] = w; }

	/** Simple setter for the x component of the vector.
	 *  @param  x  The new x component of the vector.
	 */
	inline void setX(float x) { (*this)[1] = x; }

	/** Simple setter for the y component of the vector.
	 *  @param  y  The new y component of the vector.
	 */
	inline void setY(float y) { (*this)[2] = y; }

	/** Simple setter for the z component of the vector.
	 *  @param  z  The new z component of the vector.
	 */
	inline void setZ(float z) { (*this)[3] = z; }




	/** Simple setter for the w, x, y, and z components of the quaternion.
	 *  @param  w  The new real component of the quaternion
	 *  @param  x  The new imaginary x component of the quaternion.
	 *  @param  y  The new imaginary y component of the quaternion.
	 *  @param  z  The new imaginary z component of the quaternion.
	 */
	inline void set(float w, float x, float y, float z) { setW(w); setX(x); setY(y); setZ(z); }



	inline Vector3f getForward() const { return rotate(Vector3f( 0, 1, 0)); }
	inline Vector3f getBack()    const { return rotate(Vector3f( 0, -1, 0)); }
	inline Vector3f getUp()      const { return rotate(Vector3f( 0, 0, 1)); }
	inline Vector3f getDown()    const { return rotate(Vector3f( 0, 0, -1)); }
	inline Vector3f getRight()   const { return rotate(Vector3f( 1, 0, 0)); }
	inline Vector3f getLeft()    const { return rotate(Vector3f(-1, 0, 0)); }
};

}
