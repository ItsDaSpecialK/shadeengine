//
//  VectorBase.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"

namespace ShadeEngine {

/// The Base templated class for a vector. Defines generalized functions for vectors of all sizes.
template<typename T, uint8 D>
class TVector {
public:
	/// Default No-args constructor.
	TVector() { }

	/** Simple copy constructor.
	 *  @param  copy  The vector to duplicate.
	 */
	TVector(const TVector<T, D>& copy) { for (uint8 i = 0; i < D; ++i) (*this)[i] = copy[i]; }

	/// Simple acessor for accessing the vector's fields like an array.
	T& operator[](uint8 i) { return _values[i]; }

	/// Simple acessor for accessing the vector's fields like an array.
	T operator[](uint8 i) const { return _values[i]; }

	/// Simple comparison operator for checking whether the vectors are equal.
	inline bool operator==(const TVector<T, D>& equivalent) const {
		for (unsigned int i = 0; i < D; ++i)
			if (_values[i] != equivalent[i])
				return false;
		return true;
	}

	/// Simple comparison operator for checking whether the vectors are not equal.
	inline bool operator!=(const TVector<T, D>& equivalent) const { return !operator==(equivalent); }
protected:
private:
	/// The array which holds the vector's values.
	T _values[D];
};

}
