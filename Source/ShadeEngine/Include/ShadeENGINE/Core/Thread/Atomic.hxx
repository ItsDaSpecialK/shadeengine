//
//  Atomic.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifdef SHADE_NO_MULTITHREAD
#define SHADE_ATOMIC(x) x
#else
#include <atomic>
#define SHADE_ATOMIC(x) std::atomic<x>
#endif
