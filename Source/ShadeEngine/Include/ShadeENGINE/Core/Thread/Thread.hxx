//
//  Thread.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifndef SHADE_NO_MULTITHREAD
#include <thread>
#endif

namespace ShadeEngine {
#ifndef SHADE_NO_MULTITHREAD

typedef std::thread Thread;

#endif
}
