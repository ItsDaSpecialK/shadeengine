//
//  Archive.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/14/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Serialization/Serializable.hxx"
#include "ShadeENGINE/Core/Serialization/Endianness.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

/// Base class for representing an Archive
class Archive {
public:
	/** Simple Constructor.
	 *  @param  bIsReading           Whether the stream is reading or writing.
	 *  @param  bShouldSerializeAll  Whether classes should serialize everything.
	 *  @param  bIsNetworkData       Whether data is networked or local.
	 */
	Archive(bool bIsReading, bool bShouldSerializeAll, bool bIsNetworkData);

	/// Default Destructor.
	virtual ~Archive() { }

	/** Simple getter for isReading.
	 *  @return  Whether the stream is reading or writing.
	 */
	bool getIsReading() { return _bIsReading && !_bIsWriting; }

	/** Simple negative getter for isReading.
	 *  @return  Whether the stream is reading or writing.
	 */
	bool getIsWriting() { return _bIsWriting && !_bIsReading; }

	/// Resets the isWriting and isReading fields to false.
	void resetReadWrite() { _bIsWriting = _bIsReading = false; }

	/// Sets the bitflags to reading.
	void setIsReading() { _bIsReading = true; _bIsWriting = false; }

	/// Sets the bitflags to reading.
	void setIsWriting() { _bIsWriting = true; _bIsReading = false; }

	/** Simple getter for shouldSerializeAll.
	 *  @return  Whether classes should serialize everything.
	 */
	bool getShouldSerializeAll() { return _bShouldSerializeAll; }

	/** Simple getter for isNetworkData.
	 *  @return  Whether data is networked or local.
	 */
	bool getIsNetworkData() { return _bIsNetworkData; }

	/// Catch all serialization operator for unimplemented classes.
	template<typename T>
	Archive& operator&(T& object) {
		shadeErrorFatal("Undefined Serialization");
		return *this; // @Todo: implement read write for other classes.
	}

	/** Reads the data from the archive into the object.
	 *  @param  object  The object to read data into.
	 */
	template<typename T>
	Archive& operator>>(T& object) {
		if (getIsReading()) {
			*this & object;
		} else {
			setIsReading();
			*this & object;
			resetReadWrite();
		}
		return *this;
	}

	/** Writes an object into the archive.
	 *  @param  object  The object to read data from.
	 */
	template<typename T>
	Archive& operator<<(T& object) {
		if (getIsWriting()) {
			*this & object;
		} else {
			setIsWriting();
			*this & object;
			resetReadWrite();
		}
		return *this;
	}

	/** Generic Serialization operator for classes that inherit from Serializable.
	 *  @param  The object to serialize/deserialize.
	 */
	inline Archive& operator&(Serializable& object) {
		return object.serialize(*this);
	}

	/** Serialization operator for 8-bit integers
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(int8& num);

	/** Serialization operator for 16-bit integers
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(int16& num);

	/** Serialization operator for 32-bit integers
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(int32& num);

	/** Serialization operator for 64-bit integers
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(int64& num);

	/** Serialization operator for 32-bit floats
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(float& num);

	/** Serialization operator for 64-bit doubles
	 *  @param  num  The number to serialize/deserialize.
	 */
	Archive& operator&(double& num);

protected:
	/** Writes the byte stream to the archive
	 *  @param  pByteStream  pointer to the variable to write.
	 *  @param  numBytes     the number of bytes to write.
	 */
	virtual void writeToStream(byte* pByteStream, uint16 numBytes) = 0;

	/** Reads the byte stream from the archive
	 *  @param  pByteStream  pointer to the variable to read.
	 *  @param  numBytes     the number of bytes to read.
	 */
	virtual void readFromStream(byte* pByteStream, uint16 numBytes) = 0;

private:
	/// Whether the stream is reading
	uint8 _bIsReading : 1;

	/// Whether the stream is writing
	uint8 _bIsWriting : 1;

	/// Whether classes should serialize everything
	uint8 _bShouldSerializeAll : 1;

	/// Whether data is networked or local
	uint8 _bIsNetworkData : 1;
};

}
