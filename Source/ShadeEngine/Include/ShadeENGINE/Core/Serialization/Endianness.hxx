//
//  Endianness.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

/** Checks to see whether the system is little endian.
 *  @return  true if the system is little endian, false if big endian.
 */
bool isLittleEndian();

/** Swaps the endianness of a number
 *  @param  num  The number to switch from big to little endian, or vice versa.
 */
inline void endianSwap(int16& num) {
	num = (num >> 8) |
	      (num << 8);
}

/** Swaps the endianness of a number
 *  @param  num  The number to switch from big to little endian, or vice versa.
 */
inline void endianSwap(int32& num) {
	num = (num >> 24) |
	      ((num << 8) & 0x00FF0000) |
	      ((num >> 8) & 0x0000FF00) |
	      (num << 24);
}

/** Swaps the endianness of a number
 *  @param  num  The number to switch from big to little endian, or vice versa.
 */
inline void endianSwap(int64& num) {
	num = (num >> 56) |
	      ((num << 40) & 0x00FF000000000000) |
	      ((num << 24) & 0x0000FF0000000000) |
	      ((num << 8)  & 0x000000FF00000000) |
	      ((num >> 8)  & 0x00000000FF000000) |
	      ((num >> 24) & 0x0000000000FF0000) |
	      ((num >> 40) & 0x000000000000FF00) |
	      (num << 56);
}

/** Swaps the endianness of a number
 *  @param  num  The number to switch from big to little endian, or vice versa.
 */
inline void endianSwap(float& num) {
	int32* intNum = (int32*)(&num);
	*intNum = (*intNum >> 24) |
	          ((*intNum << 8) & 0x00FF0000) |
	          ((*intNum >> 8) & 0x0000FF00) |
	          (*intNum << 24);
}

/** Swaps the endianness of a number
 *  @param  num  The number to switch from big to little endian, or vice versa.
 */
inline void endianSwap(double& num) {
	int64* intNum = (int64*)(&num);
	*intNum = (*intNum >> 56) |
	          ((*intNum << 40) & 0x00FF000000000000) |
	          ((*intNum << 24) & 0x0000FF0000000000) |
	          ((*intNum << 8)  & 0x000000FF00000000) |
	          ((*intNum >> 8)  & 0x00000000FF000000) |
	          ((*intNum >> 24) & 0x0000000000FF0000) |
	          ((*intNum >> 40) & 0x000000000000FF00) |
	          (*intNum << 56);
}

/** Converts a number to and from network order (big endian).
 *  @param  num  The number to convert to and from network order.
 */
template<typename T>
inline void networkOrder(T& num) {
	if (isLittleEndian()) {
		endianSwap(num);
	}
}
}
