//
//  Serializable.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/15/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

namespace ShadeEngine {

class Archive;

class Serializable {
public:
	virtual Archive& serialize(Archive& archive) = 0;
};

}
