//
//  ShadeError.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/14/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <iostream>

namespace ShadeEngine {

/** Method outputs an error message and closes the program, after waiting for any input.
 *  @param  msg  The error message to output.
 */
void shadeErrorFatal(const std::string& msg);

/** Method outputs an error message and closes the program, after waiting for any input.
 *  Additionally outputs an OpenGL error message.
 *  @param  msg    The error message to output.
 *  @param  glerr  The OpenGL error message to output.
 */
void shadeErrorFatal(const std::string& msg, char err[]);

}
