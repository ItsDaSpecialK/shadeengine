//
//  Timer.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

namespace Time {

/** Returns the time since the epoch in milliseconds.
 *  @return  The time since the epoch in milliseconds.
 */
uint32 getCurrentTime();

/** Sleeps for the specified duration in milliseconds.
 *  @param  timeUnits  The number of milliseconds to sleep for.
 */
void sleep(uint32 milliseconds);

}

}
