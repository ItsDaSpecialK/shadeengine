//
//  SmartBuffer.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

/// Represents an resizable array which stores elements in a random, shifting order.
template<typename T>
class TSmartBuffer {
public:
	/// Default Constructor.
	TSmartBuffer() { }

	/// Default Destructor
	virtual ~TSmartBuffer() { }

	/** Returns the size of the buffer.
	 *  @return  the number of elements in the array.
	 */
	inline uint32 size() { return _data.size(); }

	/** Resizes the internal array to the specified size.
	 *  @param  size  The desired size of the array.
	 */
	inline void resize(uint32 size) { _data.resize(size); }

	/** Adds an element to the buffer.
	 *  @param  element  The element to add to the array.
	 */
	inline void add(const T& element) { _data.push_back(element); }

	/** Removes the specified element from the buffer.
	 *  Switches the last element with this one, and then deletes it.
	 *  @param  element  The element to remove.
	 */
	inline void remove(const T& element) {
		for (uint32 i = 0; i < _data.size(); i++) {
			if (element == _data[i]) {
				removeAt(i);
				i--;
			}
		}
	}

	/** Removes the element at the specified index from the buffer.
	 *  Switches the last element with this one, and then deletes it.
	 *  @param  index  The index of the element to remove.
	 */
	inline void removeAt(uint32 index) {
		std::swap(_data[index], _data.back());
		_data.pop_back();
	}

	/// Equals operator for another smart buffer.
	inline TSmartBuffer& operator=(const TSmartBuffer& other) { this->_data = other._data; }

	/// Equals operator for a regular C array.
	inline TSmartBuffer& operator=(T* array) { this->_data = array; }

	/// Element access to the buffer.
	T& operator[](uint32 i) { return _data[i]; }

	/// Element access to the buffer.
	T operator[](uint32 i) const { return _data[i]; }

	typedef typename std::vector<T>::iterator iterator;
	typedef typename std::vector<T>::const_iterator const_iterator;

	/// Return iterator to beginning.
	inline iterator begin() { return _data.begin(); }

	/// Return iterator to beginning.
	inline const_iterator begin() const { return _data.begin(); }

	/// Return const_iterator to beginning.
	inline const_iterator cbegin() const { return _data.cbegin(); }

	/// Return iterator to end.
	inline iterator end() { return _data.end(); }

	/// Return iterator to end.
	inline const_iterator end() const { return _data.end(); }

	/// Return const_iterator to end.
	inline const_iterator cend() const { return _data.cend(); }

protected:
private:
	std::vector<T> _data;
};

}
