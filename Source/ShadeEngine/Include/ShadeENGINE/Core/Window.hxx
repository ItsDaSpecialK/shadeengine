//
//  Window.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <SDL2/SDL.h>
#ifdef WIN32
// NOTHING HERE!!!!!!!
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "SDL2/SDL_opengl.h"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include <string>

namespace ShadeEngine {

/// Simple class which represents a Window. Can currently only have one.
class Window {
public:
	/** Creates a window if none already exists.
	 *  @param  width   The desired width for the window.
	 *  @param  height  The desired height for the window.
	 *  @param  title   The desired title for the window.
	 */
	static void create(uint16 width, uint16 height, std::string& title);

	/// Closes the window, if it exists.
	static void close();

	/// Clears the Window's back buffer to black.
	static void clear();

	/// Switches the window buffers.
	inline static void render() { SDL_GL_SwapWindow(s_pWindow); }

	/** Simple getter to check whether the program should close.
	 *  @return  Whether the window should close.
	 */
	inline static bool isCloseRequested() { return s_bCloseRequested; }

	/** Simple getter to check whether the window has the focus.
	 *  @return  Whether the window has focus.
	 */
	inline static bool hasFocus() { return s_bHasFocus; }

	/** Simple setter for whether the window has focus. Dont call this yourself.
	 *  @param  bHasFocus  Whether the window has focus.
	 */
	inline static void setHasFocus(bool bHasFocus) { s_bHasFocus = bHasFocus; }

	/** Simple setter for whether the window should close. Dont call this yourself.
	 *  @param  Whether the window should close.
	 */
	inline static void setCloseRequested(bool value) { s_bCloseRequested = value; }

	/** Simple getter for the window's width.
	 *  @return  The window's width.
	 */
	inline static uint16 getWidth() { return s_width; }

	/** Simple getter for the window's height.
	 *  @return  The window's height.
	 */
	inline static uint16 getHeight() { return s_height; }

	/** Simple setter for whether the cursor should be visible.
	 *  @param  bIsVisible  Whether the window should be visible.
	 */
	inline static void setCursorVisible(bool bIsVisible) { SDL_ShowCursor(bIsVisible); }

	/** Simple getter for the window's title.
	 *  @return  the window's title.
	 */
	inline static const std::string& getTitle() { return s_title; }

	/** Simple getter for the aspect ratio.
	 *  @return  the window's aspect ratio (Width/Height).
	 */
	inline static float getAspectRatio() { return (float)getWidth() / (float)getHeight(); }

	/// Resets the mouse position to the middle of the screen.
	inline static void resetMouseLocation() { SDL_WarpMouseInWindow(s_pWindow, getWidth() / 2, getHeight() / 2); }
protected:
private:
	/// A pointer to the SDL_window.
	static SDL_Window* s_pWindow;

	/// The window's context.
	static SDL_GLContext s_glContext;

	/// A boolean storing whether the window is trying to close.
	static bool s_bCloseRequested;

	/// A boolean storing whether the window is in focus.
	static bool s_bHasFocus;

	/// The window's width.
	static uint16 s_width;

	/// The window's height.
	static uint16 s_height;

	/// The window's title.
	static std::string s_title;
};

}
