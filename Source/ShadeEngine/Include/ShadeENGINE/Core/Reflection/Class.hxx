//
//  Class.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Core/Serialization/Archive.hxx"

namespace ShadeEngine {

class Scene;
typedef void*(* SCreateFuncPtr)(Scene*);

/// Represents Classes for Reflection
class ShadeClass {
public:
	/** Default Constructor.
	 *  @param  pSuper  A pointer to the Super ShadeClass (or nullptr).
	 *  @param  pName   The ShadeClass's name.
	 */
	ShadeClass(ShadeClass* pSuper, const char* pName, SCreateFuncPtr pCreationFunction = nullptr);

	/// Simple Deconstructor.
	virtual ~ShadeClass() { }

	/** Creates a new instance of the represented class.
	 *  @param  pScene  The scene which will contain this object.
	 *  @return         A void pointer to the created instance
	 */
	virtual void* createNew(Scene* pScene) const {
		return (*_pCreateFunc)(pScene);
	}

	/** Simple getter for the class's name.
	 *  @return  The class's name.
	 */
	virtual const char* getName() const { return _pName; }

	/** Checks whether this is a Subclass of another class.
	 *  @param  pOther  the supposed superclass.
	 */
	bool isChildOf(const ShadeClass* pOther);

protected:
	/** Simple getter for the super class.
	 *  @return  The super ShadeClass.
	 */
	virtual ShadeClass* getSuperClass() const { return _pSuperClass; }

private:
	/// A pointer to the ShadeClass's superclass
	ShadeClass* _pSuperClass;

	/// The class's name.
	const char* _pName; // @TODO: std::string?

	/// A pointer to the creation function
	SCreateFuncPtr _pCreateFunc;
};

/// Pointer to ShadeClass which must be a subclass of T
template<typename T>
class TSubclass {
public:
	/// Default Constructor.
	TSubclass() : _pClass(nullptr) { }

	/// Copy Constructor
	template<typename TOther>
	TSubclass(const TSubclass<TOther>& other) {
		setToClass(*other);
		T* test = (TOther*)NULL; // verify that TOther derives from T
	}

	TSubclass(ShadeClass* pOther) {
		setToClass(pOther);
	}

	/// Simple Deconstructor.
	virtual ~TSubclass() { }

	/** Sets the class pointer to pOther, if it is a subclass of T
	 *  @param  pOther  The class you want to point to.
	 */
	void setToClass(ShadeClass* pOther) {
		if (pOther->isChildOf(T::staticClass())) {
			_pClass = pOther;
		} else {
			_pClass = nullptr;
		}
	}

	/** Gets the ShadeClass for uppermost superclass representable by this.
	 *  @return  The ShadeClass for Type T.
	 */
	ShadeClass* getTopmostClass() { return T::staticClass(); }

	/** Creates a new instance of the represented class, if there is one.
	 *  @param  pScene  The scene in which to put this object.
	 *  @return         A T pointer to the created instance
	 */
	virtual T* createNew(Scene* pScene) {
		if (_pClass == nullptr) {
			shadeErrorFatal("Why was the class null. Check the \"Generated\" code.");
			return nullptr;
		}
		return (T*)_pClass->createNew(pScene);
	}

	/// Sets this class equal to the other subclass if possible.
	template<class TOther>
	TSubclass& operator=(const TSubclass<TOther>& other) {
		setToClass(*other);
		T* Test = (TOther*)NULL; // verify that TOther derives from T
		return *this;
	}

	/// Sets this class equal to the other class if possible.
	TSubclass& operator=(ShadeClass* pOther) {
		setToClass(pOther);
		return *this;
	}

	/** Dereferences the Subclass back into the original class.
	 *  @return  The embedded ShadeClass
	 */
	ShadeClass* operator*() const {
		return _pClass;
	}

	/** Dereferences the Subclass back into the original class.
	 *  @return  The embedded ShadeClass
	 */
	ShadeClass* operator->() const {
		return _pClass;
	}

	/// Implicit Conversion to ShadeClass.
	operator ShadeClass*() const {
		return _pClass;
	}

	/** Simple getter for the class's name.
	 *  @return  The class's name.
	 */
	virtual const char* getName() const { return _pClass->getName(); }


protected:
private:
	/// A pointer to the class this should represent.
	ShadeClass* _pClass;
};

/// Currently Incomplete
class EditorClass : public ShadeClass { // @TODO: Finish TEditorClass
public:
	EditorClass(ShadeClass* pSuper, const char* pName) : ShadeClass(pSuper, pName) { }
	virtual ~EditorClass() { }
	virtual void* createNew(Scene* pScene) const override {
		void* pObject = getSuperClass()->createNew(pScene);

		// Set Variables

		return pObject;
	}
protected:
private:
};


#define SHADE_CLASS(...)// Temporary @TODO: Generate class info
#define SHADE_PROPERTY(...)// Temporary @TODO: Generate class info
#define SHADE_FUNCTION(...)// Temporary @TODO: Generate class info
#define SHADE_GENERATED_BODY// Temporary @TODO: Generate class info
#define ABSTRACT// Temporary @TODO: Generate class info

#define SHADE_REFLECTION_BEGIN(rClass, rSuper) \
public: \
	static void* createNew(Scene* pScene) { return new rClass(pScene); } \
	static ShadeClass* staticClass() { \
		static ShadeClass classVar(rSuper::staticClass(), #rClass, &createNew); \
		return &classVar; \
	} \
	virtual Archive& serialize(Archive& archive) override { \
		rSuper::serialize(archive); \

#define SHADE_REFLECTION_ABSTRACT_BEGIN(rClass, rSuper) \
public: \
	static ShadeClass* staticClass() { \
		static ShadeClass classVar(rSuper::staticClass(), #rClass, nullptr); \
		return &classVar; \
	} \
	virtual Archive& serialize(Archive& archive) override { \
		rSuper::serialize(archive); \

#define SHADE_REFLECTION_BASE_BEGIN(rClass) \
public: \
	static ShadeClass* staticClass() { \
		static ShadeClass classVar(nullptr, #rClass, nullptr); \
		return &classVar; \
	} \
	virtual Archive& serialize(Archive& archive) override { \

#define SHADE_REFLECTION_END \
	return archive; \
	} \
private: \

#define SHADE_ADD_PROPERTY(property) \
	archive & property;

}
