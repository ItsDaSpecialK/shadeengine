//
//  Camera.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/20/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Component.hxx"
#include "ShadeENGINE/Rendering/Camera.hxx"
#include "ShadeENGINE/Rendering/PerspectiveCamera.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** A Component which can be used to draw the Scene.
 *  One of the things required to be able to see things in a Scene is a camera which provides
 *  the perspective from which everything is rendered.
 */
class CameraComponent : public CComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CameraComponent(Scene* pScene);

	/// Default Destructor
	virtual ~CameraComponent() { delete _pCamera; }

	/** Simple setter for the Near-clip Plane
	 *  This defines what the closest an object can be, and still be rendered.
	 *  Meshes closer than this distance will dissappear.
	 *
	 *  @param  distance  A float representing the distance of the Near-clip Plane.
	 */
	inline void setNearClipping(float distance) { _pCamera->setNearClipping(distance); }

	/** Simple setter for the Far-clip Plane
	 *  This defines what the furthest distance and object can be, and still be rendered.
	 *  Meshes further away than this distance will dissappear.
	 *
	 *  @param  distance  A float representing the distance of the Far-clip Plane.
	 */
	inline void setFarClipping(float distance) { _pCamera->setFarClipping(distance); }

	/** Simple setter for the Aspect Ratio of the camera.
	 *  This is automatically adjusted when the viewport is resized.
	 *
	 *  @param  ratio  A float representing the aspectRatio (Width/Height).
	 */
	inline void setAspectRatio(float ratio) { _pCamera->setAspectRatio(ratio); }

	/** Simple setter for the Field of View of the camera.
	 *  This is the 'width' in degrees (if the Aspect Ratio changes,
	 *  the X axis will maintain the same 'width' in degrees, while the Y will change).
	 *
	 *  @param  FieldOfView  A float representing the Field of View in degrees.
	 */
	inline void setFieldOfView(float FieldOfView) { _pCamera->setFieldOfView(FieldOfView); }

//	/** Simple setter for the resolution scale. // @TODO: remove
//	 *  This is the factor by which the resolution is scaled
//	 *  @param  scale  A float representing how much the resolution is scaled (0 < scale <= 1).
//	 *  A value of 0.5 will make 1 fragment the size of 4 pixels, while 1.0 is a 1:1 scale.
//	 */
//	inline void setResolutionScale(float scale) { resolutionScale = scale; }
//
//	/** Simple getter for the resolution scale.
//	 *  @return  The Camera's resolution scaling coefficient.
//	 */
//	inline float GetResolutionScale() const { return resolutionScale; }

	/** Simple getter for the CameraComponent's Camera
	 *  @return  The Camera which this component represents.
	 */
	inline Camera* getCamera() const { return _pCamera; }

	/** Simple setter for the projection matrix
	 *  @param projection  The new projection matrix to use.
	 */
	inline void setProjection(Matrix projection) { _pCamera->setProjection(projection); }

	//  Updates the cameras position.
	virtual void update(uint16 delta) override;
protected:
private:
	/// A pointer to the component's camera.
	PerspectiveCamera* _pCamera;

	SHADE_REFLECTION_BEGIN(CameraComponent, CComponent)
	SHADE_REFLECTION_END
};

}
