//
//  PointLightComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Lighting/LightComponent.hxx"
#include "ShadeENGINE/Rendering/Lighting/PointLight.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** Class for placing PointLights into a Scene
 *  @see  PointLight
 */
class PointLightComponent : public CLightComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	PointLightComponent(Scene* pScene);

	/// Default Deconstructor.
	virtual ~PointLightComponent() { _pScene->removePointLight((PointLight*)_pLight); delete _pLight; }

	/** Simple setter for attenuation.
	 *  @param  attenuation  The values to set the light's attenuation to.
	 */
	inline void setAttenuation(Attenuation attenuation) { ((PointLight*)_pLight)->setAttenuation(attenuation); }

	/** Simple getter for attenuation.
	 *  @return  The light's attenuation values.
	 */
	inline Attenuation getAttenuation() { return ((PointLight*)_pLight)->getAttenuation(); }

	/** Simple setter for the light range.
	 *  @param  distance  The max light distance (caps the distance for calculations)
	 */
	inline void setRange(float distance) { ((PointLight*)_pLight)->setRange(distance); }

	/** Simple getter for the light range.
	 *  @return  The max light distance (caps the distance for calculations)
	 */
	inline float getRange() { return ((PointLight*)_pLight)->getRange(); }

	// Update the point light's position.
	virtual void update(uint16 delta) override;
protected:
private:

	SHADE_REFLECTION_BEGIN(PointLightComponent, CLightComponent)
	SHADE_REFLECTION_END
};

}
