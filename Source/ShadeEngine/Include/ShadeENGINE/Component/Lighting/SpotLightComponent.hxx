//
//  SpotLightComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Lighting/LightComponent.hxx"
#include "ShadeENGINE/Rendering/Lighting/SpotLight.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** Class for placing SpotLights into the scene
 *  @see  SpotLight
 */
class SpotLightComponent : public CLightComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	SpotLightComponent(Scene* pScene);

	/// Default Deconstructor.
	virtual ~SpotLightComponent() { _pScene->removeSpotLight((SpotLight*)_pLight); delete _pLight; }

	/** Simple setter for the SpotLight angle.
	 *  @param  degrees  The angle to set the spot light to (in degrees)
	 */
	inline void setAngle(float degrees) { ((SpotLight*)_pLight)->setAngle(degrees); }

	/** Simple getter for the SpotLight angle.
	 *	@return	The angle of the SpotLight (in degrees)
	 */
	inline float getAngle() { return ((SpotLight*)_pLight)->getAngle(); }

	/** Simple setter for attenuation.
	 *  @param  attenuation  The values to set the light's attenuation to.
	 */
	inline void setAttenuation(Attenuation attenuation) { ((SpotLight*)_pLight)->setAttenuation(attenuation); }

	/** Simple getter for attenuation.
	 *  @return  The light's attenuation values.
	 */
	inline Attenuation getAttenuation() { return ((SpotLight*)_pLight)->getAttenuation(); }

	/** Simple setter for the light range.
	 *  @param  The max light distance (caps the distance for calculations)
	 */
	inline void setRange(float distance) { ((SpotLight*)_pLight)->setRange(distance); }

	/** Simple getter for the light range.
	 *  @return  The max light distance (caps the distance for calculations)
	 */
	inline float getRange() { return ((SpotLight*)_pLight)->getRange(); }

	// Update the spotlight's position and direction.
	virtual void update(uint16 delta) override;
protected:
private:

	SHADE_REFLECTION_BEGIN(SpotLightComponent, CLightComponent)
	SHADE_REFLECTION_END
};

}
