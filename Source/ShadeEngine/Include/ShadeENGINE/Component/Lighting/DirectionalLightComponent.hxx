//
//  DirectionalLightComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/9/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Lighting/LightComponent.hxx"
#include "ShadeENGINE/Rendering/Lighting/DirectionalLight.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** Class for placing DirectionalLights into a Scene
 *  @see  DirectionalLight
 */
class DirectionalLightComponent : public CLightComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	DirectionalLightComponent(Scene* pScene);

	/// Default Deconstructor.
	virtual ~DirectionalLightComponent() { _pScene->removeDirectionalLight((DirectionalLight*)_pLight); delete _pLight; }

	// Update the lights direction.
	virtual void update(uint16 delta) override;
protected:
private:

	SHADE_REFLECTION_BEGIN(DirectionalLightComponent, CLightComponent)
	SHADE_REFLECTION_END
};

}
