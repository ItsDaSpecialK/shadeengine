//
//  LightComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Component.hxx"
#include "ShadeENGINE/Rendering/Lighting/BaseLight.hxx"

class Scene;

namespace ShadeEngine {

SHADE_CLASS(ABSTRACT)
/** Root Class for placing Lights into the Scene, and attaching them to Actors.
 *  @see  PointLightComponent
 *  @see  SpotLightComponent
 *  @see  DirectionalLightComponent
 *  @see  CBaseLight
 */
class CLightComponent : public CComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CLightComponent(Scene* pScene);

	/// Default Deconstructor.
	virtual ~CLightComponent() { }

	/** Simple getter for color.
	 *  @return  The color of the light
	 */
	inline Color getColor() { return _pLight->getColor(); }

	/** Simple setter for color.
	 *  @param  color  the color which the light should be.
	 */
	inline void setColor(Color color) { _pLight->setColor(color); }

	/** Simple getter for intensity.
	 *  @return  The intensity of the light
	 */
	inline float getIntensity() { return _pLight->getIntensity(); }

	/** Simple setter for intensity.
	 *  @param  intensity  A float representing what the intensity of the light should be.
	 */
	inline void setIntensity(float intensity) { _pLight->setIntensity(intensity); }

	/** Simple getter for whether the light casts a shadow.
	 *  @return  true if the light casts a shadow.
	 */
	inline bool getCastsShadow() { return _bCastsShadow; }

	/** Simple setter for whether the light casts a shadow.
	 *  @param  bCastShadow  whether or not the light should cast a shadow.
	 */
	inline void shouldCastShadow(bool bCastShadow) { _bCastsShadow = bCastShadow; _pLight->setCastsShadow(bCastShadow); }
protected:
	/// A pointer to the light.
	CBaseLight* _pLight;

	/// A pointer to the Scene which contains this component.
	Scene* _pScene;
private:
	/// Whether the light casts a shadow.
	SHADE_PROPERTY()
	bool _bCastsShadow;

	SHADE_REFLECTION_ABSTRACT_BEGIN(CLightComponent, CComponent)
	SHADE_REFLECTION_END
};

}
