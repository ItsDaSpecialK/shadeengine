//
//  Component.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/DynamicObject.hxx"

namespace ShadeEngine {

SHADE_CLASS(ABSTRACT)
/// Root Class for Components which can be added to Actors.
class CComponent : public CDynamicObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CComponent(Scene* pScene) : CDynamicObject(pScene) { _bTakesControllerRotation = false; }

	/// Default Destructor.
	virtual ~CComponent() { }

	/** Simple getter for bTakesControllerRotation.
	 *  @return  Whether this component matches the controller's rotation.
	 */
	inline bool takesControllerRotation() { return _bTakesControllerRotation; }

	/** Simple setter for bTakesControllerRotation.
	 *  @param  value  A boolean representing Whether this component should match the controller's rotation.
	 */
	inline void setTakesControllerRotation(bool value) { _bTakesControllerRotation = value; }

	// @TODO: init NEVER gets called for Components...
	/// Simple do-nothing override of init
	virtual void init() override { }
protected:
private:
	/// Whether the Component's rotation should equal the parent's Comtroller (applies only if Parent=Pawn).
	SHADE_PROPERTY()
	bool _bTakesControllerRotation;

	SHADE_REFLECTION_ABSTRACT_BEGIN(CComponent, CDynamicObject)
	SHADE_ADD_PROPERTY(_bTakesControllerRotation)
	SHADE_REFLECTION_END
};

}
