//
//  PawnMovementComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/8/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Component.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/// A Class to help move a Pawn. Defines common functions
class PawnMovementComponent : public CComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	PawnMovementComponent(Scene* pScene) : CComponent(pScene) { }

	/// Default Destructor.
	virtual ~PawnMovementComponent() { }

	// Required for class to not be abstract
	virtual void update(uint16 delta) override { }

	/** Moves the Parent forward by a specified distance.
	 *  @param  value  A float representing how far to move the Parent (negative is backward).
	 */
	void moveFwd(float value);

	/** Moves the Parent right by a specified distance.
	 *  @param  value  A float representing how far to move the Parent (negative is left).
	 */
	void moveRight(float value);

	/** Moves the Parent up by a specified distance.
	 *  @param  value  A float representing how far to move the Parent (negative is down).
	 */
	void moveUp(float value);

	/** Pitches the Parent up by a specified value.
	 *  @param  value  A float representing how many degrees to turn the Parent (negative is down).
	 */
	void addPitchInput(float value);

	/** Yaws the Parent Right by a specified value.
	 *  @param  value  A float representing how many degrees to turn the Parent (negative is left).
	 */
	void addYawInput(float value);

	/** Rolls the Parent Right by a specified value.
	 *  @param  value  A float representing how many degrees to turn the Parent (negative is left).
	 */
	void addRollInput(float value);
protected:
private:

	SHADE_REFLECTION_BEGIN(PawnMovementComponent, CComponent)
	SHADE_REFLECTION_END
};

}
