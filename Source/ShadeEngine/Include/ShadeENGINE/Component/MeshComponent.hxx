//
//  MeshComponent.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Component.hxx"
#include "ShadeENGINE/Rendering/Material.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"
#include "ShadeENGINE/Rendering/Shaders/ShadowMapShader.hxx"
#include "ShadeENGINE/Rendering/MeshInstance.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/** Root Class for a Mesh which can be Placed into a scene.
 *  Only subclasses of this can be rendered. Use this to give your Actors a Body.
 *  This is a bare bones Mesh component which doubles as a Static Mesh Component.
 */
class MeshComponent : public CComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	MeshComponent(Scene* pScene);

	/// Default Object Constructor.
	virtual ~MeshComponent() { delete _pMesh; }

	/** Simple getter for the Mesh Component's MeshInstance
	 *  @return  The MeshInstance which this component represents.
	 */
	inline MeshInstance* getMeshInstance() const { return _pMesh; }

	/** Simple setter for the Mesh which is to be rendered.
	 *  @param  mesh  The mesh to be used.
	 */
	inline void setMesh(CMesh* pMesh) { _pMesh->setMesh(pMesh); }

	/**	Simple setter for this Mesh's Material
	 *	@param  material  The material to be used when rendering this component.
	 *	@see              Material
	 */
	inline void setMaterial(Material* pMaterial) { _pMesh->setMaterial(pMaterial); }

	// Update the mesh's transform.
	virtual void update(uint16 delta) override;

protected:

	/// A pointer to the Mesh which this Component represents.
	SHADE_PROPERTY()
	MeshInstance* _pMesh;
private:

	SHADE_REFLECTION_ABSTRACT_BEGIN(MeshComponent, CComponent)
	SHADE_ADD_PROPERTY(_pMesh)
	SHADE_REFLECTION_END
};

typedef MeshComponent StaticMeshComponent;

}
