//
//  ShadeENGINE.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/25/16.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/BitFlags.hxx"

namespace ShadeEngine {

/** Method Initializes everything for the Engine.
 *  bitFlags  The bitflags letting us know which engine systems to initialize.
 */
void init(EEngineInitFlags bitFlags = EEngineInitFlags::ALL);

/** Method returns the EEngineInitFlags which were used to initialize the engine.
 *  @return  The EEngineInitFlags which were used to initialize the engine.
 */
EEngineInitFlags getInitFlags();

/// Method Closes everything for the Engine.
void quit();

}
