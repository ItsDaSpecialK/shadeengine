//
//  Material.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Texture.hxx"
#include "ShadeENGINE/Rendering/Shaders/SurfaceShader.hxx"

namespace ShadeEngine {

/// Class for a Material.
class Material {
public:
	/// Default Constructor.
	Material();

	/// Copy Constructor.
	Material(Material& copy);

	/// Default Destructor.
	virtual ~Material() { }

	/** Simple getter for the material's texture.
	 *  @return  The material's texture.
	 */
	inline Texture* getTexture() { return _pTexture; }

	/** Simple getter for the material's normal map.
	 *  @return  The material's normal map.
	 */
	inline Texture* getNormalMap() { return _pNormalMap; }

	/** Simple getter for the material's displacement map.
	 *  @return  The material's displacement map.
	 */
	inline Texture* getDisplacementMap() { return _pDispMap; }

	/** Simple getter for the material's color.
	 *  @return  The material's color
	 */
	inline Color getColor() { return _col; }

	/** Simple getter for the specular intensity.
	 *  @return  The material's specular intensity.
	 */
	inline float getSpecularIntensity() { return _specInten; }

	/** Simple getter for the specular exponent.
	 *  @return  The material's specular exponent.
	 */
	inline float getSpecularExponent() { return _specExp; }

	/** Simple getter for the displacement map's scale factor.
	 *  @return  The displacement map's scale factor.
	 */
	inline float getBumpScale() { return _bumpScale; }

	/** Simple setter for the Material's texture.
	 *  @param  texture  Pointer to the texture to be used.
	 */
	inline void setTexture(Texture* pTexture) { _pTexture = pTexture; }

	/** Simple setter for the Material's normal map.
	 *  @param  texture  Pointer to the texture to be used.
	 */
	inline void setNormalMap(Texture* pTexture) { _pNormalMap = pTexture; }

	/** Simple setter for the Material's displacement map.
	 *  @param  texture  Pointer to the texture to be used.
	 */
	inline void setDisplacementMap(Texture* pTexture) { _pDispMap = pTexture; }

	/** Simple setter for the Material's color.
	 *  @param  color  The color to be used.
	 */
	inline void setColor(Color color) { _col = color; }

	/** Simple setter for the Material's specular intensity.
	 *  @param  specularIntensity  The specular intensity to be used.
	 */
	inline void setSpecularIntensity(float specularIntensity) { _specInten = specularIntensity; }

	/** Simple setter for the Material's specular exponent.
	 *  @param  specularExponent  The specular intensity to be used.
	 */
	inline void setSpecularExponent(float specularExponent) { _specExp = specularExponent; }

	/** Simple setter for the displacement map's bump scale.
	 *  @param  scale  The bump scale to be used.
	 */
	inline void setBumpScale(float scale) { _bumpScale = scale; }

	/** Simple getter for the Material's shader.
	 *  @return  Pointer to the material's shader.
	 */
	SurfaceShader* getShader() { return _pShader; }

	/** Simple setter for the Material's shader.
	 *  @param  shader  Pointer to shader.
	 */
	void setShader(SurfaceShader* pShader) { _pShader = pShader; }

	/// Binds the shader and the materials parameters.
	void bind() const;

	/** Binds the material's parameters to the provided shader.
	 *  @param  pForwardShader  pointer to the shader to be used.
	 */
	void bind(Shader* pShader) const;

	/// Unbinds the textures and shader.
	void unbind() const;

	/// Unbinds the texture but not the shader.
	void unbindPreserveShader() const;
protected:
private:
	/// Pointer to the texture.
	Texture* _pTexture;

	/// Pointer to the normal map texture.
	Texture* _pNormalMap;

	/// Pointer to the displacement map texture.
	Texture* _pDispMap;

	/// Pointer to the shader.
	SurfaceShader* _pShader;

	/// Material's color
	Color _col;

	/// Material's specular intensity.
	float _specInten;

	/// Material's specular exponent.
	float _specExp;

	/// Bump map scaling.
	float _bumpScale;
};

}
