//
//  Mesh.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/9/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Mesh.hxx"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Rendering/Vertex.hxx"

namespace ShadeEngine {

/// Class for a static mesh.
class StaticMesh : public CMesh {
public:
	/** Constructor for static mesh.
	 *  @param  pVertices    Array of vertices.
	 *  @param  numVertices  Number of vertices.
	 *  @param  pIndices     Array of indices.
	 *  @param  numIndices   Number of indices.
	 */
	StaticMesh(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices, bool gen = true);

	/// Default Destructor.
	virtual ~StaticMesh() { }

	/// Renders the mesh.
	virtual void render() const;

private:
	/// Calculates the normals for all the vertices.
	void calcNormals(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices);

	/// Calculates the tangents for all the vertices.
	void calcTangents(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices);

	/// Number of indices.
	uint32 _size;
};

}
