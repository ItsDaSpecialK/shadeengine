//
//  Camera.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/Matrix.hxx"
#include "ShadeENGINE/Core/Math/Quaternion.hxx"

namespace ShadeEngine {

/// Represents a camera from which the scene is rendered.
class Camera {
public:
	/// Simple Constructor.
	Camera() { }

	/// Simple Destructor.
	virtual ~Camera() { }

	/** Simple getter for the projection matrix
	 *  @return  the camera's projection matrix
	 */
	inline Matrix getProjection() { return _projection; }

	/** A Simple getter for the Camera's render Matrix
	 *  @return  The Camera's render Matrix (v and p in mvp)
	 */
	Matrix getCameraMatrix() const;

	/** Simple setter for the projection matrix
	 *  @param  projection  The new projection matrix to use.
	 */
	inline void setProjection(Matrix projection) { _projection = projection; }

	/** Simple setter for the position.
	 *  @param  position  Reference to a position.
	 */
	inline void setPos(const Vector3f& position) { _pos = position; }

	/** Simple getter for the position
	 *  @return  Reference to the Camera's position in world space.
	 */
	inline const Vector3f& getPos() const { return _pos; }

	/** Simple setter for the rotation.
	 *  @param  rotation  Reference to a rotation.
	 */
	inline void setRot(const Quaternion& rotation) { _rot = rotation; }
private:
	/// The camera's projection matrix.
	Matrix _projection;

	/// The camera's position.
	Vector3f _pos;

	/// The camera's rotation.
	Quaternion _rot;
};
}
