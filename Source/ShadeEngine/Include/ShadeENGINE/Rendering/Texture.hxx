//
//  Texture.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifdef WIN32
#include "ShadeENGINE/Rendering/GLExtensions.hxx"
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "SDL2/SDL_opengl.h"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

class Texture {
public:
	/// Default Constructor which sets an invalid textureID.
	Texture() : _textureID(0) { }

	/** Constructor for a texture.
	 *  @param  width          The width of the texture.
	 *  @param  heigh          The height of the texture.
	 *  @param  textureTarget  The OpenGL type of texture.
	 *  @param  filter         The OpenGL filter.
	 *  @param  inputFormat    The image format for the data field.
	 *  @param  clamp          Whether or not to clamp the texture.
	 *  @param  pData          The image as a raw texture. (or nullptr).
	 */
	Texture(uint16 width,
	        uint16 height,
	        GLenum textureTarget = GL_TEXTURE_2D,
	        GLfloat filter = GL_LINEAR,
	        GLint inputFormat = GL_RGB,
	        GLfloat clamp = GL_REPEAT,
	        byte* pData = nullptr);

	/** Initializes an uninitialized texture
	 *  @param  width          The width of the texture.
	 *  @param  heigh          The height of the texture.
	 *  @param  textureTarget  The OpenGL type of texture.
	 *  @param  filter         The OpenGL filter.
	 *  @param  inputFormat    The image format for the data field.
	 *  @param  clamp          Whether or not to clamp the texture.
	 *  @param  pData          The image as a raw texture. (or nullptr).
	 */
	void init(uint16 width,
	          uint16 height,
	          GLenum textureTarget = GL_TEXTURE_2D,
	          GLfloat filter = GL_LINEAR,
	          GLint inputFormat = GL_RGB,
	          GLfloat clamp = GL_REPEAT,
	          byte* pData = nullptr);

	/// Default Destructor.
	virtual ~Texture();

	/** Binds the texture to a texture unit.
	 *  @param  unit  The texture unit to bind to.
	 */
	void bind(uint8 unit = 0) const;

	/** Returns a pointer to the OpenGL texture.
	 *  @return Pointer to the texture.
	 */
	inline GLuint getTexID() { return _textureID; }

protected:
	/// Pointer to the texture.
	GLuint _textureID;

	/// Type of texture.
	GLenum _texTarget;

private:

};

}
