//
//  Viewport.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/ViewportShader.hxx"
#include "ShadeENGINE/Rendering/StaticMesh.hxx"
#include "ShadeENGINE/Rendering/Camera.hxx"
#include "ShadeENGINE/Rendering/Texture.hxx"

namespace ShadeEngine {

class PlayerController;

/// Defines a screen region to render to. Assign a controller.
class Viewport {
public:
	/** Constructor for Viewport
	 *  @param  x       The x coordinate for the top left corner.
	 *  @param  y       The y coordinate for the top left corner.
	 *  @param  width   The width of the viewport.
	 *  @param  height  The height of the viewport.
	 */
	Viewport(uint16 x, uint16 y, uint16 width, uint16 height);

	/// Simple Destructor.
	virtual ~Viewport();

	/** Takes in a texture, and draws it to the screen in the viewport's region.
	 *  @param  pTex  A pointer to the texture to draw to the screen.
	 */
	void draw(Texture* pTex);

	/** Gets the Viewport's camera
	 *  @return  The viewport's camera.
	 */
	Camera* getCamera();

	/** Getter for the width.
	 *  @return  The viewport's width
	 */
	inline uint16 getWidth() { return _width; }

	/** Getter for the height.
	 *  @return  The viewport's height
	 */
	inline uint16 getHeight() { return _height; }

	/** Setter for viewport size.
	 *  @param  width   The viewport's width.
	 *  @param  height  The viewport's height.
	 */
	void setSize(uint16 width, uint16 height);

	/** Setter for the Player'sController.
	 *  @param  pPlayerController  A pointer to the player's controller.
	 */
	void setController(PlayerController* pPlayerController);

	/** Setter for the Depth. Decides what gets drawn over what.
	 *  @param  value  float to set the depth to (-1 to 1).
	 */
	inline void setDepth(float value) { _depth = value; }

	/** Simple setter for the resolution scale.
	 *  This is the factor by which the resolution is scaled.
	 *  A value of 0.5 will make 1 fragment the size of 4 pixels, while 1.0 is a 1:1 scale.
	 *
	 *  @param  scale  A float representing how much the resolution is scaled (0 < scale <= 1).
	 */
	inline void setResolutionScale(float scale) { _resolutionScale = scale; }

	/** Simple getter for the resolution scale.
	 *  @return  The Camera's resolution scaling coefficient.
	 */
	inline float getResolutionScale() const { return _resolutionScale; }
protected:
private:
	/// The Viewport's displaying controller
	PlayerController* _pController; // @TODO: Change to Controller (allow AI controlled camera).

	/// Pointer to A flat square mesh.
	StaticMesh* _pSurface;

	/// The depth compared to other things on the screen.
	float _depth;

	/// Viewport parameters
	uint16 _width, _height, _x, _y;

	/// Window parameters (Taken from window)
	uint16 _windowWidth, _windowHeight;

	/// The resolution scaling coefficient.
	float _resolutionScale;
};

}
