//
//  SkyMeshShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/23/18.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shader.hxx"

namespace ShadeEngine {

/// (Singleton Class) Default shader for drawing a sky mesh.
class SkyMeshShader : public Shader {
public:
	/** Gets the Shader instance
	 *  @return  Reference to the shader instance.
	 */
	static SkyMeshShader& getInstance() {
		static SkyMeshShader instance;     // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader and registers the uniforms.
	virtual void compileShader() override;

	/** Sets te shader's mvp
	 *  @param  mvp  The shaders mvp.
	 */
	inline void setMVP(Matrix mvp) { setUniformMatrix4f(_mvpUniform, mvp); }
protected:
private:
	/// Default Private constructor. Private to make singleton.
	SkyMeshShader();

	/// Deletes the copy constructor to make singleton..
	SkyMeshShader(SkyMeshShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(SkyMeshShader const&) = delete;

	/// A pointer to the mvp uniform.
	GLuint _mvpUniform;
};

}
