//
//  ForwardPointShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"
#include "ShadeENGINE/Rendering/Lighting/PointLight.hxx"

namespace ShadeEngine {

/// (Singleton Class) Default Forward lighting shader for Point lights.
class ForwardPointShader : public ForwardShader {
public:
	/** Gets the Shader instance
	 *  @return  Reference to the shader instance.
	 */
	static ForwardPointShader& getInstance() {
		static ForwardPointShader instance; // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader registers the uniforms.
	virtual void compileShader() override;

	/** Sets the shader's point light
	 *  @param  pPointLight  Pointer to the point light to use.
	 */
	void setPointLight(PointLight* pPointLight);
protected:
private:
	/// Default Private constructor. Private to make singleton.
	ForwardPointShader();

	/// Deletes the copy constructor to make singleton..
	ForwardPointShader(ForwardPointShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(ForwardPointShader const&) = delete;

	/// Pointer to the point light's color uniform.
	GLuint _pointBaseColorUniform;

	/// Pointer to the point light's intensity uniform.
	GLuint _pointBaseIntenUniform;

	/// Pointer to the point light's attenuation's constant uniform
	GLuint _pointAttenConstantUniform;

	/// Pointer to the point light's attenuation's linear uniform
	GLuint _pointAttenLinearUniform;

	/// Pointer to the point light's attenuation's quadratic uniform
	GLuint _pointAttenQuadraticUniform;

	/// Pointer to the point light's range uniform.
	GLuint _pointRangeUniform;

	/// Pointer to the point light's position uniform.
	GLuint _pointPositionUniform;
};

}
