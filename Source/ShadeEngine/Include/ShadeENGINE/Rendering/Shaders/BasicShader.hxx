//
//  BasicShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/16/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shader.hxx"

namespace ShadeEngine {

/// Basic shader which can draw something to the screen.
class BasicShader : public Shader {
public:
	/// Default Constructor.
	BasicShader() { }

	/// Default Destructor.
	virtual ~BasicShader() { }

	/// Compiles the shaders and gets the uniforms
	virtual void compileShader() override;

	/** Sets the shader's mvp
	 *  @param  mvp  The shaders mvp.
	 */
	inline void setMVP(Matrix mvp) { setUniformMatrix4f(_mvpUniform, mvp); }

	/** Sets the transform matrix uniform
	 *  @param  transform  what to set the transform uniform to.
	 */
	inline void setTransform(Matrix transform) { setUniformMatrix4f(_transformUniform, transform); }

	/** Sets the object's bump scale uniform.
	 *  @param  scale  The objects bump scale.
	 */
	inline void setBumpScale(float scale) { setUniformf(_bumpScaleUniform, scale); }

	/** Sets the camera position uniform.
	 *  @param  cameraPosition  The camera's position in world space.
	 */
	inline void setCameraPosition(Vector3f cameraPosition) { setUniformVector3f(_camPosUniform, cameraPosition); }

	/// Sets the shader as the current.
	virtual void bind() override;
protected:
private:
	/// A pointer to the mvp uniform.
	GLuint _mvpUniform;

	/// Pointer to the object's transform uniform.
	GLuint _transformUniform;

	/// Pointer to the object's bump scale uniform.
	GLuint _bumpScaleUniform;

	/// Pointer to the object's camera position uniform.
	GLuint _camPosUniform;

	/// Pointer to the shader's diffuse map sampler.
	GLuint _diffuseMapSampler;

	/// Pointer to the shader's displacement map sampler.
	GLuint _dispMapSampler;
};

}
