//
//  DefaultSurfaceShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/26/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/SurfaceShader.hxx"

namespace ShadeEngine {

/// (Singleton Class) The default surface shader.
class DefaultSurfaceShader : public SurfaceShader {
public:
	/** Gets the Surface Shader instance
	 *  @return  Reference to the surface shader instance.
	 */
	static DefaultSurfaceShader& getInstance() {
		static DefaultSurfaceShader instance; // Guaranteed to be destroyed.
		return instance;
	}
protected:
private:
	/// Default Private constructor. Private to make singleton.
	DefaultSurfaceShader();

	/// Deletes the copy constructor to make singleton..
	DefaultSurfaceShader(DefaultSurfaceShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(DefaultSurfaceShader const&) = delete;

	/// Make Private to prevent it from being called outside class.
	inline virtual void addVertexShader(const std::string& text) override { SurfaceShader::addVertexShader(text); }

	/// Make Private to prevent it from being called outside class.
	inline virtual void addGeometryShader(const std::string& text) override { SurfaceShader::addGeometryShader(text); }

	/// Make Private to prevent it from being called outside class.
	inline virtual void addFragmentShader(const std::string& text) override { SurfaceShader::addFragmentShader(text); }

	/// Make Private to prevent it from being called outside class.
	inline virtual void compileShader() override { SurfaceShader::compileShader(); }
};

}
