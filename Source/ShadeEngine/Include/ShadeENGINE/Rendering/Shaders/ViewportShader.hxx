//
//  ViewportShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shader.hxx"

namespace ShadeEngine {

/// (Singleton Class) Default shader for drawing viewports.
class ViewportShader : public Shader {
public:
	/** Gets the Shader instance
	 *  @return  Reference to the shader instance.
	 */
	static ViewportShader& getInstance() {
		static ViewportShader instance; // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader and registers the uniforms.
	virtual void compileShader() override;

	/** Simple setter for the Vieport's X scale.
	 *  @param  xScale  The viewport's X scale.
	 */
	inline void setXScale(float xScale) { setUniformf(_xScaleUniform, xScale); }

	/** Simple setter for the Vieport's Y scale.
	 *  @param  yScale  The viewport's Y scale.
	 */
	inline void setYScale(float yScale) { setUniformf(_yScaleUniform, yScale); }

	/** Simple setter for the Viewport's depth.
	 *  @param depth  The viewport's depth
	 */
	inline void setDepth(float depth) { setUniformf(_depthUniform, depth); }
protected:
private:
	/// Default Private constructor. Private to make singleton.
	ViewportShader();

	/// Deletes the copy constructor to make singleton..
	ViewportShader(ViewportShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(ViewportShader const&) = delete;

	/// Pointer to the viewport's X scale uniform.
	GLuint _xScaleUniform;

	/// Pointer to the viewport's Y scale uniform.
	GLuint _yScaleUniform;

	/// Pointer to the viewport's depth uniform.
	GLuint _depthUniform;
};

}
