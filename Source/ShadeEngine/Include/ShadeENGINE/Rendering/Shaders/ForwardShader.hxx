//
//  ForwardShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/BasicShader.hxx"
#include "ShadeENGINE/Util/Color.hxx"

namespace ShadeEngine {

/// Base shader for forward lighting passes.
class ForwardShader : public BasicShader {
public:
	/// Default Constructor.
	ForwardShader() { }

	/// Default Destructor.
	virtual ~ForwardShader() { }

	/// Compiles the shader and registers the Uniforms.
	virtual void compileShader() override;

	/** Sets the object's color uniform.
	 *  @param  color  the objects color.
	 */
	inline void setColor(Color color) { setUniformVector4f(_colorUniform, color); }

	/** Sets the object's specular intensity uniform.
	 *  @param  specularIntensity  the objects specular intensity coefficient.
	 */
	inline void setSpecularIntensity(float specularIntensity) { setUniformf(_specIntenUniform, specularIntensity); }

	/** Sets the object's specular exponent uniform.
	 *  @param  specularExponent  The objects specular exponent coefficient.
	 */
	inline void setSpecularExponent(float specularExponent) { setUniformf(_specExpUniform, specularExponent); }

	/** Sets the shader's lightMatrix
	 *  @param  mvp  The shaders mvp.
	 */
	inline void setLightMatrix(Matrix lightMatrix) { setUniformMatrix4f(_lightMatrixUniform, lightMatrix); }

	/// Sets the shader as the current.
	virtual void bind() override;
protected:
private:
	/// Pointer to the object's color uniform.
	GLuint _colorUniform;

	/// Pointer to the object's specular intensity uniform.
	GLuint _specIntenUniform;

	/// Pointer to the object's specular exponent uniform.
	GLuint _specExpUniform;

	/// Pointer to the shader's normal map sampler.
	GLuint _normalMapSampler;

	/// pointer to the light matrix uniform.
	GLuint _lightMatrixUniform;

	/// pointer to the shader's shadow map sampler.
	GLuint _shadowMapSampler;
};

}
