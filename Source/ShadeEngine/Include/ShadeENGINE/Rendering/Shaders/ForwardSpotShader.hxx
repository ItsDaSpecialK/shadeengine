//
//  ForwardSpotShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"
#include "ShadeENGINE/Rendering/Lighting/SpotLight.hxx"

namespace ShadeEngine {

/// (Singleton Class) Default Forward lighting shader for Spot lights.
class ForwardSpotShader : public ForwardShader {
public:
	/** Gets the Shader instance
	 *  @return Reference to the shader instance.
	 */
	static ForwardSpotShader& getInstance() {
		static ForwardSpotShader instance; // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader registers the uniforms.
	virtual void compileShader() override;

	/** Sets the shader's spot light
	 *  @param  pSpotLight  Pointer to the spot light to use.
	 */
	void setSpotLight(SpotLight* pSpotLight);

	/** Sets the shader's bias.
	 *  @param  bias  The bias to use when calculating shadows.
	 */
	void setBias(float bias);

	/** Sets the shader's shadow texel size.
	 *  @param  bias  The shadow texel size to use when filtering shadows.
	 */
	void setShadowTexelSize(float shadowTexelSize);
protected:
private:
	/// Default Private constructor. Private to make singleton.
	ForwardSpotShader();

	/// Deletes the copy constructor to make singleton..
	ForwardSpotShader(ForwardSpotShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(ForwardSpotShader const&) = delete;

	/// Pointer to the spot light's color uniform.
	GLuint _spotPointBaseColorUniform;

	/// Pointer to the spot light's intensity uniform.
	GLuint _spotPointBaseIntenUniform;

	/// Pointer to the spot light's attenuation's constant uniform
	GLuint _spotPointAttenConstantUniform;

	/// Pointer to the spot light's attenuation's linear uniform
	GLuint _spotPointAttenLinearUniform;

	/// Pointer to the spot light's attenuation's quadratic uniform
	GLuint _spotPointAttenQuadraticUniform;

	/// Pointer to the spot light's range uniform.
	GLuint _spotPointRangeUniform;

	/// Pointer to the spot light's position uniform.
	GLuint _spotPointPositionUniform;

	/// Pointer to the spot light's cuttoff uniform.
	GLuint _spotCutoffUniform;

	/// Pointer to the spot light's direction uniform.
	GLuint _spotDirectionUniform;

	/// Pointer to the directional light's bias uniform.
	GLuint _spotBiasUniform;

	/// Pointer to the directional light's shadow texel size uniform.
	GLuint _spotShadowTexelSizeUniform;
};

}
