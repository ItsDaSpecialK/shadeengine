//
//  ShadowMapShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/15/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shader.hxx"

namespace ShadeEngine {

/// (Singleton Class) The default Shadow map shader.
class ShadowMapShader : public Shader {
public:
	/** Gets the shadow map Shader instance
	 *  @return  Reference to the surface shader instance.
	 */
	static ShadowMapShader& getInstance() {
		static ShadowMapShader instance; // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader and registers the uniforms.
	virtual void compileShader() override;

	/** Sets te shader's mvp
	 *  @param  mvp  The shaders mvp.
	 */
	inline void setMVP(Matrix mvp) { setUniformMatrix4f(_mvpUniform, mvp); }
protected:
private:
	/// Default Private constructor. Private to make singleton.
	ShadowMapShader();

	/// Deletes the copy constructor to make singleton..
	ShadowMapShader(ShadowMapShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(ShadowMapShader const&) = delete;

	/// A pointer to the mvp uniform.
	GLuint _mvpUniform;
};

}
