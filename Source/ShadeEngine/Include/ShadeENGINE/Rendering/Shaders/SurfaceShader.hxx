//
//  SurfaceShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/BasicShader.hxx"
#include "ShadeENGINE/Util/Color.hxx"

namespace ShadeEngine {

/// Default shader for rendering a mesh with lighting.
class SurfaceShader : public BasicShader {
public:
	/// Default Constructor.
	SurfaceShader() { }

	/// Default Destructor.
	virtual ~SurfaceShader() { }

	/// Compiles the shader and registers the Uniforms.
	virtual void compileShader() override;

	/** Sets the Object's color uniform.
	 *  @param  color  The object's color.
	 */
	inline void setColor(Color color) { setUniformVector4f(_colorUniform, color); }

	/** The amount of ambient light.
	 *  @param  color  The ambient light color.
	 */
	inline void setAmbientLight(Color color) { setUniformVector4f(_ambientUniform, color); }
protected:
private:
	/// A pointer to the color uniform.
	GLuint _colorUniform;

	/// A pointer to the ambient color uniform.
	GLuint _ambientUniform;
};

}
