//
//  ForwardDirectionalShader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"
#include "ShadeENGINE/Rendering/Lighting/DirectionalLight.hxx"

namespace ShadeEngine {

/// (Singleton Class) Default Forward lighting shader for Directional lights.
class ForwardDirectionalShader : public ForwardShader {
public:
	/** Gets the Shader instance
	 *  @return  Reference to the shader instance.
	 */
	static ForwardDirectionalShader& getInstance() {
		static ForwardDirectionalShader instance; // Guaranteed to be destroyed.
		return instance;
	}

	/// Compiles the shader registers the uniforms.
	virtual void compileShader() override;

	/** Sets the shader's directional light.
	 *  @param  pDirectionalLight  Pointer to the directional light to use.
	 */
	void setDirectionalLight(DirectionalLight* pDirectionalLight);

	/** Sets the shader's bias.
	 *  @param  bias  The bias to use when calculating shadows.
	 */
	void setBias(float bias);

	/** Sets the shader's shadow texel size.
	 *  @param  bias  The shadow texel size to use when filtering shadows.
	 */
	void setShadowTexelSize(float shadowTexelSize);
protected:
private:
	/// Default Private constructor. Private to make singleton.
	ForwardDirectionalShader();

	/// Deletes the copy constructor to make singleton..
	ForwardDirectionalShader(ForwardDirectionalShader const&) = delete;

	/// Deletes the = operator to make singleton.
	void operator=(ForwardDirectionalShader const&) = delete;

	/// Pointer to the directional light's color uniform.
	GLuint _dirBaseColorUniform;

	/// Pointer to the directional light's intensity uniform.
	GLuint _dirBaseIntenUniform;

	/// Pointer to the directional light's direction uniform.
	GLuint _dirDirectionUniform;

	/// Pointer to the directional light's bias uniform.
	GLuint _dirBiasUniform;

	/// Pointer to the directional light's shadow texel size uniform.
	GLuint _dirShadowTexelSizeUniform;
};

}
