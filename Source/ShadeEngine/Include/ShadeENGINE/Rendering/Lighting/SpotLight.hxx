//
//  SpotLight.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Lighting/PointLight.hxx"

namespace ShadeEngine {

/// Class for a point light which shines in one direction (ex. Flashlight)
class SpotLight : public PointLight {
public:
	/// Default Constructor.
	SpotLight();

	/// Default Destructor.
	virtual ~SpotLight() { }

	/** Simple setter for the Angle of the light's cone.
	 *  @param  degrees  The angle from the forward vector.
	 */
	inline void setAngle(float degrees) { _angle = degrees; _cutoff = cosf(toRadians(_angle / 2.f)); setCastsShadow(_pShadow != nullptr); }

	/** Simple getter for the Angle of the light's cone.
	 *  @return  The angle from the light's forward vector.
	 */
	inline float getAngle() { return _angle; }

	/** Simple getter for the light's cuttoff value.
	 *  This is a value calculated from the angle. It is used internally in the lighting calculations.
	 *
	 *  @return  the cutoff value.
	 */
	inline float getCutoff() { return _cutoff; }

	/** Sets up the light to cast a shadow, if it should
	 *  @param  castsShadow  whether the light should cast a shadow.
	 */
	virtual void setCastsShadow(bool castsShadow) override;

	/** Simple getter for the rotation.
	 *  @return  Reference to the rotation.
	 */
	inline const Quaternion& getRot() const { return _rot; }

	/** Simple setter for the rotation.
	 *  @param  rotation  Reference to a rotation.
	 */
	inline void setRot(const Quaternion& rotation) { _rot = rotation; }
protected:
private:
	/// The light's cutoff.
	float _cutoff;

	/// The light's angle.
	float _angle;

	/// The light's rotation.
	Quaternion _rot;
};

}
