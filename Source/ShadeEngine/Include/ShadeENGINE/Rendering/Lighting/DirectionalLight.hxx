//
//  DirectionalLight.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Lighting/BaseLight.hxx"

namespace ShadeEngine {

/// Class for a Light which shines in one direction, and is infinitely far away.
class DirectionalLight : public CBaseLight {
public:
	/// Default Constructor.
	DirectionalLight() { setCastsShadow(true); }

	/// Default Destructor.
	virtual ~DirectionalLight() { }

	/** Sets up the light to cast a shadow, if it should
	 *  @param  castsShadow  whether the light should cast a shadow.
	 */
	virtual void setCastsShadow(bool castsShadow) override;

	/** Simple getter for the rotation.
	 *  @return  Reference to the rotation.
	 */
	inline const Quaternion& getRot() const { return _rot; }

	/** Simple setter for the rotation.
	 *  @param  rotation  Reference to a rotation.
	 */
	inline void setRot(const Quaternion& rotation) { _rot = rotation; }
protected:
private:
	/// The light's rotation.
	Quaternion _rot;
};

}
