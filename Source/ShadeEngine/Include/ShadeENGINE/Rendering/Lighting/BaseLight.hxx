//
//  BaseLight.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/StaticObject.hxx"
#include "ShadeENGINE/Util/Color.hxx"

namespace ShadeEngine {

/// A struct representing the attenuation of light.
struct Attenuation {
	float constant;
	float linear;
	float quadratic;

	/// To model realistic attenuation, use Attenuation().
	Attenuation(float constant = 0, float linear = 0, float quadratic = 1) :
			constant(constant), linear(linear), quadratic(quadratic) { }
};

/// A struct containing the info to render a shadow.
struct Shadow {
	Matrix projection;
	float bias;
};

/// The Base class for a light.
class CBaseLight {
public:
	/// Default Constructor.
	CBaseLight();

	/// Default Destructor.
	virtual ~CBaseLight();

	/** Simple getter for Color.
	 *  @return  The color of the light.
	 */
	inline Color getColor() { return _col; }

	/** Simple setter for Color.
	 *  @param  color  The color of the light.
	 */
	inline void setColor(Color color) { _col = color; }

	/** Simple getter for Intensity.
	 *  @return  The intensity of the light.
	 */
	inline float getIntensity() { return _inten; }

	/** Simple setter for Intensity.
	 *  @param  intensity  The intensity of the light.
	 */
	inline void setIntensity(float intensity) { _inten = intensity; }

	/** Simple getter for the shadow info.
	 *  @return  The light's shadow, or nullptr if it doesn't cast one.
	 */
	inline Shadow* getShadow() { return _pShadow; }

	/** Sets up the light to cast a shadow, if it should
	 *  @param  castsShadow  whether the light should cast a shadow.
	 */
	virtual void setCastsShadow(bool castsShadow) = 0;
protected:
	/// The shadow info for this light.
	Shadow* _pShadow;
private:
	/// The color of the light.
	Color _col;

	/// The intensity of the light.
	float _inten;
};

}
