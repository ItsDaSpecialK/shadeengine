//
//  PointLight.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Lighting/BaseLight.hxx"

namespace ShadeEngine {

/// Class for a Light which is in one point.
class PointLight : public CBaseLight {
public:
	/// Default Constructor.
	PointLight();

	/// Default Destructor.
	virtual ~PointLight() { }

	/** Sets the light's attenuation.
	 *  @param  attenuation  The attenuation to use (Use the default for realistic results).
	 */
	inline void setAttenuation(Attenuation attenuation) { _atten = attenuation; }

	/** Simple getter for the light's attenuation.
	 *  @return  The light's attenuation.
	 */
	inline Attenuation getAttenuation() { return _atten; }

	/** Simple setter for the light's range.
	 *  This determines the maximum distance from the light where lighting
	 *  calculations still occur. This is to avoid expensive lighting
	 *  calculations for distances where it is negligible. Too small a
	 *  distance will create a hard line, while to large a distance will
	 *  cause performance issues.
	 *
	 *  @param  distance  The light's range.
	 */
	inline void setRange(float distance) { _range = distance; }

	/** Simple getter for the light's range.
	 *  @return  The light's range.
	 */
	inline float getRange() { return _range; }

	/** Sets up the light to cast a shadow, if it should
	 *  @param  castsShadow  whether the light should cast a shadow.
	 */
	virtual void setCastsShadow(bool castsShadow) override { }

	/** Simple getter for the position.
	 *  @return  Reference to the position.
	 */
	inline const Vector3f& getPos() const { return _pos; }

	/** Simple setter for the position.
	 *  @param  position  Reference to a position.
	 */
	inline void setPos(const Vector3f& position) { _pos = position; }
protected:
private:
	/// The Light's attenuation.
	Attenuation _atten;

	/// The Light's range.
	float _range;

	/// The Light's position.
	Vector3f _pos;
};

}
