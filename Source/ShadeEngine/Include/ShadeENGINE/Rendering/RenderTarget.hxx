//
//  RenderTarget.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/26/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Texture.hxx"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Core/Window.hxx"

namespace ShadeEngine {

/// This is something which can be rendered to.
class RenderTarget {
public:
	/** Creates a Render target
	 *  @param  numTextures  The number of textures to use for rendering color
	 *  @param  width        The width of the RenderTarget
	 *  @param  height       The height of the RenderTarget
	 *  @param  filters      An array of filters to use for the textures
	 *  @param  formats      An array of formats to use for the textures
	 */
	RenderTarget(uint8 numTextures = 1,
	             uint16 width = Window::getWidth(),
	             uint16 height = Window::getHeight(),
	             float* filters = nullptr,
	             GLint* formats = nullptr);

	/// Default Deconstructor
	virtual ~RenderTarget();

	/// Sets this rendertarget as the one to use.
	void useRenderTarget();

	/** Sets this rendertarget as the one to use, but not with the full dimensions.
	 *  @param  width   How much horizontal space in the rendertarget which will be used
	 *  @param  height  How much vertical space in the rendertarget which will be used
	 */
	void useRenderTarget(uint16 width, uint16 height);

	/// Sets the Default rendertarget (the window).
	static void useDefault();

	/** Sets the Default rendertarget (the window), but doesn't use the full dimensions.
	 *  @param  width   How much horizontal space in the rendertarget which will be used
	 *  @param  height  How much vertical space in the rendertarget which will be used
	 */
	static void useDefault(uint16 width, uint16 height);

	/** Getter for the texture at the index.
	 *  @param  textureIndex  The index of the texture to be returned
	 *  @return               The texture at the specified index.
	 */
	Texture* getTexture(uint8 textureIndex) const;

protected:
private:

	/// A pointer to the frame buffer object.
	uint32 _fbo;

	/// A pointer to the depth render buffer.
	uint32 _depthRenBuf;

	/// The width of the Render Target
	uint16 _width;

	/// The height of the Render Target
	uint16 _height;

	/// vector holding the textures used by this render target
	Texture* _pTextures;

	/// The number of textures;
	uint8 _numTextures;

};

}
