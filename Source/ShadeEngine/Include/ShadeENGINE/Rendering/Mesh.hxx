//
//  Mesh.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/6/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifdef WIN32
#include "ShadeENGINE/Rendering/GLExtensions.hxx"
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "SDL2/SDL_opengl.h"

namespace ShadeEngine {

/// Abstract Base Class for all kinds of meshes.
class CMesh {
public:
	/// Default Constructor
	CMesh();

	/// Default Destructor.
	virtual ~CMesh();

	/// Renders the mesh.
	virtual void render() const = 0;
protected:
	/// Binds this Mesh's VertexArrayObject
	void bindVao() const { glBindVertexArray(_vao); }

	/// Unbinds this Mesh's VertexArrayObject
	void unbindVao() const { glBindVertexArray(0); }

	/// Binds this Mesh's VertexBufferObject
	void bindVbo() const { glBindBuffer(GL_ARRAY_BUFFER, _vbo); }

	/// Binds this Mesh's IndexBufferObject
	void bindIbo() const { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo); }

private:
	/// Pointer to the Vertex Buffer Object.
	GLuint _vbo;

	/// Pointer to the Index Buffer Object.
	GLuint _ibo;

	/// Pointer to the Vertex Array Object.
	GLuint _vao;
};

}
