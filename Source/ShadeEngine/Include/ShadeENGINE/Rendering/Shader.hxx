//
//  Shader.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifdef WIN32
#include "ShadeENGINE/Rendering/GLExtensions.hxx"
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "SDL2/SDL_opengl.h"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Core/Math/VectorBase.hxx"
#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/Matrix.hxx"
#include <string>

namespace ShadeEngine {

/// Base class for a OpenGL shader.
class Shader {
public:
	/// Default Constructor.
	Shader();

	/// Default Destructor.
	virtual ~Shader();

	/** Adds a vertex shader to the program.
	 *  @param  text  The shader as a string.
	 */
	inline virtual void addVertexShader(const std::string& text) { addProgram(text, GL_VERTEX_SHADER); }

	/** Adds a geometry shader to the program.
	 *  @param  text  The shader as a string.
	 */
	inline virtual void addGeometryShader(const std::string& text) { addProgram(text, GL_GEOMETRY_SHADER); }

	/** Adds a fragment shader to the program.
	 *  @param  text  The shader as a string.
	 */
	inline virtual void addFragmentShader(const std::string& text) { addProgram(text, GL_FRAGMENT_SHADER); }

	/// Compiles the shader program.
	virtual void compileShader();

	/// Sets the shader as the current.
	virtual void bind() { glUseProgram(_program); }
protected:
	/** Adds a shader of the specified type to the program.
	 *  @param  text  The shader as a string.
	 *  @param  type  The type of shader. (GL_VERTEX_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER).
	 */
	virtual void addProgram(const std::string& text, GLuint type);

	/** Returns a pointer to a uniform.
	 *  @param  name  The name of the uniform.
	 *  @return       Pointer to the Uniform.
	 */
	inline GLuint getUniform(const std::string& name) { return glGetUniformLocation(_program, name.c_str()); }

	/** Sets an int uniform to a value.
	 *  @param  location  Pointer to the Uniform.
	 *  @param  value     The value for the uniform.
	 */
	inline void setUniformi(GLuint location, int32 value) { glUniform1i(location, value); }

	/** Sets a float uniform to a value.
	 *  @param  location  Pointer to the Uniform.
	 *  @param  value     The value for the uniform
	 */
	inline void setUniformf(GLuint location, float value) { glUniform1f(location, value); }

	/** Sets a uniform to a vector3.
	 *  @param  location  Pointer to the Uniform.
	 *  @param  value     The value for the uniform
	 */
	inline void setUniformVector3f(GLuint location, const Vector3f& value) { glUniform3f(location, value.getX(), value.getY(), value.getZ()); }

	/** Sets a uniform to a vector4.
	 *  @param  location  Pointer to the Uniform.
	 *  @param  value     The value for the uniform
	 */
	inline void setUniformVector4f(GLuint location, const TVector<float, 4>& value) { glUniform4f(location, value[0], value[1], value[2], value[3]); }

	/** Sets a uniform to a matrix4.
	 *  @param  location  Pointer to the Uniform.
	 *  @param  value     The value for the uniform
	 */
	inline void setUniformMatrix4f(GLuint location, const Matrix& value) { glUniformMatrix4fv(location, 1, false, &(value[0][0])); }
private:
	/// Pointer to the shader program.
	GLuint _program;

	/// Pointer to the fragment shader.
	GLuint _fragShader;

	/// Pointer to the geometry shader.
	GLuint _geomShader;

	/// Pointer to the vertex shader.
	GLuint _vertShader;
};

}
