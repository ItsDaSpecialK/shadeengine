//
//  Vertex.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/9/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/Vector.hxx"

namespace ShadeEngine {

/// Simple Vertex struct.
struct Vertex {
	/// The position
	Vector3f pos;

	/// The texture coordinate.
	Vector2f texCoord;

	/// The surface normal
	Vector3f norm;

	/// The surface tangent
	Vector3f tang;

	/** Simple constructor.
	 *  @param  position      The position.
	 *  @param  textureCoord  The texture coordinate.
	 *  @param  normal        The surface normal.
	 *  @param  tangent       The surface tangent.
	 */
	Vertex(Vector3f position, Vector2f textureCoord = Vector2f(0, 0),
	       Vector3f normal = Vector3f(0, 0, 0), Vector3f tangent = Vector3f(0, 0, 0)) {
		pos = position;
		texCoord = textureCoord;
		norm = normal;
		tang = tangent;
	}
};

}
