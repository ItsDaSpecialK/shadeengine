//
//  MeshInstance.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/6/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Mesh.hxx"
#include "ShadeENGINE/Rendering/Material.hxx"
#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/Matrix.hxx"
#include "ShadeENGINE/Rendering/Shaders/ShadowMapShader.hxx"
#include "ShadeENGINE/Rendering/Shaders/SkyMeshShader.hxx"

namespace ShadeEngine {

/// Base class for a MeshInstance.
class MeshInstance {
public:
	/// Default Constructor.
	MeshInstance() { _pMesh = nullptr; _rotScaleMat.initToIdentity(); }

	/// Default Destructor.
	virtual ~MeshInstance() { }

	/** Simple setter for the Mesh which is to be rendered.
	 *  @param  mesh  The mesh to be used.
	 */
	inline void setMesh(CMesh* pMesh) { _pMesh = pMesh; }

	/**	Simple setter for this instance's Material
	 *	@param  material  The material to be used when rendering this component.
	 *	@see              Material
	 */
	inline void setMaterial(Material* pMaterial) { _pMat = pMaterial; }

	/** Simple getter for the instance's Material
	 *  @return  The material which is used to render this component.
	 *  @see     Material
	 */
	inline Material* getMaterial() const { return _pMat; }

	/** Simple getter for the position.
	 *  @return  Reference to the position.
	 */
	inline const Vector3f& getPos() const { return _pos; }

	/** Simple setter for the position.
	 *  @param  position  Reference to a position.
	 */
	inline void setPos(const Vector3f& position) { _pos = position; }

	/** Simple getter for the MeshInstance's Transform Matrix
	 *  @return  The MeshInstance's Transform Matrix.
	 */
	Matrix getTransformationMatrix() const;

	/** Simple setter for the MeshInstance's Transform.
	 *  @param  transMat  Reference to transform Matrix.
	 */
	void setTransformationMatrix(const Matrix4& transMat);

	/** Simple setter for the ambient light color to be used to draw the mesh.
	 *  @param  color  The ambient light Color
	 */
	inline void setAmbientLight(Color color) { _ambientLight = color; }

	/** Method to render the StaticMesh in the right location.
	 *  @param  cam     The Camera matrix to use when rendering the mesh.
	 *  @param  camPos  The Position of the camera
	 */
	virtual void render(const Matrix cam, const Vector3f camPos) const;

	/** Method to render the StaticMesh in the right location using the specified shader.
	 *  @param  cam      The Camera matrix to use when rendering the mesh.
	 *  @param  light    The Lights's projection matrix.
	 *  @param  pShader  A pointer to the shader to be used when rendering the mesh.
	 */
	virtual void render(const Matrix cam, const Matrix light, BasicShader* pShader) const;

	/** Method to render the StaticMesh in the right location using the specified shadowmap shader.
	 *  @param  cam	     The Camera matrix to use when rendering the mesh.
	 *  @param  pShader  A pointer to the shader to be used when rendering the mesh.
	 */
	virtual void render(const Matrix cam, ShadowMapShader* pShader) const;

	/** Method to render the StaticMesh in the right location using the specified skymesh shader.
	 *  @param  cam	     The Camera matrix to use when rendering the mesh.
	 *  @param  pShader  A pointer to the shader to be used when rendering the mesh.
	 */
	virtual void render(const Matrix cam, SkyMeshShader* pShader) const;

protected:
private:
	/// The ambientLight color (when no light is striking a surface directly)
	Color _ambientLight; // @TODO: Move AmbientLight to better loc.

	/// A pointer to the Mesh this is an instance of.
	CMesh* _pMesh;

	/// The material to use for this instance.
	Material* _pMat;

	/// The MeshInstance's position.
	Vector3f _pos;

	/// The MeshInstance's rotation and scale matrix.
	Matrix3 _rotScaleMat;
};

}
