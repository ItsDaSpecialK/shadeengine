//
//  PerspectiveCamera.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Camera.hxx"

namespace ShadeEngine {

class PerspectiveCamera : public Camera {
public:
	/** Simple Constructor that sets its values based on given numbers.
	 *  @param  aspectRatio  The value to use for the camera's aspect ratio.
	 *  @param  fov          The value to use for the camera's Field of view.
	 *  @param  zNear        The value to use for the camera's Near-clip distance.
	 *  @param  zFar         The value to use for the camera's Far-clip distance.
	 */
	PerspectiveCamera(float aspectRatio = 1.777778f,
	                  float fov = 90.f,
	                  float zNear = 0.1f,
	                  float zFar = 1000.f);

	/// Default Destructor
	virtual ~PerspectiveCamera() { }

	/** Simple setter for the Near-clip Plane
	 *  This defines what the closest an object can be, and still be rendered.
	 *  Meshes closer than this distance will dissappear.
	 *  @param  distance  A float representing the distance of the Near-clip Plane.
	 */
	inline void setNearClipping(float distance) { _zNear = distance; calcProjection(); }

	/** Simple setter for the Far-clip Plane
	 *  This defines what the furthest distance and object can be, and still be rendered.
	 *  Meshes further away than this distance will dissappear.
	 *  @param  distance  A float representing the distance of the Far-clip Plane.
	 */
	inline void setFarClipping(float distance) { _zFar = distance; calcProjection(); }

	/** Simple setter for the Aspect Ratio of the camera.
	 *  This is automatically adjusted when the viewport is resized.
	 *  @param  ratio  A float representing the aspectRatio (Width/Height).
	 */
	inline void setAspectRatio(float ratio) { _aspectRatio = ratio; calcProjection(); }

	/** Simple setter for the Field of View of the camera.
	 *  This is the 'width' in degrees (if the Aspect Ratio changes,
	 *  the X axis will maintain the same 'width' in degrees, while the Y will change).
	 *  @param  FieldOfView  A float representing the Field of View in degrees.
	 */
	inline void setFieldOfView(float FieldOfView) { _fov = FieldOfView; calcProjection(); }

protected:
private:
	/// Calculates the projection matrix based on the stored values.
	void calcProjection();

	/// The Near-clip distance.
	float _zNear;

	/// The Far-clip distance.
	float _zFar;

	/// The camera's aspect ratio.
	float _aspectRatio;

	/// The camera's Field of view.
	float _fov;
};
}
