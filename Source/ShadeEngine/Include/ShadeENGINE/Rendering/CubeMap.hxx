//
//  CubeMap.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/23/18.
//
//  Copyright (c) 2018 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Texture.hxx"

namespace ShadeEngine {

class CubeMap : public Texture {
public:
	/// Default Constructor which sets an invalid textureID.
	CubeMap() { }

	/** Constructor for a texture.
	 *  @param  width          The width of the texture.
	 *  @param  heigh          The height of the texture.
	 *  @param  textureTarget  The OpenGL type of texture.
	 *  @param  filter         The OpenGL filter.
	 *  @param  inputFormat    The image format for the data field.
	 *  @param  clamp          Whether or not to clamp the texture.
	 *  @param  pData          An array of Images. (or nullptr).
	 */
	CubeMap(uint16 width,
	        uint16 height,
	        GLfloat filter = GL_LINEAR,
	        GLint inputFormat = GL_RGB,
	        GLfloat clamp = GL_REPEAT,
	        byte** pData = nullptr);

	/** Initializes an uninitialized texture
	 *  @param  width          The width of the texture.
	 *  @param  heigh          The height of the texture.
	 *  @param  textureTarget  The OpenGL type of texture.
	 *  @param  filter         The OpenGL filter.
	 *  @param  inputFormat    The image format for the data field.
	 *  @param  clamp          Whether or not to clamp the texture.
	 *  @param  pData          An array of Images. (or nullptr).
	 */
	void init(uint16 width,
	          uint16 height,
	          GLfloat filter = GL_LINEAR,
	          GLint inputFormat = GL_RGB,
	          GLfloat clamp = GL_REPEAT,
	          byte** pData = nullptr);

	/// Default Destructor.
	virtual ~CubeMap() { }
protected:
private:

};

}
