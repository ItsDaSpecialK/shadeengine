//
//  EngineManager.hxx
//  EngineManager
//
//  Created by Konrad Kraemer on 3/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"
#include "ShadeENGINE/Engine/Game/GameMode.hxx"
#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"
#include "ShadeENGINE/Engine/AssetManager.hxx"
#include "ShadeENGINE/Engine/BitFlags.hxx"

namespace ShadeEngine {

/// The Core Engine class.
class EngineManager {
public:
	/** Creates an Engine manager.
	 *  @param  bitFlags  The systems to initialize for this engine instance.
	 */
	EngineManager(EEngineInitFlags bitFlags = EEngineInitFlags::ALL);

	/// Private destructor, so only the quit function can delete this.
	~EngineManager();

	/// Method Starts the execution of the Engine in another thread.
	void start();

	/// Runs the engine in the current thread. Contains the main engine loop.
	void run();

	/// Method Stops the execution of the Engine.
	void stop();

	/** Method Loads a Map's assets.
	 *  @param  path  A string representing the path to the map.
	 */
	void loadMapFromFile(std::string path);

	/** Method Closes a Map's assets.
	 *  @param  name  A string representing the map's name.
	 */
	void closeMap(std::string name);

	/** Method changes the map which is currently displayed.
	 *  @param  name            A string representing the map's name.
	 *  @param  pGameModeClass  The ShadeClass of the gamemode to use.
	 */
	void setActiveMap(std::string name, ShadeClass* pGameModeClass);

	/** Method for setting the map directly with a scene, bypassing the asset manager.
	*  @param  scene           A pointer to the scene to use.
	*  @param  pGameModeClass  The ShadeClass of the gamemode to use.
	*/
	void setScene(Scene* pScene, ShadeClass* pGameModeClass);

	/** Method Set's the Engine's assetManager, if it hasnt been set yet.
	 *  @param  pAssetManager  Sets the Engine's assetManager, if it doesn't have one.
	 */
	void setAssetManager(CAssetManager* pAssetManager);

	/** Method Returns the Engine's AssetManager
	 *  @return  The engine's AssetManager
	 */
	const CAssetManager& getAssetManager();

	/// Method increments the player count.
	void addPlayer() { _numPlayers++; }

	/// Method decrements the player count.
	void removePlayer() { _numPlayers--; }

	/** Simple setter for the framerate limiter.
	 *  @param  fpsLimit  The maximum allowed frames per second. 0 to disable the limiter.
	 */
	void setFpsLimit(uint16 fpsLimit) { _fpsLimit = fpsLimit; }

	/** Simple getter for the framerate limiting value.
	 *  @return  the current value set by the framerate limiter, or 0 if disabled.
	 */
	uint16 getFpsLimit() { return _fpsLimit; }

	/** Simple setter for the update rate limiter.
	 *  @param  tickTime  The number of milliseconds each update should strive for.
	 */
	void setTickTime(uint16 tickTime) { _tickTime = tickTime; }

	/** Simple getter for the update limiting value.
	 *  @return  the current value set by the update limiter.
	 */
	uint16 getTickTime() { return _tickTime; }
protected:
private:

	/** Method gets called every frame and updates everything.
	 *  @param  delta  A uint16 representing the milliseconds since the last call.
	 */
	void update(uint16 delta);

	/// Boolean value which stores whether the engine is running.
	bool _bIsRunning;

	/// represents the max frames per second.
	uint16 _fpsLimit;

	/// represents the intended duration of each update call in milliseconds.
	uint16 _tickTime;

	/// A pointer to the current scene.
	Scene* _pScene;

	/// A pointer to the current Game mode.
	CGameMode* _pGameMode;

	/// The currently used asset manager.
	CAssetManager* _pAssetMan;

	/// The bitFlags passed into the engine.
	EEngineInitFlags _bitFlags;

	/// A uint8 of the current number of local players.
	uint8 _numPlayers; // @TODO: better way of handling players (Controllers should be held by ShadeEngine)
};

}
