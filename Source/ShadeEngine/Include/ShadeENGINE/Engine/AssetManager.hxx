//
//  AssetManager.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Engine/AssetStorage/AssetCache.hxx"

namespace ShadeEngine {

class CAssetManager {
public:
	/// Simple Constructor
	CAssetManager() { }

	/// Simple destructor
	virtual ~CAssetManager() { }

	/** Method Checks whether the asset caches contain the asset.
	 *  @param  id  The id of the asset.
	 */
	virtual bool contains(uint32 id) const = 0;

	/** Method Loads a asset cache to memory.
	 *  @param  path  A string representing the path to the asset cache.
	 */
	virtual void loadMapFromFile(std::string path) = 0;

	/** Method Closes an asset cache.
	 *  @param  name  A string representing the asset cache's name.
	 */
	virtual void closeMap(std::string name) = 0;

	/** Method returns an asset cache's scene
	 *  @param  name  A string representing the asset cache's name.
	 *  @return       A asset cache's scene, or NULL if name is not a loaded cache's name.
	 */
	virtual Scene* getScene(std::string name) = 0;

	/** Getter for an EditorClass.
	 *  @param  id  The id of the asset.
	 *  @return     The EdClass or nullptr if it doesn't exist.
	 */
	EditorClass* getEdClass(uint32 id) const;

	/** Getter for a Material.
	 *  @param  id  The id of the asset.
	 *  @return     The Material or nullptr if it doesn't exist.
	 */
	Material* getMaterial(uint32 id) const;

	/** Getter for a StaticMesh.
	 *  @param  id  The id of the asset.
	 *  @return     The StaticMesh or nullptr if it doesn't exist.
	 */
	StaticMesh* getStaticMesh(uint32 id) const;

	/** Getter for a Texture.
	 *  @param  id  The id of the asset.
	 *  @return     The Texture or nullptr if it doesn't exist.
	 */
	Texture* getTexture(uint32 id) const;
protected:
private:
	/** Getter for an asset.
	 *  @param  id  The id of the asset.
	 *  @return     The asset or nullptr if it doesn't exist.
	 */
	virtual AssetContainer* getAsset(uint32 id) const = 0;
};

/// Will manage all the user content for the game engine.
class SAssetManager : public CAssetManager {
public:
	/// Simple Constructor
	SAssetManager() { }

	/// Simple destructor
	virtual ~SAssetManager();

	/** Method Checks whether the asset caches contain the asset.
	 *  @param  id  The id of the asset.
	 */
	virtual bool contains(uint32 id) const override;

	/** Method Loads a asset cache to memory.
	 *  @param  path  A string representing the path to the asset cache.
	 */
	virtual void loadMapFromFile(std::string path) override;

	/** Method Closes an asset cache.
	 *  @param  name  A string representing the asset cache's name.
	 */
	virtual void closeMap(std::string name) override;

	/** Method returns an asset cache's scene
	 *  @param  name  A string representing the asset cache's name.
	 *  @return       A asset cache's scene, or NULL if name is not a loaded cache's name.
	 */
	virtual Scene* getScene(std::string name) override;
protected:
private:
	/** Getter for an asset.
	 *  @param  id  The id of the asset.
	 *  @return     The asset or nullptr if it doesn't exist.
	 */
	virtual AssetContainer* getAsset(uint32 id) const override;

	/// A pointer to a vector holding all the assets.
	std::vector<AssetCache*> _maps;
};

}
