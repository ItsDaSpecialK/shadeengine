//
//  AssetCache.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/20/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Scene/Scene.hxx"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Engine/AssetStorage/AssetContainer.hxx"

namespace ShadeEngine {

/// Class which stores a scenario and its assets.
class AssetCache {
public:
	/** Simple Constructor for the asset cache.
	 *  @param  name        The asset cache's name
	 *  @param  assetArray  An array of assets
	 *  @param  length      The length of the asset array
	 */
	AssetCache(std::string name, AssetContainer* pAssetArray, uint32 length);

	/// Simple Destructor.
	virtual ~AssetCache();

	/** Simple getter for the scene.
	 *  @return  The asset cache's scene.
	 */
	inline Scene* getScene() { return _pScene; }

	/** Simple getter for the asset cache's name.
	 *  @return  The asset cache's name.
	 */
	inline std::string getName() { return _name; }

	/** Simple getter for the smallest id.
	 *  @return minId
	 */
	inline uint32 getMinId() { return _minId; }

	/** Simple getter for the largest id.
	 *  @return maxId
	 */
	inline uint32 getMaxId() { return _maxId; }

	/** Getter for an asset.
	 *  @param  id  The id of the asset.
	 *  @return     The asset or nullptr if it doesn't exist.
	 */
	AssetContainer* getAsset(uint32 id);

	/** Method Checks whether the vector contains the asset.
	 *  @param  id  The id of the asset.
	 */
	bool contains(uint32 id);

protected:
private:

	/// A pointer to the scene formed from the asset cache's scenario.
	Scene* _pScene;

	/// The name of the asset cache.
	std::string _name;

	/// The largest id contained within this asset cache.
	uint32 _maxId;

	/// The smallest id contained within this asset cache.
	uint32 _minId;

	// An array of the asset cache's assets
	AssetContainer* _pAssets;

	// The number of assets contained by this cache
	uint32 _numAssets;
};

}
