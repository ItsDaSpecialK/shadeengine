//
//  AssetContainer.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 2/2/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

enum class EAssetType : uint8 {
	MATERIAL = 1,
	STATIC_MESH,
	TEXTURE,
	EDCLASS,
	SCENARIO
};

/// Container for assets.
struct AssetContainer {
	void* pData = nullptr;
	uint32 id;
	EAssetType type;
	bool operator==(const AssetContainer& other) const {
		return (pData == other.pData && id == other.id
		        && type == other.type);
	}
	bool deleteData();
	virtual ~AssetContainer() { deleteData(); }
};

}
