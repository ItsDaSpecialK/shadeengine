//
//  AssetArrayTree.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Engine/AssetStorage/AssetContainer.hxx"
#include "ShadeENGINE/Engine/AssetManager.hxx"
#include <vector>
#include <string>
#include <stdio.h>

namespace ShadeEngine {

/// A tree containing pointers to assets for the editor.
class AssetArrayTree {
public:
	/// Simple Constructor. //@TODO: different constructor, which populates the tree
	AssetArrayTree() { _pRoot = new AssetArrayTreeDirectoryNode(); }

	/// Simple Destructor.
	virtual ~AssetArrayTree() { delete _pRoot; }

	/** Method which populates the tree based off of the id's contained within the file.
	 *  @param  pProjectFile  The file containing the id's of the assets.
	 *  @param  numEntries    The number of entries in the root folder.
	 */
	void populate(FILE* pProjectFile, uint32 numEntries) {
		populate(pProjectFile, numEntries, _pRoot);
	}

	/** Adds an asset to the tree and array.
	 *  @param  id    The id of the asset which is to be added.
	 *  @param  type  The type of the asset.
	 *  @param  path  The path to the asset to be added.
	 */
	void add(uint32 id, EAssetType type, std::string path);

	/** Removes data from the tree and array.
	 *  @param  path  The path to the asset to be removed.
	 *  @return       true if a node was removed.
	 */
	inline bool remove(std::string path) {
		return remove(path, _pRoot);
	}

	/** Increments the reference count of the specified asset, and loads it to
	 *  Memory if it hasn't alreay been.
	 *
	 *  @param  id        The id of the asset to load.
	 *  @param  assetMan  The asset manager to load the asset for.
	 *  @return           true if the method was successful.
	 */
	bool load(uint32 id, const CAssetManager& assetMan);

	/** Decrements the reference count of the specified asset, and unloads it from
	 *  Memory if it has no more references.
	 *  @param  id  The id of the asset to unload.
	 *  @return     true if the method was successful.
	 */
	bool unload(uint32 id);

	/** Gets the container with the specified id.
	 *  @param  id  The id of the asset to be returned.
	 *  @return     the container with the specified id, or null.
	 */
	AssetContainer* getContainer(uint32 id) const;

	/** Gets the id of the asset pointed to by the path.
	 *  @param  path  The path of the asset to be found.
	 *  @return       The id of the asset pointed to, or NULL otherwise.
	 */
	inline uint32 getIdFromPath(std::string path) {
		return getIdFromPath(path, _pRoot);
	}

protected:
private:
	struct AssetArrayTreeDirectoryNode;

	/// Root struct for asset tree nodes.
	struct AssetArrayTreeNode {
		std::string name;
		bool bIsDir;
		AssetArrayTreeNode() { }
		virtual ~AssetArrayTreeNode() { if (pNext != nullptr) delete pNext; }
		AssetArrayTreeDirectoryNode* pParent = nullptr;
		AssetArrayTreeNode* pNext = nullptr;
	};

	/// Struct for a node which contains other nodes.
	struct AssetArrayTreeDirectoryNode : AssetArrayTreeNode {
		AssetArrayTreeNode* pChildren = nullptr;
		AssetArrayTreeDirectoryNode() : AssetArrayTreeNode() { bIsDir = true; }
		virtual ~AssetArrayTreeDirectoryNode() { if (pChildren != nullptr) delete pChildren; }
	};

	/// Struct for a node containing an asset.
	struct AssetArrayTreeLeafNode : AssetArrayTreeNode {
		AssetContainer* pContainer = nullptr;
		uint8 referenceCount = 0;
		AssetArrayTreeLeafNode() : AssetArrayTreeNode() { bIsDir = false; }
		virtual ~AssetArrayTreeLeafNode() { if (pContainer != nullptr) delete pContainer; }
	};

	/** Recursive helper method which populates the tree based
	 *  off of the id's contained within the file.
	 *
	 *  @param  pProjectFile  The file containing the id's of the assets.
	 *  @param  numEntries    The number of entries in the root folder.
	 *  @param  pParent       A pointer to the parent directory used for recursion.
	 */
	void populate(FILE* pProjectFile, uint32 numEntries, AssetArrayTreeDirectoryNode* pParent);

	/** Recursive method adds a node to the tree.
	 *  @param  pNew     The new leaf node to add.
	 *  @param  path     The path to the node.
	 *  @param  pParent  A pointer to the parent node who's children should be added to.
	 */
	void add(AssetArrayTreeLeafNode* pNew, std::string path, AssetArrayTreeDirectoryNode* pParent);

	/** Recursive method removes a node from the tree.
	 *  @param  path     The path to the node.
	 *  @param  pParent  A pointer to the parent node containing a node to be removed.
	 *  @return          true if a node was removed.
	 */
	bool remove(std::string path, AssetArrayTreeDirectoryNode* pParent);

	/** Recursive method gets the id of the asset pointed to by the path.
	 *  @param  path     The path of the asset to be found.
	 *  @param  pParent  A pointer to the parent node containing a node to found.
	 *  @return          The id of the asset pointed to, or NULL otherwise.
	 */
	uint32 getIdFromPath(std::string path, AssetArrayTreeDirectoryNode* pParent);

	/** Gets the node with the specified id.
	 *  @param  id  The id of the node to be returned.
	 *  @return     the node with the specified id, or null.
	 */
	AssetArrayTreeLeafNode* getNode(uint32 id) const;

	AssetArrayTreeDirectoryNode* _pRoot = nullptr;
	std::vector<AssetArrayTreeLeafNode*> _assets;
};

}
