//
//  EdScenario.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/21/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

/// Contains a scenario for the editor.
class EdScenario {
public:
	/** Constructor which takes in 4 inputs to set up a scenario.
	 *  @param  name          the name of the scenario.
	 *  @param  assetIdArray  an array of the Ids of the assets used by this scenario.
	 *  @param  arrayLength   the length of the previous array.
	 *  @param  numActors     The number of actors in this scene.
	 */
	EdScenario(std::string name, uint32* assetIdArray, uint32 arrayLength, uint32 numActors);

	/// Simple Destructor.
	virtual ~EdScenario();

	/** Simple getter for the scene.
	 *  @return  The scenario's scene.
	 */
	inline Scene* getScene() { return _pScene; }

	/** Simple getter for the scenario's name.
	 *  @return  The scenario's name.
	 */
	inline std::string getName() { return _name; }

	/** Simple getter for the number of actors in this scenario.
	 *  @return  The number of actors in this scenario
	 */
	inline uint32 getNumActors() { return _numActors; }
protected:
private:
	friend class EdAssetManager;

	/// A pointer to the scene formed from the asset cache's scenario.
	Scene* _pScene;

	/// The name of the Scenario.
	std::string _name;

	// An array the ID's referenced by the scenario.
	uint32* _pAssetIDs;

	// The number of ID's used by this scenario.
	uint32 _numAssetIDs;

	// The number of actors in this scenario.
	uint32 _numActors;
};

}
