//
//  EdAssetManager.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 2/4/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/AssetManager.hxx"
#include "ShadeENGINE/Engine/AssetStorage/AssetArrayTree.hxx"
#include "ShadeENGINE/Engine/AssetStorage/AssetCache.hxx"
#include <vector>

namespace ShadeEngine {

/// Will manage all the user content for the Editor.
class EdAssetManager : public CAssetManager {
public:
	/** Constructor which recieves the path to a project file
	 *  @param  projectFile  The path to the project file to load.
	 */
	EdAssetManager(std::string projectFile);

	/// Simple destructor
	virtual ~EdAssetManager() { }

	/** Method Checks whether the asset caches contain the asset.
	 *  @param  id  The id of the asset.
	 */
	inline virtual bool contains(uint32 id) const override {
		return _assetTree.getContainer(id) == nullptr;
	}

	/** Method Loads a asset cache to memory.
	 *  @param  path  A string representing the path to the asset cache.
	 */
	virtual void loadMapFromFile(std::string path) override;

	/** Method Closes an asset cache.
	 *  @param  name  A string representing the asset cache's name.
	 */
	virtual void closeMap(std::string name) override;

	/** Method returns an asset cache's scene
	 *  @param  name  A string representing the asset cache's name.
	 *  @return       A asset cache's scene, or NULL if name is not a loaded cache's name.
	 */
	virtual Scene* getScene(std::string name) override;
protected:
private:
	friend AssetArrayTree;

	/** Getter for an asset.
	 *  @param  id  The id of the asset.
	 *  @return     The asset or nullptr if it doesn't exist.
	 */
	inline virtual AssetContainer* getAsset(uint32 id) const override {
		return _assetTree.getContainer(id);
	}

	/** Method loads a raw asset into memory
	 *  @param  path  The path where the asset is located
	 *  @param  type  The type of the asset
	 *  @return       A void pointer to the loaded asset, or null if unsucessfull.
	 */
	void* loadAssetFromPath(std::string path, EAssetType type);

	/** Method Loads a scenario to memory.
	 *  @param  path  A string representing the path to the scenario
	 *  @return       The loaded scenario.
	 */
	void* loadMapFromPath(std::string path);

	/// A tree containing all the assets
	AssetArrayTree _assetTree;

	/// The root directory for all the assets
	std::string _assetRoot;
};

}
