//
//  Input.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include <SDL2/SDL.h>

namespace ShadeEngine {

enum class EMouseAxis : bool { X, Y };

/** Passes Raw input to Controllers
 *  @see  CController
 */
class InputManager {
public:
	/// Simple Initializer for the Input Manager.
	static void createInputManager();

	/** Checks for updates on inputs.
	 *  @param  delta  The time in milliseconds since the last call.
	 */
	static void update(uint16 delta);

	/** Simple setter for Mouse Capture.
	 *  @param  shouldCapture  A boolean representing whether the mouse should be captured.
	 */
	static void setMouseCapture(bool shouldCapture) { bCaptureMouse = shouldCapture; }

	/** Simple getter for whether a key is currently depressed.
	 *  @param  index  the Scancode for the key you want to check.
	 *  @return        A boolean representing whether the key is depressed.
	 */
	static inline bool getKeyboardInput(uint16 index) { return bKeyboard[index]; }

	/** Simple getter for whether a mouse button is currently depressed.
	 *  @param  index  the Scancode for the mouse button you want to check.
	 *  @return        A boolean representing whether the mouse button is depressed.
	 */
	static inline bool getMouseButtonInput(uint16 index) { return bMouse[index]; }

	/** Simple getter for the mouse movement.
	 *  @param  axis  Which mouse axis to poll against (X or Y).
	 *  @return       A float representing how far the mouse moved in the specified axis.
	 */
	static inline float getMouseAxisInput(EMouseAxis axis) {
		if (axis == EMouseAxis::X)
			return mouseX;
		return mouseY;
	}

	/** Simple setter for the mouse center.
	 *  @param  X  A uint16 representing what the center X coordinate is.
	 *  @param  Y  A uint16 representing what the center Y coordinate is.
	 */
	static void setMouseCenter(uint16 X, uint16 Y);
private:
	/// An variable which stores the events to be processed by the input manager
	static SDL_Event e;

	///Should the mouse be captured
	static bool bCaptureMouse;

	/// Maximum number of Key Scancodes
	static const uint16 NUM_KEYS = 512;

	/// Maximum number of Mouse Scancodes
	static const uint16 NUM_MOUSEBUTTONS = 256;

	/// Boolean Array of Keys (depressed: yes or no)
	static bool bKeyboard[NUM_KEYS];

	/// Boolean Array of Mouse Button (depressed: yes or no)
	static bool bMouse[NUM_MOUSEBUTTONS];

	/// The current mouse X coordinate
	static int16 mouseX;

	/// The current mouse Y coordinate
	static int16 mouseY;

	/// The current central mouse X coordinate
	static uint16 mouseCenterX;

	/// The current central mouse Y coordinate
	static uint16 mouseCenterY;
};

}
