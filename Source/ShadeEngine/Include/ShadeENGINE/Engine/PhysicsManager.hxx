//
//  PhysicsManager.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

namespace ShadeEngine {

/// Will be in charge of handling physics interactions for the scene.
class PhysicsManager {
public:

protected:
private:

};

}
