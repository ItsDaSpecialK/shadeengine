//
//  Scene.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Scene/SceneGraph.hxx"
#include "ShadeENGINE/Engine/RenderManager.hxx"
#include "ShadeENGINE/Engine/Game/SpawnPoint.hxx"
#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"

namespace ShadeEngine {

class CSGManager;
class Actor;
class CComponent;
class CStaticObject;

/// Encompasses everything present in a world.
class Scene {
public:
	/// Simple Constructor.
	Scene() { }

	/// Simple Destructor.
	virtual ~Scene();

	/** Spawns an Object and returns a pointer to it.
	 *  @param  pClass    The ShadeClass you want to spawn.
	 *  @param  position  The position you want to spawn the Object at.
	 *  @param  rotation  The rotation you want to spawn the Object with.
	 *  @param  scale     The scale you want to spawn the Object with.
	 *  @return           The newly created Object
	 */
	template<typename T>
	T* spawn(ShadeClass* pClass,
	         const Vector3f& position = Vector3f(0, 0, 0),
	         const Quaternion& rotation = Quaternion(),
	         const Vector3f& scale = Vector3f(1, 1, 1)) {
		return (T*)createObject(pClass, position, rotation, scale);
	}

	/** Destroys an actor in the scene.
	 *  @param  pActor  A pointer to the actor you want deleted.
	 */
	void destroy(Actor* pActor);

	/** Attaches an actor to another. Transformations on the parent will affect the child.
	 *  @param  pActor   The child.
	 *  @param  pParent  The parent.
	 */
	void attach(Actor* pActor, Actor* pParent);

	/** Sets an actor's parent to be the scene.
	 *  @param  pActor  The actor you want to detach.
	 */
	void detach(Actor* pActor);

	/** Updates everything in the scene.
	 *  @param  delta  the time since the last calling in milliseconds.
	 */
	void update(uint16 delta) { _sceneGraph.update(delta); }

	/// Renders the Scene.
	inline void render() { _renderMan.render(); }

	/** Adds a Mesh to the Scene.
	 *  @param  pMeshComp  The Mesh component to be added.
	 */
	inline void addMesh(MeshInstance* pMeshComp) { _renderMan.addMesh(pMeshComp); }

	/** Sets the Ambient light color
	 *  @param  color  The color you want the ambient light to be.
	 */
	inline void setAmbientLight(Color color) { _renderMan.setAmbientLight(color); }

	/** Adds a DirectionalLight to the Scene.
	 *  @param  pLight  The Directional light you want to add.
	 */
	inline void addDirectionalLight(DirectionalLight* pLight) { _renderMan.addDirectionalLight(pLight); }

	/** Adds a PointLight to the Scene.
	 *  @param  pLight  The Point light you want to add.
	 */
	inline void addPointLight(PointLight* pLight) { _renderMan.addPointLight(pLight); }

	/** Adds a SpotLight to the Scene.
	 *  @param  pLight  The Spot light you want to add.
	 */
	inline void addSpotLight(SpotLight* pLight) { _renderMan.addSpotLight(pLight); }

	/** Removes a DirectionalLight from the Scene.
	 *  @param  pLight  The Directional light to remove.
	 */
	inline void removeDirectionalLight(DirectionalLight* pLight) { _renderMan.removeDirectionalLight(pLight); }

	/** Removes a PointLight from the Scene.
	 *  @param  pLight  The Point light to remove.
	 */
	inline void removePointLight(PointLight* pLight) { _renderMan.removePointLight(pLight); }

	/** Removes a SpotLight from the Scene.
	 *  @param  pLight  The Spot light to remove.
	 */
	inline void removeSpotLight(SpotLight* pLight) { _renderMan.removeSpotLight(pLight); }

	/** Adds a Viewport to the Scene.
	 *  @param  pViewport  The Viewport to add to the scene.
	 */
	inline void addViewport(Viewport* pViewport) { _renderMan.addViewport(pViewport); }

	/** Removes a Viewport from the Scene.
	 *  @param  pViewport  The Viewport to remove.
	 */
	inline void removeViewport(Viewport* pViewport) { _renderMan.removeViewport(pViewport); }

	/** Adds a SpawnPoint to the Scene.
	 *  @param  pSpawnPoint  The Spawn point to add.
	 */
	inline void addSpawnPoint(SpawnPoint* pSpawnPoint) { _spawnPoints.add(pSpawnPoint); }

	/** Removes a SpawnPoint from the Scene.
	 *  @param  pSpawnPoint  The Spawn point to remove.
	 */
	inline void removeSpawnPoint(SpawnPoint* pSpawnPoint) { _spawnPoints.remove(pSpawnPoint); }

	/** Getter for the list of SpawnPoints
	 *  @return  a smart buffer of the spawn points.
	 */
	inline TSmartBuffer<SpawnPoint*>& getSpawnPointList() { return _spawnPoints; }

	/** Gets the CSG Manager, or creates one if it doesn't yet exist.
	 *  @return  The Scene's CSG Manager.
	 */
	CSGManager* getCSGManager();

	/** Sets the Scene's SkyMesh.
	 *  @param  pMeshComp  The Mesh component to be use.
	 */
	inline void setSkyMesh(MeshInstance* pMeshComp) { _renderMan.setSkyMesh(pMeshComp); }
protected:
private:
	/// The scene's SceneGraph
	SceneGraph _sceneGraph;

	/// The scene's RenderManager
	RenderManager _renderMan;

	/// A pointer to the CSG Manager
	CSGManager* _pCSGMan = nullptr;

	/// The scene's Spawn points.
	TSmartBuffer<SpawnPoint*> _spawnPoints;

	/** Creates a new Object and adds it to the scene.
	 *  @param  pClass    The Class of the object to create.
	 *  @param  position  The position you want to create it at.
	 *  @param  rotation  The rotation you want to create it with.
	 *  @param  scale     The scale you want to create it with.
	 *  @return           The newly created Object.
	 */
	CObject* createObject(ShadeClass* pClass, const Vector3f& position, const Quaternion& rotation, const Vector3f& scale);
};

}
