//
//  SceneGraph.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"
#include "ShadeENGINE/Engine/Object/Object.hxx"

namespace ShadeEngine {

/// Keeps track of everything in the scene.
class SceneGraph : public TSmartBuffer<CObject*> {
public:
	/// Simple Constructor.
	SceneGraph();

	/// Simple Destructor.
	virtual ~SceneGraph();

	/// Updates everything in the scene.
	void update(uint16 delta);
protected:
private:

};

}
