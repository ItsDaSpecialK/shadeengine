//
//  BitFlags.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/26/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

/// Bitflags which determine which engine systems get initialized.
enum class EEngineInitFlags : uint32 {
	NONE = 0x0,
	ASSET_MANAGER = 1 << 0,
	ALL = 0xFFFFFFFF
};

EEngineInitFlags operator|(EEngineInitFlags a, EEngineInitFlags b);
EEngineInitFlags operator&(EEngineInitFlags a, EEngineInitFlags b);
EEngineInitFlags operator~(EEngineInitFlags a);

#define SHADE_NONE ShadeEngine::EEngineInitFlags::NONE
#define SHADE_ASSET_MANAGER ShadeEngine::EEngineInitFlags::ASSET_MANAGER
#define SHADE_ALL ShadeEngine::EEngineInitFlags::ALL

}
