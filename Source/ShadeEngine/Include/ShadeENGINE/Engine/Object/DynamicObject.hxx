//
//  DynamicObject.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/25/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/Object.hxx"

namespace ShadeEngine {

SHADE_CLASS(ABSTRACT)
/// Root class for an Object which updates dynamically.
class CDynamicObject : public CObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CDynamicObject(Scene* pScene) : CObject(pScene) { }

	/// Default Destructor.
	virtual ~CDynamicObject() { }
protected:
private:

	SHADE_REFLECTION_ABSTRACT_BEGIN(CDynamicObject, CObject)
	SHADE_REFLECTION_END
};

}
