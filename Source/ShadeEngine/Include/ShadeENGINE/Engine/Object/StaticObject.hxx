//
//  StaticObject.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/Object.hxx"

namespace ShadeEngine {

SHADE_CLASS(ABSTRACT)
/// Default Object which doesn't change.
class CStaticObject : public CObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CStaticObject(Scene* pScene) : CObject(pScene) { }

	/// Default Destructor.
	virtual ~CStaticObject() { }

	/// Does nothing, because static objects should not update.
	virtual void update(uint16 delta) override final { }
protected:
private:

	SHADE_REFLECTION_ABSTRACT_BEGIN(CStaticObject, CObject)
	SHADE_REFLECTION_END
};

}
