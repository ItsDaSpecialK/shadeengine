//
//  Object.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Reflection/Class.hxx"
#include "ShadeENGINE/Core/Serialization/Serializable.hxx"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Engine/Object/Transform.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/// Root class for anything which can be added to the scene.
class CObject : public Serializable {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CObject(Scene* pScene) { }

	/// Default Deconstructor.
	virtual ~CObject() { }

	/// Initializes the object.
	virtual void init() = 0;

	/** Updates the object
	 *  @param  delta  The time since the last calling in Milliseconds
	 */
	virtual void update(uint16 delta) = 0;

	/** Simple constant getter for transform.
	 *  @return  A constant reference to the Object's transform.
	 */
	inline const Transform& getTransform() const { return _transform; }

	/** Simple getter for transform.
	 *  @return  A reference to the Object's transform.
	 */
	inline Transform& getTransform() { return _transform; }
protected:
private:
	/// The Object's transform.
	SHADE_PROPERTY()
	Transform _transform;

	SHADE_REFLECTION_BASE_BEGIN(CObject)
	SHADE_ADD_PROPERTY(_transform)
	SHADE_REFLECTION_END
};

}
