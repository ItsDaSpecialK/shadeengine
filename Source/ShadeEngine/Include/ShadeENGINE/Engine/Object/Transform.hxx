//
//  Transform.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/Quaternion.hxx"
#include "ShadeENGINE/Core/Math/Matrix.hxx"

namespace ShadeEngine {

/// A class representing the 3D transformation of any object.
class Transform {
public:
	/** Simple Constructor.
	 *  @param  position  A Vector representing the position.
	 *  @param  rotation  A Quaternion representing the rotation.
	 *  @param  size      A Vector representing the scale.
	 */
	Transform(const Vector3f& position = Vector3f(0, 0, 0),
	          const Quaternion& rotation = Quaternion(),
	          const Vector3f& size = Vector3f(1, 1, 1));

	/** Simple getter for the position (relative to parent).
	 *  @return  Reference to the position (relative to parent).
	 */
	inline const Vector3f& getPos() const { return _pos; }

	/** Simple getter for the rotation (relative to parent).
	 *  @return  Reference to the rotation (relative to parent).
	 */
	inline const Quaternion& getRot() const { return _rot; }

	/** Simple getter for the scale (relative to parent).
	 *  @return  Reference to the scale (relative to parent).
	 */
	inline const Vector3f& getScale() const { return _scale; }

	/** Simple setter for the position (relative to parent).
	 *  @param  position  Reference to a position (relative to parent).
	 */
	inline void setPos(const Vector3f& position) { _pos = position; }

	/** Simple setter for the rotation (relative to parent).
	 *  @param  rotation  Reference to a rotation (relative to parent).
	 */
	inline void setRot(const Quaternion& rotation) { _rot = rotation; }

	/** Simple setter for the scale (relative to parent).
	 *  @param  size  Reference to a scale (relative to parent).
	 */
	inline void setScale(const Vector3f& size) { _scale = size; }

	/** Simple setter for the parent.
	 *  @param  pTransform  Pointer to another transform to set as parent.
	 */
	inline void setParent(Transform* pTransform) { _pParent = pTransform; }

	/** Simple getter for the parent.
	 *  @return  Pointer to the transform's parent.
	 */
	inline Transform* getParent() { return _pParent; }

	/** Rotates the transform by the specified amount (relative to parent).
	 *  @param  rotation  A Quaternion specifying how much to rotate by (relative to parent).
	 */
	void rotate(const Quaternion& rotation);

	/** Rotates the transform by the specified amount (relative to parent).
	 *  @param  axis   A Vector representing the axis of rotation (relative to parent).
	 *  @param  angle  A float representing the angle of rotation in degrees.
	 */
	inline void rotate(const Vector3f axis, const float angle) { rotate(Quaternion(axis, angle)); }

	// @NOTE: Next two currently only used in Camera.

	/** Getter for the final position, factoring in the parent's transform.
	 *  @return  Vector representing the final position.
	 */
	inline Vector3f getTransformedPos() const { return Vector3f(getParentMatrix().transform(_pos)); }

	/** Getter for the final rotation, factoring in the parent's rotation.
	 *  @return  Quaternion representing the final rotation.
	 */
	Quaternion getTransformedRot() const;

	/** Getter for the parent's transformation matrix.
	 *  @return  Matrix4f representing the parent's transform.
	 */
	Matrix getParentMatrix() const;

	/** Getter for final transformation matrix.
	 *  @return  Matrix4f representing the transform in world space.
	 */
	Matrix getTransformationMatrix() const;

private:
	/// The transform's position.
	Vector3f _pos;

	/// The transform's rotation.
	Quaternion _rot;

	/// The transform's scale.
	Vector3f _scale;

	/// The transform's parent.
	Transform* _pParent;
};

}
