//
//  CSG.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/BSP/BSPTreeNode.hxx"

namespace ShadeEngine {

/// Static Class which contains the core CSG methods
class CSG {
public:
	/** Returns a new BSP tree created by adding the two together.
	 *  @param  tree1  The first tree (WILL BE DELETED).
	 *  @param  tree2  The second tree (WILL BE DELETED).
	 *  @return        A new BSP tree created by adding the two together.
	 */
	static BSPTreeNode* add(BSPTreeNode* pTree1, BSPTreeNode* pTree2);

	/** Returns a new BSP tree created by subtracting the second tree from the first.
	 *  @param  tree1  The first tree (WILL BE DELETED).
	 *  @param  tree2  The tree which is subtracted from the first (WILL BE DELETED).
	 *  @return        A new BSP tree created by subtracting the second tree from the first.
	 */
	static BSPTreeNode* subtract(BSPTreeNode* pTree1, BSPTreeNode* pTree2);

	/** Returns a new BSP tree of the intersection of the two trees.
	 *  @param  tree1  The first tree (WILL BE DELETED).
	 *  @param  tree2  The second tree (WILL BE DELETED).
	 *  @return        A new BSP tree created by the intersection of the two trees.
	 */
	static BSPTreeNode* intersect(BSPTreeNode* pTree1, BSPTreeNode* pTree2);
};

}
