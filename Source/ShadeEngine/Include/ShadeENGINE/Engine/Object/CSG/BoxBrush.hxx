//
//  BoxBrush.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/CSG/Brush.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/// Represents a box shaped brush.
class BoxBrush : public Brush {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	BoxBrush(Scene* pScene);

	/// Default Destructor.
	virtual ~BoxBrush() { };

	/** Sets the size of the Box
	 *  @param  size  A Vector representing the Width, Depth, and Height of the Box.
	 */
	inline void setSize(Vector3f size) { getTransform().setScale(size / 2.f); }

	/** Simple getter for the Box's Size
	 *  @return  A Vector representing the Width, Depth, and Height of the Box.
	 */
	inline Vector3f getSize() { return (getTransform().getScale() / 2.f); }

	/** Simple setter for the Box's Height
	 *  @param  height  A float for the height to give the box.
	 */
	void setHeight(float height);

	/** Simple setter for the Box's Width
	 *  @param  width  A float for the width to give the box.
	 */
	void setWidth(float width);

	/** Simple setter for the Box's Depth
	 *  @param  depth  A float for the depth to give the box.
	 */
	void setDepth(float depth);

protected:
private:

	SHADE_REFLECTION_BEGIN(BoxBrush, Brush)
	SHADE_REFLECTION_END
};

}
