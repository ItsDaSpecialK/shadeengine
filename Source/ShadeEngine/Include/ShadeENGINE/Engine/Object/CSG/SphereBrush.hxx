//
//  SphereBrush.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/08/18.
//
//  Copyright (c) 2018 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/CSG/Brush.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/// Represents a sphere shaped brush.
class SphereBrush : public Brush {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	SphereBrush(Scene* pScene);

	/// Default Destructor.
	virtual ~SphereBrush() { };

	/** Sets the radius of the Sphere.
	 *  @param  radius  A float representing the radius.
	 */
	inline void setRadius(float radius) { getTransform().setScale(Vector3f(radius, radius, radius)); }

	/** Simple getter for the sphere's radius.
	 *  @return  A float representing the radius.
	 */
	inline Vector3f getRadius() { return (getTransform().getScale().getX() / 2.f); }

	/** Setter for the tesselation of the sphere.
	 *  Note that this value is exponential.
	 *
	 *  @param  tessellation  The value to use for tessellation.
	 */
	void setTessellation(uint8 tessellation = 2) { init(tessellation); }

protected:
private:

	/** Initializes the brush
	*  @param  tessellation  The amount of tesseallation to use when initializing the brush.
	*/
	void init(uint8 tessellation = 3);

	/** Recursively splits a polygon
	 *  @param  poly          The polygon to split.
	 *  @param  tessellation  The amount of tessellation to use when initializing the brush.
	 */
	void splitTriangle(Polygon poly, uint8 tessellation);

	SHADE_REFLECTION_BEGIN(SphereBrush, Brush)
	SHADE_REFLECTION_END
};

}
