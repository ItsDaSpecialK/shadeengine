//
//  Brush.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Engine/BSP/Polygon.hxx"
#include "ShadeENGINE/Engine/Object/StaticObject.hxx"
#include "ShadeENGINE/Rendering/Material.hxx"

namespace ShadeEngine {

enum class ECSGOperation : uint32 {
	ADDITION = 0,
	SUBTRACTION = 1,
	INTERSECTION = 2
};

SHADE_CLASS()
/// Base Class for Brushes to be used in CSG
class Brush : public CStaticObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	Brush(Scene* pScene) : CStaticObject(pScene) { _operation = ECSGOperation::ADDITION; }

	/// Default Destructor.
	virtual ~Brush() { }

	/** Simple getter for the Brush's polygons
	 *  @return  The brush's polygons.
	 */
	inline const std::vector<Polygon>& getPolys() const { return _polygons; }

	/// Simple do-nothing override of init
	virtual void init() override { }

	/** Simple getter for the operation to apply with this brush.
	 *  @return  the operation to apply with this brush.
	 */
	inline ECSGOperation getOperation() const { return _operation; }

	/** Simple setter for the operation to apply with this brush.
	 *  @param  operation  The operation to apply with this brush.
	 */
	inline void setOperation(ECSGOperation operation) { _operation = operation; }

	/** Simple setter for this brush's material
	 *  @param  pMat  The material to use for this brush.
	 */
	inline void setMaterial(Material* pMat) { _pMat = pMat; }

	/** Simple getter for this brush's material
	*  @return  The material to this brush uses.
	*/
	inline Material* getMaterial() { return _pMat; }
protected:
	/// The Brush's Mesh Component.
//	SHADE_PROPERTY()
	std::vector<Polygon> _polygons;
private:
	ECSGOperation _operation;

	/// This brush's material
	Material* _pMat;

	SHADE_REFLECTION_BEGIN(Brush, CStaticObject)
	SHADE_REFLECTION_END
};

}
