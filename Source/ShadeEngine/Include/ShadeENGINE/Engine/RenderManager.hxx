//
//  RenderManager.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"
#include "ShadeENGINE/Rendering/Camera.hxx"
#include "ShadeENGINE/Rendering/MeshInstance.hxx"
#include "ShadeENGINE/Rendering/Lighting/DirectionalLight.hxx"
#include "ShadeENGINE/Rendering/Lighting/PointLight.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardDirectionalShader.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardPointShader.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardSpotShader.hxx"
#include "ShadeENGINE/Rendering/Shaders/ShadowMapShader.hxx"
#include "ShadeENGINE/Rendering/Viewport.hxx"
#include "ShadeENGINE/Rendering/RenderTarget.hxx"

namespace ShadeEngine {

/// Handles the rendering for the scene.
class RenderManager {
public:
	/// Default Constructor.
	RenderManager();

	/// Default Destructor.
	virtual ~RenderManager();

	/// Renders all the meshes.
	void render();

	/** Adds a mesh to the list.
	 *  @param  pMesh  A pointer to the mesh to be added.
	 */
	void addMesh(MeshInstance* pMesh) { _meshes.add(pMesh); }

	/** Removes a mesh from the list.
	 *  @param  pMesh  A pointer to the mesh to be removed.
	 */
	void removeMesh(MeshInstance* pMesh) { _meshes.remove(pMesh); }

	/** Sets the RenderManager's Ambient Light.
	 *  @param  color  The color for the ambient light.
	 */
	inline void setAmbientLight(Color color) { _ambientLight = color; }

	/** Adds a Directional light to the list.
	 *  @param  pLight  A pointer to the directional light to be added.
	 */
	inline void addDirectionalLight(DirectionalLight* pLight) { _dirLights.add(pLight); }

	/** Removes a Directional light from the list.
	 *  @param  pLight  A pointer to the directional light to be removed.
	 */
	inline void removeDirectionalLight(DirectionalLight* pLight) { _dirLights.remove(pLight); }

	/** Adds a Point light to the list.
	 *  @param  pLight  A pointer to the point light to be added.
	 */
	inline void addPointLight(PointLight* pLight) { _pointLights.add(pLight); }

	/** Removes a Point light from the list.
	 *  @param  pLight  A pointer to the point light to be removed.
	 */
	inline void removePointLight(PointLight* pLight) { _pointLights.remove(pLight); }

	/** Adds a Spot light to the list.
	 *  @param  pLight  A pointer to the spot light to be added.
	 */
	inline void addSpotLight(SpotLight* pLight) { _spotLights.add(pLight); }

	/** Removes a Spot light to the list.
	 *  @param  pLight  A pointer to the spot light to be removed.
	 */
	inline void removeSpotLight(SpotLight* pLight) { _spotLights.remove(pLight); }

	/** Adds a Viewport to the list.
	 *  @param  pViewport  A pointer to the viewport to be added.
	 */
	inline void addViewport(Viewport* pViewport) { _viewports.add(pViewport); }

	/** Removes a Viewport from the list.
	 *  @param  pViewport  A pointer to the viewport to be removed.
	 */
	inline void removeViewport(Viewport* pViewport) { _viewports.remove(pViewport); }

	/** Sets the skymesh
	 *  @param  pMesh  A pointer to the mesh to use.
	 */
	void setSkyMesh(MeshInstance* pMesh) { _pSkyMesh = pMesh; _meshes.remove(pMesh); }
protected:
private:
	/// A linked list of the meshes.
	TSmartBuffer<MeshInstance*> _meshes; // @TODO: Kd Tree

	/// A linked list of the directional lights.
	TSmartBuffer<DirectionalLight*> _dirLights;

	/// A linked list of the point lights.
	TSmartBuffer<PointLight*> _pointLights;

	/// A linked list of the spot lights.
	TSmartBuffer<SpotLight*> _spotLights;

	/// The ambient light color.
	Color _ambientLight;

	/// A linked list of the viewports.
	TSmartBuffer<Viewport*> _viewports;

	/// A boolean value whether to render unlit.
	bool _bUnlit;

	/// A boolean value whether to render in wireframe mode.
	bool _bWireframe;

	/// The render manager's render target
	RenderTarget _renderTarget;

	/// The render target for shadow maps.
	RenderTarget* _shadowTarget;

	/// A pointer to the depth render buffer.
	GLuint _depthRenBuf;

	/// An alternate camera to use when rendering shadow maps.
	Camera* _pShadowCam;

	/// The mesh to be used as a skymesh.
	MeshInstance* _pSkyMesh;

	/** Renders the meshes to the Frame buffer for a viewport's camera's perspective
	 *  @param  pViewport  The viewport to render for.
	 */
	void renderMeshesToFBO(Viewport* pViewport);

	/** Renders the scene from a camera's perspective using the foward rendering technique.
	 *  The current version can only be used for opaque objects.
	 *
	 *  @param  pCam  the camera from whose perspective to render from.
	 */
	void forwardRender(Camera* pCam);

	/** Renders the skymesh.
	 *  @param  pCam  the camera from whose perspective to render from.
	 */
	void renderSkymesh(Camera* pCam);
};

}
