//
//  CSGManager.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/21/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Engine/Object/CSG/Brush.hxx"
#include "ShadeENGINE/Rendering/MeshInstance.hxx"
#include "ShadeENGINE/Rendering/StaticMesh.hxx"

namespace ShadeEngine {

class RenderManager;

/// Class which manages the current state of CSG in the scene.
class CSGManager {
public:
	/** Simple constructor for the CSGManager which takes in a RenderManager
	 *  which will render all the CSG Geometry in the scene
	 *
	 *  @param  pRenMan  A pointer to the RenderManager to use when creating CSG Geometry
	 */
	CSGManager(RenderManager* pRenMan) { _pRenMan = pRenMan; }

	/// Default Deconstructor
	virtual ~CSGManager() { deleteMeshes(); }

	/** Adds a brush to the list, and rebuilds the model using the new brushes.
	 *  @param  pBrush  A Pointer to the brush to be added.
	 */
	inline void addBrush(Brush* pBrush) { _brushes.push_back(pBrush); }

	/** Removes a brush from the list, and rebuilds the model using the new brushes.
	 *  @param  pBrush  A Pointer to the brush to be removed.
	 */
	void removeBrush(Brush* pBrush);

	/// Rebuilds the mesh using the contained brushes.
	void rebuildMesh();
protected:
private:
	/** Method which generates a vector of polygons from a brush
	 *  @param  pBrush  The brush to generate polys from.
	 *  @param  matId   The material id to use for polys generated from this brush
	 */
	std::vector<Polygon> generatePolysFromBrush(Brush* pBrush, uint16 matId) const;

	/** Generates meshes from a list of polygons.
	*  @param  polygons   The list of polygons to generate the meshes from.
	*  @param  numMeshes  The number of meshes to return
	*  @return           An array of generated mesh pointers.
	*/
	static StaticMesh** generateMeshesFromPolys(std::vector<Polygon>& polygons, uint16 numMeshes);

	/// Deletes all the Meshes in _meshes;
	void deleteMeshes();

	/// A pointer to the RenderManager in which to show the CSG Geometry
	RenderManager* _pRenMan;

	/// The vector containing the CSG Geometry meshes
	std::vector<StaticMesh*> _meshes;

	/// The vector containing the mesh instances
	std::vector<MeshInstance*> _meshInstances;

	/// The vector containing all of the brushes.
	std::vector<Brush*> _brushes;
};

}
