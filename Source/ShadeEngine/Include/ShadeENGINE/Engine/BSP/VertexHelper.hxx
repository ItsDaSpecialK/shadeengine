//
//  VertexHelpers.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Rendering/Vertex.hxx"

namespace ShadeEngine {

/// Contains vertex methods needed for BSP splicing
class VertexHelper {
public:
	/** Flips a vertex's direction.
	 *  @param  vertex  The vertex to flip.
	 */
	static void flipVertex(Vertex& vertex);

	/** Interpolates between two Vertices.
	 *  @param  start  The starting vertex.
	 *  @param  end    The ending vertex.
	 *  @param  t      The distribution (0 is 100% start, 1 is 100% end)
	 *  @return        The interpolated vertex.
	 */
	static Vertex interpolate(const Vertex& start, const Vertex& end, float t);
};

}
