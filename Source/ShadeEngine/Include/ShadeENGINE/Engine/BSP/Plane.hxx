//
//  Plane.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/Vector.hxx"
#include <vector>

namespace ShadeEngine {

class Polygon;

/// Represents a plane in 3D Space
class Plane {
public:
	/// Default Constructor.
	Plane() { }

	/** Simple Copy Constructor.
	 *  @param  other  The other plane to copy.
	 */
	Plane(const Plane& other);

	/** Constructor which creates a plane from 3 points.
	 *  @param  a  The first point.
	 *  @param  b  The second point.
	 *  @param  c  The third point.
	 */
	Plane(Vector3f a, Vector3f b, Vector3f c);

	/// Empty Destructor.
	virtual ~Plane() { }

	/** Checks to see whether the normal is valid (length isn't 0).
	 *  @return  true if the normal is valid.
	 */
	bool hasValidNormal() const;

	/// Flips the direction the plane is facing.
	void flip();

	/**	Splits the polygon into multiple polygons and puts it into a list.
	 *  @param  polygon        The polygon which will be split into smaller ones if it is spanning the plane.
	 *  @param  coplanarFront  The list where coplanar polygons get placed, if they are facing the same direction.
	 *  @param  coplanarBack   The list where coplanar polygons get placed, if they are facing the opposite direction.
	 *  @param  front          The list where polygons in front of the plane get placed.
	 *  @param  back           The list where polygons behind the plane get placed.
	 */
	void splitPolygon(Polygon& polygon,
	                  std::vector<Polygon>& coplanarFront,
	                  std::vector<Polygon>& coplanarBack,
	                  std::vector<Polygon>& front,
	                  std::vector<Polygon>& back) const;
protected:
private:
	/// The normal direction of the plane
	Vector3f _normal;

	/// The planes distance offset from the origin (in the normal direction).
	float _offset;
};

}
