//
//  Polygon.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Rendering/Vertex.hxx"
#include "ShadeENGINE/Engine/BSP/Plane.hxx"

namespace ShadeEngine {

/// Represents a polygon for BSP.
class Polygon {
public:
	/** Creates a polygon from a list of vertices.
	 *  @param  vertexList  The vertices for the polygon.
	 *  @param  matId       The material id for this polygon
	 */
	Polygon(const std::vector<Vertex>& vertexList, uint16 matId = 0);

	/** Simple Copy constructor
	 *  @param  other  The other polygon to copy.
	 */
	Polygon(const Polygon& other);

	/// Empty Deconstructor.
	virtual ~Polygon() { }

	/// Flips the polygon's direction. Reverses the vertex winding and flips the plane.
	void flip();

	/// Simple getter for the vertices
	inline const std::vector<Vertex>& getVertices() const { return _vertices; }

	/// Simple getter for the plane.
	inline const Plane& getPlane() const { return _plane; }

	/// The polygon's material id.
	uint16 _matId;

protected:
private:
	/// A list of the polygon's vertices.
	std::vector<Vertex> _vertices;

	/// The polygon's BSP plane.
	Plane _plane;
};

}
