//
//  BSPTreeNode.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <vector>
#include "ShadeENGINE/Engine/BSP/Polygon.hxx"

namespace ShadeEngine {

class BSPTreeNode {
public:
	/// Default constructor.
	BSPTreeNode();

	/** Constructs the tree with a list of polygons.
	 *  @param  polygons  The list of polygons to populate the tree with.
	 */
	BSPTreeNode(const std::vector<Polygon>& polygons);

	/// Default Deconstructor.
	virtual ~BSPTreeNode();

	/** Creates a duplicate of this tree.
	 *  @return  An exact copy of this tree.
	 */
	BSPTreeNode* clone() const;

	/** Deletes the polygons from this tree which are within the other tree.
	 *  @param  other  The other tree to check against.
	 */
	void clipTo(BSPTreeNode* other);

	/// Inverts the tree.
	void invert();

	/** Populates the tree with a list of polygons.
	 *  @param  polygons  The list of polygons to populate the tree with.
	 */
	void build(const std::vector<Polygon>& polygons);

	/** Creates a new list without the polygons from the list which are within the tree.
	 *  @param  polygons  The polygons to clip against this tree.
	 *  @return           A list of polygons clipped against this tree.
	 */
	std::vector<Polygon> clipPolygons(std::vector<Polygon>& polygons);

	/** Makes a list of all the polygons in the tree.
	 *  @return  A list of the polygons in this tree.
	 */
	std::vector<Polygon> allPolygons();

protected:
private:
	/// A list of the polygons which are coplanar with the node.
	std::vector<Polygon> _polygons;

	/// A child tree which is in front of this plane.
	BSPTreeNode* _pFront;

	/// A child tree which is behind this plane.
	BSPTreeNode* _pBack;

	/// The plane which this node clips against.
	Plane _plane;
};

}
