//
//  SpawnPoint.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/StaticObject.hxx"

namespace ShadeEngine {

SHADE_CLASS()
/// Represents a Spawn point
class SpawnPoint : public CStaticObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	SpawnPoint(Scene* pScene) : CStaticObject(pScene) { }

	/// Default destructor.
	virtual ~SpawnPoint() { }

	/// Simple do-nothing override of init
	virtual void init() override { }
protected:
private:

	SHADE_REFLECTION_BEGIN(SpawnPoint, CStaticObject)
	SHADE_REFLECTION_END
};

}
