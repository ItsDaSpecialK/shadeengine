//
//  Controller.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"
#include "ShadeENGINE/Engine/Object/DynamicObject.hxx"

namespace ShadeEngine {

class Pawn;

SHADE_CLASS(ABSTRACT)
/// Class Controlls a Pawn. Sends Input to Input Component.
class CController : public CDynamicObject {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	CController(Scene* pScene) : CDynamicObject(pScene) { _pPawn = nullptr; }

	/// Simple Deconstructor.
	virtual ~CController() { }

	/** Start controlling a Pawn, if not already
	 *  @param  The pawn to start controlling
	 */
	virtual void possess(Pawn* pTarget);

	/// Stop controlling the current pawn.
	void unPossess();

	/** Make the controller pitch up by the specified amount.
	 *  @param  value  How much to pitch up by.
	 */
	void addPitchInput(float value);

	/** Make the controller yaw right by the specified amount.
	 *  @param  value  How much to yaw right by.
	 */
	void addYawInput(float value);

	/** Make the controller roll right by the specified amount.
	 *  @param  value  How much to roll right by.
	 */
	void addRollInput(float value);

	/// Simple do-nothing override of init
	virtual void init() override { }
protected:
	/** Simple getter for the possessed pawn.
	 *  @return  The controlled possessed.
	 */
	inline Pawn* getPawn() { return _pPawn; }
private:
	/// Pointer to the possessed pawn.
	Pawn* _pPawn;

	SHADE_REFLECTION_ABSTRACT_BEGIN(CController, CDynamicObject)
	SHADE_REFLECTION_END
};

}
