//
//  GameMode.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Object/Object.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"
#include "ShadeENGINE/Actor/Pawn.hxx"
#include "ShadeENGINE/Engine/Game/PlayerController.hxx"
#include "ShadeENGINE/Engine/Game/SpawnPoint.hxx"
#include "ShadeENGINE/Core/DataStruct/SmartBuffer.hxx"

namespace ShadeEngine {

/// Struct to pass as parameter to GameMode constructor.
struct GameModeConstructionStruct {
	uint8 numPlayers;
};

SHADE_CLASS(ABSTRACT)
/// Root class for Game Modes.
class CGameMode : public Serializable {
	SHADE_GENERATED_BODY
public:

	/// Default Object Constructor.
	CGameMode(Scene* pScene);

	/// Default Destructor.
	virtual ~CGameMode() { for (PlayerController* pController : _controllers) delete pController; }

	/** Picks a spawn point from the scene.
	 *  @return  The spawn point to spawn the pawn at.
	 */
	virtual SpawnPoint* chooseSpawnPoint() = 0;

	/** Spawns a Pawn at a spawn point
	 *  @return  A newly created pawn at a spawn point.
	 */
	Pawn* spawnPlayer();

	/// Spawns Initial pawns with controllers.
	void initialSpawn();

	/// Updates the controllers
	virtual void update(uint16 delta); // @TODO: why is this here.

	/** Adds a controller to the Game mode.
	 *  @return  The newly created controller.
	 */
	virtual PlayerController* addPlayer();

	void addPlayers(uint8 numPlayers) { for (uint8 i = 0; i < numPlayers; ++i) addPlayer(); }

	/** Returns a controller
	 *  @param  playerNum  The index of the player controller to get.
	 */
	PlayerController* getController(uint8 playerNum);

	/** Returns the pointer list of Spawn Points.
	 *  @return  A pointer to a smart buffer of spawn points.
	 */
	TSmartBuffer<SpawnPoint*>& getSpawnPointList() { return _pScene->getSpawnPointList(); }
protected:
	/// A pointer to the default player class.
	SHADE_PROPERTY()
	TSubclass<Pawn> _playerClass;
private:
	/// A smart buffer of controllers.
	TSmartBuffer<PlayerController*> _controllers; // @TODO: Store player controllers elsewhere (Shade Engine). Add other controllers (AI and Networked Players)

	/// A pointer to the scene.
	Scene* _pScene;

	SHADE_REFLECTION_BASE_BEGIN(CGameMode)
	SHADE_ADD_PROPERTY(_playerClass)
	SHADE_REFLECTION_END
};

}
