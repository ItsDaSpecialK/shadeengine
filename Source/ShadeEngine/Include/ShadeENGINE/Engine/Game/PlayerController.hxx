//
//  PlayerController.hxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Engine/Game/Controller.hxx"
#include "ShadeENGINE/Engine/InputManager.hxx"
#include "ShadeENGINE/Actor/Pawn.hxx"

namespace ShadeEngine {

/// Enum for Input Type
enum class EEventType : uint8 {
	KEYBOARD,
	MOUSE_BUTTON,
	MOUSE_AXIS
};

/// Struct for an Action (boolean)
struct Action {
	/// The action's name
	std::string name;

	/// List of the "Keycodes" for the input options.
	TSmartBuffer<uint16> events;

	/// List of the types of the inputs.
	TSmartBuffer<EEventType> eventTypes;

	/// The current value of the action.
	bool value;

	/// The last value of the action.
	bool valueOld;
};

/// Struct for an Axis (float)
struct Axis {
	/// The Axis' name
	std::string name;

	/// List of the "Keycodes" for the input options.
	TSmartBuffer<uint16> events;

	/// List of the types of the inputs.
	TSmartBuffer<EEventType> eventTypes;

	/// The value of the action.
	float value;

	/// The value multipliers for each of the inputs.
	TSmartBuffer<float> multipliers;
};

SHADE_CLASS()
/// Class for a controller operated by a local player.
class PlayerController : public CController {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	PlayerController(Scene* pScene);

	/// Simple Destructor.
	virtual ~PlayerController();

	/** Sets the input map for the controller.
	 *  @param  actionArray  Array of the actions.
	 *  @param  numActions   Total number of actions.
	 *  @param  axisArray    Array of the axes.
	 *  @param  numAxes      Total number of axes.
	 */
	void setMapping(Action* pActionArray[], uint8 numActions, Axis* pAxisArray[], uint8 numAxes);

	/// Gets a pointer to the possessed pawn's camera.
	CameraComponent* getCameraComponent();

	/** Updates the Actions and Axes, and lets the pawn process the inputs.
	 *  @param  delta  The number of milliseconds since the last call.
	 */
	virtual void update(uint16 delta) override;

	/** Start controlling a Pawn, if not already
	 *  @param  pTarget  The pawn to start controlling
	 */
	virtual void possess(Pawn* pTarget) override;

	/** Simple setter for the aspect ratio.
	 *  @param  ratio  What to set the aspect ratio to.
	 */
	void setAspectRatio(float ratio); // why is this here?
protected:
private:
	/// The number of actions.
	uint8 _actionsLength;

	/// An array of Action pointers.
	Action** _pActions;

	/// The number of axes.
	uint8 _axesLength;

	/// An array of Axis pointers.
	Axis** _pAxes;

	/// The aspect ratio.
	float _aspectRatio;

	/// Updates the Actions.
	inline void updateActions();

	/** Updates the Axes.
	 *  @param  delta  The number of milliseconds since the last call.
	 */
	inline void updateAxes(uint16 delta);

	SHADE_REFLECTION_BEGIN(PlayerController, CController)
	SHADE_REFLECTION_END
};

}
