#version 330 core

in vec2 fragTexCoord;

uniform sampler2D sampler;
uniform vec4 color;

out vec4 fragColor;

void main() {
	vec4 texColor = texture(sampler, fragTexCoord.xy);

	fragColor = texColor * vec4(1 - color.w, 1 - color.w, 1 - color.w, 1 - color.w) + color * vec4(color.w, color.w, color.w, 1);
}
