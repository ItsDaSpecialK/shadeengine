#version 330 core

in vec2 fragTexCoord;
in vec3 fragWorldPos;
in mat3 tbnMatrix;

uniform sampler2D diffuse;
uniform sampler2D dispMap;
uniform vec4 color;
uniform vec3 camPos;
uniform vec4 ambientLight;
uniform float bumpScale;

out vec4 fragColor;

void main() {
	vec3 directionToEye = normalize(camPos - fragWorldPos);
	vec2 texCoord = fragTexCoord + ((directionToEye * tbnMatrix).xy * (texture(dispMap, fragTexCoord).r - 0.5) * bumpScale);
	
	vec4 baseColor = texture(diffuse, texCoord) * vec4(1 - color.w, 1 - color.w, 1 - color.w, 1 - color.w) + color * vec4(color.w, color.w, color.w, 1);
	fragColor = baseColor * vec4(ambientLight.rgb, 1);
}

/*TONEMAPPING:
 float A = 0.15;
 float B = 0.50;
 float C = 0.10;
 float D = 0.20;
 float E = 0.02;
 float F = 0.30;
 float W = 11.2;
 
 vec3 Uncharted2Tonemap(vec3 x)
 {
 return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
 }
 
 void main() {
 vec3 texColor = texture(sampler, vec2(fragTexCoord.x * xScale, fragTexCoord.y * yScale));
 texColor *= 16; // Hardcoded Exposure Adjustment
 
 float ExposureBias = 2.0f;
 vec3 curr = Uncharted2Tonemap(ExposureBias*texColor);
 
 vec3 whiteScale = 1.0f/Uncharted2Tonemap(W);
 vec3 color = curr*whiteScale;
 
 vec3 retColor = pow(color,vec3(1/2.2));
 return vec4(retColor,1);
 }
 
 */
