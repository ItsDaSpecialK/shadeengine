#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;

out vec2 fragTexCoord;

uniform mat4 transform;

void main() {
	fragTexCoord = texCoord;
	gl_Position = transform * vec4(position, 1.0);
}
