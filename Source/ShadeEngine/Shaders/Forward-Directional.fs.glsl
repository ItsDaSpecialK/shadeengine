#version 330 core

in vec2 fragTexCoord;
in vec3 fragWorldPos;
in vec3 shadowMapCoords;
in mat3 tbnMatrix;

out vec4 fragColor;

struct BaseLight {
	vec4 color;
	float intensity;
};

struct DirectionalLight {
	BaseLight base;
	vec3 direction;
};

uniform DirectionalLight directionalLight;
uniform sampler2D diffuse;
uniform sampler2D normalMap;
uniform sampler2D dispMap;
uniform sampler2D shadowMap;
uniform vec4 color;
uniform vec3 camPos;
uniform float specularIntensity;
uniform float specularExponent;
uniform float bumpScale;
uniform float bias;
uniform float shadowTexelSize;

vec4 calcLight(BaseLight base, vec3 direction, vec3 normal) {
	float diffuseFactor = dot(normal, -direction);

	vec4 diffuseColor = vec4(0,0,0,0);
	vec4 specularColor = vec4(0,0,0,0);

	if (diffuseFactor > 0) {
		diffuseColor = base.color * base.intensity * diffuseFactor;

		vec3 directionToEye = normalize(camPos - fragWorldPos);
		vec3 reflectDirection = normalize(reflect(direction, normal));

		float specularFactor = dot(directionToEye, reflectDirection);
		specularFactor = pow(specularFactor, specularExponent);

		if(specularFactor > 0) {
			specularColor = vec4(base.color.xyz, 1.f) * specularIntensity * specularFactor;
		}
	}
	return diffuseColor + specularColor;
}

float calcShadow(sampler2D shadow, vec3 originalShadowMapCoords) {
	return step(originalShadowMapCoords.z - bias, texture(shadow, originalShadowMapCoords.xy).r);
}

float calcShadowLinear(sampler2D shadow, vec3 originalShadowMapCoords) {
	vec2 pixelPos = originalShadowMapCoords.xy / shadowTexelSize - vec2(0.5f);
	vec2 fracPart = fract(pixelPos);
	vec2 startTexel = (pixelPos - fracPart) * shadowTexelSize;
	
	float botLeft = calcShadow(shadow, vec3(startTexel, originalShadowMapCoords.z));
	float botRight = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, 0.f), originalShadowMapCoords.z));
	float topLeft = calcShadow(shadow, vec3(startTexel + vec2(0.f, shadowTexelSize), originalShadowMapCoords.z));
	float topRight = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, shadowTexelSize), originalShadowMapCoords.z));
	
	float leftLerp = mix(botLeft, topLeft, fracPart.y);
	float rightLerp = mix(botRight, topRight, fracPart.y);
	
	return mix(leftLerp, rightLerp, fracPart.x);
}

float calcShadowPCF(sampler2D shadow, vec3 originalShadowMapCoords) {
	const float SAMPLE_ITERATIONS = 2.f;
	const float SAMPLE_ENDPOINT = (SAMPLE_ITERATIONS-1.f)/2.f;
	
	float result = 0.f;
	for (float y = -SAMPLE_ENDPOINT; y <= SAMPLE_ENDPOINT; y += 1.f) {
		for (float x = -SAMPLE_ENDPOINT; x <= SAMPLE_ENDPOINT; x += 1.f) {
			vec3 coordsOffset = vec3(vec2(x,y) * shadowTexelSize, 0.f);
			result += calcShadow(shadowMap, originalShadowMapCoords + coordsOffset);
		}
	}
	
	const float NUM_SAMPLES = SAMPLE_ITERATIONS * SAMPLE_ITERATIONS;
	return result / NUM_SAMPLES;
}

float calcShadowPCFLinear(sampler2D shadow, vec3 originalShadowMapCoords) {
	// Automatically includes Linear sampling
	vec2 pixelPos = originalShadowMapCoords.xy / shadowTexelSize - vec2(0.5f);
	vec2 fracPart = fract(pixelPos);
	vec2 startTexel = (pixelPos - fracPart) * shadowTexelSize;
	
	float t00 = calcShadow(shadow, vec3(startTexel + vec2(-shadowTexelSize, -shadowTexelSize), originalShadowMapCoords.z));
	float t30 = calcShadow(shadow, vec3(startTexel + vec2(2 * shadowTexelSize, -shadowTexelSize), originalShadowMapCoords.z));
	float t03 = calcShadow(shadow, vec3(startTexel + vec2(-shadowTexelSize, 2 * shadowTexelSize), originalShadowMapCoords.z));
	float t33 = calcShadow(shadow, vec3(startTexel + vec2(2 * shadowTexelSize, 2 * shadowTexelSize), originalShadowMapCoords.z));
	
	// If corners are same, bail early
	if (t00 == t30 && t03 == t33 && t00 == t03) {
		return t00;
	}
	
	float t10 = calcShadow(shadow, vec3(startTexel + vec2(0.f, -shadowTexelSize), originalShadowMapCoords.z));
	float t20 = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, -shadowTexelSize), originalShadowMapCoords.z));
	
	float t01 = calcShadow(shadow, vec3(startTexel + vec2(-shadowTexelSize, 0.f), originalShadowMapCoords.z));
	float t11 = calcShadow(shadow, vec3(startTexel, originalShadowMapCoords.z));
	float t21 = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, 0.f), originalShadowMapCoords.z));
	float t31 = calcShadow(shadow, vec3(startTexel + vec2(2 * shadowTexelSize, 0.f), originalShadowMapCoords.z));
	
	float t02 = calcShadow(shadow, vec3(startTexel + vec2(-shadowTexelSize, shadowTexelSize), originalShadowMapCoords.z));
	float t12 = calcShadow(shadow, vec3(startTexel + vec2(0.f, shadowTexelSize), originalShadowMapCoords.z));
	float t22 = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, shadowTexelSize), originalShadowMapCoords.z));
	float t32 = calcShadow(shadow, vec3(startTexel + vec2(2 * shadowTexelSize, shadowTexelSize), originalShadowMapCoords.z));
	
	float t13 = calcShadow(shadow, vec3(startTexel + vec2(0.f, 2 * shadowTexelSize), originalShadowMapCoords.z));
	float t23 = calcShadow(shadow, vec3(startTexel + vec2(shadowTexelSize, 2 * shadowTexelSize), originalShadowMapCoords.z));
	
	float ll = mix(t00, t03, fracPart.y);
	float lm = mix(t10, t13, fracPart.y);
	float rm = mix(t20, t23, fracPart.y);
	float rr = mix(t30, t33, fracPart.y);
	
	float tm = mix(t01, t31, fracPart.x);
	float bm = mix(t02, t32, fracPart.x);
	float corners = mix(ll, rr, fracPart.x);
	
	float result = corners + lm + rm + tm + bm + t11 + t12 + t21 + t22;
	return result / 9.f;
}

void main() {
	vec3 directionToEye = normalize(camPos - fragWorldPos);
	vec2 texCoord = fragTexCoord + ((directionToEye * tbnMatrix).xy * (texture(dispMap, fragTexCoord).r - 0.5) * bumpScale);

	vec4 texColor = texture(diffuse, texCoord);
	vec4 baseColor = texColor * vec4(1 - color.a, 1 - color.a, 1 - color.a, 1 - color.a) + color * vec4(color.a, color.a, color.a, 1);
	vec3 normal = normalize(tbnMatrix * (255.f/128.f * texture(normalMap, texCoord).xyz - 1));

	vec4 lightingAmount = calcLight(directionalLight.base, directionalLight.direction, normal)
	* calcShadowPCFLinear(shadowMap, shadowMapCoords);
	fragColor = baseColor * lightingAmount;
}
