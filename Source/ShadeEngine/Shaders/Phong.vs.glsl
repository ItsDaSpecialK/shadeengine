#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec2 fragTexCoord;
out vec3 fragNormal;
out vec3 fragWorldPos;

uniform mat4 transform;
uniform mat4 transformProjected;

void main() {
	fragTexCoord = texCoord;
	fragNormal = (transform * vec4(normal, 0.0)).xyz;
	fragWorldPos = (transform * vec4(position, 1.0)).xyz;
	gl_Position = transformProjected * vec4(position, 1.0);
}
