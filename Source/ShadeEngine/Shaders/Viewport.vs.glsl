#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;

out vec2 fragTexCoord;

uniform float depth;

void main() {
	fragTexCoord = texCoord;
	gl_Position = vec4(position.xy, depth, 1.0);
}