#version 330 core

layout (location = 0) in vec3 position;
out vec3 fragTexCoord;
uniform mat4 MVP;

void main() {
	//Must convert from Z up to Y up, and from RHS to LHS (Inside shape is LHS)
	fragTexCoord = position.xzy;
	gl_Position = (MVP * vec4(position, 1.0)).xyww;
}
