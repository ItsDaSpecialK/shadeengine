#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;

out vec2 fragTexCoord;
out vec3 fragWorldPos;
out vec3 shadowMapCoords;
out mat3 tbnMatrix;

uniform mat4 transform;
uniform mat4 MVP;
uniform mat4 lightMatrix;

void main() {
	fragTexCoord = texCoord;
	fragWorldPos = (transform * vec4(position, 1.0)).xyz;
	
	vec3 norm = normalize((transform * vec4(normal, 0.0)).xyz);
	vec3 tang = normalize((transform * vec4(tangent, 0.0)).xyz);
	tang = normalize(tang - dot(tang, norm) * norm);
	vec3 bitang = cross(norm, tang);
	tbnMatrix = mat3(tang, bitang, norm);
	
	vec4 shadowMapScreenCoords = lightMatrix * vec4(position, 1.0);
	shadowMapCoords = shadowMapScreenCoords.xyz / shadowMapScreenCoords.w;
	gl_Position = MVP * vec4(position, 1.0);
}
