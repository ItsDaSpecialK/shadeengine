#version 330 core

in vec3 fragTexCoord;
uniform samplerCube skyMesh;
out vec4 fragColor;

void main() {
	fragColor = texture(skyMesh, fragTexCoord);
}
