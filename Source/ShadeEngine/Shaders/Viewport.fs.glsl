#version 330 core

in vec2 fragTexCoord;

out vec4 fragColor;

uniform sampler2D sampler;
uniform float xScale;
uniform float yScale;

void main() {
	vec4 texColor = texture(sampler, vec2(fragTexCoord.x * xScale, fragTexCoord.y * yScale));
	fragColor = vec4(pow(texColor.rgb, vec3(1/2.2)), texColor.a);
}