#version 330 core

const int MAX_POINT_LIGHTS = 4;
const int MAX_SPOT_LIGHTS = 4;

in vec2 fragTexCoord;
in vec3 fragNormal;
in vec3 fragWorldPos;

struct BaseLight {
	vec4 color;
	float intensity;
};

struct DirectionalLight {
	BaseLight base;
	vec3 direction;
};

struct Attenuation { //physically accurate: ?
	float constant;  // 0
	float linear;    // 0
	float quadratic; // 1
};

struct PointLight {
	BaseLight base;
	Attenuation atten;
	vec3 position;
	float range;
};

struct SpotLight {
	PointLight pointLight;
	vec3 direction;
	float cutoff;
};

uniform DirectionalLight directionalLight;
uniform sampler2D sampler;
uniform vec4 color;
uniform vec4 ambientLight;
uniform vec3 camPos;
uniform float specularIntensity;
uniform float specularExponent;
uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_SPOT_LIGHTS];

out vec4 fragColor;


vec4 calcLight(BaseLight base, vec3 direction, vec3 normal) {
	float diffuseFactor = dot(normal, -direction);

	vec4 diffuseColor = vec4(0,0,0,0);
	vec4 specularColor = vec4(0,0,0,0);

	if (diffuseFactor > 0) {
		diffuseColor = base.color * base.intensity * diffuseFactor;

		vec3 directionToEye = normalize(camPos - fragWorldPos);
		vec3 reflectDirection = normalize(reflect(direction, normal));

		float specularFactor = dot(directionToEye, reflectDirection);
		specularFactor = pow(specularFactor, specularExponent);

		if(specularFactor > 0) {
			specularColor = vec4(base.color.xyz, 1.0) * specularIntensity * specularFactor;
		}
	}
	return diffuseColor + specularColor;
}

vec4 calcPointLight(PointLight pointLight, vec3 normal) {
	vec3 lightDirection = fragWorldPos - pointLight.position;
	float distanceToPoint = length(lightDirection);

	if (distanceToPoint > pointLight.range)
		return vec4(0,0,0,0);

	lightDirection = normalize(lightDirection);

	vec4 color = calcLight(pointLight.base, lightDirection, normal);

	float attenuation = pointLight.atten.constant
	                  + pointLight.atten.linear * distanceToPoint
	                  + pointLight.atten.quadratic * distanceToPoint * distanceToPoint
	                  + 0.00001;

	return color / attenuation;
}

vec4 calcSpotLight(SpotLight spotLight, vec3 normal) {

	float spotFactor = dot(lightDirection, spotLight.direction);

	vec4 color = vec4(0,0,0,0);

	if(spotFactor > spotLight.cutoff) {
		color = calcPointLight(spotLight.pointLight, normal);
		color *= 1.0 - ((1.0 - spotFactor)/(1.0 - spotLight.cutoff));
	}

	return color;
}

void main() {
	vec4 totalLight = vec4(ambientLight.rgb, 1);
	vec4 texColor = texture(sampler, fragTexCoord.xy);

	vec4 baseColor = texColor * vec4(1 - color.a, 1 - color.a, 1 - color.a, 1 - color.a) + color * vec4(color.a, color.a, color.a, 1);

	vec3 normal = normalize(fragNormal);

	totalLight += calcLight(directionalLight.base, directionalLight.direction, normal);

	for(int i = 0; i < MAX_POINT_LIGHTS; i++) {
		if(pointLights[i].base.intensity > 0) {
			totalLight += calcPointLight(pointLights[i], normal);
		}
	}

	for(int i = 0; i < MAX_SPOT_LIGHTS; i++) {
		if(spotLights[i].pointLight.base.intensity > 0) {
			totalLight += calcSpotLight(spotLights[i], normal);
		}
	}

	fragColor = baseColor * totalLight;
}
