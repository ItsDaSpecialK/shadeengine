#version 330 core

in vec2 fragTexCoord;
in vec3 fragWorldPos;
in mat3 tbnMatrix;

out vec4 fragColor;

struct BaseLight {
	vec4 color;
	float intensity;
};

struct Attenuation { //physically accurate: ?
	float constant;  // 0
	float linear;    // 0
	float quadratic; // 1
};

struct PointLight {
	BaseLight base;
	Attenuation atten;
	vec3 position;
	float range;
};

uniform PointLight pointLight;
uniform sampler2D diffuse;
uniform sampler2D normalMap;
uniform sampler2D dispMap;
uniform vec4 color;
uniform vec3 camPos;
uniform float specularIntensity;
uniform float specularExponent;
uniform float bumpScale;

vec4 calcLight(BaseLight base, vec3 direction, vec3 normal) {
	float diffuseFactor = dot(normal, -direction);
	
	vec4 diffuseColor = vec4(0,0,0,0);
	vec4 specularColor = vec4(0,0,0,0);
	
	if (diffuseFactor > 0) {
		diffuseColor = base.color * base.intensity * diffuseFactor;
		
		vec3 directionToEye = normalize(camPos - fragWorldPos);
		vec3 reflectDirection = normalize(reflect(direction, normal));
		
		float specularFactor = dot(directionToEye, reflectDirection);
		specularFactor = pow(specularFactor, specularExponent);
		
		if(specularFactor > 0) {
			specularColor = vec4(base.color.xyz, 1.0) * specularIntensity * specularFactor;
		}
	}
	return diffuseColor + specularColor;
}

vec4 calcPointLight(PointLight pointLight, vec3 normal) {
	vec3 lightDirection = fragWorldPos - pointLight.position;
	float distanceToPoint = length(lightDirection);
	
	if (distanceToPoint > pointLight.range)
		return vec4(0,0,0,0);
	
	lightDirection = normalize(lightDirection);
	
	vec4 color = calcLight(pointLight.base, lightDirection, normal);
	
	float attenuation = pointLight.atten.constant
	+ pointLight.atten.linear * distanceToPoint
	+ pointLight.atten.quadratic * distanceToPoint * distanceToPoint
	+ 0.00001;
	
	return color / attenuation;
}

void main() {
	vec3 directionToEye = normalize(camPos - fragWorldPos);
	vec2 texCoord = fragTexCoord + ((directionToEye * tbnMatrix).xy * (texture(dispMap, fragTexCoord).r - 0.5) * bumpScale);
	
	vec4 texColor = texture(diffuse, texCoord);
	vec4 baseColor = texColor * vec4(1 - color.a, 1 - color.a, 1 - color.a, 1 - color.a) + color * vec4(color.a, color.a, color.a, 1);
	vec3 normal = normalize(tbnMatrix * (255.0/128.0 * texture(normalMap, texCoord).xyz - 1));
	fragColor = baseColor * calcPointLight(pointLight, normal);
}
