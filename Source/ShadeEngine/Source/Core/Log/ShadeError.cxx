//
//  ShadeError.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/14/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include <stdio.h>
#include <cassert>
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

void shadeErrorFatal(const std::string& msg) {
	std::cerr << "ERROR: " << msg.c_str() << std::endl;
	std::cin.ignore();
	exit(1);
}

void shadeErrorFatal(const std::string& msg, char err[]) {
	std::cerr << "ERROR: " << msg.c_str() << err << std::endl;
	std::cin.ignore();
	exit(1);
}

}
