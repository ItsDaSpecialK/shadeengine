//
//  Archive.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/14/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Serialization/Archive.hxx"

namespace ShadeEngine {

bool isBigEndian() {
	union {
		uint32_t i;
		char c[4];
	} bint = { 0x01000000 };

	return bint.c[0] == 1;
}

Archive::Archive(bool bIsReading, bool bShouldSerializeAll, bool bIsNetworkData) :
		_bIsReading(bIsReading),
		_bShouldSerializeAll(bShouldSerializeAll),
		_bIsNetworkData(bIsNetworkData) { }

Archive& Archive::operator&(int8& num) {
	if (getIsWriting()) {
		writeToStream((byte*)&num, 1);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 1);
	}
	return *this;
}

Archive& Archive::operator&(int16& num) {
	if (getIsWriting()) {
		int16 temp = num;
		networkOrder(temp);
		writeToStream((byte*)&temp, 2);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 2);
		networkOrder(num);
	}
	return *this;
}

Archive& Archive::operator&(int32& num) {
	if (getIsWriting()) {
		int32 temp = num;
		networkOrder(temp);
		writeToStream((byte*)&temp, 4);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 4);
		networkOrder(num);
	}
	return *this;
}

Archive& Archive::operator&(int64& num) {
	if (getIsWriting()) {
		int64 temp = num;
		networkOrder(temp);
		writeToStream((byte*)&temp, 8);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 8);
		networkOrder(num);
	}
	return *this;
}

Archive& Archive::operator&(float& num) {
	if (getIsWriting()) {
		float temp = num;
		networkOrder(temp);
		writeToStream((byte*)&temp, 4);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 4);
		networkOrder(num);
	}
	return *this;
}

Archive& Archive::operator&(double& num) {
	if (getIsWriting()) {
		double temp = num;
		networkOrder(temp);
		writeToStream((byte*)&temp, 8);
	} else if (getIsReading()) {
		readFromStream((byte*)&num, 8);
		networkOrder(num);
	}
	return *this;
}

}
