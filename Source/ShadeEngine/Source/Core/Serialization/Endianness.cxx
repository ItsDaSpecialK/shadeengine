//
//  Endianness.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Serialization/Endianness.hxx"

namespace ShadeEngine {

bool isLittleEndian() {
	union {
		uint32 i;
		char c[4];
	} bint = { 0x01000000 };

	return bint.c[3] == 1;
}
}
