//
//  Class.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Reflection/Class.hxx"

namespace ShadeEngine {

ShadeClass::ShadeClass(ShadeClass* pSuper, const char* pName, SCreateFuncPtr pCreationFunction) :
		_pSuperClass(pSuper),
		_pName(pName),
		_pCreateFunc(pCreationFunction) { }

bool ShadeClass::isChildOf(const ShadeClass* pOther) {
	for (const ShadeClass* pClass = this; pClass != nullptr; pClass = pClass->getSuperClass()) {
		if (pClass == pOther)
			return true;
	}
	return false;
}

}
