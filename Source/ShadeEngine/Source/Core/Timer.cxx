//
//  Timer.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Timer.hxx"
#include <thread>
#include <chrono>

namespace ShadeEngine {

namespace Time {


typedef std::ratio_divide<std::chrono::high_resolution_clock::period, std::milli> timeToMilli;

uint64 start = std::chrono::high_resolution_clock::now().time_since_epoch().count()
               * timeToMilli::num / timeToMilli::den;

uint32 getCurrentTime() {
	uint64 now = std::chrono::high_resolution_clock::now().time_since_epoch().count()
	             * timeToMilli::num / timeToMilli::den;
	return uint32(now - start);
}

void sleep(uint32 argMilliseconds) {
	std::this_thread::sleep_for(std::chrono::milliseconds(argMilliseconds));
}

}

}
