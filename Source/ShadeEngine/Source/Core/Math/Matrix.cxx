//
//  Matrix.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Math/Matrix.hxx"

namespace ShadeEngine {

Matrix::Matrix(const Matrix& other) {
	for (uint8 i = 0; i < 4; ++i)
		for (uint8 j = 0; j < 4; ++j)
			_m[i][j] = other[i][j];
}

Matrix::Matrix(float i00, float i01, float i02, float i03,
               float i10, float i11, float i12, float i13,
               float i20, float i21, float i22, float i23,
               float i30, float i31, float i32, float i33) {
	(*this)[0][0] = i00;
	(*this)[0][1] = i10;
	(*this)[0][2] = i20;
	(*this)[0][3] = i30;
	(*this)[1][0] = i01;
	(*this)[1][1] = i11;
	(*this)[1][2] = i21;
	(*this)[1][3] = i31;
	(*this)[2][0] = i02;
	(*this)[2][1] = i12;
	(*this)[2][2] = i22;
	(*this)[2][3] = i32;
	(*this)[3][0] = i03;
	(*this)[3][1] = i13;
	(*this)[3][2] = i23;
	(*this)[3][3] = i33;
}

Matrix& Matrix::initToIdentity() {
	for (uint8 i = 0; i < 4; ++i) {
		for (uint8 j = 0; j < 4; ++j) {
			if (i == j)
				_m[i][j] = 1.f;
			else
				_m[i][j] = 0.f;
		}
	}

	return *this;
}

Matrix& Matrix::initToScale(const Vector3f& value) {
	for (uint8 i = 0; i < 4; ++i) {
		for (uint8 j = 0; j < 4; ++j) {
			if (i == j && i != 3)
				_m[i][j] = value[i];
			else
				_m[i][j] = 0.f;
		}
	}

	_m[3][3] = 1.f;

	return *this;
}

Matrix& Matrix::initToTranslation(const Vector3f& value) {
	for (uint8 i = 0; i < 4; ++i) {
		for (uint8 j = 0; j < 4; ++j) {
			if (i == 3 && j != 3)
				_m[i][j] = value[j];
			else if (i == j)
				_m[i][j] = 1.f;
			else
				_m[i][j] = 0.f;
		}
	}

	return *this;
}

Matrix& Matrix::initRotationFromVectors(const Vector3f& forward,
                                        const Vector3f& up,
                                        const Vector3f& right) {
	(*this)[0][0] = right.getX();
	(*this)[1][0] = right.getY();
	(*this)[2][0] = right.getZ();
	(*this)[3][0] = 0.f;

	(*this)[0][1] = up.getX();
	(*this)[1][1] = up.getY();
	(*this)[2][1] = up.getZ();
	(*this)[3][1] = 0.f;

	(*this)[0][2] = forward.getX();
	(*this)[1][2] = forward.getY();
	(*this)[2][2] = forward.getZ();
	(*this)[3][2] = 0.f;

	(*this)[0][3] = 0.f;
	(*this)[1][3] = 0.f;
	(*this)[2][3] = 0.f;
	(*this)[3][3] = 1.f;

	return *this;
}

Matrix& Matrix::initToPerspective(float fov, float aspectRatio, float zNear, float zFar ) {
	const float zRange     = zFar - zNear;
	const float tanHalfFOV = tanf(toRadians(fov) * 0.5f);

	(*this)[0][0] = 1.f / tanHalfFOV;
	(*this)[1][0] = 0.f;
	(*this)[2][0] = 0.f;
	(*this)[3][0] = 0.f;

	(*this)[0][1] = 0.f;
	(*this)[1][1] = 0.f;
	(*this)[2][1] = aspectRatio / tanHalfFOV;
	(*this)[3][1] = 0.f;

	(*this)[0][2] = 0.f;
	(*this)[1][2] = (zNear + zFar) / zRange;
	(*this)[2][2] = 0.f;
	(*this)[3][2] = -2.f * zFar * zNear / zRange;

	(*this)[0][3] = 0.f;
	(*this)[1][3] = 1.f;
	(*this)[2][3] = 0.f;
	(*this)[3][3] = 0.f;

	return *this;
}

Matrix& Matrix::initToOrthographic(float right, float left, float top, float bottom, float far, float near) {
	const float width = (right - left);
	const float height = (top - bottom);
	const float depth = (far - near);

	(*this)[0][0] = 2.f / width;
	(*this)[1][0] = 0.f;
	(*this)[2][0] = 0.f;
	(*this)[3][0] = -(right + left) / width;

	(*this)[0][1] = 0.f;
	(*this)[1][1] = 0.f;
	(*this)[2][1] = 2.f / height;
	(*this)[3][1] = -(top + bottom) / height;

	(*this)[0][2] = 0.f;
	(*this)[1][2] = 2.f / depth;
	(*this)[2][2] = 0.f;
	(*this)[3][2] = -(far + near) / depth;

	(*this)[0][3] = 0.f;
	(*this)[1][3] = 0.f;
	(*this)[2][3] = 0.f;
	(*this)[3][3] = 1.f;

	return *this;
}

Matrix Matrix::transpose() const {
	Matrix result;
	for (uint8 j = 0; j < 4; ++j) {
		for (uint8 i = 0; i < 4; ++i) {
			result[i][j] = _m[j][i];
		}
	}
	return result;
}

Vector3f Matrix::transform(const Vector3f& vec) const {
	Vector3f ret;

	for (uint8 i = 0; i < 3; ++i) {
		ret[i] = 0;
		for (uint8 j = 0; j < 3; ++j)
			ret[i] += _m[j][i] * vec[j];
		ret[i] += _m[3][i];
	}

	return ret;
}

Vector4f Matrix::transform(const Vector4f& vec) const {
	Vector4f ret;

	for (uint8 i = 0; i < 4; ++i) {
		ret[i] = 0;
		for (uint8 j = 0; j < 4; ++j)
			ret[i] += _m[j][i] * vec[j];
	}

	return ret;
}

Matrix Matrix::operator*(const Matrix& other) const {
	Matrix result;
	for (uint8 i = 0; i < 4; ++i) {
		for (uint8 j = 0; j < 4; ++j) {
			result._m[i][j] = 0.f;
			for (uint8 k = 0; k < 4; ++k)
				result._m[i][j] += _m[i][k] * other._m[k][j];
		}
	}
	return result;
}

Matrix3::Matrix3(const Matrix3& other) {
	for (uint8 i = 0; i < 3; ++i)
		for (uint8 j = 0; j < 3; ++j)
			_m[i][j] = other[i][j];
}

Matrix3::Matrix3(float i00, float i01, float i02,
                 float i10, float i11, float i12,
                 float i20, float i21, float i22) {
	(*this)[0][0] = i00;
	(*this)[0][1] = i10;
	(*this)[0][2] = i20;
	(*this)[1][0] = i01;
	(*this)[1][1] = i11;
	(*this)[1][2] = i21;
	(*this)[2][0] = i02;
	(*this)[2][1] = i12;
	(*this)[2][2] = i22;
}

Matrix3& Matrix3::initToIdentity() {
	for (uint8 i = 0; i < 3; ++i) {
		for (uint8 j = 0; j < 3; ++j) {
			if (i == j)
				_m[i][j] = 1.f;
			else
				_m[i][j] = 0.f;
		}
	}

	return *this;
}

Matrix3 Matrix3::transpose() const {
	Matrix3 result;
	for (uint8 j = 0; j < 3; ++j) {
		for (uint8 i = 0; i < 3; ++i) {
			result[i][j] = _m[j][i];
		}
	}
	return result;
}

Matrix3 Matrix3::operator*(const Matrix3& other) const {
	Matrix3 result;
	for (uint8 i = 0; i < 3; ++i) {
		for (uint8 j = 0; j < 3; ++j) {
			result._m[i][j] = 0.f;
			for (uint8 k = 0; k < 3; ++k)
				result._m[i][j] += _m[i][k] * other._m[k][j];
		}
	}
	return result;
}

}
