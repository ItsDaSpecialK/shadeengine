//
//  Vector.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Math/Vector.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"

namespace ShadeEngine {

Vector3f Vector3f::cross(const Vector3f& other) const {
	const float x = (*this)[1] * other[2] - (*this)[2] * other[1];
	const float y = (*this)[2] * other[0] - (*this)[0] * other[2];
	const float z = (*this)[0] * other[1] - (*this)[1] * other[0];

	return Vector3f(x, y, z);
}

Vector3f Vector3f::rotate(float angle, const Vector3f& other) const {
	const float sinAngle = sinf(toRadians(-angle));
	const float cosAngle = cosf(toRadians(-angle));

	return other.cross((Vector3f)((*this) * sinAngle))       // Rotation on local X
	       + (other * cosAngle)                             // Rotation on local Z
	       + (*this) * other.dot((*this) * (1 - cosAngle)); // Rotation on local Y
}

float Vector3f::dot(const Vector3f& value) const {
	float result = 0.f;
	for (uint8 i = 0; i < 3; ++i)
		result += (*this)[i] * value[i];

	return result;
}

Vector3f Vector3f::operator+(const Vector3f& addend) const {
	Vector3f result;
	for (uint8 i = 0; i < 3; ++i)
		result[i] = (*this)[i] + addend[i];

	return result;
}

Vector3f Vector3f::operator-(const Vector3f& subtrahend) const {
	Vector3f result;
	for (uint8 i = 0; i < 3; ++i)
		result[i] = (*this)[i] - subtrahend[i];

	return result;
}

Vector3f Vector3f::operator*(float multiplicand) const {
	Vector3f result;
	for (uint8 i = 0; i < 3; ++i)
		result[i] = (*this)[i] * multiplicand;

	return result;
}

Vector3f Vector3f::operator/(float dividend) const {
	Vector3f result;
	for (uint8 i = 0; i < 3; ++i)
		result[i] = (*this)[i] / dividend;

	return result;
}

Vector3f& Vector3f::operator+=(const Vector3f& addend) {
	for (uint8 i = 0; i < 3; ++i)
		(*this)[i] += addend[i];

	return *this;
}

Vector3f& Vector3f::operator-=(const Vector3f& subtrahend) {
	for (uint8 i = 0; i < 3; ++i)
		(*this)[i] -= subtrahend[i];

	return *this;
}

Vector3f& Vector3f::operator*=(float multiplicand) {
	for (uint8 i = 0; i < 3; ++i)
		(*this)[i] *= multiplicand;

	return *this;
}

Vector3f& Vector3f::operator/=(float dividend) {
	for (uint8 i = 0; i < 3; ++i)
		(*this)[i] /= dividend;

	return *this;
}

Vector2f lerp(const Vector2f& start, const Vector2f& end, float t) {
	return Vector2f(start[0] * (1 - t) + end[0] * t,
	                start[1] * (1 - t) + end[1] * t);
}

}
