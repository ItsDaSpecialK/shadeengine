//
//  Quaternion.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/26/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Math/Quaternion.hxx"

namespace ShadeEngine {

Quaternion::Quaternion(const Vector3f& axis, float angle) {
	const float sinHalfAngle = sinf(toRadians(angle) / 2);
	const float cosHalfAngle = cosf(toRadians(angle) / 2);

	setW(cosHalfAngle);
	setX(axis.getX() * sinHalfAngle);
	setY(axis.getY() * sinHalfAngle);
	setZ(axis.getZ() * sinHalfAngle);
}

float Quaternion::dot(const Quaternion& value) const {
	float result = 0.f;
	for (uint8 i = 0; i < 4; ++i)
		result += (*this)[i] * value[i];

	return result;
}

Matrix Quaternion::toRotationMatrix() const { // @Todo: implement fully locally.
	const Vector3f forward = Vector3f(2.0f * (getX() * getZ() - getW() * getY()), 2.0f * (getY() * getZ() + getW() * getX()), 1.0f - 2.0f * (getX() * getX() + getY() * getY()));
	const Vector3f up = Vector3f(2.0f * (getX() * getY() + getW() * getZ()), 1.0f - 2.0f * (getX() * getX() + getZ() * getZ()), 2.0f * (getY() * getZ() - getW() * getX()));
	const Vector3f right = Vector3f(1.0f - 2.0f * (getY() * getY() + getZ() * getZ()), 2.0f * (getX() * getY() - getW() * getZ()), 2.0f * (getX() * getZ() + getW() * getY()));

	return Matrix().initRotationFromVectors(forward, up, right);
}

Quaternion Quaternion::operator+(const Quaternion& addend) const {
	Quaternion result;
	for (uint8 i = 0; i < 4; ++i)
		result[i] = (*this)[i] + addend[i];

	return result;
}

Quaternion Quaternion::operator-(const Quaternion& subtrahend) const {
	Quaternion result;
	for (uint8 i = 0; i < 4; ++i)
		result[i] = (*this)[i] - subtrahend[i];

	return result;
}

Quaternion Quaternion::operator*(float multiplicand) const {
	Quaternion result;
	for (uint8 i = 0; i < 4; ++i)
		result[i] = (*this)[i] * multiplicand;

	return result;
}

Quaternion Quaternion::operator/(float dividend) const {
	Quaternion result;
	for (uint8 i = 0; i < 4; ++i)
		result[i] = (*this)[i] / dividend;

	return result;
}

Quaternion& Quaternion::operator+=(const Quaternion& addend) {
	for (uint8 i = 0; i < 4; ++i)
		(*this)[i] += addend[i];

	return *this;
}

Quaternion& Quaternion::operator-=(const Quaternion& subtrahend) {
	for (uint8 i = 0; i < 4; ++i)
		(*this)[i] -= subtrahend[i];

	return *this;
}

Quaternion& Quaternion::operator*=(float multiplicand) {
	for (uint8 i = 0; i < 4; ++i)
		(*this)[i] *= multiplicand;

	return *this;
}

Quaternion& Quaternion::operator/=(float dividend) {
	for (uint8 i = 0; i < 4; ++i)
		(*this)[i] /= dividend;

	return *this;
}

Quaternion Quaternion::operator*(const Quaternion& other) const {
	const float W = (getW() * other.getW()) - (getX() * other.getX()) - (getY() * other.getY()) - (getZ() * other.getZ());
	const float X = (getX() * other.getW()) + (getW() * other.getX()) + (getY() * other.getZ()) - (getZ() * other.getY());
	const float Y = (getY() * other.getW()) + (getW() * other.getY()) + (getZ() * other.getX()) - (getX() * other.getZ());
	const float Z = (getZ() * other.getW()) + (getW() * other.getZ()) + (getX() * other.getY()) - (getY() * other.getX());

	return Quaternion(W, X, Y, Z);
}

Quaternion Quaternion::operator*(const Vector3f& other) const {
	const float W = -(getX() * other.getX()) - (getY() * other.getY()) - (getZ() * other.getZ());
	const float X =  (getW() * other.getX()) + (getY() * other.getZ()) - (getZ() * other.getY());
	const float Y =  (getW() * other.getY()) + (getZ() * other.getX()) - (getX() * other.getZ());
	const float Z =  (getW() * other.getZ()) + (getX() * other.getY()) - (getY() * other.getX());

	return Quaternion(W, X, Y, Z);
}

Vector3f Quaternion::rotate(const Vector3f& vec) const {
	const float x1 = (getY() * vec.getZ()) - (getZ() * vec.getY());
	const float y1 = (getZ() * vec.getX()) - (getX() * vec.getZ());
	const float z1 = (getX() * vec.getY()) - (getY() * vec.getX());

	const float x2 = (getW() * x1) + (getY() * z1) - (getZ() * y1);
	const float y2 = (getW() * y1) + (getZ() * x1) - (getX() * z1);
	const float z2 = (getW() * z1) + (getX() * y1) - (getY() * x1);

	return Vector3f(vec.getX() + 2.f * x2,
	                vec.getY() + 2.f * y2,
	                vec.getZ() + 2.f * z2);
}

}
