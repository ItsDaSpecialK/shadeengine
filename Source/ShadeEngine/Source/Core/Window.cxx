//
//  Window.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Core/Window.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Component/CameraComponent.hxx"
#include "ShadeENGINE/Engine/InputManager.hxx"

#ifdef WIN32

#define INIT_ENTRY_POINT( funcname, type ) \
	funcname = (type)SDL_GL_GetProcAddress(#funcname); \
	if (!funcname) std::cerr << "#funcname() not initialized" << std::endl

PFNGLUNIFORM4FPROC glUniform4f = nullptr;
PFNGLUNIFORM3FPROC glUniform3f = nullptr;
PFNGLUNIFORM1FPROC glUniform1f = nullptr;
PFNGLUNIFORM1IPROC glUniform1i = nullptr;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = nullptr;
PFNGLACTIVETEXTUREPROC glActiveTexture = nullptr;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
PFNGLBUFFERDATAPROC glBufferData = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = nullptr;
PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers = nullptr;
PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer = nullptr;
PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers = nullptr;
PFNGLBINDRENDERBUFFERPROC glBindRenderbuffer = nullptr;
PFNGLRENDERBUFFERSTORAGEPROC glRenderbufferStorage = nullptr;
PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer = nullptr;
PFNGLFRAMEBUFFERTEXTUREPROC glFramebufferTexture = nullptr;
PFNGLDRAWBUFFERSPROC glDrawBuffers = nullptr;
PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus = nullptr;
PFNGLDELETERENDERBUFFERSPROC glDeleteRenderbuffers = nullptr;
PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers = nullptr;
#endif

namespace ShadeEngine {

SDL_Window* Window::s_pWindow = nullptr;
SDL_GLContext Window::s_glContext = nullptr;
bool Window::s_bCloseRequested = false;
bool Window::s_bHasFocus = false;
uint16 Window::s_width;
uint16 Window::s_height;
std::string Window::s_title;

void Window::create(uint16 width, uint16 height, std::string& title) {

	if (s_pWindow != nullptr) {
		shadeErrorFatal("Can't Create a window. Already have one.");
		return; // ERROR: Already exists.
	}

	Window::s_width = width;
	Window::s_height = height;
	Window::s_title = title;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);//MSAA
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);//MSAA

	s_pWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (s_pWindow == nullptr)
		shadeErrorFatal("Couldn't create Window");

	s_bHasFocus = true;

	s_glContext = SDL_GL_CreateContext(s_pWindow);
	if (s_glContext == nullptr)
		shadeErrorFatal("Couldn't create SDL_GL_Context");

	SDL_GL_MakeCurrent(s_pWindow, s_glContext);
	SDL_GL_SetSwapInterval(0); // disable vsync (not needed for dblbuffer)

#ifdef WIN32
	INIT_ENTRY_POINT(glUniform4f, PFNGLUNIFORM4FPROC);
	INIT_ENTRY_POINT(glUniform3f, PFNGLUNIFORM3FPROC);
	INIT_ENTRY_POINT(glUniform1f, PFNGLUNIFORM1FPROC);
	INIT_ENTRY_POINT(glUniform1i, PFNGLUNIFORM1IPROC);
	INIT_ENTRY_POINT(glUniformMatrix4fv, PFNGLUNIFORMMATRIX4FVPROC);
	INIT_ENTRY_POINT(glUseProgram, PFNGLUSEPROGRAMPROC);
	INIT_ENTRY_POINT(glGetUniformLocation, PFNGLGETUNIFORMLOCATIONPROC);
	INIT_ENTRY_POINT(glActiveTexture, PFNGLACTIVETEXTUREPROC);
	INIT_ENTRY_POINT(glGenVertexArrays, PFNGLGENVERTEXARRAYSPROC);
	INIT_ENTRY_POINT(glBindVertexArray, PFNGLBINDVERTEXARRAYPROC);
	INIT_ENTRY_POINT(glGenBuffers, PFNGLGENBUFFERSPROC);
	INIT_ENTRY_POINT(glCreateProgram, PFNGLCREATEPROGRAMPROC);
	INIT_ENTRY_POINT(glDeleteProgram, PFNGLDELETEPROGRAMPROC);
	INIT_ENTRY_POINT(glDeleteShader, PFNGLDELETESHADERPROC);
	INIT_ENTRY_POINT(glCreateShader, PFNGLCREATESHADERPROC);
	INIT_ENTRY_POINT(glShaderSource, PFNGLSHADERSOURCEPROC);
	INIT_ENTRY_POINT(glCompileShader, PFNGLCOMPILESHADERPROC);
	INIT_ENTRY_POINT(glGetShaderiv, PFNGLGETSHADERIVPROC);
	INIT_ENTRY_POINT(glAttachShader, PFNGLATTACHSHADERPROC);
	INIT_ENTRY_POINT(glGetShaderInfoLog, PFNGLGETSHADERINFOLOGPROC);
	INIT_ENTRY_POINT(glLinkProgram, PFNGLLINKPROGRAMPROC);
	INIT_ENTRY_POINT(glGetProgramiv, PFNGLGETPROGRAMIVPROC);
	INIT_ENTRY_POINT(glGetProgramInfoLog, PFNGLGETPROGRAMINFOLOGPROC);
	INIT_ENTRY_POINT(glBindBuffer, PFNGLBINDBUFFERPROC);
	INIT_ENTRY_POINT(glBufferData, PFNGLBUFFERDATAPROC);
	INIT_ENTRY_POINT(glEnableVertexAttribArray, PFNGLENABLEVERTEXATTRIBARRAYPROC);
	INIT_ENTRY_POINT(glVertexAttribPointer, PFNGLVERTEXATTRIBPOINTERPROC);
	INIT_ENTRY_POINT(glDeleteBuffers, PFNGLDELETEBUFFERSPROC);
	INIT_ENTRY_POINT(glDeleteVertexArrays, PFNGLDELETEVERTEXARRAYSPROC);
	INIT_ENTRY_POINT(glGenFramebuffers, PFNGLGENFRAMEBUFFERSPROC);
	INIT_ENTRY_POINT(glBindFramebuffer, PFNGLBINDFRAMEBUFFERPROC);
	INIT_ENTRY_POINT(glGenRenderbuffers, PFNGLGENRENDERBUFFERSPROC);
	INIT_ENTRY_POINT(glBindRenderbuffer, PFNGLBINDRENDERBUFFERPROC);
	INIT_ENTRY_POINT(glRenderbufferStorage, PFNGLRENDERBUFFERSTORAGEPROC);
	INIT_ENTRY_POINT(glFramebufferRenderbuffer, PFNGLFRAMEBUFFERRENDERBUFFERPROC);
	INIT_ENTRY_POINT(glFramebufferTexture, PFNGLFRAMEBUFFERTEXTUREPROC);
	INIT_ENTRY_POINT(glDrawBuffers, PFNGLDRAWBUFFERSPROC);
	INIT_ENTRY_POINT(glCheckFramebufferStatus, PFNGLCHECKFRAMEBUFFERSTATUSPROC);
	INIT_ENTRY_POINT(glDeleteRenderbuffers, PFNGLDELETERENDERBUFFERSPROC);
	INIT_ENTRY_POINT(glDeleteFramebuffers, PFNGLDELETEFRAMEBUFFERSPROC);
#endif

	InputManager::setMouseCenter(width / 2, height / 2);
}

void Window::close() {
	SDL_GL_DeleteContext(s_glContext);
	SDL_DestroyWindow(s_pWindow);
}

void Window::clear() {
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

}
