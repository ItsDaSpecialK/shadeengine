//
//  ResourceLoader.cxx
//  EngineManager
//
//  Created by Konrad Kraemer on 5/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Util/ResourceLoader.hxx"
#include <fstream>
#include <vector>
#include <cmath>
#include <iostream>
#include "ShadeENGINE/Rendering/Shaders/DefaultSurfaceShader.hxx"
#include "ShadeENGINE/ShadeENGINE.hxx"
#include "ShadeENGINE/Util/StringHelper.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

StaticMesh* ResourceLoader::loadMesh(std::string filename) {
	std::ifstream file(filename, std::ios::in);

	if (!file) {
		shadeErrorFatal("Wasn't able to open the file");
		return nullptr;
	}

	std::vector<Vertex> vertices;
	std::vector<Vector2f> texCoords;
	std::vector<uint32> indices;

	std::string line;
	while (getline(file, line)) {
		if (line.substr(0, 2) == "v ") {
			line = line.substr(2);
			Vector3f vert;
			uint16 firstDel = line.find(" ");
			uint16 secondDel = line.substr(firstDel + 1, line.npos).find(" ");
			vert.setX(std::stof(line.substr(0, firstDel)));
			vert.setY(std::stof(line.substr(firstDel + 1, secondDel)));
			vert.setZ(std::stof(line.substr(firstDel + secondDel + 2, line.npos)));
			vertices.push_back(Vertex(vert));
		} else if (line.substr(0, 3) == "vt ") {
			line = line.substr(3);
			Vector2f coord;
			uint16 del = line.find(" ");
			coord[0] = std::stof(line.substr(0, del));
			coord[1] = std::stof(line.substr(del + 1, line.npos));
			texCoords.push_back(coord);
		} else if (line.substr(0, 2) == "f ") {
			line = line.substr(2);
			uint16 firstDel = line.find(" ");
			uint16 slashDel;
			uint16 secondDel = line.substr(firstDel + 1, line.npos).find(" ");
			uint32 a, b, c, d;
			if (line.substr(0, firstDel).find("/") != std::string::npos) { // if there are at least two coords per vertex
				slashDel = line.find("/");
				a = std::stoi(line.substr(0, slashDel));
				a--;
				d = std::stoi(line.substr(slashDel + 1, firstDel));
				d--;
				vertices[a].texCoord[0] = texCoords[d][0];
				vertices[a].texCoord[1] = texCoords[d][1];
			} else {
				a = std::stoi(line.substr(0, firstDel));
				a--;
			}
			if (line.substr(firstDel + 1, secondDel).find("/") != std::string::npos) {
				slashDel = line.substr(firstDel + 1, secondDel).find("/");
				b = std::stoi(line.substr(firstDel + 1, slashDel));
				b--;
				d = std::stoi(line.substr(slashDel + 1, secondDel));
				d--;
				vertices[b].texCoord[0] = texCoords[d][0];
				vertices[b].texCoord[1] = texCoords[d][1];
			} else {
				b = std::stoi(line.substr(firstDel + 1, secondDel));
				b--;
			}
			if (line.substr(firstDel + secondDel + 2, line.npos).find("/") != std::string::npos) {
				slashDel = line.substr(firstDel + secondDel + 2, line.npos).find("/");
				c = std::stoi(line.substr(firstDel + secondDel + 2, slashDel));
				c--;
				d = std::stoi(line.substr(slashDel + 1, line.npos));
				d--;
				vertices[c].texCoord[0] = texCoords[d][0];
				vertices[c].texCoord[1] = texCoords[d][1];
			} else {
				c = std::stoi(line.substr(firstDel + secondDel + 2, line.npos));
				c--;
			}
			indices.push_back(a);
			indices.push_back(b);
			indices.push_back(c);
		}
	}

	if (vertices.size() == 0 || indices.size() == 0) {
		shadeErrorFatal("There are no vertices or indices");
		return nullptr;
	}

	return new StaticMesh(&(vertices[0]), (uint32)vertices.size(), &(indices[0]), (uint32)indices.size());
}

Texture* ResourceLoader::loadTexture(std::string filename) {
	uint16 width, height;
	byte* pData = loadRawTexture(filename, width, height);

	Texture* pTex = new Texture(width, height, GL_TEXTURE_2D, GL_LINEAR, GL_BGR, GL_REPEAT, pData); // @LEAK

	delete[] pData;
	return pTex;
}

CubeMap* ResourceLoader::loadCubeMap(std::vector<std::string> filenames) {
	uint16 width, height;
	uint16 firstWidth, firstHeight;

	byte* pData[6];
	for (uint8 i = 0; i < 6; ++i) {
		pData[i] = loadRawTexture(filenames[i], width, height, true);
		if (i == 0) {
			firstWidth = width;
			firstHeight = height;
		} else if (width != firstWidth || height != firstHeight) {
			shadeErrorFatal("The Widths and Heights of the textures must be the same");
		}
	}

	CubeMap* pCubeMap = new CubeMap(width, height, GL_LINEAR, GL_BGR, GL_CLAMP_TO_EDGE, pData);

	for (uint8 i = 0; i < 6; ++i)
		delete[] pData[i];
	return pCubeMap;
}

byte* ResourceLoader::loadRawTexture(std::string filename, uint16& width, uint16& height, bool invertY) {
	// Data read from the header of the BMP file
	byte header[54];
	uint16 dataPos;
	uint32 imageSize;
	byte* pData;

	// Open the file
	FILE*  file = fopen(filename.c_str(), "rb"); // @LEAK?? Function must exit early...
	if (!file) {
		shadeErrorFatal("Wasn't able to open the file.");
		return nullptr;
	}

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		shadeErrorFatal("Not a correct BMP file");
		return nullptr;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		shadeErrorFatal("Not a BMP file");
		return nullptr;
	}
	// Make sure this is a 24bpp file
	if (*(uint8*)&(header[0x1E]) != 0) {
		std::cout << "Not a correct BMP file" << *(uint8*)&(header[0x1E]) << std::endl;
		shadeErrorFatal("Couldnt read BMP");
		return nullptr;
	}
	if (*(uint8*)&(header[0x1C]) != 24) {
		std::cout << "Not a correct BMP file" << *(uint8*)&(header[0x1C]) << std::endl;
		shadeErrorFatal("Couldnt read BMP");
		return nullptr;
	}

	dataPos    = *(uint16*)&(header[0x0A]);
	imageSize  = *(uint32*)&(header[0x22]);
	width      = *(uint16*)&(header[0x12]);
	height     = *(uint16*)&(header[0x16]);


	if (imageSize == 0)
		imageSize = width * height * 3;
	if (dataPos == 0)
		dataPos = 54;


	if (width != height) {
		shadeErrorFatal("Not A Square!");
		return nullptr;
	}
	if (imageSize != width * height * 3) {
		std::cerr << "Imagesize does not match dimensions" << std::endl;
	}
	if (dataPos != 54) {
		shadeErrorFatal("Data Start Location Incorrect");
		return nullptr;
	}

	pData = new byte[imageSize];
	if (invertY) {
		uint32 rowSize = width * 3;
		byte* writeAddr = pData + imageSize - rowSize;
		for (; writeAddr >= pData; writeAddr -= rowSize) {
			fread(writeAddr, 1, rowSize, file);
		}
	} else {
		fread(pData, 1, imageSize, file);
	}
	fclose(file);

	if (StringHelper::getFilename(filename).find("_normal") == std::string::npos &&
	    StringHelper::getFilename(filename).find("_disp") == std::string::npos) { // @TODO: Actual normal map detection.
		// Manual gamma correction.
		for (uint32 i = 0; i < imageSize; i++) {
			float num = pData[i] / 255.0;
			num = std::pow(num, 2.2);
			pData[i] = (byte)(num * 255);
		}
	}
	return pData;
}

struct MaterialFile {
	uint32 engineIdent;
	uint32 id;
	uint32 version;
	float specInten;
	float specExp;
	float bumpScale;
	float color[4];
	uint32 textureID;
	uint32 normalMapID;
	uint32 displacementMapID;
};


Material* ResourceLoader::loadMaterial(std::string filename, const CAssetManager& assetMan) {
	Material* pMat = new Material(); // @LEAK

	std::ifstream file(filename, std::ios::binary | std::ios::ate);
	if (!file.is_open()) {
		shadeErrorFatal("Couldn't locate file");
		return nullptr;
	}
	if (file.tellg() < sizeof(MaterialFile)) {
		shadeErrorFatal("Invalid File. File too small");
		return nullptr;
	}
	// byte* pMemblock = new byte[HEADER_SIZE];
	MaterialFile* pFile = new MaterialFile(); // @FixedLEAK

	file.seekg(0, std::ios::beg);
	file.read((char*)(pFile), sizeof(MaterialFile));

	if (pFile->engineIdent != 6571347 ||
	    pFile->id != 1179926861 ||
	    pFile->version != 2) {

		shadeErrorFatal("Not a recognized Material");
		return nullptr;
	}

	pMat->setSpecularIntensity(pFile->specInten);
	pMat->setSpecularExponent(pFile->specExp);
	pMat->setColor(Color(pFile->color[0], pFile->color[1], pFile->color[2], pFile->color[3]));
	pMat->setShader(&DefaultSurfaceShader::getInstance());
	pMat->setTexture(assetMan.getTexture(pFile->textureID));
	if (pFile->normalMapID != 0) {
		pMat->setNormalMap(assetMan.getTexture(pFile->normalMapID));
	} else {
		byte colors[] = { 128, 128, 255 };
		static Texture defaultNormalMap(1, 1, GL_TEXTURE_2D, GL_LINEAR, GL_RGB, GL_REPEAT, colors);
		pMat->setNormalMap(&defaultNormalMap);
	}
	if (pFile->displacementMapID != 0) {
		pMat->setDisplacementMap(assetMan.getTexture(pFile->displacementMapID));
		pMat->setBumpScale(pFile->bumpScale);
	}
	delete pFile;
	return pMat;
}

}
