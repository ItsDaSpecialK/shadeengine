//
//  MapLoader.cxx
//  EngineManager
//
//  Created by Konrad Kraemer on 3/21/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Util/MapLoader.hxx"
#include "ShadeENGINE/Actor/StaticMeshActor.hxx"
#include "ShadeENGINE/ShadeENGINE.hxx"
#include "ShadeENGINE/Engine/CSGManager.hxx"
#include "ShadeENGINE/Engine/Object/CSG/Brush.hxx"
#include "ShadeENGINE/Engine/Object/CSG/BoxBrush.hxx"

namespace ShadeEngine {

EdScenario* MapLoader::createMap(FILE* pFile) {
	if (pFile == nullptr) shadeErrorFatal("Map file doesn't exist");
	struct MapFileHeader {
		uint32 head;
		uint32 sedm;
		uint32 version;
		uint32 numActors;
		char name[32];
		char buildDate[32];
		uint32 numAssets;
		uint32 startAddress;
		uint32 padding;
		uint32 foot;

	} header;
	if (fread(&header, 1, sizeof(MapFileHeader), pFile) != sizeof(MapFileHeader)) {
		shadeErrorFatal("Not a correct map file, header is wrong size");
	} else if (header.head != 1751474532) {
		shadeErrorFatal("Not a correct map file, header id is wrong");
	} else if (header.sedm != 1298416979) {
		shadeErrorFatal("Not a correct map file, header filetype is wrong");
	} else if (header.version != 2) {
		shadeErrorFatal("Not a correct map file, header version is wrong");
	} else if (header.foot != 1718579060) {
		shadeErrorFatal("Not a correct map file, header footer is wrong");
	}
	uint32 numAssets = header.numAssets;
	uint32* assetIDs = new uint32[numAssets];
	for (uint32 i = 0; i < numAssets; i++) {
		uint32 id = 0;
		fread(&id, 4, 1, pFile);
		assetIDs[i] = id;
	}
	fseek(pFile, header.startAddress, SEEK_SET);
	return new EdScenario(header.name, assetIDs, numAssets, header.numActors);
}

void MapLoader::loadScenarioScene(EdScenario* pScenario, FILE* pFile, const CAssetManager& assetMan) {
	Scene* pScene = pScenario->getScene();

	uint32 numActors = pScenario->getNumActors();
	for (uint32 i = 0; i < numActors; i++) {
		fpos_t pos;
		fgetpos(pFile, &pos);
		uint32 typeName;
		fread(&typeName, 1, 4, pFile);
		fsetpos(pFile, &pos);
		switch (typeName) { // @TODO: make this universal, and not individual cases
			case 1752395123: // smsh
				MapLoader::setupStaticMesh(pScene, pFile, assetMan);
				break;
			case 1197962083: // csgG
				MapLoader::setupCSGGeometry(pScene, pFile, assetMan);
				break;
			case 1853321331: // spwn
				MapLoader::setupSpawnPoint(pScene, pFile);
				break;
			case 1734962276: // dlig
				MapLoader::setupDirectionalLight(pScene, pFile);
				break;
			case 1734962288: // plig
				MapLoader::setupPointLight(pScene, pFile);
				break;
			case 1734962291: // slig
				MapLoader::setupSpotLight(pScene, pFile);
				break;
			default:
				shadeErrorFatal("Unknown type when reading map.");
				break;
		}
	}
}

void MapLoader::setupStaticMesh(Scene* pScene, FILE* pFile, const CAssetManager& assetMan) {
	struct MapStaticMeshActorParams {
		uint32 smsh;
		float pos[3];
		float rot[4];
		float scale[3];
		uint32 meshID;
		uint32 materialID;
	} statMeshParams;
	if (fread(&statMeshParams, 1, sizeof(MapStaticMeshActorParams), pFile)
	    != sizeof(MapStaticMeshActorParams)) {
		shadeErrorFatal("Corrupt File");
	}
	// Create the StaticMeshActor
	Vector3f pos = Vector3f(statMeshParams.pos[0],
	                        statMeshParams.pos[1],
	                        statMeshParams.pos[2]);
	Quaternion rot = Quaternion(statMeshParams.rot[3],
	                            statMeshParams.rot[0],
	                            statMeshParams.rot[1],
	                            statMeshParams.rot[2]);
	Vector3f scale = Vector3f(statMeshParams.scale[0],
	                          statMeshParams.scale[1],
	                          statMeshParams.scale[2]);
	StaticMeshActor* pMeshActor =
		pScene->spawn<StaticMeshActor>(StaticMeshActor::staticClass(),
		                               pos,
		                               rot);  // @LEAK-id0
	pMeshActor->getTransform().setScale(scale);
	pMeshActor->setMesh(assetMan.getStaticMesh(statMeshParams.meshID));
	pMeshActor->setMaterial(assetMan.getMaterial(statMeshParams.materialID));

}

void MapLoader::setupSpawnPoint(Scene* pScene, FILE* pFile) {
	struct MapSpawnPointParams {
		uint32 spwn;
		float pos[3];
		float rot[4];
	} spawnParams;
	if (fread(&spawnParams, 1, sizeof(MapSpawnPointParams), pFile)
	    != sizeof(MapSpawnPointParams)) {
		shadeErrorFatal("Corrupt File");
	}
	// Create the StaticMeshActor
	Vector3f pos = Vector3f(spawnParams.pos[0],
	                        spawnParams.pos[1],
	                        spawnParams.pos[2]);
	Quaternion rot = Quaternion(spawnParams.rot[3],
	                            spawnParams.rot[0],
	                            spawnParams.rot[1],
	                            spawnParams.rot[2]);
	SpawnPoint* pSpawn = new SpawnPoint(pScene); // @LEAK
	pSpawn->getTransform().setPos(pos);
	pSpawn->getTransform().setRot(rot);
	pScene->addSpawnPoint(pSpawn);
}

void MapLoader::setupDirectionalLight(Scene* pScene, FILE* pFile) {
	struct MapDirLightParams {
		uint32 dlig;
		float pos[3];
		float rot[4];
		float color[4];
		float intensity;
	} lightParams;
	if (fread(&lightParams, 1, sizeof(MapDirLightParams), pFile)
	    != sizeof(MapDirLightParams)) {
		shadeErrorFatal("Corrupt File");
	}
	// Create the StaticMeshActor
//	Vector3f pos = Vector3f(lightParams.pos[0],
//	                        lightParams.pos[1],
//	                        lightParams.pos[2]);
	Quaternion rot = Quaternion(lightParams.rot[3],
	                            lightParams.rot[0],
	                            lightParams.rot[1],
	                            lightParams.rot[2]);
	Color col = Color(lightParams.color[0],
	                  lightParams.color[1],
	                  lightParams.color[2],
	                  lightParams.color[3]);
	DirectionalLight* pDirLight = new DirectionalLight();
	pDirLight->setColor(col);
	pDirLight->setIntensity(lightParams.intensity);
	pDirLight->setRot(rot);
	// pDirLight->setPos(pos);
	pScene->addDirectionalLight(pDirLight);
}

void MapLoader::setupPointLight(Scene* pScene, FILE* pFile) {
	struct MapPtLightParams {
		uint32 plig;
		float pos[3];
		float color[4];
		float intensity;
		float atten[3];
		float range;
	} lightParams;
	if (fread(&lightParams, 1, sizeof(MapPtLightParams), pFile)
	    != sizeof(MapPtLightParams)) {
		shadeErrorFatal("Corrupt File");
	}
	// Create the StaticMeshActor
	Vector3f pos = Vector3f(lightParams.pos[0],
	                        lightParams.pos[1],
	                        lightParams.pos[2]);
	Color col = Color(lightParams.color[0],
	                  lightParams.color[1],
	                  lightParams.color[2],
	                  lightParams.color[3]);
	Attenuation atten = Attenuation(lightParams.atten[0],
	                                lightParams.atten[1],
	                                lightParams.atten[2]);
	PointLight* pPtLight = new PointLight();
	pPtLight->setColor(col);
	pPtLight->setIntensity(lightParams.intensity);
	pPtLight->setPos(pos);
	pPtLight->setAttenuation(atten);
	pPtLight->setRange(lightParams.range);
	pScene->addPointLight(pPtLight);
}

void MapLoader::setupSpotLight(Scene* pScene, FILE* pFile) {
	struct MapSptLightParams {
		uint32 slig;
		float pos[3];
		float rot[4];
		float color[4];
		float intensity;
		float atten[3];
		float range;
		float angle;
	} lightParams;
	if (fread(&lightParams, 1, sizeof(MapSptLightParams), pFile)
	    != sizeof(MapSptLightParams)) {
		shadeErrorFatal("Corrupt File");
	}
	// Create the StaticMeshActor
	Vector3f pos = Vector3f(lightParams.pos[0],
	                        lightParams.pos[1],
	                        lightParams.pos[2]);
	Quaternion rot = Quaternion(lightParams.rot[3],
	                            lightParams.rot[0],
	                            lightParams.rot[1],
	                            lightParams.rot[2]);
	Color col = Color(lightParams.color[0],
	                  lightParams.color[1],
	                  lightParams.color[2],
	                  lightParams.color[3]);
	Attenuation atten = Attenuation(lightParams.atten[0],
	                                lightParams.atten[1],
	                                lightParams.atten[2]);
	SpotLight* pSptLight = new SpotLight();
	pSptLight->setColor(col);
	pSptLight->setIntensity(lightParams.intensity);
	pSptLight->setPos(pos);
	pSptLight->setRot(rot);
	pSptLight->setAttenuation(atten);
	pSptLight->setRange(lightParams.range);
	pSptLight->setAngle(lightParams.angle);
	pScene->addSpotLight(pSptLight);
}

void MapLoader::setupCSGGeometry(Scene* pScene, FILE* pFile, const CAssetManager& assetMan) {
	struct MapCSGHeader {
		uint32 csgG;
		uint32 materialID;
		uint32 numBrushes;
	} csgHeader;
	if (fread(&csgHeader, 1, sizeof(MapCSGHeader), pFile)
	    != sizeof(MapCSGHeader)) {
		shadeErrorFatal("Corrupt File");
	}
	// pScene->getCSGManager()->setMaterial(assetMan.getMaterial(csgHeader.materialID));
	for (uint32 i = 0; i < csgHeader.numBrushes; i++) {
		fpos_t pos;
		fgetpos(pFile, &pos);
		uint32 typeName;
		fread(&typeName, 1, 4, pFile);
		fsetpos(pFile, &pos);
		switch (typeName) { // @TODO: make this universal, and not individual cases
			case 1115189090: // boxB
				MapLoader::loadBoxBrushFromMap(pScene, pFile);
				break;
			default:
				shadeErrorFatal("Unknown type when reading map.");
				break;
		}
	}
}

void MapLoader::loadBoxBrushFromMap(Scene* pScene, FILE* pFile) {
	struct MapBoxBrushParams {
		uint32 boxB;
		ECSGOperation operation;
		float pos[3];
		float rot[4];
		float scale[3];
	} brushParams;
	if (fread(&brushParams, 1, sizeof(MapBoxBrushParams), pFile)
	    != sizeof(MapBoxBrushParams)) {
		shadeErrorFatal("Corrupt File");
	}
	BoxBrush* pBrush = pScene->spawn<BoxBrush>(BoxBrush::staticClass(),
	                                           Vector3f(brushParams.pos[0],
	                                                    brushParams.pos[1],
	                                                    brushParams.pos[2]),
	                                           Quaternion(brushParams.rot[3],
	                                                      brushParams.rot[0],
	                                                      brushParams.rot[1],
	                                                      brushParams.rot[2]));
	pBrush->setSize(Vector3f(brushParams.scale[0],
	                         brushParams.scale[1],
	                         brushParams.scale[2]));
	pBrush->setOperation(brushParams.operation);
	pScene->getCSGManager()->addBrush(pBrush);
	pScene->getCSGManager()->rebuildMesh();
}

}
