//
//  StringHelper.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Util/StringHelper.hxx"
#include "ShadeENGINE/Core/Math/ShadeIntegers.hxx"

namespace ShadeEngine {

std::string StringHelper::getExtension(std::string filename) {
	return filename.substr(filename.find_last_of(".") + 1);
}

std::string StringHelper::getFilename(std::string path) {
	uint16 lastSlash = path.find_last_of("/") + 1;
	uint16 dot = path.find_last_of(".");
	return path.substr(lastSlash, dot - lastSlash);
}

std::string StringHelper::getContainingFolder(std::string path) {
	uint16 lastSlash = path.find_last_of("/") + 1;
	return path.substr(0, lastSlash);
}

std::string StringHelper::getStringFromFile(uint32 address, FILE* pFile) {
	fpos_t pos;
	fgetpos(pFile, &pos);

	uint32 strLen = 0;

	fseek(pFile, address, SEEK_SET);
	fread(&strLen, sizeof(uint32), 1, pFile);

	char* pString = new char[strLen];
	fread(pString, 1, strLen, pFile);
	pString[strLen - 1] = NULL;
	fsetpos(pFile, &pos);
	std::string result = pString;
	delete[] pString;
	return result;
}

}
