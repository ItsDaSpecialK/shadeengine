//
//  DirectoryScanner.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Util/DirectoryScanner.hxx"
#include <iostream>
#ifdef WIN32
#include <Dirent/dirent.h>
#else
#include <dirent.h>
#endif
#include <sys/stat.h>
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include <cstring>

namespace ShadeEngine {

Folder* DirectoryScanner::getAllFilesInDirectory(std::string baseDir) {
	Folder* pFolder = new Folder();
	pFolder->path = baseDir;

	DIR* dir;
	char path[512];
	struct dirent* ent;

	dir = opendir(baseDir.c_str());

	if (dir != nullptr) {
		while ((ent = readdir(dir)) != nullptr) {
			// do not allow "." or ".."
			if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
				continue;
			}
			path[0] = NULL;
			path[1] = NULL;
			strcat(path, baseDir.c_str());
			strcat(path, "/");
			strcat(path, ent->d_name);

			FolderItem* pItem;

			if (ent->d_type == DT_DIR) { // its a directory
				pItem = getAllFilesInDirectory(path); // walk the directory
				pItem->isFile = false;
			} else { // its a file
				pItem = new FolderItem();
				pItem->path = path;
				pItem->isFile = true;
			}
			pItem->name = ent->d_name;
			pItem->pNext = pFolder->pChild;
			pFolder->pChild = pItem;
		}
		closedir(dir);
	} else {
		shadeErrorFatal("Failed to walk Directory");
	}
	return pFolder;
}

}
