//
//  ShadeENGINE.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/25/16.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/ShadeENGINE.hxx"
#include <SDL2/SDL.h>
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

EEngineInitFlags g_bitFlags = EEngineInitFlags::ALL;

void init(EEngineInitFlags argBitFlags) {
	g_bitFlags = argBitFlags;
	/// @TODO: Implement initialization
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		shadeErrorFatal("SDL couldn't start");
	// Dynamically load libraries based on bit flags.
	// Create Managers from loaded libraries and add them to the engine manager.
	// Keep track of loaded libraries.
}

EEngineInitFlags getInitFlags() {
	return g_bitFlags;
}

void quit() {
	// @TODO: find some way to make sure than no more EngineManagers exist.
	// @TODO: unload libraries which were loaded in init.
	SDL_Quit();
}

}
