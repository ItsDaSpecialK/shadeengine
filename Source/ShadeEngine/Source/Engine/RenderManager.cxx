//
//  RenderManager.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#ifdef WIN32
#include "ShadeENGINE/Rendering/GLExtensions.hxx"
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "ShadeENGINE/Engine/RenderManager.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Engine/InputManager.hxx"
#include "ShadeENGINE/Core/Window.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"
#include "ShadeENGINE/Rendering/RenderTarget.hxx"

namespace ShadeEngine {

#define SHADOW_MAP_RESOLUTION 1024

RenderManager::RenderManager() {
	GLint shadowFormat = GL_DEPTH_COMPONENT;
	float filters[] = { GL_NEAREST };
	_shadowTarget = new RenderTarget(1,
	                                 SHADOW_MAP_RESOLUTION,
	                                 SHADOW_MAP_RESOLUTION,
	                                 filters,
	                                 &shadowFormat);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	// glEnable(GL_DEPTH_CLAMP);
	_ambientLight = Color(0.1f, 0.1f, 0.1f, 1.f);
	_bUnlit = false;
	_bWireframe = false;
	_pShadowCam = new Camera();
	_pSkyMesh = nullptr;
}

RenderManager::~RenderManager() {
	for (DirectionalLight* pLight : _dirLights) delete pLight;
	for (PointLight* pLight : _pointLights) delete pLight;
	for (SpotLight* pLight : _spotLights) delete pLight;
	for (Viewport* pViewport : _viewports) delete pViewport;
	delete _pShadowCam;
	delete _shadowTarget;
}

void RenderManager::render() {
	for (Viewport* pViewport : _viewports) {
		renderMeshesToFBO(pViewport);
		pViewport->draw(_renderTarget.getTexture(0));
	}
}

inline void RenderManager::renderMeshesToFBO(Viewport* pViewport) {
	Camera* pCam = pViewport->getCamera();

	if (pCam == nullptr)
		shadeErrorFatal("RenderManager doesnt have a camera to render with");

	_renderTarget.useRenderTarget();
	
	if (InputManager::getKeyboardInput(SDL_SCANCODE_F2)) {
		// STANDARD:
		_bUnlit = false;
		_bWireframe = false;
	} else if (InputManager::getKeyboardInput(SDL_SCANCODE_F3)) {
		// WIREFRAME:
		_bUnlit = true;
		_bWireframe = true;
	} else if (InputManager::getKeyboardInput(SDL_SCANCODE_F4)) {
		// UNLIT:
		_bUnlit = true;
		_bWireframe = false;
	}
	
//	float scale = pViewport->getResolutionScale();
//	uint16 width = (uint16)ceilf(scale * pViewport->getWidth());
//	uint16 height = (uint16)ceilf(scale * pViewport->getHeight());
//	glViewport(0, 0, width, height);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	if (!_pSkyMesh || _bWireframe)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	else
		glClear(GL_DEPTH_BUFFER_BIT);

	if (_bWireframe) {
		glDisable(GL_CULL_FACE); // Draw Back Facing Triangles
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	} else {
		glEnable(GL_CULL_FACE);
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}

	forwardRender(pCam);
	renderSkymesh(pCam);

	RenderTarget::useDefault();
}

inline void RenderManager::forwardRender(Camera* pCam) {

	Matrix camMatrix = pCam->getCameraMatrix();
	Vector3f camPosition = pCam->getPos();

	for (MeshInstance* pMesh : _meshes) {
		// AMBIENT PASS:
		if (_bUnlit)
			pMesh->setAmbientLight(Color(1.f, 1.f, 1.f, 1.f));
		else
			pMesh->setAmbientLight(_ambientLight);
		pMesh->render(camMatrix, camPosition);
	}

	// LIGHTING PASSES:
	if (!_bUnlit) {
		ShadowMapShader& shadowMapShader = ShadowMapShader::getInstance();

		Matrix lightMatrix;

		// DIRECTIONAL LIGHT PASS:
		ForwardDirectionalShader& forDirShader = ForwardDirectionalShader::getInstance();
		for (DirectionalLight* pDirLight : _dirLights) {
			Shadow* pShadow = pDirLight->getShadow();
			float bias = 0.f;

			_shadowTarget->useRenderTarget();
			glClear(GL_DEPTH_BUFFER_BIT);

			if (pShadow != nullptr) {
				bias = pShadow->bias;
				_pShadowCam->setProjection(pShadow->projection);
				_pShadowCam->setPos(camPosition);
				_pShadowCam->setRot(pDirLight->getRot());
				lightMatrix = _pShadowCam->getCameraMatrix();
				shadowMapShader.bind();
				for (MeshInstance* pMesh : _meshes) {
					pMesh->render(lightMatrix, &shadowMapShader);
				}
			} else {
				lightMatrix.initToScale(Vector3f(0, 0, 0));
			}
			forDirShader.bind();
			_renderTarget.useRenderTarget();
			_shadowTarget->getTexture(0)->bind(3);
			forDirShader.setShadowTexelSize(1.f / SHADOW_MAP_RESOLUTION);

			forDirShader.setBias(bias);
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			glDepthMask(GL_FALSE);
			glDepthFunc(GL_EQUAL);
			for (MeshInstance* pMesh : _meshes) {
				forDirShader.setDirectionalLight(pDirLight);
				forDirShader.setCameraPosition(camPosition);
				pMesh->render(camMatrix, lightMatrix, &forDirShader);
			}
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
			glDepthFunc(GL_LESS);
			if (pShadow != nullptr) {
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		}

		// POINT LIGHT PASS:
		ForwardPointShader& forPtShader = ForwardPointShader::getInstance();
		forPtShader.bind();
		for (PointLight* pPtLight : _pointLights) {
			Shadow* pShadow = pPtLight->getShadow();
			if (pShadow != nullptr) {
				_shadowTarget->useRenderTarget();
				glClear(GL_DEPTH_BUFFER_BIT);


				_renderTarget.useRenderTarget();
			}
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			glDepthMask(false);
			glDepthFunc(GL_EQUAL);
			for (MeshInstance* pMesh : _meshes) {
				forPtShader.setPointLight(pPtLight);
				forPtShader.setCameraPosition(camPosition);
				pMesh->render(camMatrix, lightMatrix, &forPtShader);
			}
			glDisable(GL_BLEND);
			glDepthMask(true);
			glDepthFunc(GL_LESS);
		}

		// SPOT LIGHT PASS:
		ForwardSpotShader& forSptShader = ForwardSpotShader::getInstance();
		for (SpotLight* pSptLight : _spotLights) {
			Shadow* pShadow = pSptLight->getShadow();
			float bias = 0.f;

			_shadowTarget->useRenderTarget();
			glClear(GL_DEPTH_BUFFER_BIT);

			if (pShadow != nullptr) {
				bias = pShadow->bias;
				_pShadowCam->setProjection(pShadow->projection);
				_pShadowCam->setPos(pSptLight->getPos());
				_pShadowCam->setRot(pSptLight->getRot());
				lightMatrix = _pShadowCam->getCameraMatrix();
				shadowMapShader.bind();
				for (MeshInstance* pMesh : _meshes) {
					pMesh->render(lightMatrix, &shadowMapShader);
				}
			} else {
				lightMatrix.initToScale(Vector3f(0, 0, 0));
			}
			forSptShader.bind();
			_renderTarget.useRenderTarget();
			_shadowTarget->getTexture(0)->bind(3);
			forSptShader.setShadowTexelSize(1.f / SHADOW_MAP_RESOLUTION);

			forSptShader.setBias(bias);
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			glDepthMask(GL_FALSE);
			glDepthFunc(GL_EQUAL);
			for (MeshInstance* pMesh : _meshes) {
				forSptShader.setSpotLight(pSptLight);
				forSptShader.setCameraPosition(camPosition);
				pMesh->render(camMatrix, lightMatrix, &forSptShader);
			}
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
			glDepthFunc(GL_LESS);
			if (pShadow != nullptr) {
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		}
		// END LIGHTING PASSES
	}
}

inline void RenderManager::renderSkymesh(Camera* pCam) {
	if (!_pSkyMesh)
		return;
	glDepthFunc(GL_LEQUAL);
	Matrix camMatrix = pCam->getCameraMatrix();
	_pSkyMesh->setPos(pCam->getPos());
	SkyMeshShader& skyMeshShader = SkyMeshShader::getInstance();
	skyMeshShader.bind();
	_pSkyMesh->render(camMatrix, &skyMeshShader);
	glDepthFunc(GL_LESS);
}

}
