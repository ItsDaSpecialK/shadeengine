//
//  AssetManager.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetManager.hxx"

namespace ShadeEngine {

EditorClass* CAssetManager::getEdClass(uint32 id) const {
	AssetContainer* pCont = getAsset(id);
	if (pCont == nullptr) {
		shadeErrorFatal("Asset doesn't exist");
		return nullptr; // @TODO: return default asset.
	}
	if (pCont->type == EAssetType::EDCLASS) {
		return (EditorClass*)pCont->pData;
	} else {
		shadeErrorFatal("Asset is of incorrect type");
		return nullptr;
	}
}

Material* CAssetManager::getMaterial(uint32 id) const {
	AssetContainer* pCont = getAsset(id);
	if (pCont == nullptr) {
		shadeErrorFatal("Asset doesn't exist");
		return nullptr; // @TODO: return default asset.
	}
	if (pCont->type == EAssetType::MATERIAL) {
		return (Material*)pCont->pData;
	} else {
		shadeErrorFatal("Asset is of incorrect type");
		return nullptr;
	}
}

StaticMesh* CAssetManager::getStaticMesh(uint32 id) const {
	AssetContainer* pCont = getAsset(id);
	if (pCont == nullptr) {
		shadeErrorFatal("Asset doesn't exist");
		return nullptr; // @TODO: return default asset.
	}
	if (pCont->type == EAssetType::STATIC_MESH) {
		return (StaticMesh*)pCont->pData;
	} else {
		shadeErrorFatal("Asset is of incorrect type");
		return nullptr;
	}
}

Texture* CAssetManager::getTexture(uint32 id) const {
	AssetContainer* pCont = getAsset(id);
	if (pCont == nullptr) {
		shadeErrorFatal("Asset doesn't exist");
		return nullptr; // @TODO: return default asset.
	}
	if (pCont->type == EAssetType::TEXTURE) {
		return (Texture*)pCont->pData;
	} else {
		shadeErrorFatal("Asset is of incorrect type");
		return nullptr;
	}
}

SAssetManager::~SAssetManager() {
	for (AssetCache* map : _maps) {
		delete map;
	}
}

bool SAssetManager::contains(uint32 id) const {
	int64 min = 0;
	int64 max = (int64)(_maps.size() - 1);

	// Binary Search
	while (min <= max) {
		int64 mid = (max - min) / 2 + min;
		if (_maps[mid]->getMinId() <= id && _maps[mid]->getMaxId() >= id) {
			return _maps[mid]->contains(id);
		} else if (_maps[mid]->getMaxId() < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	return false;
}

AssetContainer* SAssetManager::getAsset(uint32 id) const {
	uint32 min = 0;
	uint32 max = (uint32)(_maps.size() - 1);

	// Binary Search
	while (min <= max) {
		uint32 mid = (max - min) / 2 + min;
		if (_maps[mid]->getMinId() <= id && _maps[mid]->getMaxId() >= id) {
			AssetContainer* pCont = _maps[mid]->getAsset(id);
			if (pCont != nullptr) {
				return pCont;
			} else {
				shadeErrorFatal("Asset Doesn't Exist");
				return nullptr;
			}
		} else if (_maps[mid]->getMaxId() < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	shadeErrorFatal("Asset Doesn't Exist");
	return nullptr;
}

void SAssetManager::loadMapFromFile(std::string path) {
	// @TODO: Map Loading.
	shadeErrorFatal("Method not yet implemented");
}

void SAssetManager::closeMap(std::string name) {
	for (auto it = _maps.begin(); it != _maps.end(); it++) {
		if ((*it)->getName() == name) {
			_maps.erase(it);
			return;
		}
	}
	shadeErrorFatal("Map Doesn't Exist");
}

Scene* SAssetManager::getScene(std::string name) {
	for (AssetCache* pMap : _maps) {
		if (pMap->getName() == name) {
			return pMap->getScene();
		}
	}
	shadeErrorFatal("Map Doesn't Exist");
	return nullptr;
}

}
