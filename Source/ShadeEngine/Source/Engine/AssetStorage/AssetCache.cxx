//
//  AssetCache.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/20/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetStorage/AssetCache.hxx"

namespace ShadeEngine {

AssetCache::AssetCache(std::string name,
                       AssetContainer* pAssetArray,
                       uint32 length) : _name(name) {
	_pScene = new Scene();
	_minId = 0;
	_maxId = 0;
	_pAssets = pAssetArray;
	_numAssets = length;
}

AssetCache::~AssetCache() {
	delete _pScene;
	delete[] _pAssets;
}

bool AssetCache::contains(uint32 id) {
	uint32 min = 0;
	uint32 max = _numAssets - 1;

	// Binary Search
	while (min <= max) {
		uint32 mid = (max - min) / 2 + min;
		if (_pAssets[mid].id == id) {
			return true;
		} else if (_pAssets[mid].id < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	return false;
}

AssetContainer* AssetCache::getAsset(uint32 id) {
	uint32 min = 0;
	uint32 max = _numAssets - 1;

	// Binary Search
	while (min <= max) {
		uint32 mid = (max - min) / 2 + min;
		if (_pAssets[mid].id == id) {
			return &(_pAssets[mid]);
		} else if (_pAssets[mid].id < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	shadeErrorFatal("Binary Search Failed. Couldn't get the asset");
	return nullptr;
}

}
