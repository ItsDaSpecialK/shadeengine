//
//  EdAssetManager.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 2/4/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetStorage/EdAssetManager.hxx"
#include "ShadeENGINE/Util/ResourceLoader.hxx"
#include "ShadeENGINE/Util/MapLoader.hxx"
#include "ShadeENGINE/Util/StringHelper.hxx"
#include "ShadeENGINE/Engine/AssetStorage/EdScenario.hxx"

namespace ShadeEngine {

EdAssetManager::EdAssetManager(std::string projectFile) {
	_assetRoot = StringHelper::getContainingFolder(projectFile) + "Assets/";
	FILE* pProjectFile = fopen(projectFile.c_str(), "rb"); // LEAK??
	if (pProjectFile == nullptr) shadeErrorFatal("Project file doesn't exist");
	struct ProjectFileHeader {
		uint32 head;
		uint32 sepf;
		uint32 version;
		uint32 numFiles;
		uint32 padding[3];
		uint32 foot;

	}* header = new ProjectFileHeader();
	if (fread(header, 1, sizeof(ProjectFileHeader), pProjectFile) != sizeof(ProjectFileHeader)) {
		shadeErrorFatal("Not a correct project file, header is wrong size");
	} else if (header->head != 1751474532) {
		shadeErrorFatal("Not a correct project file, header id is wrong");
	} else if (header->sepf != 1179665747) {
		shadeErrorFatal("Not a correct project file, header filetype is wrong");
	} else if (header->version != 1) {
		shadeErrorFatal("Not a correct project file, header version is wrong");
	} else if (header->foot != 1718579060) {
		shadeErrorFatal("Not a correct project file, header footer is wrong");
	}
	if (header->numFiles != 0) {
		_assetTree.populate(pProjectFile, header->numFiles);
	}
	fclose(pProjectFile);
	delete header;
}

void EdAssetManager::loadMapFromFile(std::string path) {
	uint32 id = _assetTree.getIdFromPath(path);
	if (id == 0) {
		shadeErrorFatal("Map doesn't exist.");
	}
	if (_assetTree.getContainer(id)->type != EAssetType::SCENARIO) {
		shadeErrorFatal("Cannot load map. this isn't a map.");
	}
	_assetTree.load(id, *this);
}

void EdAssetManager::closeMap(std::string name) {
	uint32 id = _assetTree.getIdFromPath(name);
	if (id == 0) {
		shadeErrorFatal("Map doesn't exist.");
	}
	AssetContainer* pContainer = _assetTree.getContainer(id);
	if (pContainer->type != EAssetType::SCENARIO) {
		shadeErrorFatal("Cannot close map. this isn't a map.");
	}
	EdScenario* pScenario = ((EdScenario*)(pContainer->pData));
	uint32 numAssetIDs = pScenario->_numAssetIDs;
	for (uint32 i = 0; i < numAssetIDs; i++) {
		uint32 assetID = pScenario->_pAssetIDs[i];
		_assetTree.unload(assetID);
	}
	_assetTree.unload(id);
}

Scene* EdAssetManager::getScene(std::string name) {
	uint32 id = _assetTree.getIdFromPath(name);
	if (id == 0) {
		shadeErrorFatal("Scene doesn't exist??");
	}
	return ((EdScenario*)(_assetTree.getContainer(id)->pData))->getScene();
}

void* EdAssetManager::loadAssetFromPath(std::string path, EAssetType type) {
	std::string fullPath = _assetRoot + path;
	switch (type) {
		case EAssetType::MATERIAL:
			return ResourceLoader::loadMaterial(fullPath, *this);
		case EAssetType::STATIC_MESH:
			return ResourceLoader::loadMesh(fullPath);
		case EAssetType::TEXTURE:
			return ResourceLoader::loadTexture(fullPath);
		case EAssetType::EDCLASS:
			// @TODO: EdClass Loading.
			shadeErrorFatal("Can't Load EdClasses Yet.");
			break;
		case EAssetType::SCENARIO:
			return loadMapFromPath(fullPath);
		default:
			shadeErrorFatal("Unknown/Unhandled Type");
			break;
	}
	shadeErrorFatal("Method not yet implemented");
	return nullptr;
}

void* EdAssetManager::loadMapFromPath(std::string path) {
	FILE* pMapFile = fopen(path.c_str(), "rb");
	EdScenario* pScenario = MapLoader::createMap(pMapFile);
	// Do Stuff Here
	uint32 numAssetIDs = pScenario->_numAssetIDs;
	for (uint32 i = 0; i < numAssetIDs; i++) {
		uint32 id = pScenario->_pAssetIDs[i];
		_assetTree.load(id, *this);
	}
	MapLoader::loadScenarioScene(pScenario, pMapFile, *this);
	fclose(pMapFile);
	return pScenario;
}

}
