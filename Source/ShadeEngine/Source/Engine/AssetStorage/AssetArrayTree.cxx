//
//  AssetArrayTree.cxx
//  EngineManager
//
//  Created by Konrad Kraemer on 11/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetStorage/AssetArrayTree.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Engine/AssetStorage/EdAssetManager.hxx"
#include "ShadeENGINE/Util/StringHelper.hxx"
#include "ShadeENGINE/ShadeENGINE.hxx"

namespace ShadeEngine {

void AssetArrayTree::populate(FILE* pProjectFile, uint32 numEntries, AssetArrayTreeDirectoryNode* pParent) {
	// @TODO: compare with current directory, and maintain up to date with current directory.
	if (pProjectFile == nullptr) shadeErrorFatal("Null was passed in.");
	struct ProjectFileEntry {
		uint32 numItems;
		uint32 stringAddr;
		uint32 id;
		EAssetType type;

	}* entry = new ProjectFileEntry();
	for (uint32 i = 0; i < numEntries; i++) {
		if (fread(entry, 1, sizeof(ProjectFileEntry), pProjectFile) != sizeof(ProjectFileEntry)) {
			shadeErrorFatal("Error, entry was not of the correct size.");
		}
		if (entry->numItems != 0) {
			// Then it is a directory
			AssetArrayTreeDirectoryNode* pNode = new AssetArrayTreeDirectoryNode(); // @LEAK
			pNode->name = StringHelper::getStringFromFile(entry->stringAddr, pProjectFile);
			pNode->pNext = pParent->pChildren;
			pParent->pChildren = pNode;
			pNode->pParent = pParent;
			populate(pProjectFile, entry->numItems, pNode);
		} else {
			AssetArrayTreeLeafNode* pNode = new AssetArrayTreeLeafNode(); // @LEAK
			pNode->pContainer = new AssetContainer(); // @LEAK
			pNode->pContainer->id = entry->id;
			pNode->pContainer->type = entry->type;
			pNode->name = StringHelper::getStringFromFile(entry->stringAddr, pProjectFile);
			pNode->pNext = pParent->pChildren;
			pParent->pChildren = pNode;
			pNode->pParent = pParent;

			// Add To Array
			int64 min = 0;
			int64 max = (int64)(_assets.size()) - 1;
			while (min <= max) {
				int64 mid = (max - min) / 2 + min;
				if (_assets[mid]->pContainer->id < entry->id) {
					min = mid + 1;
				} else {
					max = mid - 1;
				}
			}
			_assets.insert(_assets.begin() + min, pNode);
		}
	}
	delete entry;
}

void AssetArrayTree::add(uint32 id, EAssetType type, std::string path) {
	// Create Container
	AssetArrayTreeLeafNode* pNew = new AssetArrayTreeLeafNode();
	pNew->pContainer = new AssetContainer();
	pNew->pContainer->type = type;
	pNew->pContainer->id = id;

	// Add to tree
	add(pNew, path, _pRoot);

	// Add To Array
	int64 min = 0;
	int64 max = (int64)(_assets.size()) - 1;
	while (min <= max) {
		int64 mid = (max - min) / 2 + min;
		if (_assets[mid]->pContainer->id < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	_assets.insert(_assets.begin() + min, pNew);
}

void AssetArrayTree::add(AssetArrayTreeLeafNode* pNew, std::string path, AssetArrayTreeDirectoryNode* pParent) {
	std::string name;
	std::string subPath = "";

	size_t slash = path.find("/");
	if (slash != std::string::npos) {
		name = path.substr(0, slash);
		subPath = path.substr(slash + 1);
	} else {
		name = path;
	}

	AssetArrayTreeNode* pPrevious = nullptr;

	for (AssetArrayTreeNode* pNode = pParent->pChildren; pNode != nullptr; pNode = pNode->pNext) {
		if (pNode->name == name) {
			if (pNode->bIsDir) {
				return add(pNew, subPath, (AssetArrayTreeDirectoryNode*)pNode);
			} else {
				shadeErrorFatal("Name is not a Directory. File might already exist (if subpath = \"\")");
			}
		}
		pPrevious = pNode;
	}
	// At this point, the file/directory doesn't exist

	if (subPath.length() == 0) { // This is the final directory
		pNew->name = name;
		pNew->pParent = pParent;
		if (pParent->pChildren == nullptr) { // There is no element in tree yet
			pParent->pChildren = pNew;
		} else {
			pPrevious->pNext = pNew;
		}
	} else { // Should make a directory
		AssetArrayTreeDirectoryNode* pDirect = new AssetArrayTreeDirectoryNode();
		pDirect->name = name;
		pDirect->pParent = pParent;
		if (pParent->pChildren == nullptr) { // There is no element in tree yet
			pParent->pChildren = pDirect;
		} else {
			pPrevious->pNext = pDirect;
		}
		add(pNew, subPath, pDirect);
	}
}

bool AssetArrayTree::remove(std::string path, AssetArrayTreeDirectoryNode* pParent) {
	std::string name;
	std::string subPath = "";

	size_t slash = path.find("/");
	if (slash != std::string::npos) {
		name = path.substr(0, slash);
		subPath = path.substr(slash + 1);
	} else {
		name = path;
	}

	AssetArrayTreeNode* pPrevious = nullptr;

	for (AssetArrayTreeNode* pNode = pParent->pChildren; pNode != nullptr; pNode = pNode->pNext) {
		if (pNode->name == name) {
			if (subPath.length() == 0) {
				if (pNode->bIsDir && ((AssetArrayTreeDirectoryNode*)pNode)->pChildren != nullptr) {
					shadeErrorFatal("Cannot delete directory. Must delete children first.");
					return false;
				} else {
					if (pParent->pChildren == pNode) { // There is no element in tree yet
						pParent->pChildren = pNode->pNext;
					} else {
						pPrevious->pNext = pNode->pNext;
					}
					pNode->pNext = nullptr;
					if (!pNode->bIsDir) {
						uint32 id = ((AssetArrayTreeLeafNode*)pNode)->pContainer->id;
						int64 min = 0;
						int64 max = (int64)(_assets.size()) - 1;

						// Binary Search
						while (min <= max) {
							int64 mid = (max - min) / 2 + min;
							if (_assets[mid]->pContainer->id == id) {
								_assets.erase(_assets.begin() + mid);
							} else if (_assets[mid]->pContainer->id < id) {
								min = mid + 1;
							} else {
								max = mid - 1;
							}
						}
					}
					delete pNode;
					return true;
				}
			} else {
				// Not the final directory yet.
				if (pNode->bIsDir) {
					return remove(subPath, (AssetArrayTreeDirectoryNode*)pNode);
				} else {
					shadeErrorFatal("Cannot Remove child node. Current node is a file, not a directory");
				}
			}
		}
		pPrevious = pNode;
	}
	return false;
}

bool AssetArrayTree::load(uint32 id, const CAssetManager& assetMan) {
	AssetArrayTreeLeafNode* pNode = getNode(id);
	if (pNode == nullptr) {
		shadeErrorFatal("Couldn't load the asset.");
		return false;
	}
	if (pNode->referenceCount == 0) {
		std::string path = pNode->name;
		AssetArrayTreeDirectoryNode* pDir = pNode->pParent;
		while (pDir != _pRoot) {
			path = pDir->name + "/" + path;
			pDir = pDir->pParent;
		}
		pNode->pContainer->pData = ((EdAssetManager&)(assetMan)).loadAssetFromPath(path, pNode->pContainer->type);
	}
	pNode->referenceCount++;
	return true;
}

bool AssetArrayTree::unload(uint32 id) {
	AssetArrayTreeLeafNode* pNode = getNode(id);
	if (pNode == nullptr) {
		return false;
	}
	if (pNode->referenceCount == 0) {
		shadeErrorFatal("Can't decrease reference count. Already no references.");
	}
	if (pNode->pContainer->pData == nullptr) {
		shadeErrorFatal("Somehow, the reference count isn't 0, but there isn't anything allocated");
	}
	pNode->referenceCount--;
	if (pNode->referenceCount == 0) {
		pNode->pContainer->deleteData();
	}
	return true;
}

AssetContainer* AssetArrayTree::getContainer(uint32 id) const {
	AssetArrayTreeLeafNode* pNode = getNode(id);
	if (pNode == nullptr) {
		shadeErrorFatal("Container not found.");
		return nullptr;
	}
	return pNode->pContainer;
}

uint32 AssetArrayTree::getIdFromPath(std::string path, AssetArrayTreeDirectoryNode* pParent) {
	std::string name;
	std::string subPath;

	if (path.find("/") != std::string::npos) {
		uint16 slash = path.find("/");
		name = path.substr(0, slash);
		subPath = path.substr(slash + 1);
	} else {
		name = path;
	}

	for (AssetArrayTreeNode* pNode = pParent->pChildren; pNode != nullptr; pNode = pNode->pNext) {
		if (pNode->name == name) {
			if (pNode->bIsDir) {
				return getIdFromPath(subPath, (AssetArrayTreeDirectoryNode*)pNode);
			} else {
				return ((AssetArrayTreeLeafNode*)pNode)->pContainer->id;
			}
		}
	}
	shadeErrorFatal("Couldn't Find the id from the path");
	return NULL;
}

AssetArrayTree::AssetArrayTreeLeafNode* AssetArrayTree::getNode(uint32 id) const {
	int64 min = 0;
	int64 max = (int64)(_assets.size()) - 1;
	// Binary Search
	while (min <= max) {
		int64 mid = (max - min) / 2 + min;
		if (_assets[mid]->pContainer->id == id) {
			return _assets[mid];
		} else if (_assets[mid]->pContainer->id < id) {
			min = mid + 1;
		} else {
			max = mid - 1;
		}
	}
	shadeErrorFatal("TESTING: should never get here (binary search failed)");
	return nullptr;
}

}
