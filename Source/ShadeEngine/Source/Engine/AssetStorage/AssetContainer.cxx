//
//  AssetContainer.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 2/2/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetStorage/AssetContainer.hxx"
#include "ShadeENGINE/Rendering/Material.hxx"
#include "ShadeENGINE/Rendering/StaticMesh.hxx"
#include "ShadeENGINE/Core/Reflection/Class.hxx"
#include "ShadeENGINE/Engine/AssetStorage/EdScenario.hxx"

namespace ShadeEngine {

bool AssetContainer::deleteData() {
	if (pData != nullptr) {
		switch (type) {
			case EAssetType::MATERIAL:
				delete ((Material*)pData);
				break;
			case EAssetType::STATIC_MESH:
				delete ((StaticMesh*)pData);
				break;
			case EAssetType::TEXTURE:
				delete ((Texture*)pData);
				break;
			case EAssetType::EDCLASS:
				delete ((EditorClass*)pData);
				break;
			case EAssetType::SCENARIO:
				delete ((EdScenario*)pData);
				break;
			default:
				return false;
		}
		return true;
	}
	return false;
}

}
