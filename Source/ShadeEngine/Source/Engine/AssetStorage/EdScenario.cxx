//
//  EdScenario.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/21/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/AssetStorage/EdScenario.hxx"

namespace ShadeEngine {

EdScenario::EdScenario(std::string name,
                       uint32* pAssetIdArray,
                       uint32 arrayLength,
                       uint32 numActors) {
	_pScene = new Scene();
	_pAssetIDs = pAssetIdArray;
	_numAssetIDs = arrayLength;
	_numActors = numActors;
}

EdScenario::~EdScenario() {
	delete _pScene;
	delete _pAssetIDs;
}

}
