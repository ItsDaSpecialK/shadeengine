//
//  EngineManager.cxx
//  EngineManager
//
//  Created by Konrad Kraemer on 3/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/EngineManager.hxx"
#include <SDL2/SDL.h>
#include "ShadeENGINE/Core/Window.hxx"
#include "ShadeENGINE/Core/Timer.hxx"
#include "ShadeENGINE/Engine/InputManager.hxx"
#include <iostream>
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Util/StringHelper.hxx"
#include "ShadeENGINE/ShadeENGINE.hxx"
#include "ShadeENGINE/Core/Thread/Thread.hxx"

namespace ShadeEngine {

EngineManager::EngineManager(EEngineInitFlags bitFlags) {
	_bIsRunning = false;
	_fpsLimit = 0;
	_tickTime = 0; // 10;
	_pScene = nullptr;
	_pGameMode = nullptr;
	_numPlayers = 1;
	InputManager::createInputManager();
	_bitFlags = bitFlags & getInitFlags();

	if ((uint32)(_bitFlags & EEngineInitFlags::ASSET_MANAGER)) {
		_pAssetMan = new SAssetManager();
	} else {
		_pAssetMan = nullptr;
	}

	// Splash Screen, Load Maps, That kinda stuff
}

EngineManager::~EngineManager() {
	if (_pGameMode != nullptr) {
		delete _pGameMode;
	}

	if ((uint32)(_bitFlags & EEngineInitFlags::ASSET_MANAGER)) {
		delete _pAssetMan;
	}

}

void EngineManager::update(uint16 delta) {
	Window::clear();
	InputManager::update(delta);
	_pGameMode->update(delta);
	_pScene->update(delta);

	_pScene->render();
	Window::render();
	if (InputManager::getKeyboardInput(SDL_SCANCODE_Q) &&
	    InputManager::getKeyboardInput(SDL_SCANCODE_LCTRL))
		_bIsRunning = false;

	if (Window::isCloseRequested())
		_bIsRunning = false;

}

void EngineManager::run() {
	if (_pGameMode == nullptr)
		shadeErrorFatal("No Gamemode");
	_pGameMode->initialSpawn();

	if (_bIsRunning)
		shadeErrorFatal("Engine is already running");

	_bIsRunning = true;


	uint32 startTime = Time::getCurrentTime();
	uint32 endTime = startTime;
	int16 accumulator = 0;
	uint32 frameDelta = 0;

	// for fps measurement.
	uint32 startSecond = startTime;
	uint16 frameCounter = 0;

	while (_bIsRunning) {
		uint16 tickTime = _tickTime;

		if (tickTime > 0) { // if tick time is set, run a constant timestep (best with physics)
			update(tickTime);
			accumulator -= tickTime;
			endTime = Time::getCurrentTime();
			frameDelta = endTime - startTime; // @WARN: Will go awry after 49 days.
			accumulator += frameDelta;
			if (accumulator < 0) { // Frame took shorter than it should have.
				                   // sleep for a bit
				Time::sleep(-accumulator);

				// Get the new frame end time and update the frame delta in the accumulator.
				endTime = Time::getCurrentTime();
				accumulator -= frameDelta; // previously calculated frameDelta.
				frameDelta = endTime - startTime;
				accumulator += frameDelta; // newly calculated frameDelta.

			} else if (accumulator >= tickTime) { // skipped a frame
				accumulator = tickTime;
			}
		} else { // if tick time isn't set, run a dynamic timestep.
			update(frameDelta);
			endTime = Time::getCurrentTime();
			frameDelta = endTime - startTime;
		}

		startTime = endTime;



		frameCounter++;
		if (startTime - startSecond >= 1000) { // 1 sec has passed
			std::cout << "Average Frame Time: " << (float)(startTime - startSecond) / frameCounter << "ms\t";
			std::cout << "FPS:" << frameCounter << std::endl;
			frameCounter = 0;
			startSecond = startTime;
		}

	}
}

void EngineManager::start() {
#ifdef SHADE_NO_MULTITHREAD
	shadeErrorFatal("Threading not yet enabled. Use EngineManager::run() instead");
#else
	shadeErrorFatal("Threading not yet implemented.");
	Thread(&EngineManager::run, this).detach();
#endif
}

void EngineManager::stop() {
	_bIsRunning = false;
}

void EngineManager::loadMapFromFile(std::string path) {
	if (_pAssetMan == nullptr) {
		shadeErrorFatal("Can't load a map. Don't have an asset manager.");
	}
	_pAssetMan->loadMapFromFile(path);
}

void EngineManager::setActiveMap(std::string name, ShadeClass* pGameModeClass) {
	if (_pAssetMan == nullptr) {
		shadeErrorFatal("Can't set a active map. Don't have an asset manager.");
	}
	_pScene = _pAssetMan->getScene(name);

	if (_pGameMode) {
		delete _pGameMode;
	}
	if (pGameModeClass->isChildOf(CGameMode::staticClass())) {
		_pGameMode = (CGameMode*)pGameModeClass->createNew(_pScene);
		// _pGameMode->addPlayers(_numPlayers);
	}
}

void EngineManager::setScene(Scene* pScene, ShadeClass* pGameModeClass) {
	if (pScene == nullptr) {
		shadeErrorFatal("Scene was null");
	}
	_pScene = pScene;

	if (_pGameMode) {
		delete _pGameMode;
	}
	if (pGameModeClass->isChildOf(CGameMode::staticClass())) {
		_pGameMode = (CGameMode*)pGameModeClass->createNew(_pScene);
	}
}

void EngineManager::closeMap(std::string name) {
	if (_pAssetMan == nullptr) {
		shadeErrorFatal("Can't close a map. Don't have an asset manager.");
	}
	if (_pAssetMan->getScene(name) == _pScene) {
		shadeErrorFatal("Cannot close map. Currently in use");
	}
	_pAssetMan->closeMap(name);
}

void EngineManager::setAssetManager(CAssetManager* pAssetManager) {
	if (_pAssetMan != nullptr) {
		shadeErrorFatal("Already has an asset manager.");
	}
	_pAssetMan = pAssetManager;
}

const CAssetManager& EngineManager::getAssetManager() {
	if (_pAssetMan == nullptr) {
		shadeErrorFatal("Don't have an assetManager");
	}
	return *_pAssetMan;
}

}
