//
//  Input.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/24/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/InputManager.hxx"
#include "ShadeENGINE/Core/Window.hxx"

namespace ShadeEngine {

SDL_Event InputManager::e;
bool InputManager::bCaptureMouse;
bool InputManager::bKeyboard[NUM_KEYS];
bool InputManager::bMouse[NUM_MOUSEBUTTONS];
int16 InputManager::mouseX;
int16 InputManager::mouseY;
uint16 InputManager::mouseCenterX;
uint16 InputManager::mouseCenterY;

void InputManager::createInputManager() {
	SDL_ShowCursor(false);
	bCaptureMouse = true;

	for (uint16 i = 0; i < NUM_KEYS; i++) {
		bKeyboard[i] = false;
	}
	for (uint16 i = 0; i < NUM_MOUSEBUTTONS; i++) {
		bMouse[i] = false;
	}
	mouseCenterX = Window::getWidth() / 2;
	mouseCenterY = Window::getHeight() / 2;
	if (bCaptureMouse)
		Window::resetMouseLocation();
}

void InputManager::setMouseCenter(uint16 X, uint16 Y) {
	mouseCenterX = X;
	mouseCenterY = Y;
	if (bCaptureMouse) {
		Window::resetMouseLocation();
	}
}

void InputManager::update(uint16 delta) {
	if (bCaptureMouse) {
		mouseX = 0;
		mouseY = 0;
	}
	bool bHasFocus = Window::hasFocus();

	while (SDL_PollEvent(&e)) {
		int value = 0;

		if (e.type == SDL_WINDOWEVENT) { // @TODO: Make Window Lose focus when you use mission control.
			switch (e.window.event) {
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					Window::setHasFocus(true);
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					Window::setHasFocus(false);
					break;
			}
		}
		if (e.type == SDL_QUIT) {
			Window::setCloseRequested(true);
		}

		if (bHasFocus) {
			switch (e.type) {
				case SDL_MOUSEMOTION:
					if (bCaptureMouse) {
						mouseX = e.motion.x - mouseCenterX; // @TODO: Use Xrel and Yrel
						mouseY = mouseCenterY - e.motion.y;
					} else {
						mouseX = e.motion.x - mouseCenterX;
						mouseY = mouseCenterY - e.motion.y;
					}
					break;
				case SDL_KEYDOWN:
					value = e.key.keysym.scancode;
					bKeyboard[value] = true;
					break;
				case SDL_KEYUP:
					value = e.key.keysym.scancode;
					bKeyboard[value] = false;
					break;
				case SDL_MOUSEBUTTONDOWN:
					value = e.button.button;
					bMouse[value] = true;
					break;
				case SDL_MOUSEBUTTONUP:
					value = e.button.button;
					bMouse[value] = false;
					break;
				default:
					break;
			}

		}
	}

	if (bCaptureMouse && bHasFocus) {
		Window::resetMouseLocation();
	}
}

}
