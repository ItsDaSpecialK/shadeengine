//
//  PlayerController.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Game/PlayerController.hxx"

namespace ShadeEngine {

PlayerController::PlayerController(Scene* pScene) : CController(pScene) {
	_pActions = nullptr;
	_actionsLength = 0;
	_pAxes = nullptr;
	_axesLength = 0;
	_aspectRatio = 1;
}

PlayerController::~PlayerController() {
	for (int i = 0; i < _actionsLength; i++) {
		delete _pActions[i];
	}
	for (int i = 0; i < _axesLength; i++) {
		delete _pAxes[i];
	}
	delete[] _pActions;
	delete[] _pAxes;
}

void PlayerController::setMapping(Action* pActionArray[], uint8 numActions, Axis* pAxisArray[], uint8 numAxes) {
	_pActions = pActionArray;
	_actionsLength = numActions;
	_pAxes = pAxisArray;
	_axesLength = numAxes;
}

void PlayerController::update(uint16 delta) {
	if (getPawn() != nullptr) { // no need to read inputs
		// actions
		updateActions();

		// axes
		updateAxes(delta);
	}
}

inline void PlayerController::updateActions() {
	for (uint8 i = 0; i < _actionsLength; i++) {
		Action* action = _pActions[i];
		action->valueOld = action->value;
		action->value = false;
		auto events = action->events.begin(); // Iterator
		auto eventTypes = action->eventTypes.begin(); // Iterator
		for (; events != action->events.end() && eventTypes != action->eventTypes.end();
		     ++events, ++eventTypes) {
			switch (*eventTypes) {
				case EEventType::KEYBOARD:
					if (InputManager::getKeyboardInput(*events))
						action->value = true;
					break;
				case EEventType::MOUSE_BUTTON:
					if (InputManager::getMouseButtonInput(*events))
						action->value = true;
					break;
				default:
					break;
			}
		}
		// After the values have been Updated
		if (action->value != action->valueOld) {
			if (action->value == true) {
				getPawn()->processActionInput(action->name, EActionType::PRESSED);
			} else {
				getPawn()->processActionInput(action->name, EActionType::RELEASED);
			}
		}
	}
}

inline void PlayerController::updateAxes(uint16 delta) {
	for (uint8 i = 0; i < _axesLength; i++) {
		Axis* axis = _pAxes[i];
		axis->value = 0;
		auto events = axis->events.begin(); // Iterator
		auto eventTypes = axis->eventTypes.begin(); // Iterator
		auto multipliers = axis->multipliers.begin();
		for (; events != axis->events.end() && eventTypes != axis->eventTypes.end() && multipliers != axis->multipliers.end();
		     ++events, ++eventTypes, ++multipliers) {
			switch (*eventTypes) {
				case EEventType::KEYBOARD:
					if (InputManager::getKeyboardInput(*events))
						axis->value += *multipliers;
					break;
				case EEventType::MOUSE_BUTTON:
					if (InputManager::getMouseButtonInput(*events))
						axis->value = *multipliers;
					break;
				case EEventType::MOUSE_AXIS:
					if (*events == (uint16)EMouseAxis::X)
						axis->value += InputManager::getMouseAxisInput(EMouseAxis::X) * *multipliers;
					else
						axis->value += InputManager::getMouseAxisInput(EMouseAxis::Y) * *multipliers;
					break;
				default:
					break;
			}
		}
		// After the values have been Updated
		if (axis->value != 0) {
			getPawn()->processAxisInput(axis->name, axis->value, delta);
		}
	}
}

CameraComponent* PlayerController::getCameraComponent() {
	Pawn* pPawn = getPawn();
	if (pPawn != nullptr)
		return pPawn->getCamera();
	shadeErrorFatal("Pawn was null.");
	return nullptr;
}

void PlayerController::possess(Pawn* pTarget) {
	CController::possess(pTarget);
	pTarget->getCamera()->setAspectRatio(_aspectRatio);
}

void PlayerController::setAspectRatio(float ratio) {
	_aspectRatio = ratio;
	Pawn* pPawn = getPawn();
	if (pPawn != nullptr) {
		pPawn->getCamera()->setAspectRatio(_aspectRatio);
	}
}

}
