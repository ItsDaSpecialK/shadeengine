//
//  Controller.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Game/Controller.hxx"
#include "ShadeENGINE/Actor/Pawn.hxx"

namespace ShadeEngine {

void CController::possess(Pawn* pTarget) {
	_pPawn = pTarget;
	pTarget->setController(this);
	getTransform().setParent(&(pTarget->getTransform()));
}

void CController::unPossess() {
	_pPawn->setController(nullptr);
	_pPawn = nullptr;
	getTransform().setParent(nullptr);
}

void CController::addPitchInput(float value) {
	Transform& pTrans = getTransform();
	pTrans.rotate(pTrans.getRot().getRight(), value);
}

void CController::addYawInput(float value) {
	Transform& pTrans = getTransform();
	pTrans.rotate(Vector3f(0, -1, 0), value);
}

void CController::addRollInput(float value) {
	Transform& pTrans = getTransform();
	pTrans.rotate(pTrans.getRot().getForward(), value);
}

}
