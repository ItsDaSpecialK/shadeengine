//
//  GameMode.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 9/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Game/GameMode.hxx"

namespace ShadeEngine {

CGameMode::CGameMode(Scene* pScene) {
	_pScene = pScene;
	addPlayer();
}

Pawn* CGameMode::spawnPlayer() {
	Transform& trans = chooseSpawnPoint()->getTransform();
	return _pScene->spawn<Pawn>(*_playerClass, trans.getPos(), trans.getRot());
}

void CGameMode::initialSpawn() {
	for (PlayerController* pController : _controllers) {
		Pawn* pPlayerPawn = spawnPlayer();
		pController->possess(pPlayerPawn);
	}
}

void CGameMode::update(uint16 delta) {
	for (PlayerController* pController : _controllers) {
		pController->update(delta);
	}
}

PlayerController* CGameMode::addPlayer() {
	PlayerController* pController = new PlayerController(nullptr); // @LEAK
	_controllers.add(pController);
	return pController;
}

PlayerController* CGameMode::getController(uint8 playerNum) {
	auto pController = _controllers.begin();
	for (uint8 i = 0; pController != _controllers.end() && i < playerNum; i++) {
		pController++;
	}
	if (pController == _controllers.end()) {
		shadeErrorFatal("Controller wasn't found.");
		return nullptr;
	}
	return *pController;
}

}
