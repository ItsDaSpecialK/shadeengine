//
//  BitFlags.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/26/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/BitFlags.hxx"

namespace ShadeEngine {

EEngineInitFlags operator|(EEngineInitFlags a, EEngineInitFlags b) {
	return EEngineInitFlags(int(a) | int(b));
}

EEngineInitFlags operator&(EEngineInitFlags a, EEngineInitFlags b) {
	return EEngineInitFlags(int(a) & int(b));
}

EEngineInitFlags operator~(EEngineInitFlags a) {
	return EEngineInitFlags(~int(a));
}

}
