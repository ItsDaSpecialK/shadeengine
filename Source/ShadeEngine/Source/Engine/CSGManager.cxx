//
//  CSGManager.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/CSGManager.hxx"
#include "ShadeENGINE/Engine/BSP/BSPTreeNode.hxx"
#include "ShadeENGINE/Engine/Object/CSG/CSG.hxx"
// #include "ShadeENGINE/Rendering/Shaders/DefaultSurfaceShader.hxx"
#include "ShadeENGINE/Engine/RenderManager.hxx"
#include <algorithm>

namespace ShadeEngine {

void CSGManager::removeBrush(Brush* pBrush) {
	for (auto it = _brushes.begin(); it != _brushes.end(); it++) {
		if (*it == pBrush) {
			_brushes.erase(it);
			return;
		}
	}
	shadeErrorFatal("Cannot Remove brush. Doesn't exist.");
}

void CSGManager::rebuildMesh() { // @TODO: this should run in it's own thread.
	BSPTreeNode* pTree = new BSPTreeNode(); // @FixedLEAK

	std::vector<Material*> materials;

	if (_brushes.size() == 0) {
		deleteMeshes();
		delete pTree;
		return;
	}

	if (_brushes.size() > 0) {
		materials.push_back(_brushes[0]->getMaterial());
		pTree->build(generatePolysFromBrush(_brushes[0], 0));
		if (_brushes[0]->getOperation() == ECSGOperation::SUBTRACTION) {
			pTree->invert();
		}
	}
	for (uint32 i = 1; i < _brushes.size(); i++) {
		uint16 matId = 0;
		Material* mat = _brushes[i]->getMaterial();
		auto itr = std::find(materials.begin(), materials.end(), mat);
		if (itr == materials.cend()) {
			// Material doesn't exist
			matId = materials.size();
			materials.push_back(mat);
		} else {
			// Material already exists
			matId = std::distance(materials.begin(), itr);
		}

		BSPTreeNode* pTemp = new BSPTreeNode(generatePolysFromBrush(_brushes[i], matId));
		switch (_brushes[i]->getOperation()) {
			case ECSGOperation::ADDITION:
				pTree = CSG::add(pTree, pTemp);
				break;
			case ECSGOperation::SUBTRACTION:
				pTree = CSG::subtract(pTree, pTemp);
				break;
			case ECSGOperation::INTERSECTION:
			default:
				shadeErrorFatal("Unsupported CSG Operation");
				break;
		}
	}
	std::vector<Polygon> polys = pTree->allPolygons();
	uint16 numMeshes = materials.size();

	deleteMeshes();
	StaticMesh** pMeshes = generateMeshesFromPolys(polys, numMeshes);

	for (uint16 i = 0; i < numMeshes; ++i) {
		StaticMesh* pMesh = pMeshes[i];
		if (!pMesh)
			continue;

		_meshes.push_back(pMesh);
		MeshInstance* pInstance = new MeshInstance();
		pInstance->setMesh(pMesh);
		pInstance->setMaterial(materials[i]);
		_meshInstances.push_back(pInstance);
		_pRenMan->addMesh(pInstance);
	}
	delete pTree;
	delete[] pMeshes;
}

std::vector<Polygon> CSGManager::generatePolysFromBrush(Brush* pBrush, uint16 argMatId) const {
	std::vector<Polygon> result;
	const std::vector<Polygon>& brushPolys = pBrush->getPolys();
	const Matrix& transMat = pBrush->getTransform().getTransformationMatrix();

	for (uint32 i = 0; i < brushPolys.size(); ++i) {
		const Polygon& brushPoly = brushPolys[i];
		const std::vector<Vertex>& brushPolyList = brushPoly.getVertices();
		std::vector<Vertex> newPolyList;
		for (uint32 j = 0; j < brushPolyList.size(); ++j) {
			// Do this, except transform it.
			Vector3f pos = transMat.transform( brushPolyList[j].pos);
			Vector2f texCoord = brushPolyList[j].texCoord;
			Vector3f norm = brushPolyList[j].norm;
			Vector3f tang = brushPolyList[j].tang;

			Vector4f temp(norm.getX(), norm.getY(), norm.getZ(), 0.0);
			temp = transMat.transform(temp);
			norm = Vector3f(temp[0], temp[1], temp[2]);

			temp = Vector4f(tang.getX(), tang.getY(), tang.getZ(), 0.0);
			temp = transMat.transform(temp);
			tang = Vector3f(temp[0], temp[1], temp[2]);

			newPolyList.push_back(Vertex(pos, texCoord, norm, tang));

		}
		result.push_back(Polygon(newPolyList, argMatId));
	}

	return result;
}

StaticMesh** CSGManager::generateMeshesFromPolys(std::vector<Polygon>& polygons, uint16 argNumMeshes) {
	if (argNumMeshes == 0)
		return nullptr;

	typedef StaticMesh* StaticMeshPtr;
	StaticMeshPtr* result = new StaticMeshPtr[argNumMeshes];
	std::vector<std::vector<Vertex>> vertices(argNumMeshes);
	std::vector<std::vector<uint32>> indices(argNumMeshes); // @TODO: actual indexing maybe?
	std::vector<int> p(argNumMeshes);

	for (Polygon& poly : polygons) {
		uint16 id = poly._matId;
		if (id >= argNumMeshes)
			shadeErrorFatal("CSG Error: ID should never be > the number of meshes");

		const std::vector<Vertex>& polyVertices = poly.getVertices();
		for (uint8 i = 2; i < polyVertices.size(); ++i) { // Creates triangle fan.
			vertices[id].push_back(polyVertices[0]); // @TODO: Delaunay triangulation
			indices[id].push_back(p[id]++);
			vertices[id].push_back(polyVertices[i - 1]);
			indices[id].push_back(p[id]++);
			vertices[id].push_back(polyVertices[i]);
			indices[id].push_back(p[id]++);
		}
	}
	for (uint16 i = 0; i < argNumMeshes; ++i) {
		if (vertices[i].size() != 0) {
			result[i] = new StaticMesh(&(vertices[i][0]), (uint32)vertices[i].size(), &(indices[i][0]), (uint32)indices[i].size(), false);
		} else {
			result[i] = nullptr;
		}
	}
	return result;
}

void CSGManager::deleteMeshes() {
	for (StaticMesh* pMesh : _meshes) {
		delete pMesh;
	}

	for (MeshInstance* pMesh : _meshInstances) {
		_pRenMan->removeMesh(pMesh);
		delete pMesh;
	}
	_meshes.clear();
	_meshInstances.clear();
}

}
