//
//  VertexHelper.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/BSP/VertexHelper.hxx"

namespace ShadeEngine {

void VertexHelper::flipVertex(Vertex& vertex) {
	vertex.norm *= -1;
	vertex.tang *= -1;
}

Vertex VertexHelper::interpolate(const Vertex& start, const Vertex& end, float t) {
	Vector3f pos = start.pos.lerp(end.pos, t);
	Vector2f texCoord = lerp(start.texCoord, end.texCoord, t);
	Vector3f norm = start.norm.lerp(end.norm, t);
	Vector3f tang = start.tang.lerp(end.tang, t);

	return Vertex(pos, texCoord, norm, tang);
}

}
