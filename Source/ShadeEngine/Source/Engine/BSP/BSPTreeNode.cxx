//
//  BSPTreeNode.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/BSP/BSPTreeNode.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

BSPTreeNode::BSPTreeNode() {
	_pFront = nullptr;
	_pBack = nullptr;
}

BSPTreeNode::BSPTreeNode(const std::vector<Polygon>& list) {
	_pFront = nullptr;
	_pBack = nullptr;
	build(list);
}

BSPTreeNode::~BSPTreeNode() {
	if (_pFront != nullptr) {
		delete _pFront;
	}
	if (_pBack != nullptr) {
		delete _pBack;
	}
}

BSPTreeNode* BSPTreeNode::clone() const {
	BSPTreeNode* result = new BSPTreeNode();
	result->_polygons = std::vector<Polygon>(_polygons);
	result->_plane = Plane(_plane);
	if (_pFront != nullptr) {
		result->_pFront = _pFront->clone();
	} // don't need else because front gets set to null in constructor
	if (_pBack != nullptr) {
		result->_pBack = _pBack->clone();
	}
	return result;
}

void BSPTreeNode::clipTo(BSPTreeNode* other) {
	_polygons = other->clipPolygons(_polygons);
	if (_pFront != nullptr) {
		_pFront->clipTo(other);
	}
	if (_pBack != nullptr) {
		_pBack->clipTo(other);
	}
}

void BSPTreeNode::invert() {
	for (Polygon& poly : _polygons) {
		poly.flip();
	}
	_plane.flip();
	if (_pFront != nullptr) {
		_pFront->invert();
	}
	if (_pBack != nullptr) {
		_pBack->invert();
	}
	std::swap(_pFront, _pBack);
}

void BSPTreeNode::build(const std::vector<Polygon>& list) {
	if (list.size() == 0) {
		return;
	}
	if (!_plane.hasValidNormal()) {
		_plane = Plane(list[0].getPlane()); ///Should use a heuristic
	}
	std::vector<Polygon> listFront;
	std::vector<Polygon> listBack;
	for (Polygon poly : list) {
		_plane.splitPolygon(poly, _polygons, _polygons, listFront, listBack);
	}
	if (listFront.size() != 0) {
		if (_pFront == nullptr) {
			_pFront = new BSPTreeNode();
		}
		_pFront->build(listFront);
	}
	if (listBack.size() != 0) {
		if (_pBack == nullptr) {
			_pBack = new BSPTreeNode(); // @LEAK
		}
		_pBack->build(listBack);
	}
}

std::vector<Polygon> BSPTreeNode::clipPolygons(std::vector<Polygon>& list) {
	if (!_plane.hasValidNormal()) {
		return list;
	}
	std::vector<Polygon> listFront;
	std::vector<Polygon> listBack;
	for (Polygon poly : list) {
		_plane.splitPolygon(poly, listFront, listBack, listFront, listBack);
	}
	if (_pFront != nullptr) {
		listFront = _pFront->clipPolygons(listFront);
	}
	if (_pBack != nullptr) {
		listBack = _pBack->clipPolygons(listBack);
	} else {
		listBack.clear();
	}
	// put listBack into the back of listFront
	listFront.insert(listFront.end(), listBack.begin(), listBack.end());

	return listFront;
}

std::vector<Polygon> BSPTreeNode::allPolygons() {
	std::vector<Polygon> list = _polygons;
	std::vector<Polygon> listFront;
	std::vector<Polygon> listBack;
	if (_pFront != nullptr) {
		listFront = _pFront->allPolygons();
	}
	if (_pBack != nullptr) {
		listBack = _pBack->allPolygons();
	}
	list.insert(list.end(), listFront.begin(), listFront.end());
	list.insert(list.end(), listBack.begin(), listBack.end());
	return list;
}

}
