//
//  Plane.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/BSP/Plane.hxx"
#include "ShadeENGINE/Engine/BSP/Polygon.hxx"
#include "ShadeENGINE/Engine/BSP/VertexHelper.hxx"

namespace ShadeEngine {

const float COPLANAR_TOLERANCE = 0.00001f;

Plane::Plane(Vector3f a, Vector3f b, Vector3f c) {
	_normal = b - a;
	_normal = _normal.cross(c - a);
	_normal.normalize();

	_offset = _normal.dot(a);
}

Plane::Plane(const Plane& other) {
	_normal = Vector3f(other._normal);
	_offset = other._offset;
}

bool Plane::hasValidNormal() const {
	return _normal.lengthSq() > 0.f;
}

void Plane::flip() {
	_normal *= -1;
	_offset *= -1;
}

void Plane::splitPolygon(Polygon& polygon, std::vector<Polygon>& coplanarFront,
                         std::vector<Polygon>& coplanarBack, std::vector<Polygon>& front,
                         std::vector<Polygon>& back) const {
	enum : uint8 {
		COPLANAR = 0,
		FRONT = 1,
		BACK = 2,
		SPANNING = 3
	};

	std::vector<Vertex> vertices = polygon.getVertices();

	uint8 polygonType = 0;
	std::vector<uint8> vertexTypes;

	// Set each vertex's type (Coplanar, front, or back)
	// Uses the vertices' types to classify the polygon.
	for (Vertex vert : vertices) {
		float distance = _normal.dot(vert.pos) - _offset;
		uint8 type = 0;
		if (distance < -COPLANAR_TOLERANCE) {
			type = BACK;
		} else if (distance > COPLANAR_TOLERANCE) {
			type = FRONT;
		} else {
			type = COPLANAR;
		}
		polygonType |= type;
		vertexTypes.push_back(type);
	}


	// Put the polygon into the appropriate list, or split it, if it is spanning.

	switch (polygonType) {
		case COPLANAR:
			if (_normal.dot(polygon.getPlane()._normal) > 0) {
				coplanarFront.push_back(polygon);
			} else {
				coplanarBack.push_back(polygon);
			}
			break;
		case FRONT:
			front.push_back(polygon);
			break;
		case BACK:
			back.push_back(polygon);
			break;
		case SPANNING:
			std::vector<Vertex> frontTemp;
			std::vector<Vertex> backTemp;

			for (uint8 i = 0; i < vertices.size(); i++) {
				int j = i + 1;
				if (j >= vertices.size()) {
					j = 0;
				}

				uint8 indexType = vertexTypes[i];
				uint8 nextIndexType = vertexTypes[j];

				Vertex currentVert = vertices[i];
				Vertex nextVert = vertices[j];

				if (indexType != BACK) {
					frontTemp.push_back(currentVert);
				}
				if (indexType != FRONT) {
					backTemp.push_back(currentVert);
				}
				if ((indexType | nextIndexType) == SPANNING) {
					float t = (_offset - _normal.dot(currentVert.pos)) / _normal.dot(nextVert.pos - currentVert.pos);
					Vertex newVert = VertexHelper::interpolate(currentVert, nextVert, t);
					frontTemp.push_back(newVert);
					backTemp.push_back(newVert);
				}
			}
			if (frontTemp.size() >= 3) {
				front.push_back(Polygon(frontTemp, polygon._matId));
			}
			if (backTemp.size() >= 3) {
				back.push_back(Polygon(backTemp, polygon._matId));
			}
			break;
	}


}

}
