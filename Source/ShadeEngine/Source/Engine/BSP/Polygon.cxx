//
//  Polygon.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/BSP/Polygon.hxx"
#include <algorithm>

namespace ShadeEngine {

Polygon::Polygon(const std::vector<Vertex>& vertexList, uint16 matId) : _vertices(vertexList),
		_plane(_vertices[0].pos, _vertices[1].pos, _vertices[2].pos),
		_matId(matId) { }

Polygon::Polygon(const Polygon& other) {
	_vertices = std::vector<Vertex>(other._vertices);
	_plane = Plane(other._plane);
	_matId = other._matId;
}

void Polygon::flip() {
	std::reverse(_vertices.begin(), _vertices.end());
	for (Vertex& vert : _vertices) {
		vert.norm *= -1;
		vert.tang *= -1;
	}
	_plane.flip();
}

}
