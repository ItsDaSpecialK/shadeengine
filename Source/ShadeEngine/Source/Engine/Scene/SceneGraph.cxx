//
//  SceneGraph.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Scene/SceneGraph.hxx"

namespace ShadeEngine {

SceneGraph::SceneGraph() { }

SceneGraph::~SceneGraph() {
	for (CObject* pObject : (*this)) {
		delete pObject;
	}
}

void SceneGraph::update(uint16 delta) {
	for (CObject* pObject : (*this)) {
		pObject->update(delta);
	}
}

}
