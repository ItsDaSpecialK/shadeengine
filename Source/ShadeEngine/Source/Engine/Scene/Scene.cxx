//
//  Scene.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Scene/Scene.hxx"
#include "ShadeENGINE/Actor/Pawn.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Engine/Object/StaticObject.hxx"
#include "ShadeENGINE/Engine/CSGManager.hxx"
#include "ShadeENGINE/Engine/Object/CSG/BoxBrush.hxx"

namespace ShadeEngine {

Scene::~Scene() {
	for (SpawnPoint* pSpawnPoint : _spawnPoints) delete pSpawnPoint;
	if (_pCSGMan != nullptr) {
		delete _pCSGMan;
	}
}

void Scene::attach(Actor* pActor, Actor* pParent) {
	// 1. Check That they're both in the same scene
	if (pActor->getScene() != pParent->getScene())
		shadeErrorFatal("Tried to attach Actor to actor from another scene");
	// 2. Get World Pos

	// 3. Find Attachee Position

	// 4. Calculate the difference

	// 5. Set New Pos to the difference

	// 6. Attach to Actor's linkedList
	pActor->getTransform().setParent(&pParent->getTransform());
}

void Scene::detach(Actor* pActor) {
	// 1. Get World Pos

	// 2. Set New Pos to world pos

	// 3. Attach to root
	pActor->getTransform().setParent(nullptr);
}

void Scene::destroy(Actor* pActor) {
	_sceneGraph.remove(pActor);
	delete pActor;
}

CObject* Scene::createObject(ShadeClass* pClass, const Vector3f& position, const Quaternion& rotation, const Vector3f& scale) {
	CObject* pObject = (CObject*)pClass->createNew(this); // @LEAK-id0
	pObject->getTransform().setPos(position);
	pObject->getTransform().setRot(rotation);
	pObject->getTransform().setScale(scale);
	pObject->init();
	_sceneGraph.add(pObject);
	return pObject;
}

CSGManager* Scene::getCSGManager() {
	if (_pCSGMan == nullptr) {
		_pCSGMan = new CSGManager(&_renderMan);
	}
	return _pCSGMan;
}

}
