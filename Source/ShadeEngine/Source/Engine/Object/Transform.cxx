//
//  Transform.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/18/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Object/Transform.hxx"

namespace ShadeEngine {

Transform::Transform(const Vector3f& position, const Quaternion& rotation, const Vector3f& size) {
	_pos = position;
	_rot = rotation;
	_scale = size;
	_pParent = nullptr;
}

Matrix Transform::getTransformationMatrix() const {
	Matrix translation;
	Matrix scaling;
	translation.initToTranslation(_pos);
	scaling.initToScale(_scale);

	return scaling * _rot.toRotationMatrix() * translation * getParentMatrix();
}

void Transform::rotate(const Quaternion& rotation) {
	_rot = rotation * _rot;
	_rot.normalize();
}

Matrix Transform::getParentMatrix() const {
	if (_pParent == nullptr) {
		Matrix result;
		result.initToIdentity();
		return result;
	}
	return _pParent->getTransformationMatrix();
}

Quaternion Transform::getTransformedRot() const {
	Quaternion parentRot;

	if (_pParent)
		parentRot = _pParent->getTransformedRot();

	return parentRot * _rot;
}

}
