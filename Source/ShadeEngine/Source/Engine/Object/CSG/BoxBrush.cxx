//
//  BoxBrush.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/22/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Object/CSG/BoxBrush.hxx"

namespace ShadeEngine {

BoxBrush::BoxBrush(Scene* pScene) : Brush(pScene) {
	_polygons.push_back(Polygon({ Vertex(Vector3f( 1.f, 1.f, 1.f), Vector2f(1.f, 1.f), Vector3f(0, 0, 1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, 1.f, 1.f), Vector2f(0, 1.f), Vector3f(0, 0, 1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, -1.f, 1.f), Vector2f(0, 0), Vector3f(0, 0, 1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f( 1.f, -1.f, 1.f), Vector2f(1.f, 0), Vector3f(0, 0, 1.f), Vector3f(1.f, 0, 0)) }));

	_polygons.push_back(Polygon({ Vertex(Vector3f(-1.f, 1.f, 1.f), Vector2f(1.f, 1.f), Vector3f(0, 1.f, 0), Vector3f(-1.f, 0, 0)),
	                              Vertex(Vector3f( 1.f, 1.f, 1.f), Vector2f(0, 1.f), Vector3f(0, 1.f, 0), Vector3f(-1.f, 0, 0)),
	                              Vertex(Vector3f( 1.f, 1.f, -1.f), Vector2f(0, 0), Vector3f(0, 1.f, 0), Vector3f(-1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, 1.f, -1.f), Vector2f(1.f, 0), Vector3f(0, 1.f, 0), Vector3f(-1.f, 0, 0)) }));

	_polygons.push_back(Polygon({ Vertex(Vector3f(-1.f, -1.f, 1.f), Vector2f(1.f, 1.f), Vector3f(-1.f, 0, 0), Vector3f(0, -1.f, 0)),
	                              Vertex(Vector3f(-1.f, 1.f, 1.f), Vector2f(0, 1.f), Vector3f(-1.f, 0, 0), Vector3f(0, -1.f, 0)),
	                              Vertex(Vector3f(-1.f, 1.f, -1.f), Vector2f(0, 0), Vector3f(-1.f, 0, 0), Vector3f(0, -1.f, 0)),
	                              Vertex(Vector3f(-1.f, -1.f, -1.f), Vector2f(1.f, 0), Vector3f(-1.f, 0, 0), Vector3f(0, -1.f, 0)) }));

	_polygons.push_back(Polygon({ Vertex(Vector3f( 1.f, 1.f, 1.f), Vector2f(1.f, 1.f), Vector3f(1.f, 0, 0), Vector3f(0, 1.f, 0)),
	                              Vertex(Vector3f( 1.f, -1.f, 1.f), Vector2f(0, 1.f), Vector3f(1.f, 0, 0), Vector3f(0, 1.f, 0)),
	                              Vertex(Vector3f( 1.f, -1.f, -1.f), Vector2f(0, 0), Vector3f(1.f, 0, 0), Vector3f(0, 1.f, 0)),
	                              Vertex(Vector3f( 1.f, 1.f, -1.f), Vector2f(1.f, 0), Vector3f(1.f, 0, 0), Vector3f(0, 1.f, 0)) }));

	_polygons.push_back(Polygon({ Vertex(Vector3f( 1.f, -1.f, -1.f), Vector2f(1.f, 1.f), Vector3f(0, 0, -1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, -1.f, -1.f), Vector2f(0, 1.f), Vector3f(0, 0, -1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, 1.f, -1.f), Vector2f(0, 0), Vector3f(0, 0, -1.f), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f( 1.f, 1.f, -1.f), Vector2f(1.f, 0), Vector3f(0, 0, -1.f), Vector3f(1.f, 0, 0)) }));

	_polygons.push_back(Polygon({ Vertex(Vector3f( 1.f, -1.f, 1.f), Vector2f(1.f, 1.f), Vector3f(0, -1.f, 0), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, -1.f, 1.f), Vector2f(0, 1.f), Vector3f(0, -1.f, 0), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f(-1.f, -1.f, -1.f), Vector2f(0, 0), Vector3f(0, -1.f, 0), Vector3f(1.f, 0, 0)),
	                              Vertex(Vector3f( 1.f, -1.f, -1.f), Vector2f(1.f, 0), Vector3f(0, -1.f, 0), Vector3f(1.f, 0, 0)) }));
}

void BoxBrush::setHeight(float height) {
	Vector3f scale = getTransform().getScale();
	getTransform().setScale(Vector3f(scale.getX(), height / 2.f, scale.getZ()));
}

void BoxBrush::setWidth(float width) {
	Vector3f scale = getTransform().getScale();
	getTransform().setScale(Vector3f(width / 2.f, scale.getY(), scale.getZ()));
}

void BoxBrush::setDepth(float depth) {
	Vector3f scale = getTransform().getScale();
	getTransform().setScale(Vector3f(scale.getX(), scale.getY(), depth / 2.f));
}

}
