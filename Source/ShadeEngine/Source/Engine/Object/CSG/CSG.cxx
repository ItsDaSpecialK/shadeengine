//
//  CSG.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Object/CSG/CSG.hxx"

namespace ShadeEngine {

BSPTreeNode* CSG::add(BSPTreeNode* pA, BSPTreeNode* pB) {
	pA->clipTo(pB);
	pB->clipTo(pA);
	pB->invert();
	pB->clipTo(pA);
	pB->invert();
	std::vector<Polygon> bPolys = pB->allPolygons();
	pA->build(bPolys);
	std::vector<Polygon> aPolys = pA->allPolygons();
	BSPTreeNode* result = new BSPTreeNode(aPolys);
	delete pA;
	delete pB;
	return result;
}

BSPTreeNode* CSG::subtract(BSPTreeNode* pA, BSPTreeNode* pB) {
	pA->invert();
	pA->clipTo(pB);
	pB->clipTo(pA);
	pB->invert();
	pB->clipTo(pA);
	pB->invert();
	std::vector<Polygon> bPolys = pB->allPolygons();
	pA->build(bPolys);
	pA->invert();
	std::vector<Polygon> aPolys = pA->allPolygons();
	BSPTreeNode* result = new BSPTreeNode(aPolys);
	delete pA;
	delete pB;
	return result;
}

BSPTreeNode* CSG::intersect(BSPTreeNode* pA, BSPTreeNode* pB) {
	pA->invert();
	pB->clipTo(pA);
	pB->invert();
	pA->clipTo(pB);
	pB->clipTo(pA);
	std::vector<Polygon> bPolys = pB->allPolygons();
	pA->build(bPolys);
	pA->invert();
	std::vector<Polygon> aPolys = pA->allPolygons();
	BSPTreeNode* result = new BSPTreeNode(aPolys);
	delete pA;
	delete pB;
	return result;
}

}
