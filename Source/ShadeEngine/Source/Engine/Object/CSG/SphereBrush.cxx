//
//  SphereBrush.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/08/18.
//
//  Copyright (c) 2018 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Engine/Object/CSG/SphereBrush.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"
#include "ShadeENGINE/Engine/BSP/VertexHelper.hxx"

namespace ShadeEngine {

SphereBrush::SphereBrush(Scene* pScene) : Brush(pScene) {
	init();
}


Vertex midpoint(const Vertex& first, const Vertex& second, int8 depth) {
	Vector3f pos = (first.pos + second.pos) / 2.f;

	const float offsets[5] = { 0.850650847f, 0.961938381f, 0.990438879f, 0.997606874f, 0.999401510f };

	if (depth > 4) {
		pos.normalize();
	} else {
		pos /= offsets[depth];
	}

	float texCoordX = (first.texCoord.getX() + second.texCoord.getX()) / 2.f;
	float texCoordY = (first.texCoord.getY() + second.texCoord.getY()) / 2.f;
	Vector2f texCoord = (Vector2f(texCoordX, texCoordY));
	Vector3f norm = (first.norm + second.norm) / 2.f;
	Vector3f tang = (first.tang + second.tang) / 2.f;

	return Vertex(pos, texCoord, norm, tang);
}

void SphereBrush::splitTriangle(Polygon poly, uint8 argTessellation) {
	if (argTessellation == 0) {
		argTessellation = 1;
	}

	const Vertex& v1 = poly.getVertices()[0];
	const Vertex& v2 = poly.getVertices()[1];
	const Vertex& v3 = poly.getVertices()[2];

	std::vector<std::vector<Vertex>> vertices(argTessellation + 1);
	vertices[0].push_back(v1);
	for (uint8 i = 1; i <= argTessellation; ++i) {
		Vertex left = VertexHelper::interpolate(v1, v2, (float)i / float(argTessellation));
		Vertex right = VertexHelper::interpolate(v1, v3, (float)i / float(argTessellation));

		left.pos.normalize();
		right.pos.normalize();

		for (uint8 j = 0; j <= i; ++j) {
			Vertex temp = VertexHelper::interpolate(left, right, (float)j / (float)i);
			temp.pos.normalize();
			vertices[i].push_back(temp);
		}
	}

	for (uint8 i = 0; i < argTessellation; ++i) {
		_polygons.push_back(Polygon({ vertices[i][0], vertices[i + 1][0], vertices[i + 1][1] }));
		for (uint8 j = 1; j <= i; ++j) {
			_polygons.push_back(Polygon({ vertices[i][j], vertices[i][j - 1], vertices[i + 1][j] }));
			_polygons.push_back(Polygon({ vertices[i][j], vertices[i + 1][j], vertices[i + 1][j + 1] }));
		}
	}

}

void SphereBrush::init(uint8 tessellation) {
	_polygons.clear();

	splitTriangle(Polygon({ Vertex(Vector3f(0.000000f, 0.000000f, 1.000000f), Vector2f(0.100000f, 1.000000f), Vector3f(0.000000f, 0.000000f, 1.000000f), Vector3f(0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.894427f, 0.000000f, 0.447214f), Vector2f(0.000000f, 0.666667f), Vector3f(0.894427f, 0.000000f, 0.447214f), Vector3f(0.000000f, 1.000000f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, 0.850651f, 0.447214f), Vector2f(0.200000f, 0.666667f), Vector3f(0.276393f, 0.850651f, 0.447214f), Vector3f(-0.951057f, 0.309017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.000000f, 0.000000f, 1.000000f), Vector2f(0.300000f, 1.000000f), Vector3f(0.000000f, 0.000000f, 1.000000f), Vector3f(0.951056f, 0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, 0.850651f, 0.447214f), Vector2f(0.200000f, 0.666667f), Vector3f(0.276393f, 0.850651f, 0.447214f), Vector3f(-0.951057f, 0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector2f(0.400000f, 0.666667f), Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector3f(-0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.000000f, 0.000000f, 1.000000f), Vector2f(0.500000f, 1.000000f), Vector3f(0.000000f, 0.000000f, 1.000000f), Vector3f(0.000000f, 1.000000f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector2f(0.400000f, 0.666667f), Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector3f(-0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector2f(0.600000f, 0.666667f), Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector3f(0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.000000f, 0.000000f, 1.000000f), Vector2f(0.700000f, 1.000000f), Vector3f(0.000000f, 0.000000f, 1.000000f), Vector3f(-0.951056f, 0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector2f(0.600000f, 0.666667f), Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector3f(0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, -0.850651f, 0.447214f), Vector2f(0.800000f, 0.666667f), Vector3f(0.276393f, -0.850651f, 0.447214f), Vector3f(0.951057f, 0.309017f, -0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.000000f, 0.000000f, 1.000000f), Vector2f(0.900000f, 1.000000f), Vector3f(0.000000f, 0.000000f, 1.000000f), Vector3f(-0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, -0.850651f, 0.447214f), Vector2f(0.800000f, 0.666667f), Vector3f(0.276393f, -0.850651f, 0.447214f), Vector3f(0.951057f, 0.309017f, -0.000000f)),
	                        Vertex(Vector3f(0.894427f, 0.000000f, 0.447214f), Vector2f(1.000000f, 0.666667f), Vector3f(0.894427f, 0.000000f, 0.447214f), Vector3f(0.000000f, 1.000000f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector2f(0.300000f, 0.333333f), Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector3f(-0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.723607f, 0.525731f, -0.447214f), Vector2f(0.100000f, 0.333333f), Vector3f(0.723607f, 0.525731f, -0.447214f), Vector3f(-0.587785f, 0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.000000f, 0.000000f, -1.000000f), Vector2f(0.200000f, 0.000000f), Vector3f(0.000000f, 0.000000f, -1.000000f), Vector3f(-0.951056f, 0.309017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector2f(0.500000f, 0.333333f), Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector3f(-0.000000f, -1.000000f, 0.000000f)),
	                        Vertex(Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector2f(0.300000f, 0.333333f), Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector3f(-0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.000000f, 0.000000f, -1.000000f), Vector2f(0.400000f, 0.000000f), Vector3f(0.000000f, 0.000000f, -1.000000f), Vector3f(-0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector2f(0.700000f, 0.333333f), Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector3f(0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector2f(0.500000f, 0.333333f), Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector3f(-0.000000f, -1.000000f, 0.000000f)),
	                        Vertex(Vector3f(0.000000f, 0.000000f, -1.000000f), Vector2f(0.600000f, 0.000000f), Vector3f(0.000000f, 0.000000f, -1.000000f), Vector3f(0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, -0.525731f, -0.447214f), Vector2f(0.900000f, 0.333333f), Vector3f(0.723607f, -0.525731f, -0.447214f), Vector3f(0.587785f, 0.809017f, -0.000000f)),
	                        Vertex(Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector2f(0.700000f, 0.333333f), Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector3f(0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.000000f, 0.000000f, -1.000000f), Vector2f(0.800000f, 0.000000f), Vector3f(0.000000f, 0.000000f, -1.000000f), Vector3f(0.951056f, 0.309017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, 0.525731f, -0.447214f), Vector2f(1.100000f, 0.333333f), Vector3f(0.723607f, 0.525731f, -0.447214f), Vector3f(-0.587785f, 0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.723607f, -0.525731f, -0.447214f), Vector2f(0.900000f, 0.333333f), Vector3f(0.723607f, -0.525731f, -0.447214f), Vector3f(0.587785f, 0.809017f, -0.000000f)),
	                        Vertex(Vector3f(0.000000f, 0.000000f, -1.000000f), Vector2f(1.000000f, 0.000000f), Vector3f(0.000000f, 0.000000f, -1.000000f), Vector3f(0.000000f, 1.000000f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, 0.525731f, -0.447214f), Vector2f(0.100000f, 0.333333f), Vector3f(0.723607f, 0.525731f, -0.447214f), Vector3f(-0.587785f, 0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, 0.850651f, 0.447214f), Vector2f(0.200000f, 0.666667f), Vector3f(0.276393f, 0.850651f, 0.447214f), Vector3f(-0.951057f, 0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.894427f, 0.000000f, 0.447214f), Vector2f(0.000000f, 0.666667f), Vector3f(0.894427f, 0.000000f, 0.447214f), Vector3f(0.000000f, 1.000000f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector2f(0.300000f, 0.333333f), Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector3f(-0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector2f(0.400000f, 0.666667f), Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector3f(-0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, 0.850651f, 0.447214f), Vector2f(0.200000f, 0.666667f), Vector3f(0.276393f, 0.850651f, 0.447214f), Vector3f(-0.951057f, 0.309017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector2f(0.500000f, 0.333333f), Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector3f(-0.000000f, -1.000000f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector2f(0.600000f, 0.666667f), Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector3f(0.587785f, -0.809017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector2f(0.400000f, 0.666667f), Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector3f(-0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector2f(0.700000f, 0.333333f), Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector3f(0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, -0.850651f, 0.447214f), Vector2f(0.800000f, 0.666667f), Vector3f(0.276393f, -0.850651f, 0.447214f), Vector3f(0.951057f, 0.309017f, -0.000000f)),
	                        Vertex(Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector2f(0.600000f, 0.666667f), Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector3f(0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, -0.525731f, -0.447214f), Vector2f(0.900000f, 0.333333f), Vector3f(0.723607f, -0.525731f, -0.447214f), Vector3f(0.587785f, 0.809017f, -0.000000f)),
	                        Vertex(Vector3f(0.894427f, 0.000000f, 0.447214f), Vector2f(1.000000f, 0.666667f), Vector3f(0.894427f, 0.000000f, 0.447214f), Vector3f(0.000000f, 1.000000f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, -0.850651f, 0.447214f), Vector2f(0.800000f, 0.666667f), Vector3f(0.276393f, -0.850651f, 0.447214f), Vector3f(0.951057f, 0.309017f, -0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, 0.525731f, -0.447214f), Vector2f(0.100000f, 0.333333f), Vector3f(0.723607f, 0.525731f, -0.447214f), Vector3f(-0.587785f, 0.809017f, 0.000000f)),
	                        Vertex(Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector2f(0.300000f, 0.333333f), Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector3f(-0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.276393f, 0.850651f, 0.447214f), Vector2f(0.200000f, 0.666667f), Vector3f(0.276393f, 0.850651f, 0.447214f), Vector3f(-0.951057f, 0.309017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector2f(0.300000f, 0.333333f), Vector3f(-0.276393f, 0.850651f, -0.447214f), Vector3f(-0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector2f(0.500000f, 0.333333f), Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector3f(-0.000000f, -1.000000f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector2f(0.400000f, 0.666667f), Vector3f(-0.723607f, 0.525731f, 0.447214f), Vector3f(-0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector2f(0.500000f, 0.333333f), Vector3f(-0.894427f, 0.000000f, -0.447214f), Vector3f(-0.000000f, -1.000000f, 0.000000f)),
	                        Vertex(Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector2f(0.700000f, 0.333333f), Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector3f(0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector2f(0.600000f, 0.666667f), Vector3f(-0.723607f, -0.525731f, 0.447214f), Vector3f(0.587785f, -0.809017f, 0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector2f(0.700000f, 0.333333f), Vector3f(-0.276393f, -0.850651f, -0.447214f), Vector3f(0.951057f, -0.309017f, 0.000000f)),
	                        Vertex(Vector3f(0.723607f, -0.525731f, -0.447214f), Vector2f(0.900000f, 0.333333f), Vector3f(0.723607f, -0.525731f, -0.447214f), Vector3f(0.587785f, 0.809017f, -0.000000f)),
	                        Vertex(Vector3f(0.276393f, -0.850651f, 0.447214f), Vector2f(0.800000f, 0.666667f), Vector3f(0.276393f, -0.850651f, 0.447214f), Vector3f(0.951057f, 0.309017f, -0.000000f)) }), tessellation);

	splitTriangle(Polygon({ Vertex(Vector3f(0.723607f, -0.525731f, -0.447214f), Vector2f(0.900000f, 0.333333f), Vector3f(0.723607f, -0.525731f, -0.447214f), Vector3f(0.587785f, 0.809017f, -0.000000f)),
	                        Vertex(Vector3f(0.723607f, 0.525731f, -0.447214f), Vector2f(1.100000f, 0.333333f), Vector3f(0.723607f, 0.525731f, -0.447214f), Vector3f(-0.587785f, 0.809017f, 0.000000f)),
	                        Vertex(Vector3f(0.894427f, 0.000000f, 0.447214f), Vector2f(1.000000f, 0.666667f), Vector3f(0.894427f, 0.000000f, 0.447214f), Vector3f(0.000000f, 1.000000f, 0.000000f)) }), tessellation);
}

}
