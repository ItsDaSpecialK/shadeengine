//
//  Pawn.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/13/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Actor/Pawn.hxx"
#include "ShadeENGINE/Engine/Game/Controller.hxx"

namespace ShadeEngine {

Pawn::Pawn(Scene* pScene) : Actor(pScene) {
	_pCameraComponent = addComponent<CameraComponent>();
}

void Pawn::update(uint16 delta) {
	Actor::update(delta);
	for (CComponent* pComp : _components) {
		if (pComp->takesControllerRotation())
			pComp->getTransform().setRot(_pController->getTransform().getRot());
	}
}

}
