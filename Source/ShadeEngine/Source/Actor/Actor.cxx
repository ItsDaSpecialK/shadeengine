//
//  Actor.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Actor/Actor.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"
#include "ShadeENGINE/Component/MeshComponent.hxx"

namespace ShadeEngine {

Actor::Actor(Scene* pScene) : CDynamicObject(pScene) {
	_pScene = pScene;
}

Actor::~Actor() {
	for (CComponent* pComp : _components) delete pComp;
}

inline void Actor::attachTo(Actor* pOther) {
	_pScene->attach(this, pOther);
}
inline void Actor::detatch() {
	_pScene->detach(this);
}

void Actor::update(uint16 delta) {
	for (CComponent* pComp : _components) {
		pComp->update(delta);
	}
}

void Actor::attachComponent(CComponent* pComponent) {
	_components.add(pComponent);
	pComponent->getTransform().setParent(&(getTransform()));
	MeshComponent* pMeshComp = dynamic_cast<MeshComponent*>(pComponent);
	if (pMeshComp != nullptr)
		_pScene->addMesh(pMeshComp->getMeshInstance());
}

}
