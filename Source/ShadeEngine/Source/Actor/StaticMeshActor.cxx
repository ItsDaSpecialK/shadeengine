//
//  StaticMeshActor.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/12/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Actor/StaticMeshActor.hxx"

namespace ShadeEngine {

StaticMeshActor::StaticMeshActor(Scene* pScene) : Actor(pScene) {
	_pMeshComp = addComponent<StaticMeshComponent>();
}

}
