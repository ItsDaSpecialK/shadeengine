//
//  MeshComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/6/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/MeshComponent.hxx"

using namespace ShadeEngine;

MeshComponent::MeshComponent(Scene* pScene) : CComponent(pScene) {
	_pMesh = new MeshInstance();
}

void MeshComponent::update(uint16 delta) {
	_pMesh->setTransformationMatrix(getTransform().getTransformationMatrix());
}
