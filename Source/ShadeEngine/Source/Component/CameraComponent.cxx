//
//  Camera.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/20/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/CameraComponent.hxx"

using namespace ShadeEngine;

CameraComponent::CameraComponent(Scene* pScene) : CComponent(pScene) {
	_pCamera = new PerspectiveCamera();
}

void CameraComponent::update(uint16 delta) {
	_pCamera->setPos(getTransform().getTransformedPos());
	_pCamera->setRot(getTransform().getTransformedRot());
}
