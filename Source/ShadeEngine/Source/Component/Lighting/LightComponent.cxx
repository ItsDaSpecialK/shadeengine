//
//  LightComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/Lighting/LightComponent.hxx"

namespace ShadeEngine {

CLightComponent::CLightComponent(Scene* pScene) : CComponent(pScene) {
	_pScene = pScene;
	_bCastsShadow = true;
}
}
