//
//  SpotLightComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/Lighting/SpotLightComponent.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

SpotLightComponent::SpotLightComponent(Scene* pScene) : CLightComponent(pScene) {
	_pLight = new SpotLight();
	pScene->addSpotLight((SpotLight*)_pLight);
}

void SpotLightComponent::update(uint16 delta) {
	((SpotLight*)_pLight)->setPos(getTransform().getTransformedPos());
	((SpotLight*)_pLight)->setRot(getTransform().getTransformedRot());
}

}
