//
//  DirectionalLightComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/9/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/Lighting/DirectionalLightComponent.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

DirectionalLightComponent::DirectionalLightComponent(Scene* pScene) : CLightComponent(pScene) {
	_pLight = new DirectionalLight();
	pScene->addDirectionalLight((DirectionalLight*)_pLight);
}

void DirectionalLightComponent::update(uint16 delta) {
	((DirectionalLight*)_pLight)->setRot(getTransform().getTransformedRot());
}

}
