//
//  PointLightComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/Lighting/PointLightComponent.hxx"
#include "ShadeENGINE/Engine/Scene/Scene.hxx"

namespace ShadeEngine {

PointLightComponent::PointLightComponent(Scene* pScene) : CLightComponent(pScene) {
	_pLight = new PointLight();
	pScene->addPointLight((PointLight*)_pLight);
}

void PointLightComponent::update(uint16 delta) {
	((PointLight*)_pLight)->setPos(getTransform().getTransformedPos());
}

}
