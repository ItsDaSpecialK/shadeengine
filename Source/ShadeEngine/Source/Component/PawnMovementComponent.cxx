//
//  PawnMovementComponent.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/8/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Component/PawnMovementComponent.hxx"

namespace ShadeEngine {

void PawnMovementComponent::moveFwd(float value) {
	Transform* pTrans = getTransform().getParent();
	Vector3f dist = pTrans->getRot().getForward();
	dist = dist * value;
	pTrans->setPos(pTrans->getPos() + dist);
}

void PawnMovementComponent::moveRight(float value) {
	Transform* pTrans = getTransform().getParent();
	Vector3f dist = pTrans->getRot().getRight();
	dist = dist * value;
	pTrans->setPos(pTrans->getPos() + dist);
}

void PawnMovementComponent::moveUp(float value) {
	Transform* pTrans = getTransform().getParent();
	Vector3f dist = pTrans->getRot().getUp();
	dist = dist * value;
	pTrans->setPos(pTrans->getPos() + dist);
}

void PawnMovementComponent::addPitchInput(float value) {
	Transform* pTrans = getTransform().getParent();
	pTrans->rotate(pTrans->getRot().getRight(), value);
}

void PawnMovementComponent::addYawInput(float value) {
	Transform* pTrans = getTransform().getParent();
	pTrans->rotate(Vector3f(0, 0, -1), value);
}

void PawnMovementComponent::addRollInput(float value) {
	Transform* pTrans = getTransform().getParent();
	pTrans->rotate(pTrans->getRot().getForward(), value);
}

}
