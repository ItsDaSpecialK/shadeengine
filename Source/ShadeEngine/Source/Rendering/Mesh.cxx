//
//  Mesh.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/6/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Mesh.hxx"

using namespace ShadeEngine;

CMesh::CMesh() {
	glGenVertexArrays(1, &_vao);
	glGenBuffers(1, &_vbo);
	glGenBuffers(1, &_ibo);
}

CMesh::~CMesh() {
	glDeleteBuffers(1, &_vbo);
	glDeleteBuffers(1, &_ibo);
	glDeleteVertexArrays(1, &_vao);
}
