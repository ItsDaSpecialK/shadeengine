//
//  PerspectiveCamera.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/PerspectiveCamera.hxx"

namespace ShadeEngine {

PerspectiveCamera::PerspectiveCamera(float argAspectRatio, float argFOV, float argZNear, float argZFar) {
	_aspectRatio = argAspectRatio;
	_fov = argFOV;
	_zNear = argZNear;
	_zFar = argZFar;
	calcProjection();
}

void PerspectiveCamera::calcProjection() {
	Matrix projection;
	projection.initToPerspective(_fov, _aspectRatio, _zNear, _zFar);
	setProjection(projection);
}
}
