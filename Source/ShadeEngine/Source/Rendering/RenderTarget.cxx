//
//  RenderTarget.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 3/26/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#ifdef WIN32
#include "ShadeENGINE/Rendering/GLExtensions.hxx"
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include "ShadeENGINE/Rendering/RenderTarget.hxx"
#include "SDL2/SDL_opengl.h"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

RenderTarget::RenderTarget(uint8 argNumTextures,
                           uint16 argWidth,
                           uint16 argHeight,
                           float* argFilters,
                           GLint* argFormats) {
	if (argNumTextures > 16) {
		shadeErrorFatal("Error. There can't be more than 16 textures");
	}

	_depthRenBuf = 0;
	_numTextures = argNumTextures;
	_width = argWidth;
	_height = argHeight;
	bool bHasDepthTexture = false;

	glGenFramebuffers(1, &_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

	_pTextures = new Texture[argNumTextures];
	uint32* pDrawBuffers = new uint32[_numTextures];
	uint8 numColorBuffers = 0;

	for (uint8 i = 0; i < _numTextures; i++) {
		float textureFilter;
		if (argFilters == nullptr)
			textureFilter = GL_LINEAR;
		else
			textureFilter = argFilters[i];

		float textureFormat;
		if (argFormats == nullptr)
			textureFormat = GL_RGB;
		else
			textureFormat = argFormats[i];





		if (textureFormat == GL_STENCIL)
			shadeErrorFatal("Error. Stencil buffer not yet supported");

		if (textureFormat != GL_DEPTH_COMPONENT) {
			// Color Attachment
			_pTextures[i].init(_width, _height, GL_TEXTURE_2D, textureFilter, textureFormat);

			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
			                     _pTextures[i].getTexID(), 0);
			pDrawBuffers[numColorBuffers++] = GL_COLOR_ATTACHMENT0 + i;
		} else {
			// Depth Attachment
			if (bHasDepthTexture)
				shadeErrorFatal("Cannot have multiple depth Textures");
			bHasDepthTexture = true;

			_pTextures[i].init(_width, _height, GL_TEXTURE_2D, textureFilter, GL_DEPTH_COMPONENT, GL_CLAMP_TO_BORDER);
			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
			                     _pTextures[i].getTexID(), 0);

			GLfloat color[] = { 1, 1, 1, 1 };
			glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
		}
	}

	if (!bHasDepthTexture) {
		glGenRenderbuffers(1, &_depthRenBuf);
		glBindRenderbuffer(GL_RENDERBUFFER, _depthRenBuf);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenBuf);
	}

	if (numColorBuffers == 0) {
		glDrawBuffer(GL_NONE);
	} else {
		glDrawBuffers(numColorBuffers, pDrawBuffers);
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << glCheckFramebufferStatus(GL_FRAMEBUFFER) << std::endl;
		shadeErrorFatal("Couldn't Create Framebuffer");
	}

	delete[] pDrawBuffers;
}

RenderTarget::~RenderTarget() {
	if (_depthRenBuf != 0) {
		glDeleteRenderbuffers(1, &_depthRenBuf);
	}
	delete[] _pTextures;
	glDeleteFramebuffers(1, &_fbo);
}

void RenderTarget::useDefault() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, Window::getWidth(), Window::getHeight());
}

void RenderTarget::useDefault(uint16 width, uint16 height) {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width, height);
}

void RenderTarget::useRenderTarget() {
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
	glViewport(0, 0, _width, _height);
}

void RenderTarget::useRenderTarget(uint16 width, uint16 height) {
	glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
	glViewport(0, 0, width, height);
}

Texture* RenderTarget::getTexture(uint8 textureIndex) const {
	if (textureIndex >= _numTextures) {
		shadeErrorFatal("Cant get texture at this index. Index is out of bounds");
	}
	return &_pTextures[textureIndex];
}

}
