//
//  SkyMeshShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/23/18.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/SkyMeshShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

#define STRINGIFY(A) #A

namespace ShadeEngine {

SkyMeshShader::SkyMeshShader() {
	addVertexShader("#version 330 core\n" STRINGIFY(layout(location = 0) in vec3 position;

	                                                out vec3 fragTexCoord;

	                                                uniform mat4 MVP;

	                                                void main() {
			fragTexCoord = position.xzy;
			gl_Position = (MVP * vec4(position, 1.0)).xyww;
		}));

	addFragmentShader("#version 330 core\n" STRINGIFY(in vec3 fragTexCoord;

	                                                  uniform samplerCube skyMesh;

	                                                  out vec4 fragColor;

	                                                  void main() {
			fragColor = texture(skyMesh, fragTexCoord);
		}));

	compileShader();
}

void SkyMeshShader::compileShader() {
	Shader::compileShader();

	_mvpUniform = getUniform("MVP");
	if (_mvpUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"MVP\"");
}

}
