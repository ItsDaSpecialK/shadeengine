//
//  ForwardPointShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/ForwardPointShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

#define STRINGIFY(A) #A

namespace ShadeEngine {

ForwardPointShader::ForwardPointShader() {
	addVertexShader("#version 330 core\n" STRINGIFY(

						layout(location = 0) in vec3 position;
						layout(location = 1) in vec2 texCoord;
						layout(location = 2) in vec3 normal;
						layout(location = 3) in vec3 tangent;

						out vec2 fragTexCoord;
						out vec3 fragWorldPos;
						out mat3 tbnMatrix;

						uniform mat4 transform;
						uniform mat4 MVP;

						void main() {
			fragTexCoord = texCoord;
			fragWorldPos = (transform * vec4(position, 1.0)).xyz;

			vec3 norm = normalize((transform * vec4(normal, 0.0)).xyz);
			vec3 tang = normalize((transform * vec4(tangent, 0.0)).xyz);
			tang = normalize(tang - dot(tang, norm) * norm);
			vec3 bitang = cross(norm, tang);
			tbnMatrix = mat3(tang, bitang, norm);

			gl_Position = MVP * vec4(position, 1.0);
		}));

	addFragmentShader("#version 330 core\n" STRINGIFY(in vec2 fragTexCoord;
	                                                  in vec3 fragWorldPos;
	                                                  in mat3 tbnMatrix;

	                                                  out vec4 fragColor;

	                                                  struct BaseLight {
			vec4 color;
			float intensity;
		};

	                                                  struct Attenuation { // physically accurate: ?
			float constant;      // 0
			float linear;        // 0
			float quadratic;     // 1
		};

	                                                  struct PointLight {
			BaseLight base;
			Attenuation atten;
			vec3 position;
			float range;
		};

	                                                  uniform PointLight pointLight;
	                                                  uniform sampler2D diffuse;
	                                                  uniform sampler2D normalMap;
	                                                  uniform sampler2D dispMap;
	                                                  uniform vec4 color;
	                                                  uniform vec3 camPos;
	                                                  uniform float specularIntensity;
	                                                  uniform float specularExponent;
	                                                  uniform float bumpScale;

	                                                  vec4 calcLight(BaseLight base, vec3 direction, vec3 normal) {
			float diffuseFactor = dot(normal, -direction);

			vec4 diffuseColor = vec4(0, 0, 0, 0);
			vec4 specularColor = vec4(0, 0, 0, 0);

			if (diffuseFactor > 0) {
			    diffuseColor = base.color * base.intensity * diffuseFactor;

			    vec3 directionToEye = normalize(camPos - fragWorldPos);
			    vec3 reflectDirection = normalize(reflect(direction, normal));

			    float specularFactor = dot(directionToEye, reflectDirection);
			    specularFactor = pow(specularFactor, specularExponent);

			    if (specularFactor > 0) {
			        specularColor = vec4(base.color.xyz, 1.0) * specularIntensity * specularFactor;
				}
			}
			return diffuseColor + specularColor;
		}

	                                                  vec4 calcPointLight(PointLight pointLight, vec3 normal) {
			vec3 lightDirection = fragWorldPos - pointLight.position;
			float distanceToPoint = length(lightDirection);

			if (distanceToPoint > pointLight.range)
				return vec4(0, 0, 0, 0);

			lightDirection = normalize(lightDirection);

			vec4 color = calcLight(pointLight.base, lightDirection, normal);

			float attenuation = pointLight.atten.constant
			                    + pointLight.atten.linear * distanceToPoint
			                    + pointLight.atten.quadratic * distanceToPoint * distanceToPoint
			                    + 0.00001;

			return color / attenuation;
		}

	                                                  void main() {
			vec3 directionToEye = normalize(camPos - fragWorldPos);
			vec2 texCoord = fragTexCoord + ((directionToEye * tbnMatrix).xy * (texture(dispMap, fragTexCoord).r - 0.5) * bumpScale);

			vec4 texColor = texture(diffuse, texCoord);
			vec4 baseColor = texColor * vec4(1 - color.a, 1 - color.a, 1 - color.a, 1 - color.a) + color * vec4(color.a, color.a, color.a, 1);
			vec3 normal = normalize(tbnMatrix * (255.0 / 128.0 * texture(normalMap, texCoord).xyz - 1));
			fragColor = baseColor * calcPointLight(pointLight, normal);
		}));

	compileShader();
}

void ForwardPointShader::compileShader() {
	ForwardShader::compileShader();

	_pointBaseColorUniform = getUniform("pointLight.base.color");
	if (_pointBaseColorUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.base.color\"");

	_pointBaseIntenUniform = getUniform("pointLight.base.intensity");
	if (_pointBaseIntenUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.base.intensity\"");

	_pointAttenConstantUniform = getUniform("pointLight.atten.constant");
	if (_pointAttenConstantUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.atten.constant\"");

	_pointAttenLinearUniform = getUniform("pointLight.atten.linear");
	if (_pointAttenLinearUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.atten.linear\"");

	_pointAttenQuadraticUniform = getUniform("pointLight.atten.quadratic");
	if (_pointAttenQuadraticUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.atten.quadratic\"");

	_pointRangeUniform = getUniform("pointLight.range");
	if (_pointRangeUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.range\"");

	_pointPositionUniform = getUniform("pointLight.position");
	if (_pointPositionUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"pointLight.position\"");
}

void ForwardPointShader::setPointLight(PointLight* pPointLight) {
	setUniformVector4f(_pointBaseColorUniform, pPointLight->getColor());
	setUniformf(_pointBaseIntenUniform, pPointLight->getIntensity());
	setUniformf(_pointAttenConstantUniform, pPointLight->getAttenuation().constant);
	setUniformf(_pointAttenLinearUniform, pPointLight->getAttenuation().linear);
	setUniformf(_pointAttenQuadraticUniform, pPointLight->getAttenuation().quadratic);
	setUniformf(_pointRangeUniform, pPointLight->getRange());
	setUniformVector3f(_pointPositionUniform, pPointLight->getPos());
}

}
