//
//  DefaultSurfaceShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 8/26/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/DefaultSurfaceShader.hxx"

#define STRINGIFY(A) #A

namespace ShadeEngine {

DefaultSurfaceShader::DefaultSurfaceShader() {

	addVertexShader("#version 330 core\n" STRINGIFY(layout(location = 0) in vec3 position;
	                                                layout(location = 1) in vec2 texCoord;
	                                                layout(location = 2) in vec3 normal;
	                                                layout(location = 3) in vec3 tangent;

	                                                out vec2 fragTexCoord;
	                                                out vec3 fragWorldPos;
	                                                out mat3 tbnMatrix;

	                                                uniform mat4 MVP;
	                                                uniform mat4 transform;

	                                                void main() {
			fragTexCoord = texCoord;
			fragWorldPos = (transform * vec4(position, 1.0)).xyz;

			vec3 norm = normalize((transform * vec4(normal, 0.0)).xyz);
			vec3 tang = normalize((transform * vec4(tangent, 0.0)).xyz);
			tang = normalize(tang - dot(tang, norm) * norm);
			vec3 bitang = cross(norm, tang);
			tbnMatrix = mat3(tang, bitang, norm);

			gl_Position = MVP * vec4(position, 1.0);
		}));

	addFragmentShader("#version 330 core\n" STRINGIFY(in vec2 fragTexCoord;
	                                                  in vec3 fragWorldPos;
	                                                  in mat3 tbnMatrix;

	                                                  uniform sampler2D diffuse;
	                                                  uniform sampler2D dispMap;
	                                                  uniform vec4 color;
	                                                  uniform vec3 camPos;
	                                                  uniform vec4 ambientLight;
	                                                  uniform float bumpScale;

	                                                  out vec4 fragColor;

	                                                  void main() {
			vec3 directionToEye = normalize(camPos - fragWorldPos);
			vec2 texCoord = fragTexCoord + ((directionToEye * tbnMatrix).xy * (texture(dispMap, fragTexCoord).r - 0.5) * bumpScale);

			vec4 baseColor = texture(diffuse, texCoord) * vec4(1 - color.w, 1 - color.w, 1 - color.w, 1 - color.w) + color * vec4(color.w, color.w, color.w, 1);
			fragColor = baseColor * vec4(ambientLight.rgb, 1);
		}));

	compileShader();

}

}
