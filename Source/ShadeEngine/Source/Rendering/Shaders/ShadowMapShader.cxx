//
//  ShadowMapShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/15/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/ShadowMapShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

#define STRINGIFY(A) #A

namespace ShadeEngine {

ShadowMapShader::ShadowMapShader() {

	addVertexShader("#version 330 core\n" STRINGIFY(layout(location = 0) in vec3 position;
	                                                uniform mat4 MVP;

	                                                void main() {
			gl_Position = MVP * vec4(position, 1.0);
		}));

	addFragmentShader("#version 330 core\n" STRINGIFY(out vec4 fragColor;

	                                                  void main() {
			fragColor = vec4(gl_FragCoord.z);
		}));

	compileShader();

}

void ShadowMapShader::compileShader() {
	Shader::compileShader();

	_mvpUniform = getUniform("MVP");
	if (_mvpUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"MVP\"");
}

}
