//
//  ViewportShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/ViewportShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

ViewportShader::ViewportShader() {
	addVertexShader("#version 330 core\n"

	                "layout (location = 0) in vec3 position;\n"
	                "layout (location = 1) in vec2 texCoord;\n"

	                "out vec2 fragTexCoord;\n"

	                "uniform float depth;\n"

	                "void main() {\n"
	                "	fragTexCoord = texCoord;\n"
	                "	gl_Position = vec4(position.xy, depth, 1.0);\n"
	                "}\n");

	addFragmentShader("#version 330 core\n"

	                  "in vec2 fragTexCoord;\n"

	                  "out vec4 fragColor;\n"

	                  "uniform sampler2D sampler;\n"
	                  "uniform float xScale;\n"
	                  "uniform float yScale;\n"

	                  "void main() {\n"
	                  "	  vec4 texColor = texture(sampler, vec2(fragTexCoord.x * xScale, fragTexCoord.y * yScale));\n"
	                  "   fragColor = vec4(pow(texColor.rgb, vec3(1.0/2.2)), texColor.a);\n"
	                  "}\n");

	compileShader();
}

void ViewportShader::compileShader() {
	Shader::compileShader();

	_xScaleUniform = getUniform("xScale");
	if (_xScaleUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"xScale\"");

	_yScaleUniform = getUniform("yScale");
	if (_yScaleUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"yScale\"");

	_depthUniform = getUniform("depth");
	if (_depthUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"depth\"");
}

}
