//
//  BasicShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/16/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/BasicShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

void BasicShader::compileShader() {
	Shader::compileShader();

	_mvpUniform = getUniform("MVP");
	if (_mvpUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"MVP\"");

	_transformUniform = getUniform("transform");
	if (_transformUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"transform\"");

	_bumpScaleUniform = getUniform("bumpScale");
	if (_bumpScaleUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"bumpScale\"");

	_camPosUniform = getUniform("camPos");
	if (_camPosUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"camPos\"");

	_diffuseMapSampler = getUniform("diffuse");
	if (_diffuseMapSampler == 0xFFFFFFFF)
		shadeErrorFatal("Sampler doesn't exist: \"diffuse\"");

	_dispMapSampler = getUniform("dispMap");
	if (_dispMapSampler == 0xFFFFFFFF)
		shadeErrorFatal("Sampler doesn't exist: \"dispMap\"");
}

void BasicShader::bind() {
	Shader::bind();
	setUniformi(_diffuseMapSampler, 0);
	setUniformi(_dispMapSampler, 2);
}

}
