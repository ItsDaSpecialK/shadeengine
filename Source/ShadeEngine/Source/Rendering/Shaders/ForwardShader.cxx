//
//  ForwardShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

void ForwardShader::compileShader() {
	BasicShader::compileShader();

	_colorUniform = getUniform("color");
	if (_colorUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"color\"");

	_specIntenUniform = getUniform("specularIntensity");
	if (_specIntenUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"specularIntensity\"");

	_specExpUniform = getUniform("specularExponent");
	if (_specExpUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"specularExponent\"");

	_normalMapSampler = getUniform("normalMap");
	if (_normalMapSampler == 0xFFFFFFFF)
		shadeErrorFatal("Sampler doesn't exist: \"normalMap\"");

	_lightMatrixUniform = getUniform("lightMatrix");
	if (_lightMatrixUniform == 0xFFFFFFFF)
		// shadeErrorFatal("Uniform doesn't exist: \"lightMatrix\"");
		std::cerr << "Uniform doesn't exist: \"lightMatrix\"\n";

	_shadowMapSampler = getUniform("shadowMap");
	if (_shadowMapSampler == 0xFFFFFFFF)
		// shadeErrorFatal("Uniform doesn't exist: \"shadowMap\"");
		std::cerr << "Uniform doesn't exist: \"shadowMap\"\n";
}

void ForwardShader::bind() {
	BasicShader::bind();
	setUniformi(_normalMapSampler, 1);
	setUniformi(_shadowMapSampler, 3);
}

}
