//
//  SurfaceShader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shaders/SurfaceShader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

void SurfaceShader::compileShader() {
	BasicShader::compileShader();

	_colorUniform = getUniform("color");
	if (_colorUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"color\"");

	_ambientUniform = getUniform("ambientLight");
	if (_ambientUniform == 0xFFFFFFFF)
		shadeErrorFatal("Uniform doesn't exist: \"ambientLight\"");
}

}
