//
//  Camera.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Camera.hxx"

using namespace ShadeEngine;

Matrix Camera::getCameraMatrix() const {
	Matrix cameraRotation = _rot.conjugate().toRotationMatrix();
	Matrix cameraTranslation;

	// Have to negate the direction because we dont actually move the camera,
	// we move the world around it.
	cameraTranslation.initToTranslation(_pos * -1);

	return cameraTranslation * cameraRotation * _projection;
}
