//
//  Shader.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/10/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Shader.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

Shader::Shader() {
	_program = glCreateProgram();
	if (!_program)
		shadeErrorFatal("Couldn't create Shader Program");
	_vertShader = 0;
	_geomShader = 0;
	_fragShader = 0;
}

Shader::~Shader() {
	glDeleteProgram(_program);

	if (_vertShader)
		glDeleteShader(_vertShader);
	if (_geomShader)
		glDeleteShader(_geomShader);
	if (_fragShader)
		glDeleteShader(_fragShader);
}

void Shader::addProgram(const std::string& text, GLuint type) {
	int shader = glCreateShader(type);
	if (!shader)
		shadeErrorFatal("Couldn't create Shader");

	const GLchar* literal[1];
	literal[0] = text.c_str();
	GLint lengths[1];
	lengths[0] = (GLint)text.length();

	glShaderSource(shader, 1, literal, lengths);
	glCompileShader(shader);

	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success) {
		glAttachShader(_program, shader);

		switch (type) {
			case GL_VERTEX_SHADER:
				if (_vertShader) {
					shadeErrorFatal("Vertex Shader already Exists.");
				}
				_vertShader = shader;
				break;
			case GL_GEOMETRY_SHADER:
				if (_geomShader) {
					shadeErrorFatal("Geometry Shader already Exists.");
				}
				_geomShader = shader;
				break;
			case GL_FRAGMENT_SHADER:
				if (_fragShader) {
					shadeErrorFatal("Fragment Shader already Exists.");
				}
				_fragShader = shader;
				break;
			default:
				break;
		}

	} else {
		GLchar InfoLog[1024];
		glGetShaderInfoLog(shader, 1024, nullptr, InfoLog);

		switch (type) {
			case GL_VERTEX_SHADER:
				shadeErrorFatal("Vertex Shader Compile Error: ", InfoLog);
				break;
			case GL_GEOMETRY_SHADER:
				shadeErrorFatal("Geometry Shader Compile Error: ", InfoLog);
				break;
			case GL_FRAGMENT_SHADER:
				shadeErrorFatal("Fragment Shader Compile Error: ", InfoLog);
				break;
			default:
				shadeErrorFatal("Unknown Shader Compile Error: ", InfoLog);
				break;
		}
	}

}

void Shader::compileShader() {
	glLinkProgram(_program);

	GLint success = 0;
	glGetProgramiv(_program, GL_LINK_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];
		glGetProgramInfoLog(_program, 1024, nullptr, InfoLog);
		shadeErrorFatal("Shader Link Error: ", InfoLog);
	}
}

}
