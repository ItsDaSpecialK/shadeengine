//
//  CubeMap.cpp
//  ShadeEngine
//
//  Created by Konrad Kraemer on 11/23/18.
//
//  Copyright (c) 2018 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/CubeMap.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

CubeMap::CubeMap(uint16 width,
                 uint16 height,
                 GLfloat filter,
                 GLint inputFormat,
                 GLfloat clamp,
                 byte** data) {
	init(width, height, filter, inputFormat, clamp, data);
}

void CubeMap::init(uint16 width,
                   uint16 height,
                   GLfloat filter,
                   GLint inputFormat,
                   GLfloat clamp,
                   byte** data) {
	glGenTextures(1, &_textureID);

	if (width > 0 && height > 0) {
		glBindTexture(GL_TEXTURE_CUBE_MAP, _textureID);

		_texTarget = GL_TEXTURE_CUBE_MAP;

		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, filter);

		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, clamp);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, clamp);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, clamp);

		for (uint8 i = 0; i < 6; ++i) {
			byte* inputData = data ? data[i] : nullptr;
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB,
			             width, height, 0, inputFormat, GL_UNSIGNED_BYTE, inputData);
		}
	}
}

}
