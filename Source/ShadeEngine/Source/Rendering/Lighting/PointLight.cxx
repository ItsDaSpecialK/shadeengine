//
//  PointLight.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Lighting/PointLight.hxx"

namespace ShadeEngine {

PointLight::PointLight() {
	_range = 20.f;
	_atten = Attenuation();
	setCastsShadow(true);
}

}
