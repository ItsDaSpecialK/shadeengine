//
//  SpotLight.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/17/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Lighting/SpotLight.hxx"

namespace ShadeEngine {

SpotLight::SpotLight() {
	setAngle(36.87f);
	setRange(30.f);
	setCastsShadow(true);
}

void SpotLight::setCastsShadow(bool castsShadow) {
	if (castsShadow) {
		if (_pShadow == nullptr)
			_pShadow = new Shadow();

		_pShadow->projection.initToPerspective(getAngle(), 1, 0.1f, getRange());
		_pShadow->bias = 0.0005f;
	} else {
		if (_pShadow != nullptr) {
			delete _pShadow;
			_pShadow = nullptr;
		}
	}
}

}
