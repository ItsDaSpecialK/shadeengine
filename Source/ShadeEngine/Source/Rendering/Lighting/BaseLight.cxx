//
//  BaseLight.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 6/15/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Lighting/BaseLight.hxx"

namespace ShadeEngine {

CBaseLight::CBaseLight() {
	_col = Color(0.f, 0.f, 0.f, 0.f);
	_inten = 0.f;
	_pShadow = nullptr;
}

CBaseLight::~CBaseLight() {
	if (_pShadow != nullptr) {
		delete _pShadow;
	}
}
}
