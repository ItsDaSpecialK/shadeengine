//
//  DirectionalLight.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 4/9/16.
//
//  Copyright (c) 2016 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Lighting/DirectionalLight.hxx"

namespace ShadeEngine {

void DirectionalLight::setCastsShadow(bool castsShadow) {
	if (castsShadow) {
		if (_pShadow == nullptr) {
			_pShadow = new Shadow();
			_pShadow->projection.initToOrthographic(40, -40, 40, -40, 40, -40);
			_pShadow->bias = 0.005f;
		}
	} else {
		if (_pShadow != nullptr) {
			delete _pShadow;
			_pShadow = nullptr;
		}
	}
}
}
