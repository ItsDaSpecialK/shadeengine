//
//  Mesh.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/9/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/StaticMesh.hxx"

using namespace ShadeEngine;

StaticMesh::StaticMesh(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices, bool gen) {
	bindVao();

	if (gen) {
		calcNormals(pVertices, numVertices, pIndices, numIndices);
		calcTangents(pVertices, numVertices, pIndices, numIndices);
	}

	bindVbo();
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), pVertices, GL_STATIC_DRAW);
	bindIbo();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(uint32), pIndices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)sizeof(Vector3f));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(Vector3f) + sizeof(Vector2f)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(sizeof(Vector3f) + sizeof(Vector2f) + sizeof(Vector3f)));
	unbindVao();

	_size = numIndices;
}

void StaticMesh::render() const {
	bindVao();

	glDrawElements(GL_TRIANGLES, _size, GL_UNSIGNED_INT, 0);

	unbindVao();
}

void StaticMesh::calcNormals(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices) {
	for (uint32 i = 0; i < numIndices; i += 3) {
		uint32 i0 = pIndices[i];
		uint32 i1 = pIndices[i + 1];
		uint32 i2 = pIndices[i + 2];

		Vector3f v1 = pVertices[i1].pos - pVertices[i0].pos;
		Vector3f v2 = pVertices[i2].pos - pVertices[i0].pos;

		Vector3f normal = v1.cross(v2).normalize();

		pVertices[i0].norm += normal;
		pVertices[i1].norm += normal;
		pVertices[i2].norm += normal;
	}

	for (uint32 i = 0; i < numVertices; i++)
		pVertices[i].norm.normalize();
}

void StaticMesh::calcTangents(Vertex* pVertices, uint32 numVertices, uint32* pIndices, uint32 numIndices) {
	for (uint32 i = 0; i < numIndices; i += 3) {
		uint32 i0 = pIndices[i];
		uint32 i1 = pIndices[i + 1];
		uint32 i2 = pIndices[i + 2];

		Vector3f v1 = pVertices[i1].pos - pVertices[i0].pos;
		Vector3f v2 = pVertices[i2].pos - pVertices[i0].pos;

		float dU1 = pVertices[i1].texCoord[0] - pVertices[i0].texCoord[0];
		float dV1 = pVertices[i1].texCoord[1] - pVertices[i0].texCoord[1];
		float dU2 = pVertices[i2].texCoord[0] - pVertices[i0].texCoord[0];
		float dV2 = pVertices[i2].texCoord[1] - pVertices[i0].texCoord[1];

		Vector3f tangent;

		if (pVertices[i1].texCoord[0] - pVertices[i0].texCoord[0] != 0 ||
		    pVertices[i1].texCoord[1] - pVertices[i0].texCoord[1] != 0 ||
		    pVertices[i2].texCoord[0] - pVertices[i0].texCoord[0] != 0 ||
		    pVertices[i2].texCoord[1] - pVertices[i0].texCoord[1] != 0) {
			// If there are no texCoords, or it is a degenerate triangle,
			// don't make a tangent.

			float det = 1.0f / (dU1 * dV2 - dU2 * dV1);

			tangent.setX(det * (dV2 * v1.getX() - dV1 * v2.getX()));
			tangent.setY(det * (dV2 * v1.getY() - dV1 * v2.getY()));
			tangent.setZ(det * (dV2 * v1.getZ() - dV1 * v2.getZ()));
		}

		pVertices[i0].tang += tangent;
		pVertices[i1].tang += tangent;
		pVertices[i2].tang += tangent;
	}

	for (uint32 i = 0; i < numVertices; i++) {
		if (pVertices[i].tang == Vector3f(0, 0, 0)) {
			// If there is no valid tangent

			Vector3f tangent(1, 0, 0);
			if (pVertices[i].norm == Vector3f(1, 0, 0))
				tangent = Vector3f(0, 1, 0);
			tangent = tangent - (pVertices[i].norm * tangent.dot(pVertices[i].norm));

			pVertices[i].tang = tangent;
		}
		pVertices[i].tang.normalize();
	}
}
