//
//  MeshInstance.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 12/6/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/MeshInstance.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"

using namespace ShadeEngine;

void MeshInstance::render(const Matrix cam, const Vector3f camPos) const {
	if (!_pMat || !_pMesh) {
		shadeErrorFatal("Cannot Render the MeshComponent, Either the material or mesh are null");
		return;
	}
	getMaterial()->bind();
	Matrix trans = getTransformationMatrix();
	SurfaceShader* pShader = getMaterial()->getShader();
	pShader->setCameraPosition(camPos);
	pShader->setTransform(trans);
	pShader->setMVP(trans * cam);
	pShader->setAmbientLight(_ambientLight);
	_pMesh->render();
	getMaterial()->unbind();
}

void MeshInstance::render(const Matrix cam, const Matrix light, BasicShader* pShader) const {
	if (!_pMat || !_pMesh) {
		shadeErrorFatal("Cannot Render the MeshComponent, Either the material or mesh are null");
		return;
	}
	getMaterial()->bind(pShader);
	Matrix trans = getTransformationMatrix();
	pShader->setTransform(trans);
	pShader->setMVP(trans * cam);
	ForwardShader* pForwardShader = dynamic_cast<ForwardShader*>(pShader);
	if (pForwardShader != nullptr) {
		const static Matrix bias({ 0.5, 0.0, 0.0, 0.5,
		                           0.0, 0.5, 0.0, 0.5,
		                           0.0, 0.0, 0.5, 0.5,
		                           0.0, 0.0, 0.0, 1.0 });
		pForwardShader->setLightMatrix(trans * light * bias);
	}
	_pMesh->render();
	getMaterial()->unbindPreserveShader();
}

void MeshInstance::render(const Matrix cam, ShadowMapShader* pShader) const {
	if (!_pMesh) {
		shadeErrorFatal("Cannot Render the MeshComponent, the mesh is null");
		return;
	}
	Matrix trans = getTransformationMatrix();
	pShader->setMVP(trans * cam);
	_pMesh->render();
}

void MeshInstance::render(const Matrix cam, SkyMeshShader* pShader) const {
	if (!_pMat || !_pMesh) {
		shadeErrorFatal("Cannot Render the MeshComponent, Either the material or mesh are null");
		return;
	}
	getMaterial()->bind(pShader);
	Matrix trans = getTransformationMatrix();
	pShader->setMVP(trans * cam);
	_pMesh->render();
	getMaterial()->unbind();
}

void MeshInstance::setTransformationMatrix(const Matrix4& transMat) {
	for (uint8 i = 0; i < 3; ++i) {
		for (uint8 j = 0; j < 3; ++j) {
			_rotScaleMat[i][j] = transMat[i][j];
		}
	}
	for (uint8 i = 0; i < 3; ++i)
		_pos[i] = transMat[3][i];
}

Matrix MeshInstance::getTransformationMatrix() const {
	Matrix result;
	for (uint8 i = 0; i < 3; ++i) {
		for (uint8 j = 0; j < 3; ++j) {
			result[i][j] = _rotScaleMat[i][j];
		}
	}
	for (uint8 i = 0; i < 3; ++i)
		result[3][i] = _pos[i];

	for (uint8 i = 0; i < 3; ++i)
		result[i][3] = 0.f;
	result[3][3] = 1.f;

	return result;
}
