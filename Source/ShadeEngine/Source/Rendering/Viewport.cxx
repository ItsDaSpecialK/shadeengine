//
//  Viewport.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 7/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Viewport.hxx"
#include "ShadeENGINE/Core/Window.hxx"
#include "ShadeENGINE/Engine/Game/PlayerController.hxx"

namespace ShadeEngine {

Viewport::Viewport(uint16 xCoord, uint16 yCoord, uint16 width, uint16 height) {
	_width = width;
	_height = height;
	_windowWidth = Window::getWidth();
	_windowHeight = Window::getHeight();
	_resolutionScale = 1.f;
	_x = xCoord;
	_y = yCoord;
	_depth = 0;
	_pController = nullptr;
	Vertex array[] = { Vertex(Vector3f(-1.f, -1.f, 0), Vector2f(0, 0)),
		               Vertex(Vector3f(1.f, -1.f, 0), Vector2f(1, 0)),
		               Vertex(Vector3f(-1.f, 1.f, 0), Vector2f(0, 1)),
		               Vertex(Vector3f(1.f, 1.f, 0), Vector2f(1, 1)) };

	uint32 indexes[] = { 0, 1, 2,
		                 3, 2, 1 };

	_pSurface = new StaticMesh(array, 4, indexes, 6); // @LEAK???

	/*Vertex array[] = {Vertex(Vector(-1.f, -1.f, 0),  Vector2f(0,0)),
	 Vertex(Vector(0.f, 1.f, 0.f),  Vector2f(1.f,0)),
	 Vertex(Vector(1.f, -1.f, 0.f), Vector2f(0,1.f)),
	 Vertex(Vector(0.f, -1.f, 1.f), Vector2f(1.f,1.f))};

	 uint32 indexes[] = {3, 1, 0,
	 2, 1, 3,
	 0, 1, 2,
	 0, 2, 3};

	 pMesh = new StaticMesh(array, 4, indexes, 12);*/


}

Viewport::~Viewport() {
	delete _pSurface;
}

void Viewport::draw(Texture* pTex) {
	uint16 yCoord = _windowHeight - _y - _height;
	uint16 xCoord = _x;
	glViewport(xCoord, yCoord, _width, _height);
	glDisable(GL_CULL_FACE);
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	pTex->bind();
	ViewportShader& pShader = ViewportShader::getInstance();
	pShader.bind();
	pShader.setDepth(_depth);
	pShader.setXScale(_resolutionScale * (float)_width / (float)_windowWidth);
	pShader.setYScale(_resolutionScale * (float)_height / (float)_windowHeight);
	_pSurface->render();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Viewport::setSize(uint16 width, uint16 height) {
	_width = width;
	_height = height;
	if (_pController)
		_pController->setAspectRatio((float)_width / (float)_height);
}

void Viewport::setController(PlayerController* pPlayerController) {
	_pController = pPlayerController;
	if (_pController)
		_pController->setAspectRatio((float)_width / (float)_height);
}

Camera* Viewport::getCamera() {
	if (_pController)
		return _pController->getCameraComponent()->getCamera();
	shadeErrorFatal("Viewport Doesn't have a Controller");
	return nullptr;
}

}
