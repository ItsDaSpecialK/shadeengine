//
//  Material.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/22/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Material.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"
#include "ShadeENGINE/Rendering/Shaders/ForwardShader.hxx"

namespace ShadeEngine {

Material::Material() {
	_pShader = nullptr;
	_pTexture = nullptr;
	_pNormalMap = nullptr;
	_pDispMap = nullptr;
	_col = Color(0.f, 0.f, 0.f, 0.f);
	_specInten = 1.f;
	_specExp = 8.f;
	_bumpScale = 0.f; // 0.04f is a good number.
}

Material::Material(Material& copy) {
	_pShader = copy._pShader;
	_pTexture = copy._pTexture;
	_pNormalMap = copy._pNormalMap;
	_pDispMap = copy._pDispMap;
	_col = copy._col;
	_specInten = copy._specInten;
	_specExp = copy._specExp;
	_bumpScale = copy._bumpScale;
}

void Material::bind() const {
	if (_pShader == nullptr) {
		shadeErrorFatal("Cannot bind material, shader is null");
		return;
	}
	_pShader->bind();
	if (_pTexture)
		_pTexture->bind();
//	if (_pNormalMap)
//		_pNormalMap->bind(1);
	if (_pDispMap)
		_pDispMap->bind(2);
	// Don't need normal map here, because this isn't being used for lighting...
	_pShader->setColor(_col);
	_pShader->setBumpScale(_bumpScale);
}

void Material::bind(Shader* pShader) const {
	if (_pTexture)
		_pTexture->bind();
	ForwardShader* pForwardShader = dynamic_cast<ForwardShader*>(pShader);
	if (pForwardShader != nullptr) {
		if (_pNormalMap)
			_pNormalMap->bind(1);
		if (_pDispMap)
			_pDispMap->bind(2);
		pForwardShader->setColor(_col);
		pForwardShader->setSpecularIntensity(_specInten);
		pForwardShader->setSpecularExponent(_specExp);
		pForwardShader->setBumpScale(_bumpScale);
	}
}

void Material::unbind() const {
	glUseProgram(0);
	unbindPreserveShader();
}

void Material::unbindPreserveShader() const {
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);
}

}
