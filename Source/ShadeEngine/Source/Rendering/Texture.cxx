//
//  Texture.cxx
//  ShadeEngine
//
//  Created by Konrad Kraemer on 5/21/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "ShadeENGINE/Rendering/Texture.hxx"
#include "ShadeENGINE/Core/Log/ShadeError.hxx"

namespace ShadeEngine {

Texture::Texture(uint16 width,
                 uint16 height,
                 GLenum textureTarget,
                 GLfloat filter,
                 GLint inputFormat,
                 GLfloat clamp,
                 byte* data) {
	init(width, height, textureTarget, filter, inputFormat, clamp, data);
}

void Texture::init(uint16 width,
                   uint16 height,
                   GLenum textureTarget,
                   GLfloat filter,
                   GLint inputFormat,
                   GLfloat clamp,
                   byte* data) {
	if (textureTarget == GL_TEXTURE_CUBE_MAP)
		shadeErrorFatal("You should use the CubeMap class instead");

	glGenTextures(1, &_textureID);

	if (width > 0 && height > 0) {
		glBindTexture(textureTarget, _textureID);

		_texTarget = textureTarget;

		glTexParameterf(textureTarget, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameterf(textureTarget, GL_TEXTURE_MAG_FILTER, filter);

		glTexParameterf(textureTarget, GL_TEXTURE_WRAP_S, clamp);
		glTexParameterf(textureTarget, GL_TEXTURE_WRAP_T, clamp);

		if (inputFormat == GL_DEPTH_COMPONENT) {
			glTexImage2D(textureTarget, 0, GL_DEPTH_COMPONENT16, width, height,
			             0, GL_DEPTH_COMPONENT, GL_FLOAT, data);
		} else {
			glTexImage2D(textureTarget, 0, GL_RGBA, width, height,
			             0, inputFormat, GL_UNSIGNED_BYTE, data);
		}
	}
}

Texture::~Texture() {
	if (_textureID) glDeleteTextures(1, &_textureID);
}

void Texture::bind(uint8 unit) const {
	if (!_textureID)
		shadeErrorFatal("Texture Bind Error: Texture has not been initialized yet");
	if (unit > 31)
		shadeErrorFatal("Texture Bind Error: Out of Range");
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(_texTarget, _textureID);
}

}
