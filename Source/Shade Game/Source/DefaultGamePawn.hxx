//
//  DefaultGamePawn.hxx
//  Shade Game
//
//  Created by Konrad Kraemer on 7/16/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <ShadeENGINE/Actor/Pawn.hxx>
#include <ShadeENGINE/Component/PawnMovementComponent.hxx>
#include "OrbitMovementComponent.hxx"

using namespace ShadeEngine;

SHADE_CLASS()
/** Default Playable Pawn Class.
 *  Demonstrates how you can set up a Pawn.
 */
class DefaultGamePawn : public Pawn {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	DefaultGamePawn(Scene* pScene);

	/// Default Destructor
	virtual ~DefaultGamePawn() { }

	/** Override the Pawn's ProcessAxisInput() to add movement and looking to this Pawn.
	 *  @see  Pawn::ProcessAxisInput(std::string, float, uint16)
	 */
	virtual void processAxisInput(std::string name, float value, uint16 delta) override;
protected:
private:
	/// A pointer to the Movement Component which helps add movement to the Pawn
	PawnMovementComponent* pMovementComp;

	// OrbitMovementComponent* pOrbitComp;

	SHADE_REFLECTION_BEGIN(DefaultGamePawn, Pawn)
	SHADE_REFLECTION_END
};
