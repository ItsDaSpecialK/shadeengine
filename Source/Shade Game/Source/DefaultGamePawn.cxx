//
//  DefaultGamePawn.cxx
//  Shade Game
//
//  Created by Konrad Kraemer on 7/16/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "DefaultGamePawn.hxx"

DefaultGamePawn::DefaultGamePawn(ShadeEngine::Scene* pScene) : Pawn(pScene) {
	pMovementComp = addComponent<ShadeEngine::PawnMovementComponent>();
	// pOrbitComp = addComponent<OrbitMovementComponent>();

	// pOrbitComp->setOrbitPoint(Vector3f(0,0,5));
	// pOrbitComp->setOrbitDistance(10);
	// pOrbitComp->setOrbitVelocity(45);
}

void DefaultGamePawn::processAxisInput(std::string name, float value, uint16 delta) {
	if (name == "MoveFwd") // @TODO: Convert from strings to uint16 enum
		pMovementComp->moveFwd(value * delta * 0.01f);
	else if (name == "MoveRight")
		pMovementComp->moveRight(value * delta * 0.01f);
	else if (name == "MoveUp")
		pMovementComp->moveUp(value * delta * 0.01f);
	else if (name == "LookUp")
		pMovementComp->addPitchInput(value * 0.1f);
	else if (name == "LookRight")
		pMovementComp->addYawInput(value * 0.1f);
}
