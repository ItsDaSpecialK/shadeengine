//
//  DefaultGameMode.cxx
//  Shade Game
//
//  Created by Konrad Kraemer on 9/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "DefaultGameMode.hxx"
#include "DefaultGamePawn.hxx"
#include <ShadeENGINE/Rendering/Viewport.hxx>

DefaultGameMode::DefaultGameMode(ShadeEngine::Scene* pScene) : CGameMode(pScene) {
	ShadeEngine::Action* fire = new ShadeEngine::Action;
	fire->name = "Fire";
	fire->events.add(SDL_BUTTON_LEFT);
	fire->eventTypes.add(ShadeEngine::EEventType::MOUSE_BUTTON);

	ShadeEngine::Axis* moveFwd = new ShadeEngine::Axis;
	moveFwd->name = "MoveFwd";
	moveFwd->events.add(SDL_SCANCODE_W);
	moveFwd->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveFwd->multipliers.add(1.f);
	moveFwd->events.add(SDL_SCANCODE_S);
	moveFwd->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveFwd->multipliers.add(-1.f);

	ShadeEngine::Axis* moveRight = new ShadeEngine::Axis;
	moveRight->name = "MoveRight";
	moveRight->events.add(SDL_SCANCODE_D);
	moveRight->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveRight->multipliers.add(1.f);
	moveRight->events.add(SDL_SCANCODE_A);
	moveRight->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveRight->multipliers.add(-1.f);

	ShadeEngine::Axis* moveUp = new ShadeEngine::Axis;
	moveUp->name = "MoveUp";
	moveUp->events.add(SDL_SCANCODE_LSHIFT);
	moveUp->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveUp->multipliers.add(1.f);
	moveUp->events.add(SDL_SCANCODE_LCTRL);
	moveUp->eventTypes.add(ShadeEngine::EEventType::KEYBOARD);
	moveUp->multipliers.add(-1.f);

	ShadeEngine::Axis* lookUp = new ShadeEngine::Axis;
	lookUp->name = "LookUp";
	lookUp->events.add((uint16)ShadeEngine::EMouseAxis::Y);
	lookUp->eventTypes.add(ShadeEngine::EEventType::MOUSE_AXIS);
	lookUp->multipliers.add(1.f);

	ShadeEngine::Axis* lookRight = new ShadeEngine::Axis;
	lookRight->name = "LookRight";
	lookRight->events.add((uint16)ShadeEngine::EMouseAxis::X);
	lookRight->eventTypes.add(ShadeEngine::EEventType::MOUSE_AXIS);
	lookRight->multipliers.add(1.f);

	// pController = InputManager::addPlayer();
	ShadeEngine::Action** actionArray = new ShadeEngine::Action*[1]; // @FIXEDLEAK
	actionArray[0] = fire;
	ShadeEngine::Axis** axisArray = new ShadeEngine::Axis*[5]; // @FIXEDLEAK
	axisArray[0] = moveFwd;
	axisArray[1] = moveRight;
	axisArray[2] = moveUp;
	axisArray[3] = lookUp;
	axisArray[4] = lookRight;

	// pViewport = new Viewport(0,0,800,300);
	ShadeEngine::Viewport* pViewport = new ShadeEngine::Viewport(0, 0, 800, 600); // @TODO: Make a method so not every game mode has to define this.
	pScene->addViewport(pViewport);
	ShadeEngine::PlayerController* pController = getController(0);
	pController->setMapping(actionArray, 1, axisArray, 5);
	pViewport->setController(pController);

	_playerClass.setToClass(DefaultGamePawn::staticClass());
}

ShadeEngine::SpawnPoint* DefaultGameMode::chooseSpawnPoint() {
	ShadeEngine::SpawnPoint* pSpawnPointNode = getSpawnPointList()[0];
	if (pSpawnPointNode == nullptr) {
		ShadeEngine::shadeErrorFatal("No Spawn Points");
	}
	return pSpawnPointNode;
}
