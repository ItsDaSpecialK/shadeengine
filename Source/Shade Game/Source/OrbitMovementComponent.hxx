//
//  OrbitMovementComponent.hxx
//  Shade Game
//
//  Created by Konrad Kraemer on 11/28/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include "ShadeENGINE/Component/Component.hxx"
#include "ShadeENGINE/Core/Math/ShadeMath.hxx"

using namespace ShadeEngine;

#define DEG_SEC_TO_RAD_MILLI(x) (x * .0000174532925)

SHADE_CLASS()
/// A Class which moves a pawn in a circle around a point
class OrbitMovementComponent : public CComponent {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor.
	OrbitMovementComponent(Scene* pScene);

	/// Default Destructor.
	virtual ~OrbitMovementComponent() { }

	/** Moves the pawn.
	 * @param  delta  The time since this was last called in milliseconds
	 */
	virtual void update(uint16 delta) override;

	/** Simple setter for the point around which the pawn will orbit
	 *  @param  point  The point which the pawn will orbit around.
	 */
	void setOrbitPoint(Vector3f point) { _orbitPoint = point; }

	/** Simple setter for the distance at which the pawn will orbit
	 *  @param  distance  The distance at which the pawn will orbit.
	 */
	void setOrbitDistance(float distance) { _orbitDist = distance; }

	/** Simple setter for the velocity at which the pawn will orbit.
	 *  @param  velocity  The velocity in degrees/second, with negative being counter-clockwise.
	 */
	void setOrbitVelocity(float velocity) { _orbitVelocity = toRadians(velocity) / 1000; }

protected:
private:
	/// The point around which the pawn will orbit.
	SHADE_PROPERTY()
	Vector3f _orbitPoint;

	/// The distance the pawn will orbit at.
	SHADE_PROPERTY()
	float _orbitDist;

	/// The velocity at which the pawn will orbit (in radians/millisecond)
	SHADE_PROPERTY()
	float _orbitVelocity;

	/// The angle at which the pawn currently is (in radians).
	SHADE_PROPERTY()
	float _angle;

	SHADE_REFLECTION_BEGIN(OrbitMovementComponent, CComponent)
	SHADE_ADD_PROPERTY(_orbitPoint)
	SHADE_ADD_PROPERTY(_orbitDist)
	SHADE_ADD_PROPERTY(_orbitVelocity)
	SHADE_ADD_PROPERTY(_angle)
	SHADE_REFLECTION_END

};
