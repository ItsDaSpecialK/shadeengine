//
//  OrbitMovementComponent.cxx
//  Shade Game
//
//  Created by Konrad Kraemer on 11/28/17.
//
//  Copyright (c) 2017 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "OrbitMovementComponent.hxx"

OrbitMovementComponent::OrbitMovementComponent(Scene* pScene) : CComponent(pScene) {
	// Start at -90 degrees (at the bottom of the circle).
	_angle = MATH_PI * -0.5f;
}

void OrbitMovementComponent::update(uint16 delta) {
	_angle += delta * _orbitVelocity;
	if (_angle > 2 * MATH_PI)
		_angle -= 2 * MATH_PI;
	else if (_angle < -2 * MATH_PI)
		_angle += 2 * MATH_PI;

	float xPos = cosf(_angle) * _orbitDist + _orbitPoint.getX();
	float yPos = sinf(_angle) * _orbitDist + _orbitPoint.getY();
	float zPos = _orbitPoint.getZ();

	Transform* pTrans = getTransform().getParent();
	pTrans->setPos(Vector3f(xPos, yPos, zPos));
	pTrans->rotate(Vector3f(0, 0, 1), toDegrees(_orbitVelocity * delta));
}
