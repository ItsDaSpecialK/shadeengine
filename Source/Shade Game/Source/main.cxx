//
//  main.cxx
//  Shade Game
//
//  Created by Konrad Kraemer on 4/21/14.
//
//  Copyright (c) 2014 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#include "main.hxx"
#include <ShadeENGINE/Engine/EngineManager.hxx>
#include <ShadeENGINE/Engine/AssetStorage/EdAssetManager.hxx>

#include <ShadeENGINE/Util/ResourceLoader.hxx>
#include <ShadeENGINE/Rendering/Shaders/DefaultSurfaceShader.hxx>
#include <ShadeENGINE/Actor/StaticMeshActor.hxx>
#include <ShadeENGINE/Engine/CSGManager.hxx>
#include <ShadeENGINE/Engine/Object/CSG/BoxBrush.hxx>
#include <ShadeENGINE/Engine/Object/CSG/SphereBrush.hxx>
#include <ShadeENGINE/Util/StringHelper.hxx>

SHADEAPI ShadeEngine::ShadeClass* getTestActorClass() { return TestActor::staticClass(); }

SHADEAPI int gameInit() {

#ifdef WIN32
	std::string projPath("../../DefaultProject.SEProj");
#else
	std::string projPath("../../../DefaultProject.SEProj");
#endif

	ShadeEngine::init(SHADE_ALL & ~SHADE_ASSET_MANAGER);

	ShadeEngine::EngineManager* pEngineMan = new ShadeEngine::EngineManager();

	std::string str("ShadeENGINE Demo");
	ShadeEngine::Window::create(800, 600, str);


	// ShadeEngine::CAssetManager* pAssetMan = new ShadeEngine::EdAssetManager(projPath);

	// pEngineMan->setAssetManager(pAssetMan);

	// pEngineMan->loadMapFromFile("scenarios/multi/test/test.smap");
	// pEngineMan->setActiveMap("scenarios/multi/test/test.smap", DefaultGameMode::staticClass());

	Scene* pScene = new Scene();

	std::string assetRoot = StringHelper::getContainingFolder(projPath) + "Assets/";

	std::string monkeyPath("scenarios/multi/test/models/monkey.obj");
	std::string skyBoxPath("scenarios/multi/test/models/skyBox.obj");
	std::string brickPath("scenarios/multi/test/bitmaps/bricks.bmp");
	std::string brickNormalPath("scenarios/multi/test/bitmaps/bricks_normal.bmp");
	std::string brickDispPath("scenarios/multi/test/bitmaps/bricks_disp.bmp");


	StaticMesh* pMonkeyMesh = ResourceLoader::loadMesh(assetRoot + monkeyPath);
	StaticMesh* pSkyBoxMesh = ResourceLoader::loadMesh(assetRoot + skyBoxPath);
	if (!pMonkeyMesh || !pSkyBoxMesh)
		ShadeEngine::shadeErrorFatal("Couldn't load meshes");

	Texture* pBricks = ResourceLoader::loadTexture(assetRoot + brickPath);
	Texture* pBricksNormal = ResourceLoader::loadTexture(assetRoot + brickNormalPath);
	Texture* pBricksDisp = ResourceLoader::loadTexture(assetRoot + brickDispPath);
	if (!pBricks || !pBricksNormal || !pBricksDisp)
		ShadeEngine::shadeErrorFatal("Couldn't load textures");

	std::vector<std::string> skyPaths;
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/right.bmp");
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/left.bmp");
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/top.bmp");
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/bottom.bmp");
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/front.bmp");
	skyPaths.push_back(assetRoot + "scenarios/multi/test/bitmaps/sky/back.bmp");


	Texture* pSkyTex = ResourceLoader::loadCubeMap(skyPaths);
	if (!pSkyTex)
		ShadeEngine::shadeErrorFatal("Couldn't load texture");

	byte colors[] = { 128, 128, 255 };
	static Texture defaultNormalMap(1, 1, GL_TEXTURE_2D, GL_LINEAR, GL_RGB, GL_REPEAT, colors);

	Material* pMonkey1Mat = new Material();
	pMonkey1Mat->setSpecularIntensity(2.f);
	pMonkey1Mat->setSpecularExponent(32.f);
	pMonkey1Mat->setShader(&DefaultSurfaceShader::getInstance());
	pMonkey1Mat->setColor(Color::rgb(204, 204, 255));
	pMonkey1Mat->setNormalMap(&defaultNormalMap);

	Material* pMonkey2Mat = new Material();
	pMonkey2Mat->setSpecularIntensity(0.125f);
	pMonkey2Mat->setSpecularExponent(8.f);
	pMonkey2Mat->setShader(&DefaultSurfaceShader::getInstance());
	pMonkey2Mat->setColor(Color::rgb(204, 204, 255));
	pMonkey2Mat->setNormalMap(&defaultNormalMap);

	Material* pPlaneMat = new Material();
	pPlaneMat->setSpecularIntensity(0.125f);
	pPlaneMat->setSpecularExponent(8.f);
	pPlaneMat->setShader(&DefaultSurfaceShader::getInstance());
	pPlaneMat->setBumpScale(0.025f);
	pPlaneMat->setTexture(pBricks);
	pPlaneMat->setNormalMap(pBricksNormal);
	pPlaneMat->setDisplacementMap(pBricksDisp);

	Material* pSkyMat = new Material();
	pSkyMat->setTexture(pSkyTex);

	StaticMeshActor* pMonkey1 = pScene->spawn<StaticMeshActor>(StaticMeshActor::staticClass(), Vector3f(0, 0, 2));
	pMonkey1->setMesh(pMonkeyMesh);
	pMonkey1->setMaterial(pMonkey1Mat);

	StaticMeshActor* pMonkey2 = pScene->spawn<StaticMeshActor>(StaticMeshActor::staticClass(), Vector3f(0, 0, -2));
	pMonkey2->setMesh(pMonkeyMesh);
	pMonkey2->setMaterial(pMonkey2Mat);

	StaticMeshActor* pSkyBox = pScene->spawn<StaticMeshActor>(StaticMeshActor::staticClass());
	pSkyBox->setMesh(pSkyBoxMesh);
	pSkyBox->setMaterial(pSkyMat);
	pScene->setSkyMesh(pSkyBox->GetMeshComp()->getMeshInstance());

	SpawnPoint* pSpawn = new SpawnPoint(pScene);
	pSpawn->getTransform().setPos(Vector3f(0, -4, 0));
	pScene->addSpawnPoint(pSpawn);

	DirectionalLight* pDirLight = new DirectionalLight();
	pDirLight->setColor(Color::rgb(255, 255, 255));
	pDirLight->setIntensity(1.f);
	pDirLight->setRot(Quaternion(0.853553354740143f, -0.353553414344788f, -0.146446630358696f, 0.353553414344788f));
	// pDirLight->setCastsShadow(false);
	pScene->addDirectionalLight(pDirLight);

	PointLight* pPtLight1 = new PointLight();
	pPtLight1->setColor(Color::rgb(255, 0, 0));
	pPtLight1->setIntensity(5);
	pPtLight1->setPos(Vector3f(-3, 0, 0));
	pPtLight1->setRange(20);
	pScene->addPointLight(pPtLight1);

	PointLight* pPtLight2 = new PointLight();
	pPtLight2->setColor(Color::rgb(0, 0, 255));
	pPtLight2->setIntensity(5);
	pPtLight2->setPos(Vector3f(3, 0, 0));
	pPtLight2->setRange(20);
	pScene->addPointLight(pPtLight2);

	SpotLight* pSptLight = new SpotLight();
	pSptLight->setColor(Color::rgb(255, 255, 255));
	pSptLight->setIntensity(0.1f);
	pSptLight->setPos(Vector3f(0, 0, -2.8f));
	pSptLight->setAttenuation(Attenuation(0, 0, 0.05));
	pSptLight->setRange(30);
	pSptLight->setAngle(51.68f);
	pSptLight->setCastsShadow(false);
	pScene->addSpotLight(pSptLight);

	CSGManager* pCSGManager = pScene->getCSGManager();

	BoxBrush* pBoxB = pScene->spawn<BoxBrush>(BoxBrush::staticClass(), Vector3f(0, 10, -3), Quaternion(), Vector3f(20, 20, 0.005f));
	pBoxB->setOperation(ECSGOperation::ADDITION);
	pBoxB->setMaterial(pPlaneMat);
	pCSGManager->addBrush(pBoxB);

	BoxBrush* pBoxB2 = pScene->spawn<BoxBrush>(BoxBrush::staticClass(), Vector3f(0, 5, 0));
	pBoxB2->setSize(Vector3f(1.f, 1.f, 1.f));
	pBoxB2->setOperation(ECSGOperation::ADDITION);
	pBoxB2->setMaterial(pPlaneMat);
	pCSGManager->addBrush(pBoxB2);

	SphereBrush* pSphB = pScene->spawn<SphereBrush>(SphereBrush::staticClass(), Vector3f(0.49f, 4.49f, 0.49f));
	pSphB->setOperation(ECSGOperation::SUBTRACTION);
	pSphB->setMaterial(pMonkey1Mat);
	pCSGManager->addBrush(pSphB);

	pCSGManager->rebuildMesh();



	pEngineMan->setScene(pScene, DefaultGameMode::staticClass());
	pEngineMan->run();

	ShadeEngine::Window::close();
	delete pEngineMan;
	delete pScene;

	delete pPlaneMat;
	delete pMonkey2Mat;
	delete pMonkey1Mat;
	delete pBricksDisp;
	delete pBricksNormal;
	delete pBricks;
	delete pMonkeyMesh;

	// delete pAssetMan;
	ShadeEngine::quit();
	return 0;
}

#ifndef HOTSWAP
int main() {
	return gameInit();
}
#endif
