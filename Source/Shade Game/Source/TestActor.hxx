//
//  TestActor.hxx
//  Shade Game
//
//  Created by Konrad Kraemer on 6/19/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <ShadeENGINE/Actor/Actor.hxx>
#include <iostream>

using namespace ShadeEngine;

class TestActor : Actor {
public:
	TestActor(Scene* pScene) : Actor(pScene) {
		std::cout << "Makin a TestActor" << std::endl;
	}
	virtual ~TestActor() {
		std::cout << "Destroyin a TestActor" << std::endl;
	}

protected:
private:

	SHADE_REFLECTION_BEGIN(TestActor, Actor)
	SHADE_REFLECTION_END
};
