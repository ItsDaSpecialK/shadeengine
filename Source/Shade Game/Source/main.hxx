//
//  main.hxx
//  Shade Game
//
//  Created by Konrad Kraemer on 4/21/14.
//
//  Copyright (c) 2014 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#ifdef WIN32
#define SHADEAPI extern "C" __declspec(dllexport)
#else
#define SHADEAPI extern "C" __attribute__ ((visibility("default")))
#endif

#include <ShadeENGINE/ShadeENGINE.hxx>
#include <ShadeENGINE/Core/Window.hxx>
#include <iostream>
#include "TestActor.hxx"
#include "DefaultGameMode.hxx"
