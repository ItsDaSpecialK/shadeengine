//
//  DefaultGameMode.hxx
//  Shade Game
//
//  Created by Konrad Kraemer on 9/4/15.
//
//  Copyright (c) 2015 Konrad Kraemer. All rights reserved.
//  Unauthorized copying and/or distribution of this file, via any medium is strictly prohibited.
//  Proprietary and Confidential
//

#pragma once

#include <ShadeENGINE/Engine/Game/GameMode.hxx>

using namespace ShadeEngine;

SHADE_CLASS()
/** Default Game Mode
 *  Demonstrates how to setup a GameMode
 */
class DefaultGameMode : public CGameMode {
	SHADE_GENERATED_BODY
public:
	/// Default Object Constructor
	DefaultGameMode(Scene* pScene);

	/// Default Destructor
	virtual ~DefaultGameMode() { }

	/** Picks a spawn point from the scene.
	 *  @return  The spawn point to spawn the pawn at.
	 */
	virtual SpawnPoint* chooseSpawnPoint() override;
protected:
private:

	SHADE_REFLECTION_BEGIN(DefaultGameMode, CGameMode)
	SHADE_REFLECTION_END
};
