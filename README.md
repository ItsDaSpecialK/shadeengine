![](./Images/ShadeEngineLogo.png)
===========

A simple 3D Rendering Engine

Copyright (c) 2017 Konrad Kraemer. All rights reserved.

![](./Images/Demo.gif)
