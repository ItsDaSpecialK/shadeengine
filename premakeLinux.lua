workspace "ShadeENGINE"
	configurations { "Debug", "Release" }

	frameworkdirs { "/Library/Frameworks" }
	links { "GL", "SDL2", "pthread" }

	libdirs { "Extern/SDL2/Linux" }
	defines { "COMPILING_SHADE", "_REENTRANT" }
	platforms { "Standalone", "Hotswap" }

	objdir "%{prj.location}/Intermediate"

	architecture "x86_64"

project "ShadeEngine"
	kind "StaticLib"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs"
	location "Source/ShadeEngine"
	targetprefix = ""

	pic "on"

	files { "Source/ShadeEngine/Include/**.hxx", "Source/ShadeEngine/Source/**.cxx", "Source/ShadeEngine/Shaders/*.glsl" }

	includedirs { "Source/ShadeEngine/Include/", "Extern" }

	defines { "LINUX", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

project "ShadeGame" -- linux makefile doesn't escape name properly
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/Shade Game"
	targetprefix = ""

	libdirs { "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs" }
	dependson { "ShadeEngine" }

	files { "Source/Shade Game/Source/**.hxx", "Source/Shade Game/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/", "Extern" }

	links { "ShadeEngine" }

	defines { "LINUX", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"

	filter "platforms:Standalone"
		kind "ConsoleApp"

	filter "platforms:Hotswap"
		kind "SharedLib"
		defines { "HOTSWAP" }

project "ShadeEditor"
	kind "WindowedApp"
	language "C++"
	cppdialect "C++14"
	targetdir "Binaries/%{cfg.buildcfg}/%{cfg.system}"
	location "Source/ShadeEditor"

	libdirs { "Binaries/%{cfg.buildcfg}/%{cfg.system}/libs" }
	dependson { "ShadeEngine" }
	--dependson { "Shade Game" } -- Doens't work on linux because it doesn't escape the space
	entrypoint "mainCRTStartup"

	files { "Source/ShadeEditor/Source/**.hxx", "Source/ShadeEditor/Source/**.cxx" }

	sysincludedirs { "Source/ShadeEngine/Include/", "Extern" }

	links { "ShadeEngine", "dl" }

	defines { "LINUX", "SDL_MAIN_HANDLED" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
